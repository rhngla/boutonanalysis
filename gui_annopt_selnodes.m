function [] = gui_annopt_selnodes(hObject, eventdata, ~)
%Depending on the mode, this function edits ignoreind field, or flips
%ordering of the trace nodes.

hl=(eventdata.Source);
ti=str2double(hl.Tag);
ha=(hl.Parent);
hf=ha.Parent;

tol=0.1;
dat=guidata(hf);

ind=find(...
    abs(hObject.XData-eventdata.IntersectionPoint(1))<tol &...
    abs(hObject.YData-eventdata.IntersectionPoint(2))<tol &...
    abs(hObject.ZData-eventdata.IntersectionPoint(3))<tol);

if strcmp(dat.currentmode,'x') && ~isempty(ind)
    figure(hf),plot3(hObject.XData(1),hObject.YData(1),hObject.ZData(3),'or','MarkerSize',20);hold on
    startt=[1,numel(hObject.XData)];
    startt=startt(round(ind./numel(hObject.XData))+1);
    o_dat=load(dat.Time{ti}.save_pth);
    [o_dat.AM.optim,o_dat.r.optim,o_dat.annotate.ignore]=orderprofile(o_dat.AM.optim,o_dat.r.optim,o_dat.annotate.ignore,startt);
    save(dat.Time{ti}.save_pth,'-struct','o_dat')
    display(['Saving ',dat.Time{ti}.save_pth,' with new order'])
    if startt~=1;
        hObject.XData=hObject.XData(startt:-1:1);
        hObject.YData=hObject.YData(startt:-1:1);
    end
    refreshdata(hObject)
    figure(hf),plot3(hObject.XData(1),hObject.YData(1),hObject.ZData(3),'oc','MarkerSize',20);hold on
else
    if ~isempty(dat.currentpoint)
        if ~isempty(ind)
            if dat.currenttime==ti
                display('2 nodes selected')
                temp=sort([dat.currentpoint,ind]);
                if strcmp(dat.currentmode,'r')
                    dat.Time{ti}.annotate.ignore(temp(1):temp(2))=true;
                else strcmp(dat.currentmode,'a')
                    dat.Time{ti}.annotate.ignore(temp(1):temp(2))=false;
                end
                
                if isempty(dat.hi{ti})
                    dat.hi{ti}=plot3(hObject.XData(dat.Time{ti}.annotate.ignore),hObject.YData(dat.Time{ti}.annotate.ignore),hObject.ZData(dat.Time{ti}.annotate.ignore)-1,'xr','Tag','Ignored','MarkerSize',13);
                else
                    if ~isempty(dat.hi{ti})
                        dat.hi{ti}.XData=hObject.XData(dat.Time{ti}.annotate.ignore);
                        dat.hi{ti}.YData=hObject.YData(dat.Time{ti}.annotate.ignore);
                        dat.hi{ti}.ZData=hObject.ZData(dat.Time{ti}.annotate.ignore)-1;
                        refreshdata(dat.hi{ti})
                    else
                        dat.hi{ti}=plot3(hObject.XData(dat.Time{ti}.annotate.ignore),hObject.YData(dat.Time{ti}.annotate.ignore),hObject.ZData(dat.Time{ti}.annotate.ignore)-1,'xr','Tag','Ignored','MarkerSize',13);
                    end
                end
            else
                display('Time does not match. Select both points again.')
                dat.currentpoint=[];
                dat.currenttime=[];
            end
            dat.currentpoint=[];
        end
        
    else
        if ~isempty(ind)
            dat.currentpoint=ind(1);
            display(['Node ',num2str(ind),' on time ',hl.Tag,' selected. Choose another point'])
            dat.currenttime=ti;
        end
    end
end
guidata(hf,dat);
end

