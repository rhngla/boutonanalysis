%Notes:
%Excel sheet v8 is used to plot these
%Extra data in sheet v7 was removed
%EPBScore measurements are added 
%LoGxy (without 2nd norm) was added
%median333 (without 2nd norm) was added

parentdir=pwd;
parentdir=parentdir(1:(strfind(parentdir,'Plasticity')+numel('Plasticity')-1));
[xldat,temp,txt]=xlsread(isunixispc([parentdir,'\dat\EMData\Size data.xlsx']),'v8');

txt={txt{2:20,2}}';
cc=lines(4);
cc=cc(xldat(:,1),:);
no2ndnorm=false;

for t=1:numel(txt)
    if isa(txt{t},'double')
        txt{t}=num2str(txt{t});
    end
end

%xldat(xldat(:,2)==8,2:end)=nan;%Excluding Terminal bouton 
hf=13;
%{
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Green Channel
%Correlations are slightly different from before because of rounding errors.
figure(hf+1),movegui(hf+1,'northwest')
xcolno=8;
ycolno=3;
xx=xldat(:,xcolno);
yy=xldat(:,ycolno);
scatter(xx,yy,[],cc,'filled'),axis square,box on,hold on
text(xx+0.15,yy,txt,'Color',[0.4,0.4,0.4])
xlabel('Bouton weight')
ylabel('Bouton volume (\mu{m}^{3})')
xlim([0 16]);ylim([0 1]);
C=corrcoef(xx,yy,'rows','pairwise');
notnan=~isnan(xx) & ~isnan(yy);
[ff,goodness]=fit(xx(notnan),yy(notnan),fittype('a*x+b'),'StartPoint',[1,0]);
plot([0,16],ff([0,16]),'-k')
text(1,0.9,['R^{2} = ',num2str(goodness.rsquare)])
set(gca,'XTick',[0:4:16],'YTick',[0:0.2:1.0]);
drawnow

figure(hf+2),movegui(hf+2,'north')
xx=xldat(:,8);
yy=xldat(:,3)-xldat(:,7);
scatter(xx,yy,[],cc,'filled'),axis square,box on,hold on
text(xx+0.15,yy,txt,'Color',[0.4,0.4,0.4])
xlabel('Bouton weight')
ylabel('Bouton volume excluding mitochondria (\mu{m}^{3})')
xlim([0 16]);ylim([0 1])
C=corrcoef(xx,yy,'rows','pairwise');
notnan=~isnan(xx) & ~isnan(yy);
[ff,goodness]=fit(xx(notnan),yy(notnan),fittype('a*x+b'),'StartPoint',[1,0]);
plot([0,16],ff([0,16]),'-k')
text(1,0.9,['R^{2} = ',num2str(goodness.rsquare)])
set(gca,'XTick',[0:4:16],'YTick',[0:0.2:1.0]);
drawnow

figure(hf+3),movegui(hf+3,'northeast')
xx=xldat(:,8);
yy=xldat(:,4);
scatter(xx,yy,[],cc,'filled'),axis square,box on,hold on
text(xx+0.15,yy,txt,'Color',[0.4,0.4,0.4])
xlabel('Bouton weight')
ylabel('Total PSD surface area (\mu{m}^{2})')
xlim([0 16])
ylim([0 3])
notnan=~isnan(xx) & ~isnan(yy);
[ff,goodness]=fit(xx(notnan),yy(notnan),fittype('a*x+b'),'StartPoint',[1,0]);
plot([0,16],ff([0,16]),'-k')
set(gca,'XTick',[0:4:16],'YTick',[0:0.5:3.0]);
text(1,2,['R^{2} = ',num2str(goodness.rsquare)])
drawnow

figure(hf+4),movegui(hf+4,'southwest')
xx=xldat(:,3);
yy=xldat(:,4);
scatter(xx,yy,[],cc,'filled'),axis square,box on,hold on
text(xx+0.015,yy,txt,'Color',[0.4,0.4,0.4])
xlabel('Bouton volume (\mu{m}^3)')
ylabel('Total PSD surface area (\mu{m}^{2})')
xlim([0 1]);ylim([0 3]);
notnan=~isnan(xx) & ~isnan(yy);
[ff,goodness]=fit(xx(notnan),yy(notnan),fittype('a*x+b'),'StartPoint',[1,0]);
plot([0,16],ff([0,16]),'-k')
set(gca,'XTick',0:0.2:1,'YTick',0:0.5:3.0);
text(.1,2,['R^{2} = ',num2str(goodness.rsquare)])
drawnow

figure(hf+5),movegui(hf+5,'south')
xx=xldat(:,3)-xldat(:,7);
yy=xldat(:,4);
scatter(xx,yy,[],cc,'filled'),axis square,box on,hold on
text(xx+0.015,yy,txt,'Color',[0.4,0.4,0.4])
xlabel('Bouton volume excluding mitochondria (\mu{m}^3)')
ylabel('Total PSD surface area (\mu{m}^{2})')
xlim([0 1]);ylim([0 3]);
notnan=~isnan(xx) & ~isnan(yy);
[ff,goodness]=fit(xx(notnan),yy(notnan),fittype('a*x+b'),'StartPoint',[1,0]);
plot([0,16],ff([0,16]),'-k')
set(gca,'XTick',0:0.2:1,'YTick',0:0.5:3.0);
text(.1,2,['R^{2} = ',num2str(goodness.rsquare)])
drawnow


figure(hf+6),movegui(hf+6,'southwest')
xcolno=11;
ycolno=3;
xx=xldat(:,xcolno);
yy=xldat(:,ycolno);
scatter(xx,yy,[],cc,'filled'),axis square,box on,hold on
text(xx+0.15,yy,txt,'Color',[0.4,0.4,0.4])
xlabel('EPBScore bouton weight')
ylabel('Bouton volume (\mu{m}^{3})')
xlim([0 6]);ylim([0 1]);
C=corrcoef(xx,yy,'rows','pairwise');
notnan=~isnan(xx) & ~isnan(yy);
[ff,goodness]=fit(xx(notnan),yy(notnan),fittype('a*x+b'),'StartPoint',[1,0]);
plot([0,16],ff([0,16]),'-k')
text(1,0.9,['R^{2} = ',num2str(goodness.rsquare)])
set(gca,'XTick',[0:2:8],'YTick',[0:0.2:1.0]);
drawnow

figure(hf+7),movegui(hf+7,'south')
xx=xldat(:,11);
yy=xldat(:,3)-xldat(:,7);
scatter(xx,yy,[],cc,'filled'),axis square,box on,hold on
text(xx+0.15,yy,txt,'Color',[0.4,0.4,0.4])
xlabel('EPBScore bouton weight')
ylabel('Bouton volume excluding mitochondria (\mu{m}^{3})')
xlim([0 6]);ylim([0 1])
C=corrcoef(xx,yy,'rows','pairwise');
notnan=~isnan(xx) & ~isnan(yy);
[ff,goodness]=fit(xx(notnan),yy(notnan),fittype('a*x+b'),'StartPoint',[1,0]);
plot([0,16],ff([0,16]),'-k')
text(1,0.9,['R^{2} = ',num2str(goodness.rsquare)])
set(gca,'XTick',[0:2:8],'YTick',[0:0.2:1.0]);
drawnow

%LOGXY NO 2ND NORM FILTER--------------------------------------------------
figure(hf+8),movegui(hf+8,'southwest')
xcolno=12;
ycolno=3;
xx=xldat(:,xcolno);
yy=xldat(:,ycolno);
scatter(xx,yy,[],cc,'filled'),axis square,box on,hold on
text(xx+0.15,yy,txt,'Color',[0.4,0.4,0.4])
xlabel('LoGxy no 2nd norm')
ylabel('Bouton volume (\mu{m}^{3})')
xlim([0 6]);ylim([0 1]);
C=corrcoef(xx,yy,'rows','pairwise');
notnan=~isnan(xx) & ~isnan(yy);
[ff,goodness]=fit(xx(notnan),yy(notnan),fittype('a*x+b'),'StartPoint',[1,0]);
plot([0,16],ff([0,16]),'-k')
text(1,0.9,['R^{2} = ',num2str(goodness.rsquare)])
set(gca,'XTick',[0:2:8],'YTick',[0:0.2:1.0]);
drawnow

figure(hf+9),movegui(hf+9,'south')
xx=xldat(:,12);
yy=xldat(:,3)-xldat(:,7);
scatter(xx,yy,[],cc,'filled'),axis square,box on,hold on
text(xx+0.15,yy,txt,'Color',[0.4,0.4,0.4])
xlabel('LoGxy no 2nd norm')
ylabel('Bouton volume excluding mitochondria (\mu{m}^{3})')
xlim([0 6]);ylim([0 1])
C=corrcoef(xx,yy,'rows','pairwise');
notnan=~isnan(xx) & ~isnan(yy);
[ff,goodness]=fit(xx(notnan),yy(notnan),fittype('a*x+b'),'StartPoint',[1,0]);
plot([0,16],ff([0,16]),'-k')
text(1,0.9,['R^{2} = ',num2str(goodness.rsquare)])
set(gca,'XTick',[0:2:8],'YTick',[0:0.2:1.0]);
drawnow


%MEDIAN333 FILTER----------------------------------------------------------
figure(hf+10),movegui(hf+10,'southwest')
xcolno=14;
ycolno=3;
xx=xldat(:,xcolno);
yy=xldat(:,ycolno);
scatter(xx,yy,[],cc,'filled'),axis square,box on,hold on
text(xx+0.15,yy,txt,'Color',[0.4,0.4,0.4])
xlabel('Median333')
ylabel('Bouton volume (\mu{m}^{3})')
xlim([0 6]);ylim([0 1]);
C=corrcoef(xx,yy,'rows','pairwise');
notnan=~isnan(xx) & ~isnan(yy);
[ff,goodness]=fit(xx(notnan),yy(notnan),fittype('a*x+b'),'StartPoint',[1,0]);
plot([0,16],ff([0,16]),'-k')
text(1,0.9,['R^{2} = ',num2str(goodness.rsquare)])
set(gca,'XTick',[0:2:8],'YTick',[0:0.2:1.0]);
drawnow

figure(hf+11),movegui(hf+11,'south')
xx=xldat(:,14);
yy=xldat(:,3)-xldat(:,7);
scatter(xx,yy,[],cc,'filled'),axis square,box on,hold on
text(xx+0.15,yy,txt,'Color',[0.4,0.4,0.4])
xlabel('Median333')
ylabel('Bouton volume excluding mitochondria (\mu{m}^{3})')
xlim([0 6]);ylim([0 1])
C=corrcoef(xx,yy,'rows','pairwise');
notnan=~isnan(xx) & ~isnan(yy);
[ff,goodness]=fit(xx(notnan),yy(notnan),fittype('a*x+b'),'StartPoint',[1,0]);
plot([0,16],ff([0,16]),'-k')
text(1,0.9,['R^{2} = ',num2str(goodness.rsquare)])
set(gca,'XTick',[0:2:8],'YTick',[0:0.2:1.0]);
drawnow
%}
fit_this=setdiff(xldat(:,2),[7;8;18;19]);

figure(hf+1),movegui(hf+1,'northwest')
xcolno=8;
ycolno=18;
xx=xldat(:,xcolno);
yy=xldat(:,ycolno);
scatter(xx,yy,[],cc,'filled'),axis square,box on,hold on
text(xx+0.15,yy,txt,'Color',[0.4,0.4,0.4])
xlabel('Bouton weight')
ylabel('Bouton volume (\mu{m}^{3})')
xlim([0 16]);ylim([0 1.05]);
C=corrcoef(xx(fit_this),yy(fit_this),'rows','pairwise');
[ff,goodness]=fit(xx(fit_this),yy(fit_this),fittype('a*x+b'),'StartPoint',[1,0]);
plot([0,16],ff([0,16]),'-k')
text(1,0.9,['R^{2} = ',num2str(goodness.rsquare)])
set(gca,'XTick',[0:4:16],'YTick',[0:0.2:1.0]);
drawnow

figure(hf+2),movegui(hf+2,'north')
xx=xldat(:,8);
yy=xldat(:,18)-xldat(:,19);
scatter(xx,yy,[],cc,'filled'),axis square,box on,hold on
text(xx+0.15,yy,txt,'Color',[0.4,0.4,0.4])
xlabel('Bouton weight')
ylabel('Bouton volume excluding mitochondria (\mu{m}^{3})')
xlim([0 16]);ylim([0 1.05])
C=corrcoef(xx,yy,'rows','pairwise');
[ff,goodness]=fit(xx(fit_this),yy(fit_this),fittype('a*x+b'),'StartPoint',[1,0]);
plot([0,16],ff([0,16]),'-k')
text(1,0.9,['R^{2} = ',num2str(goodness.rsquare)])
set(gca,'XTick',[0:4:16],'YTick',[0:0.2:1.0]);
drawnow
%}

%{
%EPBSCOPE Comparisons
figure(hf+1),movegui(hf+1,'northwest')
xcolno=11;
ycolno=18;
xx=xldat(:,xcolno);
yy=xldat(:,ycolno);
scatter(xx,yy,[],cc,'filled'),axis square,box on,hold on
text(xx+0.15,yy,txt,'Color',[0.4,0.4,0.4])
xlabel('EPBScore Bouton weight')
ylabel('Bouton volume (\mu{m}^{3})')
xlim([0 4]);ylim([0 1.05]);
C=corrcoef(xx(fit_this),yy(fit_this),'rows','pairwise');
[ff,goodness]=fit(xx(fit_this),yy(fit_this),fittype('a*x+b'),'StartPoint',[1,0]);
plot([0,16],ff([0,16]),'-k')
text(1,0.9,['R^{2} = ',num2str(goodness.rsquare)])
set(gca,'XTick',[0:4:16],'YTick',[0:0.2:1.0]);
drawnow

figure(hf+2),movegui(hf+2,'north')
xx=xldat(:,11);
yy=xldat(:,18)-xldat(:,19);
scatter(xx,yy,[],cc,'filled'),axis square,box on,hold on
text(xx+0.15,yy,txt,'Color',[0.4,0.4,0.4])
xlabel('EPBScore bouton weight')
ylabel('Bouton volume excluding mitochondria (\mu{m}^{3})')
xlim([0 4]);ylim([0 1.05])
C=corrcoef(xx,yy,'rows','pairwise');
[ff,goodness]=fit(xx(fit_this),yy(fit_this),fittype('a*x+b'),'StartPoint',[1,0]);
plot([0,16],ff([0,16]),'-k')
text(1,0.9,['R^{2} = ',num2str(goodness.rsquare)])
set(gca,'XTick',[0:4:16],'YTick',[0:0.2:1.0]);
drawnow
%}

