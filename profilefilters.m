function [II, RR]= profilefilters (r,IM,filtertype)

%Filter options
LoG_R_min=1.5;%was 1.5;
LoG_R_step=0.01; % default is 0.1. For EM, use 0.01
LoG_R_max=3;% default is 5. For EM, use 3
meanfiltersize=[3,3,3];
medianfiltersize=[3,3,3];

if strcmp(filtertype,'LoG')
    [II,RR]=LoG_Filt(IM,r,LoG_R_min,LoG_R_step,LoG_R_max);
elseif strcmp(filtertype,'LoG3')
    [II,RR]=LoG_Filt(IM,r,3,1,3);
elseif strcmp(filtertype,'LoG15')
    [II,RR]=LoG_Filt(IM,r,1.5,1,1.5);
elseif strcmp(filtertype,'LoG155')
    [II,RR]=LoG_Filt(IM,r,1.5, 0.1, 5);
elseif strcmp(filtertype,'LoGxy')
    [II,RR] = LoG_Filt_xy(IM,r,1.5,0.1,3,2);
elseif strcmp(filtertype,'LoGxy232')
    [II,RR] = LoG_Filt_xy(IM,r,2,0.1,3,2);
elseif strcmp(filtertype,'LoGxy222')
    [II,RR] = LoG_Filt_xy(IM,r,2,0.1,2,2);
elseif strcmp(filtertype,'LoGxy332')
    [II,RR] = LoG_Filt_xy(IM,r,3,0.1,3,2);
elseif strcmp(filtertype,'LoGxy223')
    [II,RR] = LoG_Filt_xy(IM,r,2,0.1,2,3);
elseif strcmp(filtertype,'LoGxy233')
    [II,RR] = LoG_Filt_xy(IM,r,2,0.1,3,3);
elseif strcmp(filtertype,'LoGxy1533')
    [II,RR] = LoG_Filt_xy(IM,r,1.5,0.1,3,3);
elseif strcmp(filtertype,'LoGxy1531')
    [II,RR] = LoG_Filt_xy(IM,r,1.5,0.1,3,1);
    %Extra filters added for testing
    
elseif strcmp(filtertype,'median')
    [~,II]=Simple_Filts(IM,r,medianfiltersize);
    RR=ones(size(r,1),1)*(mean(medianfiltersize)/2);
elseif strcmp(filtertype,'mean')
    [II,~]=Simple_Filts(IM,r,meanfiltersize);
    RR=ones(size(r,1),1)*(mean(meanfiltersize)/2);
elseif strcmp(filtertype,'LoG5')
    [II,RR]=LoG_Filt(IM,r,5,1,5);
elseif strcmp(filtertype,'LoG7')
    [II,RR]=LoG_Filt(IM,r,7,1,7);
elseif strcmp(filtertype,'Gauss')
    [II,RR]=Gauss_Filt(IM,r,2,0.1,8);
elseif strcmp(filtertype,'Gauss05')
    [II,RR]=Gauss_Filt(IM,r,0.5,0.1,0.5);
elseif strcmp(filtertype,'Gauss1')
    [II,RR]=Gauss_Filt(IM,r,1,0.1,1);
elseif strcmp(filtertype,'Gauss2')
    [II,RR]=Gauss_Filt(IM,r,2,1,2);
elseif strcmp(filtertype,'Gauss3')
    [II,RR]=Gauss_Filt(IM,r,3,1,3);
elseif strcmp(filtertype,'Gauss5')
    [II,RR]=Gauss_Filt(IM,r,5,1,5);
elseif strcmp(filtertype,'Gauss7')
    [II,RR]=Gauss_Filt(IM,r,7,1,7);
elseif strcmp(filtertype,'mean111')
    meanfiltersize=[1,1,1];
    [II,~]=Simple_Filts(IM,r,meanfiltersize);
    RR=ones(size(r,1),1)*(mean(meanfiltersize)/2);
elseif strcmp(filtertype,'mean333')
    meanfiltersize=[3,3,3];
    [II,~]=Simple_Filts(IM,r,meanfiltersize);
    RR=ones(size(r,1),1)*(mean(meanfiltersize)/2);
elseif strcmp(filtertype,'mean555')
    meanfiltersize=[5,5,5];
    [II,~]=Simple_Filts(IM,r,meanfiltersize);
    RR=ones(size(r,1),1)*(mean(meanfiltersize)/2);
elseif strcmp(filtertype,'mean777')
    meanfiltersize=[7,7,7];
    [II,~]=Simple_Filts(IM,r,meanfiltersize);
    RR=ones(size(r,1),1)*(mean(meanfiltersize)/2);
elseif strcmp(filtertype,'mean773')
    meanfiltersize=[7,7,3];
    [II,~]=Simple_Filts(IM,r,meanfiltersize);
    RR=ones(size(r,1),1)*(mean(meanfiltersize)/2);
elseif strcmp(filtertype,'mean775')
    meanfiltersize=[7,7,5];
    [II,~]=Simple_Filts(IM,r,meanfiltersize);
    RR=ones(size(r,1),1)*(mean(meanfiltersize)/2);
elseif strcmp(filtertype,'median111')
    medianfiltersize=[1,1,1];
    [~,II]=Simple_Filts(IM,r,medianfiltersize);
    RR=ones(size(r,1),1)*(mean(medianfiltersize)/2);
elseif strcmp(filtertype,'median333')
    medianfiltersize=[3,3,3];
    [~,II]=Simple_Filts(IM,r,medianfiltersize);
    RR=ones(size(r,1),1)*(mean(medianfiltersize)/2);
elseif strcmp(filtertype,'median555')
    medianfiltersize=[5,5,5];
    [~,II]=Simple_Filts(IM,r,medianfiltersize);
    RR=ones(size(r,1),1)*(mean(medianfiltersize)/2);
elseif strcmp(filtertype,'median777')
    medianfiltersize=[7,7,7];
    [~,II]=Simple_Filts(IM,r,medianfiltersize);
    RR=ones(size(r,1),1)*(mean(medianfiltersize)/2);
elseif strcmp(filtertype,'median773')
    medianfiltersize=[7,7,3];
    [~,II]=Simple_Filts(IM,r,medianfiltersize);
    RR=ones(size(r,1),1)*(mean(medianfiltersize)/2);
elseif strcmp(filtertype,'median775')
    medianfiltersize=[7,7,5];
    [~,II]=Simple_Filts(IM,r,medianfiltersize);
    RR=ones(size(r,1),1)*(mean(medianfiltersize)/2);
end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [I_mean,I_med] = Simple_Filts(Orig,r,filt_size)
r=round(r);
sizeIM=size(Orig);
HW=(filt_size-1);

minx=round(max(1,(r(:,1)-round(HW(1)/2))));
maxx=round(min((r(:,1)+round(HW(1)/2)),sizeIM(1)));
miny=round(max(1,(r(:,2)-round(HW(2)/2))));
maxy=round(min((r(:,2)+round(HW(2)/2)),sizeIM(2)));
minz=round(max(1,(r(:,3)-round(HW(3)/2))));
maxz=round(min((r(:,3)+round(HW(3)/2)),sizeIM(3)));

I_mean=nan(size(r,1),1);
I_med=nan(size(r,1),1);
for i=1:size(r,1)
    IMval=Orig(minx(i):maxx(i),miny(i):maxy(i),minz(i):maxz(i));
    I_mean(i)=mean(IMval(:));
    I_med(i)=median(IMval(:));
end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [I,R] = LoG_Filt(Orig,r,R_min,R_step,R_max)
if R_min<0.1
    R_min=0.1;
end
s=(R_min:R_step:R_max)';

if isempty(s)
    R=zeros(size(r(:,1)));
    I=zeros(size(r(:,1)))';
else
    r=round(r); %#1
    del=0.01*R_min;
    N=size(r,1);
    
    sizeIm=size(Orig);
    if length(sizeIm)==2
        sizeIm=[sizeIm,1];
    end
    
    W=ceil(2*max(s))*2+1;
    HW=(W-1)/2;
    S=[W,W,W];
    [xtemp,ytemp,ztemp]=ind2sub(S,1:prod(S));
    
    Itemp=zeros(prod(S),N);
    for i=1:N
        xtemp1=xtemp+r(i,1)-HW-1;
        ytemp1=ytemp+r(i,2)-HW-1;
        ztemp1=ztemp+r(i,3)-HW-1;
        temp_ind=(xtemp1>=1 & xtemp1<=sizeIm(1) & ytemp1>=1 & ytemp1<=sizeIm(2) & ztemp1>=1 & ztemp1<=sizeIm(3));
        indIm=sub2ind_ASfast(sizeIm,xtemp1(temp_ind),ytemp1(temp_ind),ztemp1(temp_ind));
        
        Im_S=zeros(S);
        Im_S(temp_ind)=double(Orig(indIm));
        
        Itemp(:,i)=Im_S(:);
    end
    
    r2=(HW+1-xtemp).^2+(HW+1-ytemp).^2+(HW+1-ztemp).^2;
    Gm=exp(-(1./(s-del).^2)*r2).*((1./(s-del).^3)*ones(1,prod(S)));
    Gm=Gm./(sum(Gm,2)*ones(1,prod(S)));
    Gp=exp(-(1./(s+del).^2)*r2).*((1./(s+del).^3)*ones(1,prod(S)));
    Gp=Gp./(sum(Gp,2)*ones(1,prod(S)));
    LoG=(Gm-Gp)./(2*del).*(s*ones(1,prod(S))); %correct up to a numerical factor
    
    I_LoG=LoG*Itemp;
    if length(s)>1
        [I,ind]=max(I_LoG);
        R=s(ind);
    else
        I=I_LoG;
        R=s.*ones(size(r(:,1)));
    end
end
I=I(:);
R=R(:);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [I,Rxy] = LoG_Filt_xy(Orig,r,Rxy_min,Rxy_step,Rxy_max,Rz)
if Rxy_min<0.1
    Rxy_min=0.1;
end
s=(Rxy_min:Rxy_step:Rxy_max)';

if isempty(s)
    Rxy=zeros(size(r(:,1)));
    I=zeros(size(r(:,1)))';
else
    r=round(r); %#1
    del=0.01*Rxy_min;
    N=size(r,1);
    
    sizeIm=size(Orig);
    if length(sizeIm)==2
        sizeIm=[sizeIm,1];
    end
    
    W=ceil(2*max([Rxy_max,Rz]))*2+1;
    HW=(W-1)/2;
    S=[W,W,W];
    [xtemp,ytemp,ztemp]=ind2sub(S,1:prod(S));
    
    Itemp=zeros(prod(S),N);
    for i=1:N
        xtemp1=xtemp+r(i,1)-HW-1;
        ytemp1=ytemp+r(i,2)-HW-1;
        ztemp1=ztemp+r(i,3)-HW-1;
        temp_ind=(xtemp1>=1 & xtemp1<=sizeIm(1) & ytemp1>=1 & ytemp1<=sizeIm(2) & ztemp1>=1 & ztemp1<=sizeIm(3));
        indIm=sub2ind_ASfast(sizeIm,xtemp1(temp_ind),ytemp1(temp_ind),ztemp1(temp_ind));
        
        Im_S=zeros(S);
        Im_S(temp_ind)=double(Orig(indIm));
        
        Itemp(:,i)=Im_S(:);
    end
    
    xy2=(HW+1-xtemp).^2+(HW+1-ytemp).^2;
    Gxym=exp(-(1./(s-del).^2)*xy2).*((1./(s-del).^2)*ones(1,prod(S)));
    Gxym=Gxym./(sum(Gxym,2)*ones(1,prod(S)));
    Gxyp=exp(-(1./(s+del).^2)*xy2).*((1./(s+del).^2)*ones(1,prod(S)));
    Gxyp=Gxyp./(sum(Gxyp,2)*ones(1,prod(S)));
    
    z2=(HW+1-ztemp).^2;
    Gz=exp(-(1./Rz.^2)*z2)./Rz;
    Gz=Gz./sum(Gz,2);
    
    LoG=(Gxym-Gxyp)./(2*del).*(s*ones(1,prod(S))).*(ones(length(s),1)*Gz); %correct up to a numerical factor
    
    I_LoG=LoG*Itemp;
    if length(s)>1
        [I,ind]=max(I_LoG);
        Rxy=s(ind);
    else
        I=I_LoG;
        Rxy=s.*ones(size(r(:,1)));
    end
end
I=I(:);
Rxy=Rxy(:);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [I,R] = Gauss_Filt(Orig,r,R_min,R_step,R_max)

if R_min<0.1
    R_min=0.1;
end
s=(R_min:R_step:R_max)';

if isempty(s)
    R=zeros(size(r(:,1)));
    I=zeros(size(r(:,1)))';
else
    r=round(r); %#1
    N=size(r,1);
    
    sizeIm=size(Orig);
    if length(sizeIm)==2
        sizeIm=[sizeIm,1];
    end
    
    W=ceil(2*max(s))*2+1;
    HW=(W-1)/2;
    S=[W,W,W];
    [xtemp,ytemp,ztemp]=ind2sub(S,1:prod(S));
    
    Itemp=zeros(prod(S),N);
    for i=1:N
        xtemp1=xtemp+r(i,1)-HW-1;
        ytemp1=ytemp+r(i,2)-HW-1;
        ztemp1=ztemp+r(i,3)-HW-1;
        temp_ind=(xtemp1>=1 & xtemp1<=sizeIm(1) & ytemp1>=1 & ytemp1<=sizeIm(2) & ztemp1>=1 & ztemp1<=sizeIm(3));
        indIm=sub2ind_ASfast(sizeIm,xtemp1(temp_ind),ytemp1(temp_ind),ztemp1(temp_ind));
        
        Im_S=zeros(S);
        Im_S(temp_ind)=double(Orig(indIm));
        
        Itemp(:,i)=Im_S(:);
    end
    
    r2=(HW+1-xtemp).^2+(HW+1-ytemp).^2+(HW+1-ztemp).^2;
    Gp=exp(-(1./(s).^2)*r2).*((1./(s).^3)*ones(1,prod(S)));
    Gp=Gp./(sum(Gp,2)*ones(1,prod(S)));
    
    I_Gauss=Gp*Itemp;
    if length(s)>1
        [I,ind]=max(I_Gauss);
        R=s(ind);
    else
        I=I_Gauss;
        R=s.*ones(size(r(:,1)));
    end
end
I=I(:);
R=R(:);
end

