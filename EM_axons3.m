mind=[27.5,26.0,24.76,0];
maxd=[47.0,42.0,36.3325,15];
%1.37
%0.43ed

%Mean555
for ax=1:4
    dind=B{ax}.d.optim>mind(ax) & B{ax}.d.optim<maxd(ax);
    figure(ax+30);clf(ax+30);movegui(ax+30,'southeast')
    plot(B{ax}.d.optim(dind),B{ax}.I.G.Nofilt.norm(dind)./median(B{ax}.I.G.Nofilt.norm),'-b');hold on
    plot([min(B{ax}.d.optim(dind)),min(B{ax}.d.optim(dind))+20],[0 0],'-k')
    plot([min(B{ax}.d.optim(dind)),min(B{ax}.d.optim(dind))+20],[5 5],'-k')
    xlim([min(B{ax}.d.optim(dind)),min(B{ax}.d.optim(dind))+20])
    ylim([-0.5 5]);
    set(gca,'XTick',[min(B{ax}.d.optim(dind)):5:B{ax}.d.optim(dind)+20]);
    set(gca,'YTick',[0:4:16]);
    title(['Nofilt Axon ',num2str(ax)])
    drawnow
end

%{
%Gauss1
for ax=1:4
    dind=B{ax}.d.optim>mind(ax) & B{ax}.d.optim<maxd(ax);
    figure(ax);clf(ax);movegui(ax,'northwest')
    plot(B{ax}.d.optim(dind),B{ax}.I.G.Gauss1.norm(dind)./median(B{ax}.I.G.Gauss1.norm),'-');hold on
    plot([min(B{ax}.d.optim(dind)),min(B{ax}.d.optim(dind))+20],[0 0],'-k')
    plot([min(B{ax}.d.optim(dind)),min(B{ax}.d.optim(dind))+20],[5 5],'-k')
    xlim([min(B{ax}.d.optim(dind)),min(B{ax}.d.optim(dind))+20])
    ylim([-0.5 5]);
    set(gca,'XTick',[min(B{ax}.d.optim(dind)):5:B{ax}.d.optim(dind)+20]);
    set(gca,'YTick',[0:4:16]);
    title(['Gauss1 Axon ',num2str(ax)])
end

%Mean333
for ax=1:4
    dind=B{ax}.d.optim>mind(ax) & B{ax}.d.optim<maxd(ax);
    figure(ax+20);clf(ax+20);movegui(ax+20,'southwest')
    plot(B{ax}.d.optim(dind),B{ax}.I.G.mean333.norm(dind)./median(B{ax}.I.G.mean333.norm),'-g');hold on
    plot([min(B{ax}.d.optim(dind)),min(B{ax}.d.optim(dind))+20],[0 0],'-k')
    plot([min(B{ax}.d.optim(dind)),min(B{ax}.d.optim(dind))+20],[5 5],'-k')
    xlim([min(B{ax}.d.optim(dind)),min(B{ax}.d.optim(dind))+20])
    ylim([-0.5 5]);
    set(gca,'XTick',[min(B{ax}.d.optim(dind)):5:B{ax}.d.optim(dind)+20]);
    set(gca,'YTick',[0:4:16]);
    title(['Mean333 Axon ',num2str(ax)])
end

%Mean555
for ax=1:4
    dind=B{ax}.d.optim>mind(ax) & B{ax}.d.optim<maxd(ax);
    figure(ax+30);clf(ax+30);movegui(ax+30,'southeast')
    plot(B{ax}.d.optim(dind),B{ax}.I.G.mean555.norm(dind)./median(B{ax}.I.G.mean555.norm),'-b');hold on
    plot([min(B{ax}.d.optim(dind)),min(B{ax}.d.optim(dind))+20],[0 0],'-k')
    plot([min(B{ax}.d.optim(dind)),min(B{ax}.d.optim(dind))+20],[5 5],'-k')
    xlim([min(B{ax}.d.optim(dind)),min(B{ax}.d.optim(dind))+20])
    ylim([-0.5 5]);
    set(gca,'XTick',[min(B{ax}.d.optim(dind)):5:B{ax}.d.optim(dind)+20]);
    set(gca,'YTick',[0:4:16]);
    title(['Mean555 Axon ',num2str(ax)])
    drawnow
end

%Gauss2
for ax=1:4
    dind=B{ax}.d.optim>mind(ax) & B{ax}.d.optim<maxd(ax);
    figure(ax);clf(ax);movegui(ax,'northwest')
    plot(B{ax}.d.optim(dind),B{ax}.I.G.Gauss2.norm(dind)./median(B{ax}.I.G.Gauss2.norm),'-');hold on
    plot([min(B{ax}.d.optim(dind)),min(B{ax}.d.optim(dind))+20],[0 0],'-k')
    plot([min(B{ax}.d.optim(dind)),min(B{ax}.d.optim(dind))+20],[5 5],'-k')
    xlim([min(B{ax}.d.optim(dind)),min(B{ax}.d.optim(dind))+20])
    ylim([-0.5 5]);
    set(gca,'XTick',[min(B{ax}.d.optim(dind)):5:B{ax}.d.optim(dind)+20]);
    set(gca,'YTick',[0:4:16]);
    title(['Gauss2 Axon ',num2str(ax)])
end

%Gauss3
for ax=1:4
    dind=B{ax}.d.optim>mind(ax) & B{ax}.d.optim<maxd(ax);
    figure(ax+10);clf(ax+10);movegui(ax+10,'northeast')
    plot(B{ax}.d.optim(dind),B{ax}.I.G.Gauss3.norm(dind)./median(B{ax}.I.G.Gauss3.norm),'-r');hold on
    plot([min(B{ax}.d.optim(dind)),min(B{ax}.d.optim(dind))+20],[0 0],'-k')
    plot([min(B{ax}.d.optim(dind)),min(B{ax}.d.optim(dind))+20],[5 5],'-k')
    xlim([min(B{ax}.d.optim(dind)),min(B{ax}.d.optim(dind))+20])
    ylim([-0.5 5]);
    
    set(gca,'XTick',[min(B{ax}.d.optim(dind)):5:B{ax}.d.optim(dind)+20]);
    set(gca,'YTick',[0:4:16]);
    title(['Gauss3 Axon ',num2str(ax)])
end

%Median333
for ax=1:4
    dind=B{ax}.d.optim>mind(ax) & B{ax}.d.optim<maxd(ax);
    figure(ax+20);clf(ax+20);movegui(ax+20,'southwest')
    plot(B{ax}.d.optim(dind),B{ax}.I.G.median333.norm(dind)./median(B{ax}.I.G.median333.norm),'-g');hold on
    plot([min(B{ax}.d.optim(dind)),min(B{ax}.d.optim(dind))+20],[0 0],'-k')
    plot([min(B{ax}.d.optim(dind)),min(B{ax}.d.optim(dind))+20],[5 5],'-k')
    xlim([min(B{ax}.d.optim(dind)),min(B{ax}.d.optim(dind))+20])
    ylim([-0.5 5]);
    set(gca,'XTick',[min(B{ax}.d.optim(dind)):5:B{ax}.d.optim(dind)+20]);
    set(gca,'YTick',[0:4:16]);
    title(['Median333 Axon ',num2str(ax)])
end

%Median555
for ax=1:4
    dind=B{ax}.d.optim>mind(ax) & B{ax}.d.optim<maxd(ax);
    figure(ax+30);clf(ax+30);movegui(ax+30,'southeast')
    plot(B{ax}.d.optim(dind),B{ax}.I.G.median555.norm(dind)./median(B{ax}.I.G.median555.norm),'-b');hold on
    plot([min(B{ax}.d.optim(dind)),min(B{ax}.d.optim(dind))+20],[0 0],'-k')
    plot([min(B{ax}.d.optim(dind)),min(B{ax}.d.optim(dind))+20],[5 5],'-k')
    xlim([min(B{ax}.d.optim(dind)),min(B{ax}.d.optim(dind))+20])
    ylim([-0.5 5]);
    set(gca,'XTick',[min(B{ax}.d.optim(dind)):5:B{ax}.d.optim(dind)+20]);
    set(gca,'YTick',[0:4:16]);
    title(['Medain555 Axon ',num2str(ax)])
    drawnow
end


%LoGxy
for ax=1:4
    dind=B{ax}.d.optim>mind(ax) & B{ax}.d.optim<maxd(ax);
    figure(ax+40);clf(ax+40);movegui(ax+40,'south')
    plot(B{ax}.d.optim(dind),B{ax}.I.G.LoGxy.norm(dind),'-k');hold on
    plot([min(B{ax}.d.optim(dind)),min(B{ax}.d.optim(dind))+20],[0 0],'-k')
    plot([min(B{ax}.d.optim(dind)),min(B{ax}.d.optim(dind))+20],[5 5],'-k')
    xlim([min(B{ax}.d.optim(dind)),min(B{ax}.d.optim(dind))+20])
    ylim([-0.5 5]);
    set(gca,'XTick',[min(B{ax}.d.optim(dind)):5:B{ax}.d.optim(dind)+20]);
    set(gca,'YTick',[0:4:16]);
    title(['LoGxy Axon ',num2str(ax)])
    drawnow
end
%}