%This figure plots the cross section areas and bouton extents based on
%automated measurements performed in Neuromorpho. The raw data is in an
%excel file within /Plasticity/dat/EMData/CLEM_eLife.xlsx; 

parentdir=pwd;
parentdir=parentdir(1:(strfind(parentdir,'Plasticity')+numel('Plasticity')-1));

pos={'northwest','northeast','southwest','southeast'};
for ax=1:4
    %Following command errors out on mac but not pc:
    A=xlsread(isunixispc([parentdir,'\dat\EMData\CLEM_eLife.xlsx']),['Axon ',num2str(ax)]);
    figure(ax);clf(ax);
    movegui(ax,pos{ax})
    if ax==4 || ax==3
       A(:,1)=A(end,1)-A(:,1); 
    end
    plot(A(:,1),A(:,3),'-k');hold on
    inds=find(diff(A(:,5))~=0)-1;
    bs=inds(1:2:end);
    be=inds(2:2:end);
    plot([0 10],[0 0],'-g')
    plot([0 10],[1 1],'-g')
    for b=1:numel(bs)
        btnx=[A(bs(b),1);A(bs(b):be(b),1);A(be(b),1);A(bs(b),1)];
        btny=[0;A(bs(b):be(b),3);0;0];
        plot(btnx,btny,'-r');
    end
    xlim([0 30]);ylim([-0.05 1.05]);
    drawnow;
end

%{
%For axon 1
[a1,~]=optimal_linear_transform([0.8396,2.824,5.748,10.680,15.050],[1.285,3.32,7.638,13.37,19.49])
%For axon 2
[a2,~]=optimal_linear_transform([],[])
%For axon 3
[a3,~]=optimal_linear_transform([8.505,11.1,14.7],[6.227,9.166,13.18])
%For axon 4
[a4,~]=optimal_linear_transform([2.904,8.312,10.98,14.05],[4.718,11.12,14.03,17.64]);
OptimalStretch=(4*a1+2*a3+3*a4)./(4+2+3)
%}