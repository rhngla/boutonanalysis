%This script recreates Figure 6-figure supplement 1 of Gala_2017; 
%The newest version of the GUI was used to load the following axon:
%DL101(B-S)008-A016 @ shift of [-25, 6], 
%Image was normalized within the GUI.
%Click on the figure GUI and run this script.

hf=gcf;
UserData=hf.UserData;

dstart=14;
dend=151;
cnvs_trace_x=[];cnvs_trace_y=[];
for tt=1:(numel(UserData.Profile))
    [~,indstart]=min(abs(UserData.Profile{tt}.fit.G.LoGxy.d.man-dstart));
    [~,indend]=min(abs(UserData.Profile{tt}.fit.G.LoGxy.d.man-dend));
    cnvs_trace_x=[cnvs_trace_x;UserData.Profile{tt}.r.optim(indstart:indend,1)+UserData.shiftx(tt)];
    cnvs_trace_y=[cnvs_trace_y;UserData.Profile{tt}.r.optim(indstart:indend,2)+UserData.shifty(tt)];
end
cnvs_trace_y=round(cnvs_trace_y);
cnvs_trace_x=round(cnvs_trace_x);
M=false(size(UserData.Im));
ind=sub2ind(size(M),cnvs_trace_x,cnvs_trace_y);
ind=unique(ind);
M(ind)=true;
se = strel('line',11,75);
M=imdilate(M,se);
figure(20),imshow(M.*UserData.Im);caxis([0 10]);
clearvars -except UserData M dstart dend;

%Plot aligned profiles
nf=0.5343;%From analysis_maindataset.
offset=15;
figure(30),clf(30)
for t=1:numel(UserData.Profile)
    d=UserData.Profile{t}.fit.G.LoGxy.d.man;
    I=UserData.Profile{t}.I.G.LoGxy.norm./nf+t*offset;
    ind=d>dstart & d<dend;
    d=d(ind);
    I=I(ind);
    ind=1:2:numel(d);
    plot(d(ind),I(ind),'-','Color',[0.3 0.3 0.3],'LineWidth',1);hold on
    plot([d(1);d(end)],[t*offset;t*offset],'-','Color',[0 0 0]);hold on
end
xlim([dstart,dend]);

%Find d position of each peak (2 single peaks are removed)
lbl_list=UserData.Graph.AM(UserData.Graph.AM>0);
lbl_list=unique(lbl_list);
indmat=nan(numel(UserData.Profile),numel(lbl_list));
dpos=nan(1,numel(lbl_list));
for l=1:numel(lbl_list)
    ti=round(lbl_list(l)./10^4);
    indmat(ti,l)=UserData.Profile{ti}.fit.G.LoGxy.fg.ind(UserData.Profile{ti}.fit.G.LoGxy.fg.id==lbl_list(l));
    dpos(1,l)=UserData.Profile{ti}.fit.G.LoGxy.d.man(indmat(ti,l));
end

Imat=nan(numel(UserData.Profile),numel(lbl_list));
rxmat=nan(numel(UserData.Profile),numel(lbl_list));
rymat=nan(numel(UserData.Profile),numel(lbl_list));
for ti=1:numel(UserData.Profile)
    [~,indmat(ti,:)]=min(abs(bsxfun(@minus,UserData.Profile{ti}.fit.G.LoGxy.d.man(:),dpos)),[],1);
    Imat(ti,:)=UserData.Profile{ti}.I.G.LoGxy.norm(indmat(ti,:))./nf+ti*offset;
    rxmat(ti,:)=UserData.Profile{ti}.r.optim(indmat(ti,:),1)+UserData.shiftx(ti);
    rymat(ti,:)=UserData.Profile{ti}.r.optim(indmat(ti,:),2)+UserData.shifty(ti);
end

lbl_list(dpos<dstart | dpos>dend)=[];
indmat(dpos<dstart | dpos>dend)=[];
Imat(:,dpos<dstart | dpos>dend)=[];
rxmat(:,dpos<dstart | dpos>dend)=[];
rymat(:,dpos<dstart | dpos>dend)=[];
dpos(:,dpos<dstart | dpos>dend)=[];

tsize=3*(numel(UserData.Profile)-1);
Iplot=nan(tsize,numel(lbl_list));
dplot=nan(tsize,numel(lbl_list));
rxplot=nan(tsize,numel(lbl_list));
ryplot=nan(tsize,numel(lbl_list));

%Arrays with nans for single line plot per label; Enables ungrouping in
%powerpoint
for ti=1:numel(UserData.Profile)-1
    Iplot((ti-1)*3+1,:)=Imat(ti,:);
    Iplot((ti-1)*3+2,:)=(ti+1)*offset;
    dplot((ti-1)*3+1,:)=dpos;
    dplot((ti-1)*3+2,:)=dpos;
    alpha=5./(((rymat(ti,:)-rymat(ti+1,:)).^2+(rxmat(ti,:)-rxmat(ti+1,:)).^2).^0.5);
    rxplot((ti-1)*3+1,:)=rxmat(ti,:)+alpha.*(rxmat(ti+1,:)-rxmat(ti,:));
    rxplot((ti-1)*3+2,:)=rxmat(ti+1,:)-alpha.*(rxmat(ti+1,:)-rxmat(ti,:));
    ryplot((ti-1)*3+1,:)=rymat(ti,:)+alpha.*(rymat(ti+1,:)-rymat(ti,:));
    ryplot((ti-1)*3+2,:)=rymat(ti+1,:)-alpha.*(rymat(ti+1,:)-rymat(ti,:));
end
cc=lines(7);
figure(20)
for l=1:numel(lbl_list)
    line(ryplot(:,l),rxplot(:,l),'Color',cc((mod(lbl_list(l),7))+1,:),'LineWidth',0.5,'LineStyle','-');hold on
end
drawnow;

figure(30)
for l=1:numel(lbl_list)
    line(dplot(:,l),Iplot(:,l),'Color',cc((mod(lbl_list(l),7))+1,:),'LineWidth',0.5,'LineStyle','-');hold on
end
drawnow;