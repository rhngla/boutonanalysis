%This script registers peaks detected on manual and optimized traces of
%axons on the same stack by different users. 3d position of the peak is
%used with a distance threshold to generate automatic matches.

%re-populate flags!
pathlist;

animal={'DL001'};
timepoint=cellstr([char(double('K'):(double('T')))']);
section={'002'};
%axon={[1,2,3,4,7,8,9,10,11,12,13,14,15,17,18,19,20]};
axon={[15]};
projectn='xy';
filter={'LoG','LoGxy','LoG155','Gauss2'};
channel='G';

for an=1:numel(animal)
    for se=1:numel(section)
        New=cell(numel(timepoint),numel(axon{se}));
        for ax=1:numel(axon{se})
            axonstr=sprintf('A%03d',axon{se}(ax));
            for ti=1:numel(timepoint)
                stackid=[animal{an},timepoint{ti},section{se}];
                New{ti,ax}=load(isunixispc([profile_pth,stackid,'/',axonstr,'.mat']));
                
                %Initialize peak ids
                for f=1:numel(filter)
                    New{ti,ax}.fit.G.(filter{f}).fg.id=1000*ti+(1:numel(New{ti,ax}.fit.G.(filter{f}).fg.id))';
                end
            end
        end
    end
end

%Reset all matches
for ax=1:numel(axon{se})
    for tnow=1:numel(timepoint)
        for f=1:numel(filter)
            New{tnow,ax}.fit.G.(filter{f}).fg.manid=nan(size(New{tnow,ax}.fit.G.(filter{f}).fg.manid));
        end
    end
end

%manid is nan if not matched
d_thr=2;%in px
for ax=1:numel(axon{se})
    for f=1:numel(filter)
        for tnow=2:numel(timepoint)
            tprev=tnow-1;
            for p=1:numel(New{tnow,ax}.fit.G.(filter{f}).fg.id) %over peaks in the current time
                while tprev>0 && isnan(New{tnow,ax}.fit.G.(filter{f}).fg.manid(p))
                    dx=(New{tprev,ax}.r.optim(New{tprev,ax}.fit.G.(filter{f}).fg.ind,1)-...
                        New{tnow,ax}.r.optim(New{tnow,ax}.fit.G.(filter{f}).fg.ind(p),1));
                    dy=(New{tprev,ax}.r.optim(New{tprev,ax}.fit.G.(filter{f}).fg.ind,2)-...
                        New{tnow,ax}.r.optim(New{tnow,ax}.fit.G.(filter{f}).fg.ind(p),2));
                    dz=(New{tprev,ax}.r.optim(New{tprev,ax}.fit.G.(filter{f}).fg.ind,3)-...
                        New{tnow,ax}.r.optim(New{tnow,ax}.fit.G.(filter{f}).fg.ind(p),3));
                    
                    dd=(dx.^2+dy.^2+dz.^2).^0.5;
                    [vv,ii]=min(dd,[],1);
        
                    %display([vv,ii])
                    if vv<d_thr && sum(New{tnow,ax}.fit.G.(filter{f}).fg.manid==New{tprev,ax}.fit.G.(filter{f}).fg.manid(ii))==0
                        if isnan(New{tprev,ax}.fit.G.(filter{f}).fg.manid(ii)) %Peak not matched to anything before
                            New{tprev,ax}.fit.G.(filter{f}).fg.manid(ii)=New{tprev,ax}.fit.G.(filter{f}).fg.id(ii);
                        end
                        New{tnow,ax}.fit.G.(filter{f}).fg.manid(p)=New{tprev,ax}.fit.G.(filter{f}).fg.manid(ii);
                    end
                    tprev=tprev-1;
                end
                tprev=tnow-1;
            end
        end
    end
end

%Save data back into the folder
for an=1:numel(animal)
    for se=1:numel(section)
        for ax=1:numel(axon{se})
            axonstr=sprintf('A%03d',axon{se}(ax));
            for ti=1:numel(timepoint)
                stackid=[animal{an},timepoint{ti},section{se}];
                
                for f=1:numel(filter)
                    %Use annotated peaks to reset flags
                    New{ti,ax}.fit.G.(filter{f}).fg.flag=nan(numel(New{ti,ax}.fit.G.(filter{f}).fg.id),1);
                    [~,igind,~]=intersect(New{ti,ax}.fit.G.(filter{f}).fg.ind,find(New{ti,ax}.annotate.ignore));
                    New{ti,ax}.fit.G.(filter{f}).fg.flag(igind)=1; %# peak flag is nan by default
                    
                    %Any remaining un-matched peaks are given flag=5;
                    iso=find(isnan(New{ti,ax}.fit.G.(filter{f}).fg.manid(:)) & isnan(New{ti,ax}.fit.G.(filter{f}).fg.flag(:)));
                    New{ti,ax}.fit.G.(filter{f}).fg.flag(iso)=5;
                end
                temp=New{ti,ax};
                save(isunixispc([profile_pth,stackid,'/',axonstr,'.mat']),'-struct','temp');
            end
        end
    end
end

