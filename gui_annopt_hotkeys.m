function [] = gui_annopt_hotkeys(hObject, eventdata, ~)
hf=(eventdata.Source);
dat=guidata(hf);
if strcmp(eventdata.Character,'r')
    display('Will remove portion between selected points from analysis')
elseif strcmp(eventdata.Character,'a')
    display('Will add portion between selected points to analysis')
elseif strcmp(eventdata.Character,'x')
    display('Assign start point for this trace')
elseif strcmp(eventdata.Character,'s')
    display('')
    temp = input('Press y to save ignoreind data, any other key to continue.','s');
    if strcmp(temp,'y')
        for ti=1:numel(dat.Time)
            datorig=load(dat.Time{ti}.save_pth);
            datorig.annotate.ignore=dat.Time{ti}.annotate.ignore;
            save(dat.Time{ti}.save_pth,'-struct','datorig')
            display(['Saving ',dat.Time{ti}.save_pth])
        end
    else
        display('Data not saved')
    end
elseif strcmp(eventdata.Character,'v')
    h_elements=findall(hf,'Type','line');
    
    if ~isempty(h_elements)
        stateval=h_elements(1).Visible;
        if strcmp(stateval,'on')
            stateval='off';
        else
            stateval='on';
        end
        for i=1:numel(h_elements)
            h_elements(i).Visible=stateval;
        end
    end
end

keypressval=eventdata.Key;
switch keypressval
    case 'hyphen' %Control contrast
        hf.Children.CLim(2)=hf.Children.CLim(2)+100;
        
    case 'equal' %Control contrast
        hf.Children.CLim(2)=hf.Children.CLim(2)-100;
end  
%currentmode=guidata(hf,currentmode)
if ~isempty(regexp(eventdata.Character,'[arx]','start'))
    dat.currentmode=eventdata.Character;
end
guidata(hf,dat)
end