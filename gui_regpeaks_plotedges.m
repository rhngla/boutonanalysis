function [dat]=gui_regpeaks_plotedges(dat,f)
h_edges=findobj(f,'-regexp','Tag','Edges*');
delete(h_edges);

%All matched nodes receive label corresponding to the earliest node that is
%part of the list.
[dat.AM,dat.fg_manid]=gui_regpeaks_lblAM(dat.AM,dat.fg_id);
lbl_list=dat.AM(dat.AM>0);
lbl_list=unique(lbl_list);
n_lbls=numel(lbl_list);
cc=lines(7);
figure(f)
for l=1:n_lbls
    [n1,n2]=find(dat.AM==lbl_list(l));
    
    for i=1:numel(n1)
        X=[dat.r(n1(i),1),dat.r(n2(i),1)]';
        Y=[dat.r(n1(i),2),dat.r(n2(i),2)]';
        Z=zeros(size([dat.r(n1(i),3),dat.r(n2(i),3)]))';
        
        edgename=[num2str(n1(i)),'-',num2str((n2(i)))];
        line(Y,X,Z,'Color',cc((mod(lbl_list(l),7))+1,:),'LineWidth',1,...
            'Tag',['Edge-',edgename],...
            'UserData',struct('t',dat.t([n1(i),n2(i)]),'nodeind',dat.nodeind([n1(i),n2(i)])),...
            'ButtonDownFcn',@gui_regpeaks_deledge)
    end
end
end
