function [An]=calc_norm(An)
%This code calculates multiple normalization factors for individual axons.

nsig=3;
ampthr=1;
Ff = @(x,A,mu,sigma) (ones(size(x))*A).*exp(-(x*ones(size(A))-ones(size(x))*mu).^2./(ones(size(x))*sigma.^2)./2);
%filter={'LoG','LoG3','LoGxy','LoGxy232','LoGxy1533','LoGxy233'};
filter={'LoG','LoGxy'};
Temp=cell(numel(An.Section{1}.Time),numel(An.Section{1}.Time{1}.Axon));
for se=1:numel(An.Section)
    for ax=1:numel(An.Section{se}.Time{1}.Axon)
        for f=1:numel(filter)
            dmin=-inf;
            dmax=inf;
            for ti=1:numel(An.Section{se}.Time)
                Dat=An.Section{se}.Time{ti}.Axon{ax};
                dmin=max([dmin,min(Dat.fit.G.(filter{f}).d.man)]);
                dmax=min([dmax,max(Dat.fit.G.(filter{f}).d.man)]);
            end
            
            %If boutons are matched throughout, they will have manid
            %corresponding to the first session in the list.
            tinit=1;
            tfin=numel(An.Section{se}.Time);
            btn_inall=intersect(An.Section{se}.Time{tinit}.Axon{ax}.fit.G.(filter{f}).fg.manid,...
                An.Section{se}.Time{tfin}.Axon{ax}.fit.G.(filter{f}).fg.manid);
            
            %Remove entries from btn_inall that belong to ignored regions in
            %any time
            for ti=1:numel(An.Section{se}.Time)
                Dat=An.Section{se}.Time{ti}.Axon{ax};
                [inlist,btn_inall_ind] = ismember(Dat.fit.G.(filter{f}).fg.manid,btn_inall);
                btn_inall_ind=btn_inall_ind(btn_inall_ind~=0);
                keep=~Dat.annotate.ignore(Dat.fit.G.(filter{f}).fg.ind(inlist));
                btn_inall=btn_inall(btn_inall_ind(keep));
            end
            
            %Remove regions containing big boutons in only some sessions from
            %backbone in all sessions
            d_big_1_btnendlist=[];
            d_big_1_btnstartlist=[];
            for ti=1:numel(An.Section{se}.Time)
                %find start and end distances on manually aligned distances
                Dat=An.Section{se}.Time{ti}.Axon{ax};
                bigsinglepeaks=Dat.fit.G.(filter{f}).fg.amp>ampthr;
                ddend=Dat.fit.G.(filter{f}).fg.mu(bigsinglepeaks)+nsig*Dat.fit.G.(filter{f}).fg.sig(bigsinglepeaks);
                ddstart=Dat.fit.G.(filter{f}).fg.mu(bigsinglepeaks)-nsig*Dat.fit.G.(filter{f}).fg.sig(bigsinglepeaks);
                
                [~,ddend_ind]=min(abs(bsxfun(@minus,ddend(:),Dat.d.optim(:)')),[],2);
                [~,ddstart_ind]=min(abs(bsxfun(@minus,ddstart(:),Dat.d.optim(:)')),[],2);
                
                ddend=Dat.fit.G.(filter{f}).d.man(ddend_ind);
                ddstart=Dat.fit.G.(filter{f}).d.man(ddstart_ind);
                d_big_1_btnendlist=[d_big_1_btnendlist;ddend(:)];
                d_big_1_btnstartlist=[d_big_1_btnstartlist;ddstart(:)];
            end
            
            %Calculate union of ignored regions over all sessions, based on
            %manually aligned distances
            digstartlist=[];
            digendlist=[];
            for ti=1:numel(An.Section{se}.Time)
                Dat=An.Section{se}.Time{ti}.Axon{ax};
                igstart=find(diff(Dat.annotate.ignore)==1)+1;
                igend=find(diff(Dat.annotate.ignore)==-1);
                igstart=igstart(:);
                igend=igend(:);
                if Dat.annotate.ignore(1)==1
                    igstart=[1;igstart];
                end
                if Dat.annotate.ignore(end)==1
                    igend=[igend;numel(Dat.annotate.ignore)];
                end
                digstartlist=[digstartlist;Dat.fit.G.(filter{f}).d.man(igstart(:))];
                digendlist=[digendlist;Dat.fit.G.(filter{f}).d.man(igend(:))];
            end
            
            
            %Calculate background around boutons that are present in
            %all times
            for ti=1:numel(An.Section{se}.Time)
                Dat=An.Section{se}.Time{ti}.Axon{ax};
                [inlist,~] = ismember(Dat.fit.G.(filter{f}).fg.manid,btn_inall);
                
                btn_end=Dat.fit.G.(filter{f}).fg.mu(inlist)+nsig*Dat.fit.G.(filter{f}).fg.sig(inlist);
                btn_start=Dat.fit.G.(filter{f}).fg.mu(inlist)-nsig*Dat.fit.G.(filter{f}).fg.sig(inlist);
                
                [~,indstart]=min(abs(bsxfun(@minus,Dat.d.optim,btn_start')),[],1);
                [~,indend]=min(abs(bsxfun(@minus,Dat.d.optim,btn_end')),[],1);
                ind_bouton_in_all=[indstart(:),indend(:)];
                
                backbone=true(size(Dat.d.optim));
                
                %A: Remove regions from backbone based on present time only
                backbone(Dat.annotate.ignore)=false;
                backbone(Dat.fit.G.(filter{f}).d.man<dmin | Dat.fit.G.(filter{f}).d.man>dmax)=false;
                
                %B1: Remove regions ignored at any time from current backbone
                for jj=1:numel(digstartlist)
                    backbone(Dat.fit.G.(filter{f}).d.man>digstartlist(jj) & Dat.fit.G.(filter{f}).d.man<digendlist(jj))=false;
                end
                annotated_backbone=backbone;
                
                %B2: Remove single bouton removed at any time from current backbone
                for jj=1:numel(d_big_1_btnstartlist)
                    backbone(Dat.fit.G.(filter{f}).d.man>d_big_1_btnstartlist(jj) & Dat.fit.G.(filter{f}).d.man<d_big_1_btnendlist(jj))=false;
                end
                
                %B3: Remove each bouton detected in all sessions from current backbone
                for i=1:size(ind_bouton_in_all,1)
                    backbone(ind_bouton_in_all(i,1):ind_bouton_in_all(i,2))=false;
                end
                
                %Calculate fitted background
                bg=zeros(size(Dat.d.optim));
                for i=1:numel(Dat.fit.G.(filter{f}).bg.ind)
                    bg=bg+Ff(Dat.d.optim,Dat.fit.G.(filter{f}).bg.amp(i),Dat.fit.G.(filter{f}).bg.mu(i),Dat.fit.G.(filter{f}).bg.sig(i));
                end
                
                %Calculate fitted background in Gaussian profile(s)
                %Gauss1bg=zeros(size(Dat.d.optim));
                %for i=1:numel(Dat.fit.G.('Gauss1').bg.ind)
                %    Gauss1bg=Gauss1bg+Ff(Dat.d.optim,Dat.fit.G.('Gauss1').bg.amp(i),Dat.fit.G.('Gauss1').bg.mu(i),Dat.fit.G.('Gauss1').bg.sig(i));
                %end
                
                %Calculate fitted background in Gaussian profile(s)
                Gauss2bg=zeros(size(Dat.d.optim));
                for i=1:numel(Dat.fit.G.('Gauss2').bg.ind)
                    Gauss2bg=Gauss2bg+Ff(Dat.d.optim,Dat.fit.G.('Gauss2').bg.amp(i),Dat.fit.G.('Gauss2').bg.mu(i),Dat.fit.G.('Gauss2').bg.sig(i));
                end
                
                %Length of axon only excluding explicitly ignored (annotated) regions
                startt=find(diff(annotated_backbone)==1)+1;
                endd=find(diff(annotated_backbone)==-1)+1;
                startt=startt(:);
                endd=endd(:);
                if annotated_backbone(1)==1
                    startt=[1;startt];
                end
                if annotated_backbone(end)==1
                    endd=[endd;numel(annotated_backbone)];
                end
                axlen_legitimate=Dat.d.optim(endd)-Dat.d.optim(startt);
                axlen_legitimate=sum(axlen_legitimate);
                
                %Length of axon corresponding to the backbone
                startt=find(diff(backbone)==1)+1;
                endd=find(diff(backbone)==-1)+1;
                startt=startt(:);
                endd=endd(:);
                if backbone(1)==1
                    startt=[1;startt];
                end
                if backbone(end)==1
                    endd=[endd;numel(backbone)];
                end
                axlen=Dat.d.optim(endd)-Dat.d.optim(startt);
                axlen=sum(axlen);
                
                An.Section{se}.Time{ti}.Axon{ax}.norm.(filter{f}).profile_norm=mean(Dat.I.G.(filter{f}).norm(backbone));
                An.Section{se}.Time{ti}.Axon{ax}.norm.(filter{f}).profile_raw=mean(Dat.I.G.(filter{f}).raw(~Dat.annotate.ignore));
                An.Section{se}.Time{ti}.Axon{ax}.norm.(filter{f}).Gauss2_raw=mean(Dat.I.G.Gauss2.raw(~Dat.annotate.ignore));
                An.Section{se}.Time{ti}.Axon{ax}.norm.(filter{f}).fit_norm=mean(bg(annotated_backbone));
                %An.Section{se}.Time{ti}.Axon{ax}.norm.(filter{f}).gauss1_norm=mean(Gauss1bg(~Dat.annotate.ignore));
                An.Section{se}.Time{ti}.Axon{ax}.norm.(filter{f}).gauss2_norm=mean(Gauss2bg(~Dat.annotate.ignore));
                An.Section{se}.Time{ti}.Axon{ax}.norm.(filter{f}).axlen_legitimate=axlen_legitimate;
                An.Section{se}.Time{ti}.Axon{ax}.norm.(filter{f}).axlen_excludingpeaks=axlen;
                An.Section{se}.Time{ti}.Axon{ax}.norm.(filter{f}).commonignored=mean(Dat.I.G.(filter{f}).norm(annotated_backbone))./mean(Dat.I.G.(filter{f}).norm(~Dat.annotate.ignore));
                
                %figure(98),%plot(Dat.fit.G.LoG.d.man,Dat.I.G.LoG.raw)
                %hold on, plot(Dat.fit.G.LoG.d.man,backbone2+2*ti)
                
                %interpolate on grid using d.man
                %{
                dgrid=dmin:0.03:dmax;
                Temp{ti,ax}.(filter{f}).Igrid=interp1(Dat.fit.G.(filter{f}).d.man,Dat.I.G.(filter{f}).norm,dgrid);
                Temp{ti,ax}.(filter{f}).dgrid=dgrid;
                Temp{ti,ax}.(filter{f}).backbone=interp1(Dat.fit.G.(filter{f}).d.man,double(backbone),dgrid);
                %}
                %{
                %This block plots profiles + regions
                %chosen to estimate background.
                figure(30),
                if ti==1
                    clf(30)
                end
                
                plot(Dat.d.optim,Dat.I.G.(filter{f}).norm+10*(ti-1),'-','Color',[0.4,0.4,0.4]),hold on
                plot(Dat.d.optim(backbone),Dat.I.G.(filter{f}).norm(backbone)+10*(ti-1),'.','Color',[0.4,0,0]),hold on
                
                for i=1:numel(Dat.fit.G.(filter{f}).fg.ind)
                    temp=Dat.fit.G.(filter{f}).fg;
                    xx=(-5:0.1:+5)+temp.mu(i);
                    plot(xx,Ff(xx,temp.amp(i),temp.mu(i),temp.sig(i))+10*(ti-1),'-','Color',[0.7 0.7 0.7]),hold on;
                end
                drawnow;
                %}
                
                %{
                temp=Dat.fit.G.LoG.fg;
                temp.mu=temp.mu(inlist);
                temp.amp=temp.amp(inlist);
                temp.sig=temp.sig(inlist);
                cc=lines(numel(temp.mu));
                for ii=1:numel(temp.mu)
                    xx=(-5:0.1:+5)+temp.mu(ii);
                    plot(xx,Ff(xx,temp.amp(ii),temp.mu(ii),temp.sig(ii))+10*(ti-1),'-','Color',cc(ii,:)),hold on;
                end
             
                plot(Dat.d.optim([1,end]),repmat(An.Section{se}.Time{ti}.Axon{ax}.norm1,2,1)+10*(ti-1),'-','Color',[0.8 0 0],'LineWidth',1),hold on
                plot(Dat.d.optim([1,end]),repmat(An.Section{se}.Time{ti}.Axon{ax}.norm.(filter{f}).profile_norm,2,1)+10*(ti-1),'-','Color',[0 0.6 0],'LineWidth',1),hold on
                plot(Dat.d.optim,(1-backbone).*An.Section{se}.Time{ti}.Axon{ax}.norm.(filter{f}).profile_norm+10*(ti-1),'-','Color',[1,1,1],'LineWidth',1),hold on
                plot(Dat.d.optim([1,end]),[10*(ti-1),10*(ti-1)],'-','Color',[0 0 0],'LineWidth',0.5),hold on
                box on;ylim([-0.1 10*(ti)])
                grid on
                drawnow;
                %}
            end
        end
    end
end

