%im_pth - Raw image files parent folder
%man_pth - Path for manual trace .swc files
%optim_pth - Path for optimized trace .swc files
%profile_pth - Path for profile data structure (.mat file)
%proj_pth - Path with all projections
%err_pth - store logs for optimization and fitting
%[~,val]=system('hostname');

if ispc
    val = getenv('COMPUTERNAME');
elseif ismac
    [~,val]=system('hostname');
end

%Point parentpath to the /Plasticity folder.
parentdir=pwd;
parentdir=parentdir(1:(strfind(parentdir,'Plasticity')+numel('Plasticity')-1));

if strcmp(val(1:3),'NEU')
    
    %{
    %For main dataset
    im_pth='E:\DatasetIM\';%This dataset is on the Neurogeometry computer
    man_pth=[parentdir,'\dat\ManualTraces_v2\'];
    optim_pth=[parentdir,'\dat\OptimizedTraces_v2\'];
    profile_pth=[parentdir,'\dat\Profiles_v2\'];
    proj_pth=[parentdir,'\dat\Projections_v2\'];
    err_pth=[parentdir,'\BoutonAnalysis\code\errorlogs\'];
    
    behfold=[parentdir,'\dat\LearningData_v3\'];
    headerf=[parentdir,'\dat\IMHeaders\'];
    %}
    
    
    %For EM calculations
    im_pth=[parentdir,'\dat\EMData\DatasetIM\'];
    man_pth=[parentdir,'\dat\EMData\ManualTraces\'];
    optim_pth=[parentdir,'\dat\EMData\OptimizedTraces\'];
    profile_pth=[parentdir,'\dat\EMData\Profiles\'];
    proj_pth=[parentdir,'\dat\EMData\Projections\'];
    err_pth=[parentdir,'\BoutonAnalysis\code\errorlogs\'];
    %}
    
    %{
    %For controls: different imaging conditions
    im_pth='E:\DatasetIM\Control-1\';
    man_pth=[parentdir,'\dat\Controls\Conditions\ManualTraces\'];
    optim_pth=[parentdir,'\dat\Controls\Conditions\OptimizedTraces_v3\'];
    profile_pth=[parentdir,'\dat\Controls\Conditions\Profiles_v4\'];%Gauss fitted using LoGxy peaks mu
    %profile_pth=[parentdir,'\dat\Controls\Conditions\Profiles_v3\'];%Gauss and LoGxy fitted independently
    proj_pth=[parentdir,'\dat\Projections_v2\'];
    err_pth=[parentdir,'\BoutonAnalysis\code\errorlogs\'];
    %}
    
    %{
    %For manual vs optimized trace comparisons
    im_pth='E:\DatasetIM\Control-1\';
    %man_pth=[parentdir,\dat\Controls_v2\IsOptimizationReqd\Traces\'];
    %optim_pth=[parentdir,'\dat\Controls_v2\IsOptimizationReqd\Traces\'];
    man_pth=[parentdir,'\dat\Controls_v2\IsOptimizationReqd\Traces_v2\'];
    optim_pth=[parentdir,'\dat\Controls_v2\IsOptimizationReqd\Traces_v2\'];
    profile_pth=[parentdir,'\dat\Controls_v2\IsOptimizationReqd\Profiles_v2\'];
    proj_pth=[parentdir,'\dat\Controls_v2\IsOptimizationReqd\Projections\'];
    err_pth=[parentdir,'\BoutonAnalysis\code\errorlogs\'];
    %}
elseif strcmp(val(1:3),'RGM')
    %{
    im_pth=[parentdir,'/dat/DatasetIM/'];
    man_pth=[parentdir,'/dat/ManualTraces_v2/'];
    optim_pth=[parentdir,'/dat/OptimizedTraces_v2/'];
    profile_pth=[parentdir,'/dat/Profiles_v2/'];
    proj_pth=[parentdir,'/dat/Projections_v2/'];
    err_pth=[parentdir,'/BoutonAnalysis/code/errorlogs/'];
    %}
    
    %
    %For EM calculations
    im_pth=[parentdir,'/dat/EMData/DatasetIM/'];
    man_pth=[parentdir,'/dat/EMData/ManualTraces/'];
    optim_pth=[parentdir,'/dat/EMData/OptimizedTraces/'];
    profile_pth=[parentdir,'/dat/EMData/Profiles/'];
    proj_pth=[parentdir,'/dat/EMData/Projections/'];
    err_pth=[parentdir,'/BoutonAnalysis/code/errorlogs/'];
    %}
    
    %{
    %For controls: different imaging conditions
    im_pth='/Users/Fruity/Downloads/DatasetIM/';
    man_pth=[parentdir,'/dat/Controls/Conditions/ManualTraces/'];
    optim_pth=[parentdir,'/dat/Controls/Conditions/OptimizedTraces_v3/'];
    profile_pth=[parentdir,'/dat/Controls/Conditions/Profiles_v4/'];
    proj_pth=[parentdir,'/dat/Projections_v2/'];
    err_pth=[parentdir,'/BoutonAnalysis/code/errorlogs/'];
    %}
    
    %{
    %For manual vs optimized trace comparisons
    im_pth='/Users/Fruity/Downloads/DatasetIM/';
    man_pth=[parentdir,'/dat/Controls_v2/IsOptimizationReqd/Traces_v2/'];
    optim_pth=[parentdir,'/dat/Controls_v2/IsOptimizationReqd/Traces_v2/'];
    profile_pth=[parentdir,'/dat/Controls_v2/IsOptimizationReqd/Profiles_v2/'];
    proj_pth=[parentdir,'/dat/Controls_v2/IsOptimizationReqd/Projections/'];
    err_pth=[parentdir,'/BoutonAnalysis/code/errorlogs/'];
    %}
end

