%This script creates xy,yz,zx projections.
%IM(:,:,1)=green channel & IM(:,:,2)=red channel

pathlist;
paramset;

animal={'DL001'};
timepoint={'A','B','D','E','F','H','I'};
section={'002'};
axon={[1]};
setim=[];%For manual vs optimized trace comparisons

%From optimized profile swc
for an=1:numel(animal)
    for se=1:numel(section)
        for ti=1:numel(timepoint)
            stackid=[animal{an},timepoint{ti},section{se}];
            imid=setim;
            if isempty(imid)
                imid=stackid;
            end
            %Handle missing channel data
            if exist(isunixispc([im_pth,imid,'G.mat']),'file')
                IM_G=load(isunixispc([im_pth,imid,'G.mat']),'Original');IM_G=IM_G.Original;
            else
                display([imid,'G.mat not found!'])
                IM_G=zeros([1024,1024,312]);
            end
            
            if exist(isunixispc([im_pth,imid,'R.mat']),'file')
                IM_R=load(isunixispc([im_pth,imid,'R.mat']),'Original');IM_R=IM_R.Original;
            else
                display([imid,'R.mat not found!'])
                IM_R=zeros([1024,1024,312]);
            end
            
            %Create projection for each axon separately
            for ax=1:numel(axon{se})
                axonstr=sprintf('A%03d',axon{se}(ax));
                
                %Load optimized trace
                fname=isunixispc([optim_pth,stackid,'/',axonstr,'.swc']);
                [AM,r,R]=swc2AM(fname);
                
                sizeIM=size(IM_G);
                
                pad=20;
                minr=min(r,[],1);
                maxr=max(r,[],1);
                minx=max(minr(2)-pad,1);maxx=min(maxr(2)+pad,sizeIM(2));
                miny=max(minr(1)-pad,1);maxy=min(maxr(1)+pad,sizeIM(1));
                minz=max(minr(3)-pad,1);maxz=min(maxr(3)+pad,sizeIM(3));
                
                minx=round(minx);maxx=round(maxx);
                miny=round(miny);maxy=round(maxy);
                minz=round(minz);maxz=round(maxz);
                
                %Filter size for non-DL100 axons is 7
                [KT]=FastMarchingTube([maxy-miny+1,maxx-minx+1,maxz-minz+1],[r(:,1)-miny,r(:,2)-minx,r(:,3)-minz,],proj.fm_dist,[1 1 1]);
                Filter=false(sizeIM);
                Filter(miny:maxy,minx:maxx,minz:maxz)=KT;
                
                tempIM_G=double(Filter).*double(IM_G);
                tempIM_R=double(Filter).*double(IM_R);
                
                clim=500;
                %z-projection
                perm=[1,2,3];
                IM(:,:,1)=(max(tempIM_G,[],perm(3)));
                IM(:,:,2)=(max(tempIM_R,[],perm(3)));
                save([proj_pth,stackid,'-',axonstr,'-xy.mat'],'IM')
                
                hfig=404;
                figure(hfig),clf(hfig);
                imshow(IM(:,:,1),[0 clim]),hold on
                plotAM(AM,r,[]);
                clear IM;
                drawnow;
                
                %y-projection
                perm=[1,3,2];
                IM(:,:,1)=squeeze(max(tempIM_G,[],perm(3)));
                IM(:,:,2)=squeeze(max(tempIM_R,[],perm(3)));
                save([proj_pth,stackid,'-',axonstr,'-zx.mat'],'IM')
                
                figure(hfig+1),clf(hfig+1);
                imshow(IM(:,:,1),[0 clim]),hold on
                plotAM(AM,r(:,perm),[]);
                clear IM;
                drawnow;
                
                %x-projection
                perm=[3,2,1];
                IM(:,:,1)=squeeze(max(permute(tempIM_G,[1,3,2]),[],perm(3)));
                IM(:,:,2)=squeeze(max(permute(tempIM_R,[1,3,2]),[],perm(3)));
                save([proj_pth,stackid,'-',axonstr,'-yz.mat'],'IM')
                
                figure(hfig+2),clf(hfig+2);
                imshow(IM(:,:,1),[0 clim]),hold on
                plotAM(AM,r(:,perm),[]);
                clear IM;
                drawnow;
            end
        end
    end
end