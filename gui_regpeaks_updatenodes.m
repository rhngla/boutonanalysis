function []=gui_regpeaks_updatenodes(dat)
%Set properties of nodes based on flags and connections

node_i=sum((dat.AM+dat.AM')>0,2)==0;
set(dat.h_nodes(node_i),'Marker','o','MarkerSize',15,'MarkerEdgeColor',[1 0 0],'MarkerFaceColor','none','LineStyle','none','LineWidth',1);
node_i=sum((dat.AM+dat.AM')>0,2)>0;
%set(dat.h_nodes(node_i),'Marker','o','MarkerSize',5,'MarkerEdgeColor',[0 0.5 0.2],'MarkerFaceColor','none','LineStyle','none','LineWidth',1);
cc=lines(7);
lbl=unique(dat.fg_manid(node_i));
ii=find(node_i);
for l=1:numel(lbl)
    set(dat.h_nodes(ii(dat.fg_manid(ii)==lbl(l))),'Marker','o','MarkerSize',5,'MarkerEdgeColor',cc((mod(lbl(l),7))+1,:),'MarkerFaceColor','none','LineStyle','none','LineWidth',1);
end

node_i=dat.fg_flag<5 & dat.fg_flag>=1;
set(dat.h_nodes(node_i),'Marker','x','MarkerSize',10,'MarkerEdgeColor',[0.8 0 0],'MarkerFaceColor','none','LineStyle','none','LineWidth',0.5);
node_i=dat.fg_flag==5;
set(dat.h_nodes(node_i),'Marker','^','MarkerSize',12,'MarkerEdgeColor',[0.8 0 0],'MarkerFaceColor','none','LineStyle','none','LineWidth',0.5);
end