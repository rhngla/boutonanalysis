function [MAT,B]=EM_axons2(IM)
%This file produces data for filter comparisons in Figure4S1 of Gala_2017

if isempty(IM)
    parentdir=pwd;
    parentdir=parentdir(1:(strfind(parentdir,'Plasticity')+numel('Plasticity')-1));
    if ismac
        IM=load([parentdir,'/dat/EMData/DatasetIM/DL100I005G.mat'],'Original');
    else
        IM=load([parentdir,'\dat\EMData\DatasetIM\DL100I005G.mat'],'Original');
    end
    IM=double(IM.Original);
end
%}

pathlist;
Ff = @(x,A,mu,sigma) (ones(size(x))*A).*exp(-(x*ones(size(A))-ones(size(x))*mu).^2./(ones(size(x))*sigma.^2)./2);
%mind=[27.5,26.0,24.7,0];
mind=[27.5,26.0,20.7,0];
maxd=[47.0,42.0,36.3325,15];
cc=lines(4);

axid=[1,1,1,1,1,1,2,2,2,3,3,3,3,4,4,4,4,4,4]';
inds=[459,520,547,595,638,669,553,1,456,319,360,405,454,43,108,150,194,16,171]'; %(on optimized profiles - from EM_axons.m code)
btnwt=[13.5300,10.8200,1.9800,10.0800,8.6500,6.2500,3.6300,NaN,5.5700,9.5400,1.9900,8.5100,10.7800,5.8000,4.2300,5.9600,2.8500,1.1400,2.1900];

%EM extent estimated by hand 
%emvol=[0.8896,0.7394,0.0819,0.9380,0.6213,0.2082,0.1951,NaN,0.2005,0.5930,0.0700,0.4615,0.6678,0.3177,0.1798,0.2663,0.1191,NaN,NaN];
%mitovol=[0.1891,0.1705,0,0.2250,0.1388,0.0427,0.0358,0,0,0.1821,0,0,0.1611,0,0,0.0274,0,0,0];

%EM extent estimated by Blender tool automatically
emvol=[0.9193,0.7789,0.0930,0.9979,0.6690,0.2194,0.4834,0.5743,0.3724,0.6323,0.1016,0.4560,0.7043,0.3081,0.1975,0.2289,0.1403,nan,nan];
mitovol=[0.1891,0.1705,0.0000,0.2250,0.1388,0.0427,0.1110,0.0000,0.0270,0.1821,0.0000,0.0000,0.1611,0.0000,0.0000,0.0274,0.0000,0.0000,0.0000];
%emvol=emvol-mitovol;

MAT=[];
pos={'northwest','northeast','southwest','southeast'};
B=cell(4,1);

for ax=1:4
    A=load(isunixispc([profile_pth,sprintf('DL100I005\\A%.3d.mat',ax)]));
    
    %Filters for optimized traces
    indd=sub2ind(size(IM),round(A.r.optim(:,1)),round(A.r.optim(:,2)),round(A.r.optim(:,3)));
    A.I.G.Nofilt.raw=IM(indd);A.I.G.Nofilt.norm=A.I.G.Nofilt.raw./mean(A.I.G.Nofilt.raw);
    
    [A.I.G.Gauss1.raw,~]=profilefilters (A.r.optim,IM,'Gauss1');A.I.G.Gauss1.norm=A.I.G.Gauss1.raw./mean(A.I.G.Gauss1.raw);
    [A.I.G.Gauss2.raw,~]=profilefilters (A.r.optim,IM,'Gauss2');A.I.G.Gauss2.norm=A.I.G.Gauss2.raw./mean(A.I.G.Gauss2.raw);
    
    [A.I.G.mean333.raw,~]=profilefilters (A.r.optim,IM,'mean333');A.I.G.mean333.norm=A.I.G.mean333.raw./mean(A.I.G.mean333.raw);
    [A.I.G.mean555.raw,~]=profilefilters (A.r.optim,IM,'mean555');A.I.G.mean555.norm=A.I.G.mean555.raw./mean(A.I.G.mean555.raw);
    
    [A.I.G.median333.raw,~]=profilefilters (A.r.optim,IM,'median333');A.I.G.median333.norm=A.I.G.median333.raw./mean(A.I.G.median333.raw);
    [A.I.G.median555.raw,~]=profilefilters (A.r.optim,IM,'median555');A.I.G.median555.norm=A.I.G.median555.raw./mean(A.I.G.median555.raw);
    
    [A.I.G.LoGxy1531.raw,~]=profilefilters (A.r.optim,IM,'LoGxy1531');A.I.G.LoGxy1531.norm=A.I.G.LoGxy1531.raw./mean(A.I.G.LoGxy1531.raw);
    
    dind=A.d.optim>mind(ax) & A.d.optim<maxd(ax);
    
    %Calculate mean gaussian background
    Gauss2_bg=zeros(size(A.d.optim));
    for i=1:numel(A.fit.G.('Gauss2').bg.ind)
        temp=A.fit.G.('Gauss2').bg;
        Gauss2_bg=Gauss2_bg+Ff(A.d.optim,temp.amp(i),temp.mu(i),temp.sig(i));
    end
    
    %Calculate background at all locations on the profile
    LoGxy_bg=zeros(size(A.d.optim));
    for i=1:numel(A.fit.G.('LoGxy').bg.ind)
        temp=A.fit.G.('LoGxy').bg;
        LoGxy_bg=LoGxy_bg+Ff(A.d.optim,temp.amp(i),temp.mu(i),temp.sig(i));
    end
    
    %Normalized LoGxy intensity profile
    LoGxy_fg=A.I.G.('LoGxy').norm./mean(Gauss2_bg);
    
    %{
    figure(ax+2300),clf(ax+2300),movegui(ax+2300,pos{ax})
    title(sprintf('A%.3d.mat',ax));
    plot(A.d.optim(dind),LoGxy_fg(dind),'-','Color',cc(ax,:)),hold on
    xlim([min(A.d.optim(dind)),min(A.d.optim(dind))+20]);ylim([-1 17]);
    set(gca,'XTick',[min(A.d.optim(dind)):5:A.d.optim(dind)+20],'YTick',[0:4:16]);
    %plot(A.d.optim(dind),LoGxy_bg(dind),'-','Color',cc(ax,:)),hold on
    grid on;
    %}
    
    id=find(A.d.optim(A.fit.G.('LoGxy').fg.ind)>mind(ax) & A.d.optim(A.fit.G.('LoGxy').fg.ind)<maxd(ax));
    for b=1:numel(id)
        LoGxyamp=A.fit.G.('LoGxy').fg.amp(id(b));
        LoGxybgrnd=LoGxy_bg(A.fit.G.('LoGxy').fg.ind(id(b)));
        %{
        btn_weight=(LoGxyamp+LoGxybgrnd)./mean(Gauss2_bg);
        text(A.d.optim(A.fit.G.('LoGxy').fg.ind(id(b))),LoGxy_fg(A.fit.G.('LoGxy').fg.ind(id(b)))+0.5,...
            sprintf('%.2f,\n %.2f \n %0.2f',LoGxyamp,A.fit.G.('LoGxy').fg.ind(id(b)),btn_weight),'Color',cc(ax,:),'HorizontalAlignment','center');
        %}
    end
    [~,pk]=min(abs(bsxfun(@minus,A.fit.G.('LoGxy').fg.ind,inds(axid==ax)')),[],1);
    LoGxyPeak=(A.fit.G.('LoGxy').fg.amp(pk)+LoGxy_bg(A.fit.G.('LoGxy').fg.ind(pk))).*(mean(A.I.G.LoGxy.raw)./mean(A.I.G.Gauss2.raw));
    
    display([ax,median(A.I.G.median333.raw)]);
    display([ax,mean(Gauss2_bg)]);
    
    MAT=[MAT;[
        axid(axid==ax),...
        inds(axid==ax),...
        ...
        A.I.G.Nofilt.norm(inds(axid==ax)),...
        A.I.G.Nofilt.norm(inds(axid==ax))./median(A.I.G.Nofilt.norm),...
        A.I.G.Nofilt.norm(inds(axid==ax))./mean(Gauss2_bg),...
        ...
        A.I.G.Gauss1.norm(inds(axid==ax)),...
        A.I.G.Gauss1.norm(inds(axid==ax))./median(A.I.G.Gauss1.norm),...
        A.I.G.Gauss1.norm(inds(axid==ax))./mean(Gauss2_bg),...
        ...
        A.I.G.Gauss2.norm(inds(axid==ax)),...
        A.I.G.Gauss2.norm(inds(axid==ax))./median(A.I.G.Gauss2.norm),...
        A.I.G.Gauss2.norm(inds(axid==ax))./mean(Gauss2_bg),...
        ...
        A.I.G.mean333.norm(inds(axid==ax)),...
        A.I.G.mean333.norm(inds(axid==ax))./median(A.I.G.mean333.norm),...
        A.I.G.mean333.norm(inds(axid==ax))./mean(Gauss2_bg),...
        ...
        A.I.G.mean555.norm(inds(axid==ax)),...
        A.I.G.mean555.norm(inds(axid==ax))./median(A.I.G.mean555.norm),...
        A.I.G.mean555.norm(inds(axid==ax))./mean(Gauss2_bg),...
        ...
        A.I.G.median333.norm(inds(axid==ax)),...
        A.I.G.median333.norm(inds(axid==ax))./median(A.I.G.median333.norm),...
        A.I.G.median333.norm(inds(axid==ax))./mean(Gauss2_bg),...
        ...
        A.I.G.median555.norm(inds(axid==ax)),...
        A.I.G.median555.norm(inds(axid==ax))./median(A.I.G.median555.norm),...
        A.I.G.median555.norm(inds(axid==ax))./mean(Gauss2_bg),...
        ...
        A.I.G.LoGxy1531.norm(inds(axid==ax)),...
        A.I.G.LoGxy1531.norm(inds(axid==ax))./median(A.I.G.LoGxy1531.norm),...
        A.I.G.LoGxy1531.norm(inds(axid==ax))./mean(Gauss2_bg),...
        ...
        LoGxyPeak,...
        LoGxyPeak./median(A.I.G.LoGxy.norm),...
        LoGxyPeak./mean(Gauss2_bg)...
        ]];
    B{ax}=A;
end

yy=emvol(:);
MAT=[MAT,btnwt(:)];
MAT(isnan(yy),:)=nan;
MAT([7,8],:)=nan;%8 is the terminal bouton, 7 is the one near branch point

Lbl={'Nofilt_mean','Nofilt_median','Nofilt_shaft',...
    'Gauss1_mean','Gauss1_median','Gauss1_shaft',...
    'Gauss2_mean','Gauss2_median','Gauss2_shaft',...
    'mean333_mean','mean333_med','mean333_shaft',...
    'mean555_mean','mean555_med','mean555_shaft',...
    'median333_mean','median333_med','median333_shaft',...
    'median555_mean','median555_med','median555_shaft',...
    'LoGxy1531_mean','LoGxy1531_med','LoGxy1531_shaft',...
    'LoGxyFittedPeak_mean','LoGxyFittedPeak_med','LoGxyFittedPeak_shaft',...
    'BoutonWt'
    };

pearsonr=nan(size(MAT,2),1);
for i=3:size(MAT,2)
    xx=MAT(:,i);
    notnan=~isnan(xx) & ~isnan(yy);
    [ff,goodness]=fit(xx(notnan),yy(notnan),fittype('a*x+b'),'StartPoint',[1,0]);
    display([Lbl{i-2},': r = ',num2str(round(goodness.rsquare^0.5,3))])
    
    pearsonr(i)=goodness.rsquare^0.5;
    
    if i>=29
    cc=lines(4);
    cc=cc(axid,:);
    figure(i);clf(i)
    scatter(xx,yy,[],cc,'filled'),hold on
    plot([0,16],ff([0,16]),'-k')
    txt = num2cell(1:18,1);
    txt=txt(notnan);
    text(xx(notnan)+0.15,yy(notnan),txt','Color',[0.4,0.4,0.4])
    text(max(xx(notnan)),0.9,['r = ',num2str(goodness.rsquare^0.5)])
    xlabel(Lbl{i-2});ylabel('Bouton volume (\mu{m}^{3})')
    xlim([0 nanmax(1.1*xx)]);ylim([0 nanmax(1.1*yy)])
    box on;axis square;drawnow;
    end
    %}
end

pearsonr=pearsonr(3:end-1);
pearsonr=round(reshape(pearsonr,3,9),2)';
array2table(pearsonr,'RowNames',{'Nofilt','Gauss1','Gauss2','mean333','mean555','median333','median555','LoGxy1531','LoGxyFitted'},'VariableNames',{'Mean','Median','Shaft'})
end