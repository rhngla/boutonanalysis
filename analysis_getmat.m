function [Dat] = analysis_getmat(An)
%This function creates matrices for analysis from registered data. Input is
%obtained from analysis_loaddat.m
%fit.G.LoG.fg: list of channels and filters within each channel to load.
%Input Animal.Section{1}.Time{1}.Axon{1} & Animal.L

fitshape='G';%This should be the same as used in fitpeaks.m
channel={'G'};
%filter={{'LoG','LoG3','LoGxy','LoGxy232','LoGxy1533','LoGxy233'}};
filter={{'LoG','LoGxy'}};
%Fitfunction
if strcmp(fitshape,'G')
    Ff = @(x,A,mu,sigma) (ones(size(x))*A).*exp(-(x*ones(size(A))-ones(size(x))*mu).^2./(ones(size(x))*sigma.^2)./2);
elseif strcmp(fitshape,'L')
    Ff = @(x,A,mu,sigma) (ones(size(x))*A)./((x*ones(size(A))-ones(size(x))*mu).^2./(ones(size(x))*sigma.^2)+1);
end

for ch=1:numel(channel)
    for fi=1:numel(filter{ch})
        Dat.(channel{ch}).(filter{ch}{fi}).ax_id=[];     %(1 x btn) This is identifies section and axon
        Dat.(channel{ch}).(filter{ch}{fi}).btn_id=[];    %(1 x btn)This is bouton lbl assigned during registration
        Dat.(channel{ch}).(filter{ch}{fi}).flag=[];      %(time x btn) These are assigned during registration
        Dat.(channel{ch}).(filter{ch}{fi}).ind=[];       %(time x btn) Index along corresponding profile
        Dat.(channel{ch}).(filter{ch}{fi}).d=[];         %(1 x btn) 1d position along the aligned profile
        Dat.(channel{ch}).(filter{ch}{fi}).dorig=[];     %(time x btn) 1d position along the original profile
        Dat.(channel{ch}).(filter{ch}{fi}).blen=[];      %(time x btn) length associated with bouton
        Dat.(channel{ch}).(filter{ch}{fi}).rx=[];        %(time x btn) 3d x-position of peak in each time point
        Dat.(channel{ch}).(filter{ch}{fi}).ry=[];        %(time x btn) 3d y-position of peak in each time point
        Dat.(channel{ch}).(filter{ch}{fi}).rz=[];        %(time x btn) 3d z-position of peak in each time point
        
        Dat.(channel{ch}).(filter{ch}{fi}).Iraw=[];      %Non-normalized intensity at location on profile.
        Dat.(channel{ch}).(filter{ch}{fi}).Inorm=[];     %Intensity at location on profile. Inorm is roughly Ifg+Ibg
        Dat.(channel{ch}).(filter{ch}{fi}).Ibg=[];       %Intensity of background at given foreground peak location
        Dat.(channel{ch}).(filter{ch}{fi}).sig=[];       %Peak width for every detected peak. nan otherwise
        Dat.(channel{ch}).(filter{ch}{fi}).amp=[];       %Intensity of fitted foreground peak. nan otherwise
        
        for se=1:numel(An.Section)
            for ax=1:numel(An.Section{se}.Time{1}.Axon)
                
                %1. Initializing matrices
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                max_nbtns=numel(An.Section{se}.Time{1}.Axon{ax}.fit.(channel{ch}).(filter{ch}{fi}).fg.ind)*10;
                n_times=numel(An.Section{se}.Time);
                
                AxonMat.ax_id=ones(1,max_nbtns).*...
                    ((10^3*str2double(An.Section{se}.Time{1}.Axon{ax}.id(9)))+...
                    str2double(An.Section{se}.Time{1}.Axon{ax}.id(end-2:end)));
                
                AxonMat.btn_id=nan(1,max_nbtns);
                AxonMat.flag=nan(n_times,max_nbtns);
                AxonMat.ind=nan(n_times,max_nbtns);
                AxonMat.d=nan(1,max_nbtns);
                AxonMat.dorig=nan(n_times,max_nbtns);
                AxonMat.blen=nan(n_times,max_nbtns);
                AxonMat.rx=nan(n_times,max_nbtns);
                AxonMat.ry=nan(n_times,max_nbtns);
                AxonMat.rz=nan(n_times,max_nbtns);
                
                AxonMat.Iraw=nan(n_times,max_nbtns);
                AxonMat.Inorm=nan(n_times,max_nbtns);
                AxonMat.Ibg=nan(n_times,max_nbtns);
                AxonMat.sig=nan(n_times,max_nbtns);
                AxonMat.amp=nan(n_times,max_nbtns);
                
                %2. Populating matrices
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                dmin=-inf;dmax=inf;
                Ibg=cell(numel(An.Section{se}.Time),1);
                for ti=1:numel(An.Section{se}.Time)
                    Axon=An.Section{se}.Time{ti}.Axon{ax};
                    
                    dmin=max([(Axon.fit.(channel{ch}).(filter{ch}{fi}).d.man(1)),dmin]);
                    dmax=min([(Axon.fit.(channel{ch}).(filter{ch}{fi}).d.man(end)),dmax]);
                    nanind=isnan(Axon.fit.(channel{ch}).(filter{ch}{fi}).fg.manid);
                    Axon.fit.(channel{ch}).(filter{ch}{fi}).fg.manid(nanind)=Axon.fit.(channel{ch}).(filter{ch}{fi}).fg.id(nanind);
                    
                    [~,matind]=ismember(Axon.fit.(channel{ch}).(filter{ch}{fi}).fg.manid,AxonMat.btn_id);
                    startind=find(isnan(AxonMat.btn_id),1,'first');
                    endind=startind+sum(matind==0)-1;
                    matind(matind==0)=startind:endind;
                    
                    AxonMat.btn_id(1,matind)=Axon.fit.(channel{ch}).(filter{ch}{fi}).fg.manid;
                    AxonMat.flag(ti,matind)=Axon.fit.(channel{ch}).(filter{ch}{fi}).fg.flag;
                    AxonMat.ind(ti,matind)=Axon.fit.(channel{ch}).(filter{ch}{fi}).fg.ind;
                    AxonMat.d(1,matind)=Axon.fit.(channel{ch}).(filter{ch}{fi}).d.man(Axon.fit.(channel{ch}).(filter{ch}{fi}).fg.ind);
                    AxonMat.dorig(ti,matind)=Axon.d.optim(Axon.fit.(channel{ch}).(filter{ch}{fi}).fg.ind);
                    
                    %Extra peaks are assumed as removed from dataset (editpeaks).
                    temp=diff([dmin,AxonMat.d(1,matind),dmax]);
                    blen=(temp(1:end-1)+temp(2:end))./2;
                    blen(1)=blen(1)+temp(1)/2;
                    blen(end)=blen(end)+temp(end)/2;
                    AxonMat.blen(ti,matind)=blen;
                    
                    AxonMat.rx(ti,matind)=Axon.r.optim(Axon.fit.(channel{ch}).(filter{ch}{fi}).fg.ind,1);
                    AxonMat.ry(ti,matind)=Axon.r.optim(Axon.fit.(channel{ch}).(filter{ch}{fi}).fg.ind,2);
                    AxonMat.rz(ti,matind)=Axon.r.optim(Axon.fit.(channel{ch}).(filter{ch}{fi}).fg.ind,3);
                    
                    AxonMat.Iraw(ti,matind)=Axon.I.(channel{ch}).(filter{ch}{fi}).raw(Axon.fit.(channel{ch}).(filter{ch}{fi}).fg.ind);
                    AxonMat.Inorm(ti,matind)=Axon.I.(channel{ch}).(filter{ch}{fi}).norm(Axon.fit.(channel{ch}).(filter{ch}{fi}).fg.ind);
                    AxonMat.amp(ti,matind)=Axon.fit.(channel{ch}).(filter{ch}{fi}).fg.amp;
                    AxonMat.sig(ti,matind)=Axon.fit.(channel{ch}).(filter{ch}{fi}).fg.sig;
                    
                    Ibg{ti}=zeros(size(Axon.d.optim));
                    for p=1:numel(Axon.fit.(channel{ch}).(filter{ch}{fi}).bg.ind)
                        Ibg{ti}=Ibg{ti}+Ff(Axon.d.optim,Axon.fit.(channel{ch}).(filter{ch}{fi}).bg.amp(p),Axon.fit.(channel{ch}).(filter{ch}{fi}).bg.mu(p),Axon.fit.(channel{ch}).(filter{ch}{fi}).bg.sig(p));
                    end
                    AxonMat.Ibg(ti,matind)=Ibg{ti}(Axon.fit.(channel{ch}).(filter{ch}{fi}).fg.ind);
                end
                
                %3. Removing boutons based on flag & overlap
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                fldnm=fieldnames(AxonMat);
                
                %Remove extra columns from initialization
                remind=find(sum(isnan(AxonMat.ind),1)==size(AxonMat.ind,1));
                remind=unique(remind);
                for f=1:numel(fldnm)
                    AxonMat.(fldnm{f})(:,remind)=[];
                end
                
                %Assign distances to each peak before removing based on
                %flags or non-overlapping axon region.
                %Convention for flags:
                % Flag 0. Reset flag to nan
                % Flag 1. Peak belongs to ignore region
                % Flag 2. Cross-over/ location is ignored elsewhere
                % Flag 3. Terminal bouton
                % Flag 4. Spurious peak
                % Flag 5. No match found - keep this
                temp=AxonMat.flag;
                temp(isnan(temp))=0;%because nan~=0 = true;
                temp=temp~=0 & temp~=5;
                [~,remind1]=find(temp);
                
                remind2=find(AxonMat.d<dmin | AxonMat.d>dmax);
                
                remind=unique([remind1(:);remind2(:)]);
                for f=1:numel(fldnm)
                    AxonMat.(fldnm{f})(:,remind)=[];
                end
                
                %Sortind boutons in every axon by distance
                [~,sortind]=sort(AxonMat.d);
                for f=1:numel(fldnm)
                    AxonMat.(fldnm{f})=AxonMat.(fldnm{f})(:,sortind);
                end
                
                %4. Replace nans with corresponding value on profile
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                for ti=1:numel(An.Section{se}.Time)
                    Axon=An.Section{se}.Time{ti}.Axon{ax};
                    
                    nanind=find(isnan(AxonMat.ind(ti,:)));
                    nanind=nanind(:)';
                    
                    [~,minind]=min(abs(bsxfun(@minus,AxonMat.d(nanind),Axon.fit.(channel{ch}).(filter{ch}{fi}).d.man(:))),[],1);
                    AxonMat.ind(ti,nanind)=minind;
                    
                    AxonMat.dorig(ti,nanind)=Axon.d.optim(minind);
                    AxonMat.rx(ti,nanind)=Axon.r.optim(minind,1);
                    AxonMat.ry(ti,nanind)=Axon.r.optim(minind,2);
                    AxonMat.rz(ti,nanind)=Axon.r.optim(minind,3);
                    AxonMat.Iraw(ti,nanind)=Axon.I.(channel{ch}).(filter{ch}{fi}).raw(minind);
                    AxonMat.Inorm(ti,nanind)=Axon.I.(channel{ch}).(filter{ch}{fi}).norm(minind);
                    AxonMat.Ibg(ti,nanind)=Ibg{ti}(minind);
                end
                
                %Append axons to Dat
                for f=1:numel(fldnm)
                    Dat.(channel{ch}).(filter{ch}{fi}).(fldnm{f})=cat(2,Dat.(channel{ch}).(filter{ch}{fi}).(fldnm{f}),AxonMat.(fldnm{f}));
                end
                
                %Related to normalization
                Temp=cell(numel(An.Section{se}.Time),1);
                for ti=1:numel(An.Section{se}.Time)
                    Temp{ti}=An.Section{se}.Time{ti}.Axon{ax}.norm.(filter{ch}{fi});
                    Temp{ti}.ax_id=AxonMat.ax_id(1);
                end
                if ~isfield(Dat.(channel{ch}).(filter{ch}{fi}),'norm')
                    Dat.(channel{ch}).(filter{ch}{fi}).norm=Temp;
                else
                    Dat.(channel{ch}).(filter{ch}{fi}).norm=cat(2,Dat.(channel{ch}).(filter{ch}{fi}).norm,Temp);
                    clear Temp;
                end
            end
        end
    end
    
    if isfield(An,'L')
        Dat.L=An.L;
    end
end

