function [] = gui_editpeaks_actions(~, eventdata, ~)

%eventdata.Source
[~,f]=gcbo;
dat=guidata(f);
keypressval=eventdata.Key;
cc=lines(7);
switch keypressval;
    case 'a' %Add peak
        display('Adding peak to list');
        t=dat.currentpoint(1);
        fg_ind=dat.currentpoint(2);
        %Add only if not already present
        if sum(dat.Time{t}.fg_ind==fg_ind)==0
            dat.Time{t}.fg_ind(end+1)=fg_ind;
            dat.Time{t}.fg_id(end+1)=max(dat.Time{t}.fg_id)+1;
            dat.Time{t}.fg_manid(end+1)=nan;
            dat.Time{t}.fg_autoid(end+1)=nan;
            dat.Time{t}.fg_flag(end+1)=0;
        end
    case 'r' %Remove peak
        display('Removing peak from list');
        t=dat.currentpoint(1);
        fg_ind=dat.currentpoint(2);
        
        rem=dat.Time{t}.fg_ind==fg_ind;
        dat.Time{t}.fg_ind(rem)=[];
        dat.Time{t}.fg_id(rem)=[];
        dat.Time{t}.fg_manid(rem)=[];
        dat.Time{t}.fg_autoid(rem)=[];
        dat.Time{t}.fg_flag(rem)=[];
        
    case 's' %Save data (2step)
        display('Writing data')
        dat=guidata(f);
        conf=input('Do this only if all the timepoints that were matched are loaded!!!\n This overwrites fg_id, man_id and auto_id and can destroy pre-existing matches. Press y to save, any other key to continue','s');
        
        current_fgid=[];
        relbl_fgid=[];
        match_id=[];
        if strcmp(conf,'y')
            for ti=1:length(dat.Time)
                current_fgid=cat(1,current_fgid,dat.Time{ti}.fg_id(:));
                match_id=cat(1,match_id,dat.Time{ti}.fg_manid(:));
                
                [~,si]=sort(dat.Time{ti}.fg_ind(:));%Index, not id
                temp=nan(size(dat.Time{ti}.fg_id(:)));
                temp(si)=min(dat.Time{ti}.fg_id)+(1:numel(dat.Time{ti}.fg_id(:)))-1;
                relbl_fgid=cat(1,relbl_fgid,temp(:));
            end
            [~,new_match_id] = gui_editpeaks_lblAM (relbl_fgid,match_id);
            
            for ti=1:length(dat.Time)
                display(dat.Time{ti}.save_pth);
                orig=load(dat.Time{ti}.save_pth);
                
                [lia,~]=ismember(current_fgid,dat.Time{ti}.fg_id);
                in_ti=find(lia);
                
                mu=nan(size(in_ti));
                amp=nan(size(in_ti));
                sig=nan(size(in_ti));
                flg=nan(size(in_ti));
                fgid=relbl_fgid(in_ti);
                old_fgid=current_fgid(in_ti);
                manid=new_match_id(in_ti);
                autoid=nan(size(in_ti));
                ind=dat.Time{ti}.fg_ind(:);
                for inlist=1:numel(fgid)
                    inorig=find(orig.fit.(dat.channel).(dat.filter).fg.id==old_fgid(inlist));
                    if ~isempty(inorig)
                        mu(inlist)=orig.fit.(dat.channel).(dat.filter).fg.mu(inorig);
                        amp(inlist)=orig.fit.(dat.channel).(dat.filter).fg.amp(inorig);
                        sig(inlist)=orig.fit.(dat.channel).(dat.filter).fg.sig(inorig);
                        flg(inlist)=orig.fit.(dat.channel).(dat.filter).fg.flag(inorig);
                    end
                end
                
                orig.fit.(dat.channel).(dat.filter).fg.ind=ind;
                orig.fit.(dat.channel).(dat.filter).fg.mu=mu;
                orig.fit.(dat.channel).(dat.filter).fg.sig=sig;
                orig.fit.(dat.channel).(dat.filter).fg.amp=amp;
                orig.fit.(dat.channel).(dat.filter).fg.id=fgid;
                orig.fit.(dat.channel).(dat.filter).fg.manid=manid;
                orig.fit.(dat.channel).(dat.filter).fg.autoid=autoid;
                orig.fit.(dat.channel).(dat.filter).fg.flag=flg;
                
                [~,si]=sort(orig.fit.(dat.channel).(dat.filter).fg.ind);
                fn=fieldnames(orig.fit.(dat.channel).(dat.filter).fg);
                for f=1:numel(fn)
                    orig.fit.(dat.channel).(dat.filter).fg.(fn{f})=orig.fit.(dat.channel).(dat.filter).fg.(fn{f})(si);
                end
                %
                [orig.fit.(dat.channel).(dat.filter).fg,...
                    orig.fit.(dat.channel).(dat.filter).bg]=...
                    adjustpeaks(orig.I.(dat.channel).(dat.filter).norm,...
                    orig.d.optim,...
                    orig.fit.(dat.channel).(dat.filter).fg,...
                    orig.fit.(dat.channel).(dat.filter).bg);
                %}
                save(dat.Time{ti}.save_pth,'-struct','orig');
            end
        end
        
    case 'v' %Show/hide profile data
        h_elements=findobj(f,'-regexp','Tag','(Node*)|(Trace*)');
        
        if ~isempty(h_elements)
            stateval=h_elements(1).Visible;
            if strcmp(stateval,'on')
                stateval='off';
            else
                stateval='on';
            end
            for i=1:numel(h_elements)
                h_elements(i).Visible=stateval;
            end
        end
    case 'hyphen' %Control contrast
        f.Children.CLim(2)=f.Children.CLim(2)+0.5;
    case 'equal' %Control contrast
        f.Children.CLim(2)=f.Children.CLim(2)-0.5;
    case 'rightarrow'
        dat.currentpoint(2)=min(dat.currentpoint(2)+1,size(dat.Time{dat.currentpoint(1)}.r,1));
        delete(findobj(f,'Tag','Active'))
    case 'leftarrow'
        dat.currentpoint(2)=max(dat.currentpoint(2)-1,1);
        delete(findobj(f,'Tag','Active'))
    case 'uparrow'
        dat.currentpoint(1)=min(dat.currentpoint(1)+1,numel(dat.Time));
        dat.currentpoint(2)=min(dat.currentpoint(2),size(dat.Time{dat.currentpoint(1)}.r,1));
    case 'downarrow'
        dat.currentpoint(1)=max(dat.currentpoint(1)-1,1);
        dat.currentpoint(2)=min(dat.currentpoint(2),size(dat.Time{dat.currentpoint(1)}.r,1));
    otherwise
        display('Not a valid action key')
end

if regexp(keypressval,'\w*(?=arrow)','start')
    delete(findobj(f,'Tag','Active'))
    plot(dat.Time{dat.currentpoint(1)}.r(dat.currentpoint(2),2),dat.Time{dat.currentpoint(1)}.r(dat.currentpoint(2),1),...
        'o','MarkerSize',15,'MarkerEdgeColor',[0 0.8 0.3],'MarkerFaceColor','none','LineWidth',1.5,'Tag','Active');
    guidata(f,dat);
    
    fp=findobj('Tag','ProfilePlot');
    delete(findobj(fp,'Tag','ProfileNode'));
    figure(fp)
    xlim([dat.Time{dat.currentpoint(1)}.dman(dat.currentpoint(2),1)-30,dat.Time{dat.currentpoint(1)}.dman(dat.currentpoint(2),1)+30])
    ylim([dat.Time{dat.currentpoint(1)}.offset-15,dat.Time{dat.currentpoint(1)}.offset+30])
    plot(dat.Time{dat.currentpoint(1)}.dman(dat.currentpoint(2),1),...
        dat.Time{dat.currentpoint(1)}.I.norm(dat.currentpoint(2))+dat.Time{dat.currentpoint(1)}.offset,...
        'o','MarkerSize',15,'MarkerEdgeColor',[0 0.8 0.3],'MarkerFaceColor','none','LineWidth',1.5,'Tag','ProfileNode');drawnow;
    figure(f);
end

if strcmp(keypressval,'a') || strcmp(keypressval,'r')
    delete(findobj(f,'Tag','Node'))
    h_nodes = gobjects(numel(dat.Time),1);
    for ti=1:numel(dat.Time)
        h_nodes(ti)=plot3(dat.Time{ti}.r(dat.Time{ti}.fg_ind,2),dat.Time{ti}.r(dat.Time{ti}.fg_ind,1),2*ones(numel(dat.Time{ti}.fg_ind),1),...
            '^','MarkerSize',15,'Color',cc(mod(ti-1,7)+1,:),'LineWidth',1.5,...
            'Tag','Node','UserData',struct('t',ti),'ButtonDownFcn',@gui_editpeaks_selind);
    end
    guidata(f,dat);
end