function [AM,matchidnew] = gui_editpeaks_lblAM (origid,matchid)
%This function takes a list of unique bouton id and matched bouton id
%to create an adjacency matrix. matchid is updated to reflect AM. This will
%be used to ensure consistency after changing bouton labels.

AM=zeros(size(origid,1));
matchidnew=nan(size(origid,1),1);
mid=unique(matchid(~isnan(matchid)));
for m=1:numel(mid)
    conid=sort(origid(matchid==mid(m)));
    matchidnew(matchid==mid(m))=conid(1);
    [~,AMind] = ismember(conid,origid);
    for i=1:(numel(AMind)-1)
        AM(AMind(i),AMind(i+1))=conid(1);
        AM(AMind(i+1),AMind(i))=conid(1);
    end
end
