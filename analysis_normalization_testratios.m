%This code tests ratio of LoGxy to Gauss2 intensity, to check if the
%relationship is the same for a range of bouton weights. We hope that
%normalization of the LoGxy profile by the Gauss2 profile mean value removes
%the dependence on density of boutons - this would be true if the ratio of
%LoGxy to Gauss2 intensity was similar over a range of bouton values. In
%reality the ratio depends on bouton intensity, and we cannot completely
%eliminate effect of density with normalization. There is no way around the
%problem and we continue with this since the heuristic approach gives good
%results with EM. The approach can be improved upon if more EM data is
%available.

axon=[1,2,3,4,7,8,9,10,11,12,13,14,17,18,19,20];
ftype=('a*x+b');
cutoff=10;
cc=lines(numel(axon));
parentdir=pwd;
parentdir=parentdir(1:(strfind(parentdir,'Plasticity')+numel('Plasticity')-1));

for ax=1:numel(axon)
    axonstr=[sprintf('A%03d',axon(ax))];
    A=load(isunixispc([parentdir,'\dat\Controls\Conditions\Profiles_v3\DL001A002\',axonstr,'.mat']));
    
    figure(3),hold on,
    scatter(A.I.G.Gauss2.norm,A.I.G.LoGxy.norm,10),axis square,box on;
    
    display(sum(A.I.G.LoGxy.norm>1)./numel(A.I.G.LoGxy.norm));
    
    ind=A.I.G.LoGxy.norm>1;
    xx=A.I.G.Gauss2.norm(ind);
    yy=A.I.G.LoGxy.norm(ind);
    [ff,gg]=fit(xx(:),yy(:),ftype,'StartPoint',[1,0]);
    plot([0 10],ff([0 10]),'-','Color',cc(ax,:))
    
    ind=A.I.G.LoGxy.norm<cutoff;
    xx=A.I.G.Gauss2.norm(ind);
    yy=A.I.G.LoGxy.norm(ind);
    [ff,gg]=fit(xx(:),yy(:),ftype,'StartPoint',[1,0]);
    plot([0 5],ff([0 5]),'--','Color',cc(ax,:))
    xlim([0 10]),ylim([0 10]);
    xlabel('Gauss'),ylabel('LoGxy')
    set(gca,'FontName','Calibri','FontSize',13);
    drawnow
end


for ax=1:numel(axon)
    axonstr=[sprintf('A%03d',axon(ax))];
    A=load(isunixispc([parentdir,'\dat\Controls\Conditions\Profiles_v3\DL001A002\',axonstr,'.mat']));
    
    figure(5),hold on,
    %scatter(A.I.G.Gauss2.norm,A.I.G.LoGxy.norm,10,cc(ax,:)),axis square,box on;
    display(sum(A.I.G.LoGxy.norm>cutoff)./numel(A.I.G.LoGxy.norm));
    
    xx=A.I.G.Gauss2.norm(A.fit.G.LoGxy.fg.ind);
    yy=A.I.G.LoGxy.norm(A.fit.G.LoGxy.fg.ind);
    scatter(xx,yy,10,cc(ax,:)),axis square,box on;
    [ff,gg]=fit(xx(:),yy(:),ftype,'StartPoint',[1,0]);
    plot([0 10],ff([0 10]),'-','Color',cc(ax,:))
    
    xlim([0 10]),ylim([0 10]);
    xlabel('Gauss'),ylabel('LoGxy')
    set(gca,'FontName','Calibri','FontSize',13);
    drawnow
end