%This code displays profiles and shows a table of intensity values for all
%filters used to compare with EM data.

function []=analysis_filters()

pathlist;
Ff = @(x,A,mu,sigma) (ones(size(x))*A).*exp(-(x*ones(size(A))-ones(size(x))*mu).^2./(ones(size(x))*sigma.^2)./2);
mind=[27.5,28.0,21.0,0];
maxd=[47.0,40.0,36.0,15];
%filter={'LoG','LoG3','median333'};
filter={'LoGxy'};
cc=lines(numel(filter));
offset=5;
pos={'northwest','northeast','southwest','southeast'};
channel='G';
for ax=1:4
    A=load(isunixispc([profile_pth,sprintf('DL100I005\\A%.3d.mat',ax)]));
    dind=A.d.optim>mind(ax) & A.d.optim<maxd(ax);
    
    figure(ax+2300),clf(ax+2300),movegui(ax+2300,pos{ax})
    title(sprintf('A%.3d.mat',ax));

    for f=1:numel(filter)
        tot_bg=zeros(size(A.d.optim));
        for i=1:numel(A.fit.(channel).(filter{f}).bg.ind)
            temp=A.fit.(channel).(filter{f}).bg;
            tot_bg=tot_bg+Ff(A.d.optim,temp.amp(i),temp.mu(i),temp.sig(i));
        end
        
        plot(A.d.optim(dind),A.I.(channel).(filter{f}).norm(dind)+offset*(f-1),'-','Color',cc(f,:)),hold on
        plot(A.d.optim(dind),tot_bg(dind)+offset*(f-1),'-','Color',cc(f,:)),hold on
        set(gca,'YTick',[0:1:45]);
        grid on;
        id=find(A.d.optim(A.fit.(channel).(filter{f}).fg.ind)>mind(ax) & A.d.optim(A.fit.(channel).(filter{f}).fg.ind)<maxd(ax));
        for b=1:numel(id)
            text(A.d.optim(A.fit.(channel).(filter{f}).fg.ind(id(b))),A.I.(channel).(filter{f}).norm(A.fit.(channel).(filter{f}).fg.ind(id(b)))+offset*(f-1)+1,...
                sprintf('%.2f',A.fit.(channel).(filter{f}).fg.amp(id(b))),'Color',cc(f,:),'HorizontalAlignment','center');
            text(A.d.optim(A.fit.(channel).(filter{f}).fg.ind(id(b))),A.I.(channel).(filter{f}).norm(A.fit.(channel).(filter{f}).fg.ind(id(b)))+offset*(f-1)+1.7,...
                sprintf('%.2f',tot_bg(A.fit.(channel).(filter{f}).fg.ind(id(b)))),'Color',cc(f,:),'HorizontalAlignment','center');
            
            %             text(A.d.optim(A.fit.(channel).(filter{f}).fg.ind(id(b))),A.I.(channel).(filter{f}).norm(A.fit.(channel).(filter{f}).fg.ind(id(b)))+offset*(f-1)+2,...
            %                 sprintf('%.2f',A.I.(channel).(filter{f}).norm(A.fit.(channel).(filter{f}).fg.ind(id(b)))),'Color',[0.2 0.2 0.2],'HorizontalAlignment','center');
            %
            %             text(A.d.optim(A.fit.(channel).(filter{f}).fg.ind(id(b))),A.I.(channel).(filter{f}).norm(A.fit.(channel).(filter{f}).fg.ind(id(b)))+offset*(f-1)+3,...
            %                 sprintf('%.2f',A.fit.(channel).(filter{f}).fg.sig(id(b))),'Color',cc(f,:),'HorizontalAlignment','center');
            
            %             fprintf('Axon %d\n',ax);
            %             display([A.fit.(channel).LoG.fg.amp(id),A.fit.(channel).LoG3.fg.amp(id),A.fit.(channel).LoG5.fg.amp(id),A.fit.(channel).median333.fg.amp(id),A.fit.(channel).median555.fg.amp(id),id(:)])
        end
    end
    legend(filter,'Location','northeastoutside');
    drawnow;
end
