%Script used to plot fitted peaks in various profiles of the same axon.
%This was used to identify possible causes of variability in inter-user
%optimized traces
pathlist;

%Options
channel='G';
filter='LoGxy';

%Profiles to create
animal={'DL001'};
timepoint={'P','Q','R','S','T'};
section={'002'};
axon={[15]};
setim='DL001I002';%For manual vs optimized trace comparisons

for an=1:numel(animal)
    for se=1:numel(section)
        for ax=1:numel(axon{se})
            for ti=1:numel(timepoint)
                stackid=[animal{an},timepoint{ti},section{se}];
                axonstr=[sprintf('A%03d',axon{se}(ax))];
                Dat=load(isunixispc([profile_pth,stackid,'/',axonstr,'.mat']));
                if ti==1
                    inall=Dat.fit.(channel).(filter).fg.manid;
                else
                    inall=intersect(inall,Dat.fit.(channel).(filter).fg.manid);
                    numel(inall)
                end
            end
            
            
            for ti=1:numel(timepoint)
                stackid=[animal{an},timepoint{ti},section{se}];
                axonstr=[sprintf('A%03d',axon{se}(ax))];
                Dat=load(isunixispc([profile_pth,stackid,'/',axonstr,'.mat']));
                
                [~,inthis,~]=intersect(Dat.fit.(channel).(filter).fg.manid,inall);
                xoffset=mean(Dat.d.optim(Dat.fit.(channel).(filter).fg.ind(inthis)));
                cc=lines(numel(timepoint));
                stackid=[animal{an},timepoint{ti},section{se}];
                axonstr=[sprintf('A%03d',axon{se}(ax))];
                Dat=load(isunixispc([profile_pth,stackid,'/',axonstr,'.mat']));
                bline=0*ti;
                figure(ax+20)
                plot(Dat.d.optim-xoffset,Dat.I.(channel).(filter).norm+bline,'-','Color',cc(ti,:)),hold on
                Ff = @(x,A,mu,sigma) (ones(size(x))*A).*exp(-(x*ones(size(A))-ones(size(x))*mu).^2./(ones(size(x))*sigma.^2)./2);
                for i=1:numel(Dat.fit.(channel).(filter).fg.ind)
                    temp=Dat.fit.(channel).(filter).fg;
                    xx=(-5:0.1:+5)+temp.mu(i);
                    plot(xx-xoffset,Ff(xx,temp.amp(i),temp.mu(i),temp.sig(i))+bline,'-','Color',cc(ti,:)),hold on;
                end
                
                for i=1:numel(Dat.fit.(channel).(filter).bg.ind)
                    temp=Dat.fit.(channel).(filter).bg;
                    xx=(-5:0.1:+5)+temp.mu(i);
                    plot(xx-xoffset,Ff(xx,temp.amp(i),temp.mu(i),temp.sig(i))+bline,'-','Color',cc(ti,:)),hold on;
                end
                title(filter)
            end
        end
    end
end

