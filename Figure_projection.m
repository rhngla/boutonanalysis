%Figure-Projection
parentdir=pwd;
parentdir=parentdir(1:(strfind(parentdir,'Plasticity')+numel('Plasticity')-1));

P{1}=load(isunixispc([parentdir,'\dat\EMData\Projections\DL100I005-A001-xy.mat']));
P{2}=load(isunixispc([parentdir,'\dat\EMData\Projections\DL100I005-A002-xy.mat']));
P{3}=load(isunixispc([parentdir,'\dat\EMData\Projections\DL100I005-A003-xy.mat']));
P{4}=load(isunixispc([parentdir,'\dat\EMData\Projections\DL100I005-A004-xy.mat']));
P{5}=load(isunixispc([parentdir,'\dat\EMData\Projections\DL100I005-A006-xy.mat']));

IM=zeros(1024,1024);
for i=[3]
IM=max(IM,P{i}.IM(:,:,1));
end
figure,imshow(IM),caxis([0 2000]);
xlim([363 428]);ylim([587 652]),drawnow;%size(image)=[65,65]
set(gca,'position',[0 0 1 1],'units','normalized')

clear;

isunixispc([parentdir,])
load(isunixispc([parentdir,'\dat\EMData\DatasetIM\DL100I005G.mat']),'IM')
A{1}=load(isunixispc([parentdir,'\dat\EMData\Profiles\DL100I005\A001.mat']));
A{2}=load(isunixispc([parentdir,'\dat\EMData\Profiles\DL100I005\A002.mat']));
A{3}=load(isunixispc([parentdir,'\dat\EMData\Profiles\DL100I005\A003.mat']));
A{4}=load(isunixispc([parentdir,'\dat\EMData\Profiles\DL100I005\A004.mat']));

figure,imshow(max(IM(:,:,end-65:end),[],3)),hold on;drawnow
%caxis([0 3000]);xlim([363-98 428+98]);ylim([587-33 652+33])
caxis([0 2000]);xlim([363-98 428+98]);ylim([587-98 652+98])
cc=lines(4);set(gca,'position',[0 0 1 1],'units','normalized')
for i=1:4
    [AM, r, ~] = AdjustPPM(A{i}.AM.optim,A{i}.r.optim,zeros(size(A{i}.r.optim(:,1))),0.5);
    col=cc(i,:);
    plotAM(AM,r,col)
end