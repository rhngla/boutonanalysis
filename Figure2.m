%This script recreates Figure 2 in Gala_2017.

parentdir=pwd;
parentdir=parentdir(1:(strfind(parentdir,'Plasticity')+numel('Plasticity')-1));

proj_pth=isunixispc([parentdir,'\dat\Projections_v2\']);
IM=load(isunixispc([proj_pth,'DL083B001-A099-xy.mat']));
IM=IM.IM;
IM=IM(:,:,1);%Select green channel
%set(gca,'Position',[0.1300 0.1100 0.7750 0.8150])
figure(50),imshow(IM),caxis([0 1000]),hold on
ylim([375 400]);
xlim([272,372]);
h=gca(50);

cc=parula(10);
A=load(isunixispc([parentdir,'\dat\Profiles_v2\DL083B001\A099.mat']));

figure(51),clf(51)
ind=find(A.r.optim(:,2)<h.XLim(2) & A.r.optim(:,2)>h.XLim(1) & A.r.optim(:,1)<h.YLim(2) & A.r.optim(:,1)>h.YLim(1));
doff=min(A.d.optim(ind));
plot((A.d.optim(ind)-doff),A.I.G.LoGxy.norm(ind),'-','Color',[0.5 0.5 0.5],'LineWidth',1),hold on;

figure(50)
plot(A.r.optim(ind,2),A.r.optim(ind,1),'-')

figure(51);
G = @(x,mu,sigma,A) (ones(size(x))*A).*exp(-(x*ones(size(A))-ones(size(x))*mu).^2./(ones(size(x))*sigma.^2)./2);

%Background
peakind=ismember(A.fit.G.LoGxy.bg.ind,ind);
peakind=find(peakind);
fitx=A.d.optim(ind);
bgI=zeros(size(fitx));
for p=1:length(peakind)
    bgI=bgI+G(fitx,A.fit.G.LoGxy.bg.mu(peakind(p)),A.fit.G.LoGxy.bg.sig(peakind(p)),A.fit.G.LoGxy.bg.amp(peakind(p)));
end
plot(A.d.optim(ind)-doff,bgI,'-','Color',cc(8,:),'LineWidth',1)

%Foreground
peakind=ismember(A.fit.G.LoGxy.fg.ind,ind);
peakind=find(peakind);
for p=1:length(peakind)
    inds=(max(1,A.fit.G.LoGxy.fg.ind(peakind(p))-10):1:min(numel(A.d.optim),A.fit.G.LoGxy.fg.ind(peakind(p))+10));
    fitx=A.d.optim(inds);
    plot(A.d.optim(inds)-doff,G(fitx,A.fit.G.LoGxy.fg.mu(peakind(p)),A.fit.G.LoGxy.fg.sig(peakind(p)),A.fit.G.LoGxy.fg.amp(peakind(p))),'-','Color',cc(5,:),'LineWidth',1);
end

%Total fit
Ifit=zeros(size(A.d.optim(ind)));
for p=1:length(A.fit.G.LoGxy.fg.mu)
    fitx=A.d.optim(ind);
    Ifit=Ifit+G(fitx,A.fit.G.LoGxy.fg.mu(p),A.fit.G.LoGxy.fg.sig(p),A.fit.G.LoGxy.fg.amp(p));
end
plot(A.d.optim(ind)-doff,Ifit+bgI,'-','Color',cc(3,:),'LineWidth',1)
xlim([0 28])
ylim([0 5])
drawnow

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figure(52),clf(52)
ind=find(A.r.optim(:,2)<h.XLim(2) & A.r.optim(:,2)>h.XLim(1) & A.r.optim(:,1)<h.YLim(2) & A.r.optim(:,1)>h.YLim(1));
doff=min(A.d.optim(ind));
plot((A.d.optim(ind)-doff),A.I.G.Gauss2.norm(ind),'-','Color',[0.5 0.5 0.5],'LineWidth',1),hold on;

figure(52),
G = @(x,mu,sigma,A) (ones(size(x))*A).*exp(-(x*ones(size(A))-ones(size(x))*mu).^2./(ones(size(x))*sigma.^2)./2);

%Background
peakind=ismember(A.fit.G.Gauss2.bg.ind,ind);
peakind=find(peakind);
fitx=A.d.optim(ind);
bgI=zeros(size(fitx));
for p=1:length(peakind)
    bgI=bgI+G(fitx,A.fit.G.Gauss2.bg.mu(peakind(p)),A.fit.G.Gauss2.bg.sig(peakind(p)),A.fit.G.Gauss2.bg.amp(peakind(p)));
end
plot(A.d.optim(ind)-doff,bgI,'-','Color',cc(8,:),'LineWidth',1)

%Foreground
peakind=ismember(A.fit.G.Gauss2.fg.ind,ind);
peakind=find(peakind);
for p=1:length(peakind)
    inds=(max(1,A.fit.G.Gauss2.fg.ind(peakind(p))-15):1:min(numel(A.d.optim),A.fit.G.Gauss2.fg.ind(peakind(p))+15));
    fitx=A.d.optim(inds);
    plot(A.d.optim(inds)-doff,G(fitx,A.fit.G.Gauss2.fg.mu(peakind(p)),A.fit.G.Gauss2.fg.sig(peakind(p)),A.fit.G.Gauss2.fg.amp(peakind(p))),'-','Color',cc(5,:),'LineWidth',1);
end

%Total fit
Ifit=zeros(size(A.d.optim(ind)));
for p=1:length(A.fit.G.Gauss2.fg.mu)
    fitx=A.d.optim(ind);
    Ifit=Ifit+G(fitx,A.fit.G.Gauss2.fg.mu(p),A.fit.G.Gauss2.fg.sig(p),A.fit.G.Gauss2.fg.amp(p));
end
plot(A.d.optim(ind)-doff,Ifit+bgI,'-','Color',cc(3,:),'LineWidth',1)
xlim([0 28])
ylim([0 5])