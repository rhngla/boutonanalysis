function L = analysis_loadlearn(anid,timepoint,fitind)
%GetLearn_v2 - Pull behavior and imaging time data from header files. Uses
%hard coded values if loaded_let and fitind are not specified. Function
%is meant for use by LoadDat, or to only check full Learning data directly.
pathlist;
animal=sprintf('DL%03d',anid);
if anid==83
    if isempty(timepoint)
        timepoint=cellstr(char(double('B'):double('N'))');
        fitind=13:48;
    end
elseif anid==85
    if isempty(timepoint)
        timepoint=cellstr(char(double('C'):double('T'))');
        fitind=9:72;
    end
elseif anid==101
    if isempty(timepoint)
        timepoint=cellstr(char(double('B'):double('S'))');
        fitind=15:80;
    end
elseif anid==102
    if isempty(timepoint)
        timepoint=cellstr(char(double('B'):double('O'))');
        fitind=21:79;%Inflection point changes if starting from 23;
    end
elseif anid==88
    if isempty(timepoint)
        timepoint=cellstr(char(double('B'):double('Q'))');
        fitind=8:54;
    end
elseif anid==108
    if isempty(timepoint)
        timepoint=cellstr(char(double('B'):double('P'))');
        fitind=8:54;
    end
end
dbeh=load(isunixispc([behfold,animal,'.mat']));
H=load(isunixispc([headerf,animal,'h.mat']));
sfn=numel(H.fname);
dim=nan(numel(timepoint),1);

letterlist=cell(sfn,1);
for i=1:sfn
    letterlist{i}=H.fname{i}(13);
end

for s = 1:numel(timepoint)
    list_ind=find(cellfun(@(x) (strcmp(x,timepoint(s))),letterlist));
    if ~isempty(list_ind)
        dat = H.t2p(list_ind,1).internal.triggerTimeString;
        form = 'mm/dd/yyyy HH:MM:SS.FFF';
        dim(s) = datenum(dat,form);
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Fit here. Fit performed on absolute time.
s=fittype('a+(b)/(1+exp((mu-x)/t))');
L.t=dbeh.expdate.num;
L.measure=dbeh.percentCorrect/100;
L.stimamp=dbeh.stimAmp.mW;
[fit_f,~,~]=fit(L.t(fitind),L.measure(fitind),s,'StartPoint',[0.5,0.5,mean(L.t(fitind)),4],'Lower',[0 0 0 0]);

%Merge with imaging data points here
all_t=[dbeh.expdate.num(:);dim(:)]; %times for merged imaging+learning data
all_Lfit=fit_f(all_t);
all_Lmeasure=[L.measure(:);nan(size(dim(:)))];
%all_im_ind=[zeros(size(dbeh.expdate.num(:)));ones(size(dim(:)))];

[all_t,i]=sort(all_t);
all_Lfit=all_Lfit(i);
all_Lmeasure=all_Lmeasure(i);
%all_im_ind=all_im_ind(i);

im_t=dim(:);
im_Lfit=fit_f(im_t);
im_id=timepoint;

t_audio=dbeh.expdate.num(find(dbeh.phase==1,1,'first'));
t_stimaudio=dbeh.expdate.num(find(dbeh.phase==2,1,'first'));
t_stimonly=dbeh.expdate.num(find(dbeh.phase==3,1,'first'));
t_inflection=fsolve(@(x) (fit_f.a+(fit_f.b)/(1+exp((fit_f.mu-x)/fit_f.t)))-(fit_f.a+0.5*fit_f.b),mean(im_t));
t_startlearn=im_t(find(diff(im_Lfit)>0.01,1,'first'));
t_startlearn2=fit_f.mu-3*fit_f.t;
t_endlearn2=fit_f.mu+3*fit_f.t;

direc={'northwest','north','northeast','south','southeast'};
allan=[83,85,101,102,88,01];
anind=(allan==anid);
anc=lines(numel(allan));
anc=anc(anind,:);
direc=direc{anind};
figure,movegui(gcf,direc);hold on;
plot(all_t,all_Lfit,'--','Color',anc)
%plot(all_t,all_Lmeasure,'x','Color',anc);
plot(L.t(fitind),L.measure(fitind),'-','Color',[0.5 0.5 0.5]);
plot(im_t,im_Lfit,'.','MarkerSize',15,'Color',anc);
plot([t_audio,t_audio],[0 1],'-','Color',[0.8 0 0]);
plot([t_stimaudio,t_stimaudio],[0 1],'-','Color',[0.8 0.8 0]);
plot([t_stimonly,t_stimonly],[0 1],'-','Color',[0 0.7 0]);
plot([t_inflection,t_inflection],[0 1],'-','Color',[0.4 0.4 0.4]);
plot([t_startlearn2,t_startlearn2],[0 1],'--','Color',[0.4 0.4 0.4]);
plot([t_endlearn2,t_endlearn2],[0 1],'--','Color',[0.4 0.4 0.4]);

text(im_t,im_Lfit+0.05,im_id,'horizontalAlignment', 'center','FontSize',13)
text(im_t,im_Lfit-0.05,num2str((1:numel(im_id))'),'horizontalAlignment', 'center','FontSize',13)
text_x=(im_t(1:end-1)+im_t(2:end))./2;
text_y=fit_f(text_x)+0.02;
%text(text_x,text_y,num2str(round(diff(im_t),1)),'horizontalAlignment', 'center','FontSize',13,'Color',[0.5 0 0],'Rotation',45)
xlim([all_t(1)-1,all_t(end)+1]);ylim([0 1])
xlabel('Time')
ylabel('Performace index')
box on;
set(gca,'FontName','Calibri','FontSize',15);

L=[];
L.fit_f=fit_f;
L.im_t=im_t;
L.im_Lfit=im_Lfit;
L.im_id=im_id;
L.t_audio=t_audio;
L.t_stimaudio=t_stimaudio;
L.t_stimonly=t_stimonly;
L.t_inflection=t_inflection;
L.t_startlearn=t_startlearn;

end