function [A]=EM_crosssectionarea_v2()
%This function reads data saved from .obj files and returns cross section area of
%the axon or mitochondria as a function of position for a single axon. The
%save option is disabled. The saved data can be found in:
%\Plasticity\dat\EMData\CrossSectionEst\

for axid=1:4
    clearvars -except axid;
    
    parentdir=pwd;
    parentdir=parentdir(1:(strfind(parentdir,'Plasticity')+numel('Plasticity')-1));
    load(isunixispc([parentdir,'/dat/EMData/Blender2Mat/Obj2Txt/Axon',num2str(axid),'.mat']),'d','r','SVmito','SVnew','SVorig')

    Dat{1}=SVorig;%This field contains an older version of the EM reconstructions.
    
    Dat{2}=SVnew;%This field contains an newer version of the EM
    %reconstructions. Some axons had minor changes.
    
    Dat{3}=SVmito;%This contains the 3d vertices for mitochondrial
    %reconstructions.
    Areas=cell(size(Dat));
    for dd=1:numel(Dat)
        SV=Dat{dd};
        
        figure(11),clf(11),movegui(11,'north')
        plot3(r(:,2),r(:,1),r(:,3),'-','Color',lines(1)),hold on
        %plot3(SVmito(:,2),SVmito(:,1),SVmito(:,3),'.','Color',[0.7 0.7 0]),hold on
        plot3(SVorig(:,2),SVorig(:,1),SVorig(:,3),'.','Color',[0.7 0.7 0]),hold on
        axis equal; box on; drawnow;
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        thr1=1.8;  %Box dimensions
        thr2=0.02; %distance along tangent direction:
        thr3=1.8;  %distance inplane
        n_iter=20;%Iterations over starting points to create appropriate loop
        n_kmean_iter=20;%Number of kmeans iterations: Was 100
        loopnodes_dthr=0.3;%Max allowed dist. between successive nodes of the loop
        n_steps=5; %inter node index difference for calculation of tangent vector
        LMz=false;  %True if calculating cross section parallel to LM z direction
        Zemp =[0 0 1];
        startind=1+n_steps;%Default 1+n_steps;
        endind=size(r,1)-n_steps;%Default: size(r,1)-n_steps;
        isplot=true;
        nloops=3;
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        A=nan(size(r,1),n_kmean_iter);
        view(Zemp);
        
        nodeind=startind;
        while nodeind<=endind
            display(nodeind)
            tang_nodes=[nodeind-n_steps,nodeind+n_steps];
            
            t1=r(nodeind,:)-r(tang_nodes(1),:);
            t2=r(tang_nodes(2),:)-r(nodeind,:);
            tgt=(t1+t2)./norm(t1+t2);
            
            if LMz
                tgt=tgt-(tgt*Zemp')*Zemp;%Remove component along LM z-axis
            end
            
            inplane1=[tgt(2),-tgt(1),0];% => constructed to ensure dot product with tgt is 0
            inplane1=inplane1./norm(inplane1);
            inplane2= cross(inplane1,tgt);
            inplane2=inplane2./norm(inplane2);
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %Limit calculation to vertices within box around particular centerline
            %vertex
            box_ind =...
                SV(:,1)>(r(nodeind,1)-thr1) & SV(:,1)<(r(nodeind,1)+thr1) &...
                SV(:,2)>(r(nodeind,2)-thr1) & SV(:,2)<(r(nodeind,2)+thr1) &...
                SV(:,3)>(r(nodeind,3)-thr1) & SV(:,3)<(r(nodeind,3)+thr1);
            
            %Within box criteria
            box_ind=find(box_ind);
            SV_keep=SV(box_ind,:);
            
            SV_rel=bsxfun(@minus,SV(box_ind,:),r(nodeind,:));
            
            %Along tangent distance criteria
            keep=(abs(SV_rel*tgt')<thr2);
            
            box_ind=box_ind(keep);
            SV_rel=SV_rel(keep,:);
            SV_keep=SV_keep(keep,:);
            
            %Perpendicular to tangent distance criteria: 1
            alongtgt=bsxfun(@times,tgt,(SV_rel*tgt'));
            SV_inplane_dist=(sum((SV_rel-alongtgt).^2,2)).^0.5;
            keep=(SV_inplane_dist<thr3);
            
            box_ind=box_ind(keep);
            SV_rel=SV_rel(keep,:);
            SV_keep=SV_keep(keep,:);
            
            %Perpendicular to tangent distance criteria: 2
            %{
    alongtgt=bsxfun(@times,tgt,(SV_rel*tgt'));
    SV_inplane_dist=(sum((SV_rel-alongtgt).^2,2)).^0.5;
    keep=SV_inplane_dist<(mean(SV_inplane_dist)+2*std(SV_inplane_dist));

    box_ind=box_ind(keep);
    SV_rel=SV_rel(keep,:);
    SV_keep=SV_keep(keep,:);
            %}
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            if ~isempty(SV_keep)
                x0=SV_rel*inplane1';
                y0=SV_rel*inplane2';
                if numel(x0)>=150
                    k=75;
                elseif numel(x0)<=20
                    k=numel(x0);
                elseif numel(x0)>20 && numel(x0)<150
                    k=ceil(numel(x0)/2);
                end
                
                figure(11)
                h=plot3(SV(box_ind,2),SV(box_ind,1),SV(box_ind,3),'xr','MarkerSize',2);
                ylim([r(nodeind,1)-2, r(nodeind,1)+2])
                xlim([r(nodeind,2)-2, r(nodeind,2)+2])
                view([0 0 1])
                drawnow;
                %{
                %To graphically remove points from consideration
                figure(2),clf(2)%,movegui(2,'southeast')
                xlim(3*[-0.6 0.6]);ylim(3*[-0.6 0.6]);axis square;box on;hold on
                g1=plot(x0,y0,'.','Color',[0.5 0.5 0.5]);
                drawnow;
                %}
                
                %{
                %Create X using paintbrush
                [val,remindx0]=min(abs(bsxfun(@minus,x0(:),X(:,1)')),[],1);
                remindx0=remindx0(val<0.01);
                x0(remindx0)=[];
                y0(remindx0)=[];
    
                figure(2),clf(2),movegui(2,'southeast')
                xlim(3*[-0.6 0.6]);ylim(3*[-0.6 0.6]);axis square;box on;hold on
                g1=plot(x0,y0,'.','Color',[0.3 0.3 0.3]);
                %}
                
                for kmean_iter=1:n_kmean_iter
                    %Kmeans simplifies the number of surface points considered
                    %in calculating the cross section
                    [~,C] = kmeans([x0,y0],k);
                    x=C(:,1);y=C(:,2);
                    Dinit=(bsxfun(@minus,x,x').^2+(bsxfun(@minus,y,y').^2));
                    Dinit=Dinit+diag(nan*ones(size(Dinit,1),1)); %To avoid self connections
                    
                    %Consider biggest 3 loops
                    listofloops_Area=zeros(n_iter,nloops);
                    listofloops=cell(n_iter,nloops);
                    for ii=1:n_iter
                        %Use all to all distances in the kmeans points to find
                        %loops of various sizes
                        D=Dinit;
                        d_orig=(x.^2+y.^2);
                        foundloops=cell(1);
                        l=1;%loop number
                        
                        %Keep finding loops till less than 3 points remain:
                        while sum(nansum(D,1)>0)>2
                            temp=find(~isnan(d_orig));
                            foundloops{l}(1)=temp(randi(numel(temp)));
                            i=1;
                            [~,foundloops{l}(i+1)]=nanmin(D(foundloops{l}(i),:));
                            D(foundloops{l}(i+1),foundloops{l}(i))=nan;
                            i=i+1;
                            while foundloops{l}(i)~=foundloops{l}(1)
                                [~,foundloops{l}(i+1)]=nanmin(D(foundloops{l}(i),:));
                                D(:,foundloops{l}(i))=nan;
                                i=i+1;
                            end
                            d_orig(foundloops{l})=nan;
                            D(:,foundloops{l}(1))=nan;
                            l=l+1;
                        end
                        
                        %Delete loops compose of less than 5 points
                        for l=1:numel(foundloops)
                            if numel(foundloops{l})<5
                                foundloops{l}=[];
                            end
                        end
                        foundloops(cellfun('isempty',foundloops))=[];
                        
                        %For remaining loops calculate radius of gyration and
                        %center of mass. This is to remove loops with overlap
                        foundloops_Area=zeros(numel(foundloops),1);
                        foundloops_cm=zeros(numel(foundloops),2);
                        foundloops_rgyr=zeros(numel(foundloops),1);
                        for l=1:numel(foundloops)
                            tempA=0;
                            for i=1:numel(foundloops{l})-1
                                tempA=tempA+0.5.*(x(foundloops{l}(i))*y(foundloops{l}(i+1))-x(foundloops{l}(i+1))*y(foundloops{l}(i)));
                            end
                            foundloops_Area(l)=abs(tempA);
                            foundloops_cm(l,:)=[mean(x(foundloops{l})),mean(y(foundloops{l}))];
                            foundloops_rgyr(l)=(mean((x(foundloops{l})-foundloops_cm(l,1)).^2+(y(foundloops{l})-foundloops_cm(l,2)).^2))^0.5;
                        end
                        
                        [foundloops_Area,sortind]=sort(foundloops_Area,'descend');
                        foundloops=foundloops(sortind);
                        foundloops_cm=foundloops_cm(sortind,:);
                        foundloops_rgyr=foundloops_rgyr(sortind);
                        
                        %Find loops that overlap
                        overlappingloop=[];
                        for l=2:numel(foundloops)
                            if sum((foundloops_cm(1,:)-foundloops_cm(l,:)).^2)^0.5<foundloops_rgyr(1)
                                overlappingloop=[overlappingloop,l];
                            end
                        end
                        
                        %Delete the smaller loop that overlaps
                        foundloops(overlappingloop)=[];
                        foundloops_Area(overlappingloop)=[];
                        foundloops_cm(overlappingloop,:)=[];
                        foundloops_rgyr(overlappingloop)=[];
                        
                        %Delete the loop that is too far from center
                        %Used for the axon with a terminal bouton
                        if axid==2 && dd>1
                            distfromcl=sum(foundloops_cm.^2,2);
                            
                            faraway=distfromcl==min(distfromcl);
                            faraway=find(~faraway);
                            foundloops(faraway)=[];
                            foundloops_Area(faraway)=[];
                            foundloops_cm(faraway,:)=[];
                            foundloops_rgyr(faraway)=[];
                        end
                        
                        l=0;
                        while l<min([nloops,numel(foundloops_Area)])
                            l=l+1;
                            listofloops_Area(ii,l)=foundloops_Area(l);
                            listofloops{ii,l}=foundloops{l};
                        end
                    end
                    minsofar=inf;
                    
                    
                    %[A(nodeind,kmean_iter),iimax]=nanmax(sum(listofloops_Area,2));
                    %[~,iimax]=max(sum(listofloops_Area,2));
                    redo=true;
                    tempA=sum(listofloops_Area,2);
                    while redo
                        [~,iimax]=nanmax(tempA);
                        %Select the set of loops that return maximum area over
                        %different iterations with a given set of kmeans points
                        %that do not have any long edges
                        d_links=zeros(nloops,1);
                        for l=1:nloops
                            if ~isempty(listofloops{iimax,l})
                                dtemp=((x(listofloops{iimax,l}(1:end-1))-x(listofloops{iimax,l}(2:end))).^2+...
                                    (y(listofloops{iimax,l}(1:end-1))-y(listofloops{iimax,l}(2:end))).^2).^0.5;
                                d_links(l)=max(dtemp);
                            end
                        end
                        
                        if max(d_links)>loopnodes_dthr
                            if max(d_links)<=minsofar
                                bestmax=max(d_links);
                                bestind=iimax;
                                minsofar=bestmax;
                            end
                            tempA(iimax)=nan;
                            redo=true;
                        else
                            redo=false;
                        end
                        
                        if nnz(~isnan(tempA))==0
                            redo=false;
                            iimax=bestind;
                        end
                    end
                    A(nodeind,kmean_iter)=sum(listofloops_Area(iimax,:),2);
                    
                    if isplot
                        figure(2),movegui(2,'northwest')
                        xlim(3*[-0.6 0.6]);ylim(3*[-0.6 0.6]);axis square;box on;hold on
                        g1=plot(x0,y0,'.','Color',[0.85 0.85 0.85]);
                        g2=plot(x,y,'.','Color',[0.5 0.5 0.5]);
                        cc=lines(nloops);
                        l=1;hloops=[];
                        while ~isempty(listofloops{iimax,l}) && l<nloops
                            hl=plot(x(listofloops{iimax,l}),y(listofloops{iimax,l}),'-','Color',cc(l,:));
                            hloops=[hloops,hl];
                            l=l+1;
                        end
                        drawnow;
                        
                        if exist('g1','var')
                            delete(g1);
                        end
                        if exist('g2','var')
                            delete(g2);
                        end
                        
                        if exist('hloops','var')
                            delete(hloops);
                        end
                    end
                end
                if exist('h','var')
                    delete(h);
                end
            end
            nodeind=nodeind+1;
        end
        
        figure(7),movegui(7,'southeast')
        plot(d,median(A,2)),hold on
        xlabel('d (A.U.)');
        ylabel('Cross section area');
        box on;
        set(gca,'FontName','Calibri','FontSize',13);
        
        
        Areas{dd}=A;
    end
    fname=isunixispc([parentdir,'\dat\EMData\CrossSectionEst\A200',num2str(axid)]);
    %save(fname,'d','Areas');
end
%
loc={'northwest','northeast','southwest','southeast'};
for axid=1:4
    load(isunixispc([parentdir,'/dat/EMData/CrossSectionEst/M00',num2str(axid),'.mat']))
    figure(axid),clf(axid),movegui(axid,loc{axid})
    Amito=Areas{1};
    Anew=Areas{2};
    Aorig=Areas{3};
    Amito(isnan(Amito))=0;
    plot(d,median(Amito,2),'-','Color',[0 0.8 0.4],'LineWidth',1),hold on
    plot(d,median(Aorig,2),'-','Color',[0.4 0.4 0.4],'LineWidth',1),hold on
    %plot(d,median(Anew,2),'-','Color',[0.4 0.4 0.4],'LineWidth',1),hold on
    plot(d,median(Aorig,2)-median(Amito,2),'-','Color',[0 0.4 0.8],'LineWidth',1)
    xlim([0 25]);ylim([0 1]);
    set(gca,'XTick',[0:5:30])
    box on;
end
%}