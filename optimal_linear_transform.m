function [L,b]=optimal_linear_transform(S,T)
% This function finds the optimal linear (affine) transformation (L,b) for 
% assignment T=L*S+b. S and T are 3xN

S_cm=S-mean(S,2)*ones(1,size(S,2));
T_cm=T-mean(T,2)*ones(1,size(T,2));
Cov_SS=S_cm*S_cm';
Cov_TS=T_cm*S_cm';
L=Cov_TS/(Cov_SS+1*diag(10^-8.*ones(1,size(S,1))));
b=mean(T,2)-mean(L*S,2);

S_transformed=L*S+b*ones(1,size(S,2));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%{
figure(10)
subplot(1,2,1)
plot3(X(1,:),X(2,:),X(3,:),'r*')
axis equal, hold on, box on
plot3(Y(1,:),Y(2,:),Y(3,:),'k*')
subplot(1,2,2)
plot3(X_affine(1,:),X_affine(2,:),X_affine(3,:),'r*')
axis equal, hold on, box on
plot3(Y(1,:),Y(2,:),Y(3,:),'k*')
%}