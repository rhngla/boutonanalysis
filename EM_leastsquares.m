%The centerlines in the Old and New EM data have the same number of points.
%The centerlines were rotated and moved in Blender, so the fiduciary points
%to match the Light microscopy images had to be updated. I found a linear
%transformation using least squares regression to do so.

parentdir=pwd;
parentdir=parentdir(1:(strfind(parentdir,'Plasticity')+numel('Plasticity')-1));

ENew{1}=load(isunixispc([parentdir,'\dat\EMData\Blender2Mat\Obj2Txt\Axon1.mat']));
ENew{2}=load(isunixispc([parentdir,'\dat\EMData\Blender2Mat\Obj2Txt\Axon2.mat']));
ENew{3}=load(isunixispc([parentdir,'\dat\EMData\Blender2Mat\Obj2Txt\Axon3.mat']));
ENew{4}=load(isunixispc([parentdir,'\dat\EMData\Blender2Mat\Obj2Txt\Axon4.mat']));

EOld{1}=load(isunixispc([parentdir,'\dat\EMData\CrossSectionEst\Axon1.mat']));
EOld{2}=load(isunixispc([parentdir,'\dat\EMData\CrossSectionEst\Axon2.mat']));
EOld{3}=load(isunixispc([parentdir,'\dat\EMData\CrossSectionEst\Axon3.mat']));
EOld{4}=load(isunixispc([parentdir,'\dat\EMData\CrossSectionEst\Axon4.mat']));

%Least squares regression to find linear transformation (one-time use)
X=[EOld{1}.r;EOld{2}.r;EOld{3}.r;EOld{4}.r;];
Y=[ENew{1}.r;ENew{2}.r;ENew{3}.r;ENew{4}.r;];
Ymean=mean(Y,1);
Xmean=mean(X,1);

Xx=bsxfun(@minus,X,Xmean);
Yy=bsxfun(@minus,Y,Ymean);

B=(Xx'*Xx)\(Xx'*Yy);

EMold=...
[  -10.7100   -1.0340    6.0330
   -8.1370    0.3850    2.8410
   -4.1100   -1.4320    3.4090
   -2.2520   -1.0950    4.0120
    0.6430    0.7950    4.9040
    3.6210    1.7730    6.3450
    5.5330    2.1060    6.9990
   -1.1600    1.7890    3.0490
    0.8110    8.8370    2.9220
    5.0760    5.9080    2.1930
    3.2260    8.2180   -2.7800
    5.5970    1.2060    1.4290
    4.6360    3.6790    1.1950];

EMnew=bsxfun(@plus,bsxfun(@minus,EMold,Xmean)*B,Ymean);

cc=lines(4);
figure(1),clf(1);movegui(1,'northwest')
for i=1:4  
    plot3(ENew{i}.SVorig(:,1),ENew{i}.SVorig(:,2),ENew{i}.SVorig(:,3),'.','MarkerSize',1,'Color',cc(i,:)),hold on
    plot3(ENew{i}.r(:,1),ENew{i}.r(:,2),ENew{i}.r(:,3),'.','MarkerSize',2,'Color',[0 0 0]),hold on
end
plot3(EMnew(:,1),EMnew(:,2),EMnew(:,3),'ok','MarkerSize',20)
