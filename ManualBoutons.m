function varargout = ManualBoutons(varargin)
%Use as g = ManualBoutons(IM,'IM')
%MANUALBOUTONS MATLAB code file for ManualBoutons.fig
%      MANUALBOUTONS, by itself, creates a new MANUALBOUTONS or raises the existing
%      singleton*.
%
%      H = MANUALBOUTONS returns the handle to a new MANUALBOUTONS or the handle to
%      the existing singleton*.
%
%      MANUALBOUTONS('Property','Value',...) creates a new MANUALBOUTONS using the
%      given property value pairs. Unrecognized properties are passed via
%      varargin to ManualBoutons_OpeningFcn.  This calling syntax produces a
%      warning when there is an existing singleton*.
%
%      MANUALBOUTONS('CALLBACK') and MANUALBOUTONS('CALLBACK',hObject,...) call the
%      local function named CALLBACK in MANUALBOUTONS.M with the given input
%      arguments.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help ManualBoutons

% Last Modified by GUIDE v2.5 30-May-2016 20:08:09

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @ManualBoutons_OpeningFcn, ...
                   'gui_OutputFcn',  @ManualBoutons_OutputFcn, ...
                   'gui_LayoutFcn',  [], ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
   gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before ManualBoutons is made visible.
function ManualBoutons_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   unrecognized PropertyName/PropertyValue pairs from the
%            command line (see VARARGIN)

% Choose default command line output for ManualBoutons
handles.output = hObject;

%Set bouton and image data


prompt = {'Enter user name','Axon id'};
dlg_title = 'Input';
num_lines = 1;
temp = inputdlg(prompt,dlg_title,num_lines);
handles.uname=temp{1};
handles.axonid=temp{2};

%Load image
if ispc
    imfolder=isunixispc('C:\Users\Rohan\Dropbox\Lab\Plasticity\dat\EMData\Projections\DL100I005-');
else
    imfolder=isunixispc('/Users/Fruity/Dropbox/Lab/Plasticity/dat/EMData/Projections/DL100I005-');
end
temp=load([imfolder,handles.axonid,'-xy']);
handles.IM=temp.IM(:,:,1);
handles.IMorig=temp.IM(:,:,1);


%Load profile for normalizing image
if ispc
    profilefolder=isunixispc('C:\Users\Rohan\Dropbox\Lab\Plasticity\dat\EMData\Profiles\DL100I005\');
else
    profilefolder=isunixispc('/Users/Fruity/Dropbox/Lab/Plasticity/dat/EMData/Profiles/DL100I005/');
end
temp=load([profilefolder,handles.axonid],'I');
temp=mean(temp.I.G.LoG.raw);
handles.IM=handles.IM./temp;

%Load bouton file
if ispc
handles.boutonfolder=isunixispc('C:\Users\Rohan\Dropbox\Lab\Plasticity\dat\EMData\ManualBoutons\DL100I005\');
else
    handles.boutonfolder=isunixispc('/Users/Fruity/Dropbox/Lab/Plasticity/dat/EMData/ManualBoutons/DL100I005/');
end

if exist([handles.boutonfolder,handles.axonid,'-',handles.uname,'.mat'],'file');
    temp=load([handles.boutonfolder,handles.axonid,'-',handles.uname]);
    handles.Boutons=temp.Boutons;
else
    display('No record found - writing new file')
    handles.Boutons=cell(1,4);
    for i=1:size(handles.Boutons,2)
        handles.Boutons{1,i}=nan;
    end
end

handles.selbtnind=[];
axis(handles.MainAxis);
handles.contrastval.String=num2str(7);
imshow(handles.IM);caxis([0 str2num(handles.contrastval.String)]);hold on;
dtc=datacursormode(handles.figure1);
set(dtc,'UpdateFcn',@dtctxt)
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes ManualBoutons wait for user response (see UIRESUME)
% uiwait(handles.figure1);

% --- Outputs from this function are returned to the command line.
function varargout = ManualBoutons_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% --- Executes on button press in addpoint.
function addpoint_Callback(hObject, eventdata, handles)
% hObject    handle to addpoint (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(handles.editmode.SelectedObject.Tag,'loc')
    [btnind,present]=findbouton(hObject,handles);
    if sum(~present)>0
        display(['Adding ',num2str(sum(~present)),' bouton location(s)']);
        addind=find(~present);
        [x,y]=findpos(hObject,handles);
        for i=1:numel(addind)
            j=size(handles.Boutons,1)+1;
            handles.Boutons{j,1}=x(addind(i));
            handles.Boutons{j,2}=y(addind(i));
        end
    end
elseif strcmp(handles.editmode.SelectedObject.Tag,'sizinfo')
    [x,y]=findpos(hObject,handles);
    if ~isempty(handles.selbtnind)
        for i=1:numel(x)
            x_exist=handles.Boutons{handles.selbtnind,3};
            y_exist=handles.Boutons{handles.selbtnind,4};
            temp = x_exist==x(i) & y_exist==y(i);
            if isempty(temp) || (~isempty(temp) && sum(temp)==0)
                handles.Boutons{handles.selbtnind,3}=[x_exist(:);x(i)];
                handles.Boutons{handles.selbtnind,4}=[y_exist(:);y(i)];
            end
        end
    end
end
updateplot_Callback(hObject, eventdata, handles)
guidata(hObject,handles);

% --- Executes on button press in rempoint.
function rempoint_Callback(hObject, eventdata, handles)
% hObject    handle to rempoint (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if strcmp(handles.editmode.SelectedObject.Tag,'loc')
    handles.selbtnind=[];
    [btnind,present]=findbouton(hObject,handles);
    if sum(present)>0
        display(['Removing ',num2str(sum(present)),' bouton location(s)']);
        handles.Boutons(btnind(present),:)=[];
    else
        display('No boutons removed')
    end
elseif strcmp(handles.editmode.SelectedObject.Tag,'sizinfo')
    [x,y]=findpos(hObject,handles);
    if ~isempty(handles.selbtnind)
        remind=ismember(handles.Boutons{handles.selbtnind,3},x) & ...
            ismember(handles.Boutons{handles.selbtnind,4},y);
        handles.Boutons{handles.selbtnind,3}(remind)=[];
        handles.Boutons{handles.selbtnind,4}(remind)=[];
    else
        display('Select bouton to remove voxels from')
    end
end
updateplot_Callback(hObject, eventdata, handles)
guidata(hObject,handles);

% --- Executes on button press in selbouton.
function selbouton_Callback(hObject, eventdata, handles)
% hObject    handle to selbouton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[btnind,present]=findbouton(hObject,handles);
if sum(present)==1
    display('Bouton selected');
elseif sum(present)>1
    display('Multiple boutons selected! Try again');
    btnind=[];
elseif sum(present)==0
    display('No bouton selected!');
    btnind=[];
end
handles.selbtnind=btnind(present);
updateplot_Callback(hObject, eventdata, handles)
guidata(hObject,handles);

% --- Executes on button press in updateplot.
function updateplot_Callback(hObject, eventdata, handles)
% hObject    handle to updateplot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
cla(handles.MainAxis);
imshow(handles.IM);caxis([0 str2num(handles.contrastval.String)]);hold on;
cc=lines(size(handles.Boutons,1));
if ~isempty(handles.selbtnind)
    plot(handles.Boutons{handles.selbtnind,1},handles.Boutons{handles.selbtnind,2},'og','MarkerSize',18,'LineWidth',3);
end

if (handles.overlaylocation.Value)
    for k=1:size(handles.Boutons,1)
        plot(handles.Boutons{k,1},handles.Boutons{k,2},'sr','MarkerSize',15,'LineWidth',2);
    end
end

if (handles.overlaysize.Value)
    for k=1:size(handles.Boutons,1)
        plot(handles.Boutons{k,3},handles.Boutons{k,4},'x','MarkerSize',15,'Color',cc(k,:),'LineWidth',2);
        text(mean(handles.Boutons{k,3})+5,mean(handles.Boutons{k,4}),num2str(numel(handles.Boutons{k,4})),'Color',cc(k,:),'FontSize',12);
        if sum(~isnan(handles.Boutons{k,3}))>0
            inds=sub2ind(size(handles.IM),handles.Boutons{k,4},handles.Boutons{k,3});
            II=mean(handles.IMorig(inds));
            text(mean(handles.Boutons{k,3})+15,mean(handles.Boutons{k,4}),num2str(II),'Color',cc(k,:),'FontSize',12);
        end
    end
end

% --- Executes on button press in overlaylocation.
function overlaylocation_Callback(hObject, eventdata, handles)
% hObject    handle to overlaylocation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of overlaylocation
updateplot_Callback(hObject, eventdata, handles)


% --- Executes on button press in overlaysize.
function overlaysize_Callback(hObject, eventdata, handles)
% hObject    handle to overlaysize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of overlaysize
updateplot_Callback(hObject, eventdata, handles)

function contrastval_Callback(hObject, eventdata, handles)
% hObject    handle to contrastval (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of contrastval as text
%        str2double(get(hObject,'String')) returns contents of contrastval as a double

% --- Executes during object creation, after setting all properties.
function contrastval_CreateFcn(hObject, eventdata, handles)
% hObject    handle to contrastval (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function [ind,present]=findbouton(hObject,handles)
%This function will return nan if bouton location not found, else a
%one or more bouton indices.

dcm_obj = datacursormode(hObject.Parent);
info_struct = getCursorInfo(dcm_obj);
present=false(numel(info_struct),1);
if ~isempty([handles.Boutons{:,1}])
    ind=nan(numel(info_struct),1);
    for i=1:numel(info_struct)
        temp=find([handles.Boutons{:,1}]==info_struct(i).Position(1) &...
            [handles.Boutons{:,2}]==info_struct(i).Position(2));
        if ~isempty(temp)
            ind(i)=temp;
            present(i)=true;
        end   
    end
else
    ind=[];
end

function [x,y]=findpos(hObject,handles)
%This function will return x-y co-ordinates as determined by the data
%cursor
dcm_obj = datacursormode(hObject.Parent);
info_struct = getCursorInfo(dcm_obj);
xy=[info_struct.Position];
x=xy(1:2:end);
y=xy(2:2:end);

% --- Executes when selected object is changed in editmode.
function editmode_SelectionChangedFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in editmode 
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
switch get(eventdata.NewValue,'Tag') % Get Tag of selected object.
    case 'loc'
        display('Detect boutons');
    case 'sizinfo'
        display('Edit bouton size');
end

function txt = dtctxt(~,event_obj)
% Customizes text of data tips
txt = {[]};


% --- Executes on button press in savebutt.
function savebutt_Callback(hObject, eventdata, handles)
% hObject    handle to savebutt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Boutons=handles.Boutons;
save([handles.boutonfolder,handles.axonid,'-',handles.uname],'Boutons');
display(['Saved file ',handles.boutonfolder,handles.axonid,'-',handles.uname]);
% temp=load('Handel.mat');
% sound(temp.y, temp.Fs);