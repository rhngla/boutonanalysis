parentdir=pwd;
parentdir=parentdir(1:(strfind(parentdir,'Plasticity')+numel('Plasticity')-1));
[xldat,~,txt]=xlsread(isunixispc([parentdir,'\dat\EMData\Size data.xlsx']),'v5');

%Notes:
%v4 contains data that was sent in the update
%Data for Axon 3 in v5 comes from newly optimized trace (A007)
txt={txt{2:18,9}}';
cc=lines(4);
cc=cc(xldat(:,1),:);

for t=1:numel(txt)
    if isa(txt{t},'double')
        txt{t}=num2str(txt{t});
    end
end
%Extra normalization
extranorm.G=ones(4,1);
extranorm.R=ones(4,1);
axlbl=([1,2,3,4]);
channel={'G','R'};

Ff = @(x,A,mu,sigma) (ones(size(x))*A).*exp(-(x*ones(size(A))-ones(size(x))*mu).^2./(ones(size(x))*sigma.^2)./2);
for ax=1:4
    %figure(ax),clf(ax)
    for ch=1:numel(channel)
        A=load(isunixispc([parentdir,'\dat\EMData\Profiles\DL100I005\A00',num2str(axlbl(ax)),'.mat']));
        nsig=2.5;
        btn_end=A.fit.(channel{ch}).LoG.fg.mu+nsig*A.fit.(channel{ch}).LoG.fg.sig;
        btn_start=A.fit.(channel{ch}).LoG.fg.mu-nsig*A.fit.(channel{ch}).LoG.fg.sig;
        [~,indstart]=min(abs(bsxfun(@minus,A.d.optim,btn_start')),[],1);
        [~,indend]=min(abs(bsxfun(@minus,A.d.optim,btn_end')),[],1);
        inds=[indstart(:),indend(:)];
        backbone=true(size(A.d.optim));
        %figure,
        %plot(A.d.optim,A.I.(channel{ch}).LoG.norm,'-'),hold on
        for i=1:size(inds,1)
            backbone(inds(i,1):inds(i,2))=false;
            %plot(A.d.optim(inds(i,1):inds(i,2)),A.I.(channel{ch}).LoG.norm(inds(i,1):inds(i,2)),'-w')
        end
        extranorm.(channel{ch})(ax)=mean(A.I.(channel{ch}).LoG.norm(backbone));
        if ch==2
            figure
            plot(A.d.optim,A.I.(channel{ch}).LoG.norm,'-'),hold on
            bg=zeros(size(A.d.optim));
            for i=1:numel(A.fit.(channel{ch}).LoG.bg.ind)
                temp=A.fit.(channel{ch}).LoG.bg;
                bg=bg+Ff(A.d.optim,temp.amp(i),temp.mu(i),temp.sig(i));
            end
            
            plot(A.d.optim,bg,'-','Color',[1 0 0]);
            h=nan(4,1);
            h(1)=plot(A.d.optim([1,end]),[mean(bg),mean(bg)],'--r');
            h(2)=plot(A.d.optim([1,end]),[extranorm.(channel{ch})(ax) extranorm.(channel{ch})(ax)],'--k');
            h(3)=plot(A.d.optim([1,end]),[mean(A.I.(channel{ch}).LoG.norm) mean(A.I.(channel{ch}).LoG.norm)],'-k');
            h(4)=plot(A.d.optim([1,end]),[median(A.I.(channel{ch}).LoG.norm) median(A.I.(channel{ch}).LoG.norm)],'-b');
            drawnow;
            legend(h, {'Mean bg','mean backbone','mean axon','median axon'})
            
            Ibackbone=A.I.(channel{ch}).LoG.norm(backbone);
            n_n=sum(Ibackbone<0);
            n_p=sum(Ibackbone>0);
            mean_n=mean(Ibackbone(Ibackbone<0));
            mean_p=mean(Ibackbone(Ibackbone>0));
            fprintf('Axon %d no. of +ve %d: mean %.2f no. of -ve %d: mean %.2f | Normalization = %.2f',axlbl(ax),n_p,mean_p,n_n,mean_n,extranorm.(channel{ch})(ax));
            display((n_p*mean_p)/(n_n*mean_n))
            %extranorm.(channel{ch})(ax)=median(A.I.(channel{ch}).LoG.norm);
            %extranorm.(channel{ch})(ax)=median(bg);
            %extranorm.(channel{ch})(ax)=1;
        end
    end
end
%extranorm.G=[0.15,0.45,0.3,0.35]';
extranorm.G=extranorm.G(xldat(:,1));
extranorm.R=extranorm.R(xldat(:,1));

xldat(:,3)=xldat(:,3)./extranorm.G(:);
xldat(:,4)=xldat(:,4)./extranorm.R(:);
xldat(8,:)=nan;%Terminal bouton excluded

figure(10),clf(10)
subplot(2,4,1),hold on;
scatter(xldat(:,3),xldat(:,5),[],cc,'filled'),axis square,box on
text(xldat(:,3)+0.2,xldat(:,5),txt,'Color',[0.4,0.4,0.4])
xlabel('Green intensity')
ylabel('Bouton surface area (\mu{m}^{2})')
xlim([0 ceil(max(xldat(:,3)))])
ylim([0 ceil(max(xldat(:,5)))])
C=corrcoef(xldat(:,3),xldat(:,5),'rows','pairwise');
notnan=~isnan(xldat(:,3)) & ~isnan(xldat(:,5));
[ff,goodness]=fit(xldat(notnan,3),xldat(notnan,5),fittype('a*x+b'),'StartPoint',[1,0]);
plot([0,max(xldat(:,3))],ff([0,max(xldat(:,3))]),'-r')
text(1,7,['R^{2} = ',num2str(goodness.rsquare)])
title(num2str(C(1,2)))
drawnow

subplot(2,4,2),hold on;
scatter(xldat(:,3),xldat(:,6),[],cc,'filled'),axis square,box on
text(xldat(:,3)+0.2,xldat(:,6),txt,'Color',[0.4,0.4,0.4])
xlabel('Green intensity')
ylabel('Bouton volume (\mu{m}^{3})')
xlim([0 ceil(max(xldat(:,3)))])
ylim([0 ceil(max(xldat(:,6)))])
C=corrcoef(xldat(:,3),xldat(:,6),'rows','pairwise');
notnan=~isnan(xldat(:,3)) & ~isnan(xldat(:,6));
[ff,goodness]=fit(xldat(notnan,3),xldat(notnan,6),fittype('a*x^b'),'StartPoint',[1,1.5]);
ff
plot([0:0.1:max(xldat(:,3))],ff([0:0.1:max(xldat(:,3))]),'-r')
text(1,0.9,['R^{2} = ',num2str(goodness.rsquare)])
title(num2str(C(1,2)))
drawnow

subplot(2,4,3);
scatter(xldat(:,3),xldat(:,7),[],cc,'filled'),axis square,box on
text(xldat(:,3)+0.2,xldat(:,7),txt,'Color',[0.4,0.4,0.4])
xlabel('Green intensity')
ylabel('Synapse surface area (\mu{m}^{2})')
xlim([0 ceil(max(xldat(:,3)))])
ylim([0 ceil(max(xldat(:,7)))])
C=corrcoef(xldat(:,3),xldat(:,7),'rows','pairwise');
title(num2str(C(1,2)))
drawnow

subplot(2,4,4);
scatter(xldat(:,3),xldat(:,8),[],cc,'filled'),axis square,box on
text(xldat(:,3)+0.2,xldat(:,8),txt,'Color',[0.4,0.4,0.4])
xlabel('Green intensity')
ylabel('Synapse volume (\mu{m}^{3})')
xlim([0 ceil(max(xldat(:,3)))])
ylim([0 0.05])
C=corrcoef(xldat(:,3),xldat(:,8),'rows','pairwise');
title(num2str(C(1,2)))
drawnow

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
subplot(2,4,5);
scatter(xldat(:,4),xldat(:,5),[],cc,'filled'),axis square,box on
text(xldat(:,4)+0.2,xldat(:,5),txt,'Color',[0.4,0.4,0.4])
xlabel('Red intensity')
ylabel('Bouton surface area (\mu{m}^{2})')
xlim([0 ceil(max(xldat(:,4)))])
ylim([0 ceil(max(xldat(:,5)))])
C=corrcoef(xldat(:,4),xldat(:,5),'rows','pairwise');
title(num2str(C(1,2)))
drawnow

subplot(2,4,6);
scatter(xldat(:,4),xldat(:,6),[],cc,'filled'),axis square,box on
text(xldat(:,4)+0.2,xldat(:,6),txt,'Color',[0.4,0.4,0.4])
xlabel('Red intensity')
ylabel('Bouton volume (\mu{m}^{3})')
xlim([0 ceil(max(xldat(:,4)))])
ylim([0 ceil(max(xldat(:,6)))])
C=corrcoef(xldat(:,4),xldat(:,6),'rows','pairwise');
title(num2str(C(1,2)))
drawnow

subplot(2,4,7);
scatter(xldat(:,4),xldat(:,7),[],cc,'filled'),axis square,box on
text(xldat(:,4)+0.2,xldat(:,7),txt,'Color',[0.4,0.4,0.4])
xlabel('Red intensity')
ylabel('Synapse surface area (\mu{m}^{2})')
xlim([0 ceil(max(xldat(:,4)))])
ylim([0 ceil(max(xldat(:,7)))])
C=corrcoef(xldat(:,4),xldat(:,7),'rows','pairwise');
title(num2str(C(1,2)))
drawnow

subplot(2,4,8);
scatter(xldat(:,4),xldat(:,8),[],cc,'filled'),axis square,box on
text(xldat(:,4)+0.2,xldat(:,8),txt,'Color',[0.4,0.4,0.4])
xlabel('Red intensity')
ylabel('Synapse volume (\mu{m}^{3})')
xlim([0 ceil(max(xldat(:,4)))])
ylim([0 0.05])
C=corrcoef(xldat(:,4),xldat(:,8),'rows','pairwise');
title(num2str(C(1,2)))
drawnow

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure(11),clf(11)
subplot(1,3,1),hold on;
scatter(xldat(:,3),xldat(:,6),[],cc,'filled'),axis square,box on
text(xldat(:,3)+0.5,xldat(:,6),txt,'Color',[0.4,0.4,0.4])
xlabel('Green intensity')
ylabel('Bouton volume (\mu{m}^{3})')
xlim([0 ceil(max(xldat(:,3)))])
ylim([0 ceil(max(xldat(:,6)))])
C=corrcoef(xldat(:,3),xldat(:,6),'rows','pairwise');
notnan=~isnan(xldat(:,3)) & ~isnan(xldat(:,6));
[ff,goodness]=fit(xldat(notnan,3),xldat(notnan,6),fittype('a*x+b'),'StartPoint',[1,0]);
plot([0,max(xldat(:,3))],ff([0,max(xldat(:,3))]),'-r')
text(1,0.9,['R^{2} = ',num2str(goodness.rsquare)])
title(num2str(C(1,2)))
drawnow

subplot(1,3,2),hold on;
scatter(xldat(:,4),xldat(:,6),[],cc,'filled'),axis square,box on
text(xldat(:,4)+0.5,xldat(:,6),txt,'Color',[0.4,0.4,0.4])
xlabel('Red intensity')
ylabel('Bouton volume (\mu{m}^{3})')
xlim([0 ceil(max(xldat(:,4)))])
ylim([0 ceil(max(xldat(:,6)))])
C=corrcoef(xldat(:,4),xldat(:,6),'rows','pairwise');
notnan=~isnan(xldat(:,4)) & ~isnan(xldat(:,6));
[ff,goodness]=fit(xldat(notnan,4),xldat(notnan,6),fittype('a*x+b'),'StartPoint',[1,0]);
plot([0,max(xldat(:,4))],ff([0,max(xldat(:,4))]),'-r')
text(1,0.9,['R^{2} = ',num2str(goodness.rsquare)])
title(num2str(C(1,2)))
drawnow

subplot(1,3,3),hold on;
scatter(xldat(:,7),xldat(:,6),[],cc,'filled'),axis square,box on
text(xldat(:,7)+0.05,xldat(:,6),txt,'Color',[0.4,0.4,0.4])
xlabel('Synapse surface area (\mu{m}^{2})')
ylabel('Bouton volume (\mu{m}^{3})')
xlim([0 ceil(max(xldat(:,7)))])
ylim([0 ceil(max(xldat(:,6)))])
C=corrcoef(xldat(:,7),xldat(:,6),'rows','pairwise');
notnan=~isnan(xldat(:,7)) & ~isnan(xldat(:,6));
[ff,goodness]=fit(xldat(notnan,7),xldat(notnan,6),fittype('a*x+b'),'StartPoint',[1,0]);
plot([0,max(xldat(:,7))],ff([0,max(xldat(:,7))]),'-r')
text(.1,0.9,['R^{2} = ',num2str(goodness.rsquare)])
title(num2str(C(1,2)))
drawnow

% C=corrcoef(xldat(:,[3,4,5,6,7,8,11]),'rows','pairwise');
% figure(12),clf(12),
% imagesc(C);axis tight;colorbar,
% caxis([0 1])


