function [Animal] = analysis_loaddat(anid,tp,getlearn)
%anid: animal id
%tp: choose pre-defined set of time points
%getlearn: true/false value to load learning data

pathlist;
%Normalization related:

if anid==83
    tracer={[]};
    section = {'001','002'};
    axon = {[2,5],[1,2]};
    if tp==0
        timepoint=cellstr(char(double('B'):double('N'))');
    elseif tp==1
        timepoint=cellstr(char(double('D'):double('N'))');
    end
    fitind=13:48;
    
elseif anid==85
    tracer={[]};
    section = {'001','008','009'};
    axon={[2,3,4,5,6,8,9,12,13,14],[1],[1,3,4,5,6,7,8]};
    if tp==0
        timepoint=cellstr(char(double('C'):double('T'))');
    elseif tp==1
        timepoint=cellstr(char(double('D'):double('T'))');
    end
    fitind=9:72;
    
elseif anid==101
    tracer={[]};
    section = {'008'};
    axon = {[1,2,3,4,5,7,9,10,11,12,13,14,15,16,17,18]};
    if tp==0
        timepoint=cellstr(char(double('B'):double('H'))');
    elseif tp==1
        timepoint=cellstr(char(double('D'):double('S'))');
    end
    fitind=15:80;
    
elseif anid==102
    tracer={[]};
    section = {'001','002','008','009'};
    axon = {[1,5],[1,2,3],[1,3,4,5,6,7,8,9,10,11,12,13,14],[1,2,3,4,5,6]};
    if tp==0
        timepoint=cellstr(char(double('B'):double('O'))');
    elseif tp==1
        timepoint=cellstr(char(double('C'):double('O'))');
    end
    fitind=21:79;
    
elseif anid==88
    tracer={[]};
    section = {'001','005','008'};
    axon = {[1,2,3,4,5,6,7,8,9,10],[1,2,3,4,5,7,8,9,10,11,12],[1,2,3,4,5,6]};
    if tp==0
        timepoint=cellstr(char(double('B'):double('Q'))');
    elseif tp==1
        timepoint=cellstr(char(double('B'):double('Q'))');
    end
    fitind=8:54;
    
elseif anid==108
    
elseif anid==68
    
elseif anid==89
    
elseif anid==01
    tracer={[]};
    section = {'002'};
    axon = {[1,2,3,4,7,8,9,10,11,12,13,14,17,18,19,20]};%15 was removed
    timepoint={'A','B','D','E','F','H','I'};
elseif anid==02
    anid=001;
    tracer={[]};
    section = {'002'};
    axon = {[1,2,3,4,7,8,9,10,11,12,13,14,17,18,19,20]};%15 was removed
    timepoint={'K','L','M','N','O','P','Q','R','S','T'};
end
animal=sprintf('DL%03d',anid);

Section=cell(numel(section),1);
for se=1:numel(section)
    for ti=1:numel(timepoint)
        for ax=1:numel(axon{se})
            for tr=1:numel(tracer)
                axonstr=[sprintf('A%03d',axon{se}(ax)),tracer{tr}];
                fname=isunixispc([profile_pth,animal,timepoint{ti},section{se},'\',axonstr,'.mat']);
                if exist(fname,'file')
                    Dat=load(fname);
                    Section{se}.Time{ti}.Axon{ax}=struct(Dat);
                else
                    display([animal,timepoint{ti},section{se},'-',axonstr,' not found!']);
                end
            end
        end
    end
end

Animal.Section=Section;
if getlearn
    Animal.L=analysis_loadlearn(anid,timepoint,fitind);
end

end
