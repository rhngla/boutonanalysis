%This script will open a figure with axons from multiple time points
%staggered in the direction normal to the xy/yz/zx layout. It can take in a
%start point for each trace (to orient the traces in 1D) It allows to mark
%regions to ignore in any of the time points displayed. Only trace nodes
%will be displayed
pathlist;

animal={'DL001'};
%timepoint=cellstr([char(double('K'):(double('T')))']);
timepoint={'A','B','D','E','F','H','I'};
section={'002'};
axon={[1]};
tracer={[]}; %Default: tracer = {[]}; for 'A001.swc';
proj='xy';
%setim='DL001I002';%For manual vs optimized trace comparisons
setim=[];
isshowmanual=false;
ex=10;%down
ey=10;%right

if strcmp(proj,'xy')
    dat.perm=[1,2,3];
elseif strcmp(proj,'yz')
    dat.perm=[3,2,1];
elseif strcmp(proj,'zx')
    dat.perm=[1,3,2];
end

Canvas=zeros(3048,3048);
dx=zeros(numel(timepoint),1);dy=dx;
r=[];

an=1;se=1;ax=1;tr=1;
Time=cell(numel(timepoint),1);
for ti=1:numel(timepoint)
    axonstr=[sprintf('A%03d',axon{se}(ax)),tracer{tr}];
    stackid=[animal{an},timepoint{ti},section{se}];
    sep=20*(ti-1);
    clear IM;
    
    imid=setim;
    if isempty(imid)
        imid=stackid;
    end
    
    if exist(isunixispc([proj_pth,imid,'-',axonstr,'-',proj,'.mat']),'file')
        load(isunixispc([proj_pth,imid,'-',axonstr,'-',proj,'.mat']),'IM')
     
        sizeIM=size(IM(:,:,1));
        Time{ti}=load(isunixispc([profile_pth,stackid,'\',axonstr,'.mat']),'r','annotate');
        Time{ti}.r.optim=Time{ti}.r.optim(:,dat.perm);
        Time{ti}.save_pth=isunixispc([profile_pth,stackid,'\',axonstr,'.mat']);
        
        if ti>1
            %prevr=Time{ti-1}.r.optim;
            nowr=Time{ti}.r.optim;
            dx(ti)=mean(nowr(:,1))-mean(prevr(:,1));
            dy(ti)=mean(nowr(:,2))-mean(prevr(:,2));
        end
        prevr=Time{ti}.r.optim;
        %dxt=round(sep.*(ex)./((ex.^2+ey.^2).^0.5))+1024;
        %dyt=round(sep.*ey./((ex.^2+ey.^2).^0.5))+1024;
        dxt=nansum(dx); dyt=nansum(dy);
        dxt=-round(dxt)+size(Canvas,1)/4+1;
        dyt=-round(dyt)+size(Canvas,2)/4+1;
        dxt=dxt+ex*ti;
        dyt=dyt+ey*ti;
            
        Canvas(dxt:dxt+size(IM,1)-1,dyt:dyt+size(IM,2)-1,1)=max(Canvas(dxt:dxt+size(IM,1)-1,dyt:dyt+size(IM,2)-1,1),IM(:,:,1));
        
        Time{ti}.r.optim(:,1)=Time{ti}.r.optim(:,1)-1+dxt;
        Time{ti}.r.optim(:,2)=Time{ti}.r.optim(:,2)-1+dyt;
        if isshowmanual
            [Time{ti}.man_AM,Time{ti}.man_r,~]=swc2AM(isunixispc([man_pth,stackid,'\',axonstr,'.swc']));
            Time{ti}.man_r=Time{ti}.man_r(:,dat.perm);
            Time{ti}.man_r(:,1)=Time{ti}.man_r(:,1)-minx+offset(1);
            Time{ti}.man_r(:,2)=Time{ti}.man_r(:,2)-miny+offset(2);
        end
    else
        display([isunixispc([proj_pth,imid,'-',axonstr,'-',proj]),' not found!']);
    end
end

%Time{ti} contains shifted r-coordinates for display only, and ignoreind
%data that will be modified and saved by the gui.
f=figure(1);clf(1),
imshow(Canvas),caxis([0 max(Canvas(:))./2]),hold on,
set(f,'KeyPressFcn',@gui_annopt_hotkeys)
h_trace=nan(numel(Time),1);
dat.hi=cell(numel(Time),1);
dat.currentmode='r';
dat.currentpoint=[];
dat.currenttime=[];
dat.Time=Time;
for ti=1:numel(Time)
    h_trace(ti)=plot3(Time{ti}.r.optim(:,2),Time{ti}.r.optim(:,1),ones(size(Time{ti}.r.optim(:,1))),'.','Tag',num2str(ti));hold on
    if isshowmanual
        plotAM(Time{ti}.man_AM,Time{ti}.man_r);
    end
    set(h_trace(ti),'ButtonDownFcn',@gui_annopt_selnodes);
    dat.hi{ti}=plot3(Time{ti}.r.optim(Time{ti}.annotate.ignore,2),Time{ti}.r.optim(Time{ti}.annotate.ignore,1),zeros(size(Time{ti}.r.optim(Time{ti}.annotate.ignore,1))),'xr','Tag','Ignored','MarkerSize',13);
    text(Time{ti}.r.optim(1,2),Time{ti}.r.optim(1,1),timepoint{ti},'FontSize',15,'Color',[0.8 0 0]);
end
guidata(f,dat)
