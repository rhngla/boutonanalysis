%This script was used to create the Figure 3 - video 1 in Gala_2017
%The script generates individual frames in the movie which were later put
%together in photoshop to show a side-by-side comparison of the CLEM data.
%All the print statements have been commented out to prevent inadvertent
%over-writing of the original files.

Ff = @(x,A,mu,sigma) (ones(size(x))*A).*exp(-(x*ones(size(A))-ones(size(x))*mu).^2./(ones(size(x))*sigma.^2)./2);

parentdir=pwd;
parentdir=parentdir(1:(strfind(parentdir,'Plasticity')+numel('Plasticity')-1));

load(isunixispc([parentdir,'/dat/EMData/DatasetIM/DL100I005G.mat']),'IM')

L{1}=load(isunixispc([parentdir,'/dat/EMData/Profiles/DL100I005/A001.mat']),'r','I','d','fit');
L{2}=load(isunixispc([parentdir,'/dat/EMData/Profiles/DL100I005/A002.mat']),'r','I','d','fit');
L{3}=load(isunixispc([parentdir,'/dat/EMData/Profiles/DL100I005/A003.mat']),'r','I','d','fit');
L{4}=load(isunixispc([parentdir,'/dat/EMData/Profiles/DL100I005/A004.mat']),'r','I','d','fit');

IM=double(IM);

clim=1300;
nsteps=96;
int_ind=96;

%Zoom into LM image--------------------------------------------------------
xlim_init=[0.5;1024.5];
ylim_init=[0.5;1024.5];
zlim_init=[1;312];

xlim_final=[362.5;428.5];
ylim_final=[586.5;652.5];
zlim_final=[240;286];
x_boxline=[xlim_final(1);xlim_final(1);xlim_final(2);xlim_final(2);xlim_final(1)];
y_boxline=[ylim_final(1);ylim_final(2);ylim_final(2);ylim_final(1);ylim_final(1)];

xlim_int=[318.5 466.5];
ylim_int=[546.5 694.5];
zlim_int=[round(interp1([1;nsteps],[zlim_init(1);zlim_final(1)],int_ind));...
    round(interp1([1;nsteps],[zlim_init(2);zlim_final(2)],int_ind))];

xlim_final=xlim_int;
ylim_final=ylim_int;

%
png_ind=0;
hf=figure(1);movegui(1,'northwest')
hf.PaperPosition(3)=20;hf.PaperPosition(4)=10;
for i=1:int_ind
    x1=interp1([1;nsteps],[xlim_init(1);xlim_final(1)],i);
    x2=interp1([1;nsteps],[xlim_init(2);xlim_final(2)],i);
    y1=interp1([1;nsteps],[ylim_init(1);ylim_final(1)],i);
    y2=interp1([1;nsteps],[ylim_init(2);ylim_final(2)],i);
    z1=round(interp1([1;int_ind],[zlim_init(1);zlim_final(1)],i));
    z2=round(interp1([1;int_ind],[zlim_init(2);zlim_final(2)],i));
    hi=findobj(hf,'Type','Image');
    if isempty(hi)
        patch([0;0;2049;2049;0],[0;1025;1025;0;0],[0.2,0.2,0.2],'EdgeColor','none','FaceColor',[0.2 0.2 0.2]);hold on
        imshow(max(IM(:,:,z1:z2),[],3),[0 clim]);hold on
        hi=findobj(hf,'Type','Image');
        haxis=hi.Parent;
        haxis.XTick=[];
        haxis.YTick=[];
        hbox=plot(x_boxline,y_boxline,'-','LineWidth',2,'Color',[0.95 0.95 0.95]);
        caxis([0 clim]);
    else
        hi.CData=max(IM(:,:,z1:z2),[],3);
    end
    xlim([x1,x2+(x2-x1)]);ylim([y1,y2]);
    png_ind=png_ind+1;
    %print(isunixispc([parentdir,'/BoutonAnalysis/docs/zoomsequence/','zoom',num2str(png_ind),'.png']),'-dpng')
    drawnow;
end

cc=lines(4);

nsteps=48;
%Show Traces of axons------------------------------------------------------
IMorig=max(IM(:,:,z1:z2),[],3);
hi.CData=IMorig;caxis([0 clim]);
xlim([x1,x2+(x2-x1)]);ylim([y1,y2]);hold on;drawnow;
haxis=hi.Parent;
for ax=1:4
    for i=1:nsteps
        r=L{ax}.r.optim;
        ind=1:round(size(r,1)*(i./nsteps));
        delete(findobj(haxis,'Tag',['Axon',num2str(ax)]));
        plot3(r(ind,2),r(ind,1),3*(312-r(ind,3)),'-','Color',cc(ax,:),...
            'LineWidth',3,'Tag',['Axon',num2str(ax)]);hold on
        drawnow;
        png_ind=png_ind+1;
        %print(isunixispc([parentdir,'/BoutonAnalysis/docs/zoomsequence/','zoom',num2str(png_ind),'.png']),'-dpng')
    end
end

%Create tubes--------------------------------------------------------------
SVr=[];
for i=1:4
    SVr=[SVr;L{i}.r.optim];
end
SVr=round(SVr);
[KT]=FastMarchingTube(size(IM),SVr,4.5,[1 1 1]);
IMresctricted=double(KT>0).*IM;
IMresctricted=max(IMresctricted,[],3);

%Fade away background------------------------------------------------------
nsteps=24;
for i=1:nsteps
    II=(1-(i./nsteps)).*IMorig;
    hi.CData=max(IMresctricted,II);
    caxis([0 clim]);
    drawnow;
    png_ind=png_ind+1;
    %print(isunixispc([parentdir,'/BoutonAnalysis/docs/zoomsequence/','zoom',num2str(png_ind),'.png']),'-dpng')
end
xlim([x1,x2+(x2-x1)]);ylim([y1,y2]);hold on;drawnow;

haxons=gobjects(4,1);
for ax=1:4
    haxons(ax)=findobj(haxis,'Tag',['Axon',num2str(ax)]);
    haxons(ax).Parent=haxis;
    
    ind=L{ax}.fit.G.LoGxy.fg.ind;
    r=L{ax}.r.optim;
    bg=zeros(size(L{ax}.d.optim));
    for i=1:numel(L{ax}.fit.G.Gauss2.bg.ind)
        bg=bg+Ff(L{ax}.d.optim,L{ax}.fit.G.Gauss2.bg.amp(i),L{ax}.fit.G.Gauss2.bg.mu(i),L{ax}.fit.G.Gauss2.bg.sig(i));
    end
    normfact=mean(bg);
    sizes=L{ax}.fit.G.LoGxy.fg.amp./normfact;
    for j=1:numel(ind)
        if ~ismember(100*ax+j,[207,206,209,211,212,308,401])
            htemp=plot3(r(ind(j),2),r(ind(j),1),3*(312-r(ind(j),3)),'o',...
                'LineWidth',4,'MarkerSize',30,'MarkerFaceColor','none',...
                'MarkerEdgeColor',[0 0.8 0]);
            
            png_ind=png_ind+1;
            %print(isunixispc([parentdir,'/BoutonAnalysis/docs/zoomsequence/','zoom',num2str(png_ind),'.png']),'-dpng')
            
            delete(htemp);
            plot3(r(ind(j),2),r(ind(j),1),3*(312-r(ind(j),3)),'o',...
                'MarkerSize',7*sizes(j).^0.5,'MarkerFaceColor',cc(ax,:),'Tag',['Bouton',num2str(100*ax+j)],...
                'MarkerEdgeColor','none');
            
            png_ind=png_ind+1;
            %print(isunixispc([parentdir,'/BoutonAnalysis/docs/zoomsequence/','zoom',num2str(png_ind),'.png']),'-dpng')
        end
    end
end


xlim_final=[362.5;428.5];
ylim_final=[586.5;652.5];
nsteps=24;
for i=1:nsteps
    x1=interp1([1;nsteps],[xlim_int(1);xlim_final(1)],i);
    x2=interp1([1;nsteps],[xlim_int(2);xlim_final(2)],i);
    y1=interp1([1;nsteps],[ylim_int(1);ylim_final(1)],i);
    y2=interp1([1;nsteps],[ylim_int(2);ylim_final(2)],i);
    xlim([x1,x2+(x2-x1)]);ylim([y1,y2]);
    drawnow;
    
    png_ind=png_ind+1;
    %print(isunixispc([parentdir,'/BoutonAnalysis/docs/zoomsequence/','zoom',num2str(png_ind),'.png']),'-dpng')
end
delete(hbox);

nsteps=24;
haxis.Color=[0 0 0];
him=findobj(haxis,'Type','Image');
cmax=max(him.CData(:));
for i=1:nsteps
    caxis([0,clim+i*10000./nsteps])
    
    png_ind=png_ind+1;
    %print(isunixispc([parentdir,'/BoutonAnalysis/docs/zoomsequence/','zoom',num2str(png_ind),'.png']),'-dpng')
end

caxis([cmax+1,cmax+2]);
png_ind=png_ind+1;
%print(isunixispc([parentdir,'/BoutonAnalysis/docs/zoomsequence/','zoom',num2str(png_ind),'.png']),'-dpng')

%Cut all the extra axons and boutons before rotating
for ax=1:4
    keep=haxons(ax).XData>=x1 & haxons(ax).XData<=x2 &...
        haxons(ax).YData>=y1 & haxons(ax).YData<=y2;
    haxons(ax).XData=haxons(ax).XData(keep);
    haxons(ax).YData=haxons(ax).YData(keep);
    haxons(ax).ZData=haxons(ax).ZData(keep);
end
hbtns=findobj(haxis,'-regexp','Tag','Bouton*');
xb=cell2mat(get(hbtns,'XData'));
yb=cell2mat(get(hbtns,'YData'));
keep=xb>=x1 & xb<=x2 &...
    yb>=y1 & yb<=y2;
del=find(~keep);
delete(hbtns(del));
hbtns=findobj(haxis,'-regexp','Tag','Bouton*');
haxis.Color=[0 0 0];
xlim([x1,x2+(x2-x1)]);ylim([y1,y2]);hold on;drawnow;
%axis ij;axis equal;
%set(haxons,'Parent',haxis);
%set(hbtns,'Parent',haxis);
z_plotted=[];
for i=1:4
    z_plotted=[z_plotted;haxons(i).ZData(:)];
end
nsteps=192;
for i=1:nsteps
    %rotate(haxons,[0 1 0],360/nsteps,[mean(xlim_final),mean(ylim_final),3*(312-mean(SVr(:,3)))])
    %rotate(hbtns,[0 1 0],360/nsteps,[mean(xlim_final),mean(ylim_final),3*(312-mean(SVr(:,3)))])
    rotate(haxons,[0 1 0],360/nsteps,[mean(xlim_final),mean(ylim_final),mean(z_plotted)])
    rotate(hbtns,[0 1 0],360/nsteps,[mean(xlim_final),mean(ylim_final),mean(z_plotted)])
    drawnow;
    
    png_ind=png_ind+1;
    %print(isunixispc([parentdir,'/BoutonAnalysis/docs/zoomsequence/','zoom',num2str(png_ind),'.png']),'-dpng')
end

%Fade in background--------------------------------------------------------
nsteps=24;
for i=1:nsteps
    caxis([0,10000+clim-i*10000./nsteps])
    
    png_ind=png_ind+1;
    %print(isunixispc([parentdir,'/BoutonAnalysis/docs/zoomsequence/','zoom',num2str(png_ind),'.png']),'-dpng')
end

