function []=EM_axons()
%This function plots intensity profiles of axons used in CLEM analysis.
%Edit pathlist.m to make sure paths point to the EM data.

pathlist;
Ff = @(x,A,mu,sigma) (ones(size(x))*A).*exp(-(x*ones(size(A))-ones(size(x))*mu).^2./(ones(size(x))*sigma.^2)./2);
mind=[27.5,26.0,20.7,0];
maxd=[47.0,42.0,36.3325,15];
cc=lines(4);

pos={'northwest','northeast','southwest','southeast'};
channel='G';
for ax=1:4
    A=load(isunixispc([profile_pth,sprintf('DL100I005\\A%.3d.mat',ax)]));
    dind=A.d.optim>mind(ax) & A.d.optim<maxd(ax); %Restrict displayed profile 
    %to what is reconstructed in EM
    
    figure(ax+2300),clf(ax+2300),movegui(ax+2300,pos{ax})
    title(sprintf('A%.3d.mat',ax));
    
    %Calculate mean gaussian background
    gauss_bg=zeros(size(A.d.optim));
    for i=1:numel(A.fit.G.('Gauss2').bg.ind)
        temp=A.fit.G.('Gauss2').bg;
        gauss_bg=gauss_bg+Ff(A.d.optim,temp.amp(i),temp.mu(i),temp.sig(i));
    end
    display(mean(gauss_bg));
    
    %Calculate mean gaussian background
    LoGxy_bg=zeros(size(A.d.optim));
    for i=1:numel(A.fit.G.('LoGxy').bg.ind)
        temp=A.fit.G.('LoGxy').bg;
        LoGxy_bg=LoGxy_bg+Ff(A.d.optim,temp.amp(i),temp.mu(i),temp.sig(i));
    end
    
    %Normalized LoGxy intensity profile
    LoGxy_fg=A.I.G.('LoGxy').norm./mean(gauss_bg);
    if ax==1
        dd=A.d.optim(dind);dd=dd(end)-dd;
        dd=dd*1.20;
        plot(dd,LoGxy_fg(dind),'-','Color',cc(ax,:)),hold on
    else
        dd=A.d.optim(dind);
        dd=dd-dd(1);
        dd=dd*1.20;
        plot(dd,LoGxy_fg(dind),'-','Color',cc(ax,:)),hold on
        %xlim([min(A.d.optim(dind)),min(A.d.optim(dind))+20])
    end
    xlim([min(dd),min(dd)+25])
    ylim([-1 17]);
    %set(gca,'XTick',[min(A.d.optim(dind)):5:A.d.optim(dind)+20]);
    set(gca,'XTick',[min(dd):2.5:min(dd)+25]);
    set(gca,'YTick',0:4:16);
    %plot(A.d.optim(dind),LoGxy_bg(dind),'-','Color',cc(ax,:)),hold on
    drawnow;
    id=find(A.d.optim(A.fit.G.('LoGxy').fg.ind)>mind(ax) & A.d.optim(A.fit.G.('LoGxy').fg.ind)<maxd(ax));
    for b=1:numel(id)
        filtername='LoGxy';
        Filteramp=A.I.G.(filtername).norm(A.fit.G.('LoGxy').fg.ind(id(b)))./median(A.I.G.(filtername).norm);
        LoGxyamp=A.fit.G.('LoGxy').fg.amp(id(b));
        LoGxysig=A.fit.G.('LoGxy').fg.sig(id(b));
        LoGxybgrnd=LoGxy_bg(A.fit.G.('LoGxy').fg.ind(id(b)));
        btn_weight=(LoGxyamp+LoGxybgrnd)./mean(gauss_bg);
        %To use below text, replace with the x-coordinate used above, i.e. 'dd'.
        %text(A.d.optim(A.fit.G.('LoGxy').fg.ind(id(b))),LoGxy_fg(A.fit.G.('LoGxy').fg.ind(id(b)))+0.5,...
        %   sprintf('%.2f,\n %0.2f',LoGxyamp,btn_weight),'Color',cc(ax,:),'HorizontalAlignment','center','FontSize',15);
    end
    grid on;
    %}
end
end