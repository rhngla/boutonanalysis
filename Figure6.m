function []=Figure6()
%This code recreates plots in Figure 6 of Gala_2017.
%This code plots the comparison of control data vs. long term imaging
%experiment data showing how the probabilistic definition of boutons can be
%used to calculate bouton addition, elimination and also significant
%changes in weight as a function of time.

%-----Parameters for probability calculations----
thr=2;
alpha=0.2389;
level=1.96^2;
%------------------------------------------------

%The original data were obtained using analysis_loaddata in the
%/release folder. Conditions data was converted into new format.
%Dataset was refined and overwritten. Qualitatively similar results are
%obtained. The data used to create the plots in paper
parentdir=pwd;
parentdir=parentdir(1:(strfind(parentdir,'Plasticity')+numel('Plasticity')-1));

%Figure6_v1.mat contains the dataset for the paper;(Figure6_v2,Figure6_v3
%etc are later versions of this file). See note above.
load(isunixispc([parentdir,'/BoutonAnalysis/dat/Figure6_v1.mat']),'D83','D85','D101','D102','D88','D01');


D01=apply_normalization(D01);
D83=apply_normalization(D83);
D85=apply_normalization(D85);
D101=apply_normalization(D101);
D102=apply_normalization(D102);
D88=apply_normalization(D88);

D01=calc_probability(D01,alpha,thr,1);
D83=calc_probability(D83,alpha,thr,2);
D85=calc_probability(D85,alpha,thr,2);
D101=calc_probability(D101,alpha,thr,2);
D102=calc_probability(D102,alpha,thr,2);
D88=calc_probability(D88,alpha,thr,2);

%Combining datasets:
Dx=[];alignby='t_stimonly';
%Dx=[];alignby='t_start';
% t_start
% t_stimaudio
% t_stimonly
[Dx,D83]=combine_datasets(D83,Dx,alignby);
[Dx,D85]=combine_datasets(D85,Dx,alignby);
[Dx,D101]=combine_datasets(D101,Dx,alignby);
[Dx,D102]=combine_datasets(D102,Dx,alignby);
[Dx,D88]=combine_datasets(D88,Dx,alignby);

%Ifg contains correctly normalized amp+bg or profile intensity.
%amp is nan if the peak was no peak was detected/ marked

calc_measure(D01.G.LoGxy.Ifg,D01.G.LoGxy.P,~isnan(D01.G.LoGxy.amp),1);
% calc_measure(D83.G.LoGxy.Ifg,D83.G.LoGxy.P,~isnan(D83.G.LoGxy.amp),2)
% calc_measure(D85.G.LoGxy.Ifg,D85.G.LoGxy.P,~isnan(D85.G.LoGxy.amp),3)
% calc_measure(D101.G.LoGxy.Ifg,D101.G.LoGxy.P,~isnan(D101.G.LoGxy.amp),4)
% calc_measure(D102.G.LoGxy.Ifg,D102.G.LoGxy.P,~isnan(D102.G.LoGxy.amp),5)
% calc_measure(D88.G.LoGxy.Ifg,D88.G.LoGxy.P,~isnan(D88.G.LoGxy.amp),6)
calc_measure(Dx.Ifg,Dx.P,~isnan(Dx.amp),7);


significantdelta_count(D01.G.LoGxy.P,D01.G.LoGxy.Ifg,~isnan(D01.G.LoGxy.amp),alpha,level,1);
% significantdelta_count(D83.G.LoGxy.P,D83.G.LoGxy.Ifg,~isnan(D83.G.LoGxy.amp),alpha,level,2);
% significantdelta_count(D85.G.LoGxy.P,D85.G.LoGxy.Ifg,~isnan(D85.G.LoGxy.amp),alpha,level,3);
% significantdelta_count(D101.G.LoGxy.P,D101.G.LoGxy.Ifg,~isnan(D101.G.LoGxy.amp),alpha,level,4);
% significantdelta_count(D102.G.LoGxy.P,D102.G.LoGxy.Ifg,~isnan(D102.G.LoGxy.amp),alpha,level,5);
% significantdelta_count(D88.G.LoGxy.P,D88.G.LoGxy.Ifg,~isnan(D88.G.LoGxy.amp),alpha,level,6);
significantdelta_count(Dx.P,Dx.Ifg,~isnan(Dx.amp),alpha,level,7);

figure(10),title(['Aligned by ',alignby(3:end)])
figure(11),title(['Aligned by ',alignby(3:end)])
figure(12),title(['Aligned by ',alignby(3:end)])
end

function D=apply_normalization(D)
filter='LoGxy';
X=~isnan(D.G.(filter).amp);
I=D.G.(filter).amp+D.G.(filter).Ibg;
I(isnan(I))=D.G.(filter).Inorm(isnan(I));
d=D.G.(filter).d;
normtype='gauss_norm';%Options: gauss1_norm,gauss2_norm,fit_norm,profile_norm,none
axlbl=unique(D.G.(filter).ax_id,'stable');
ax_id=D.G.(filter).ax_id;
Nf=ones(size(D.G.(filter).amp,1),numel(axlbl));
ax_length=ones(size(D.G.(filter).amp,1),numel(axlbl));

for ax=1:numel(axlbl)
    ax_id(ax_id==axlbl(ax))=ax;
    for ti=1:size(D.G.(filter).norm,1)
        if ~strcmp(normtype,'none')
            Nf(ti,ax)=D.G.(filter).norm{ti,ax}.(normtype);
        end
        ax_length(ti,ax)=D.G.(filter).norm{ti,ax}.axlen_legitimate;
    end
end

disp('Normalization averaged over time: ')
Nf=mean(Nf,1);
Nf=bsxfun(@times,ones(size(D.G.(filter).amp,1),1),Nf);
display(Nf)

for ax=1:numel(axlbl)
    ax_id(ax_id==axlbl(ax))=ax;
    for ti=1:size(D.G.(filter).norm,1)
        I(ti,ax_id==ax)=I(ti,ax_id==ax)./Nf(ti,ax);
    end
end
D.G.LoGxy.Ifg=I;
end

function D=calc_probability(D,alpha,thr,sel)
I=D.G.LoGxy.Ifg;
P=nan(size(I));

I(I<eps)=abs(eps);
sigma=(alpha*I(:)./2).^0.5;
x=(thr-I(:))./((2^0.5)*sigma);
P(:)=(1/2)*(1-erf(x));
D.G.LoGxy.P=P;

display(mean(sum(P,2)))

if sel==1
    Ff = @(x,A,mu,sigma) (ones(size(x))*A).*exp(-(x*ones(size(A))-ones(size(x))*mu).^2./(ones(size(x))*sigma.^2)./2);
    mu=[3 1.5];
    sigma=((alpha*mu)./2).^0.5;
    amp=1./(2*pi*sigma.^2).^0.5;
    
    x=0:0.02:5;
    xp=thr:0.02:5;
    figure(13),clf(13),movegui(13,'south')
    cc=lines(2);
    for i=1:numel(mu)
        y=Ff(x,amp(i),mu(i),sigma(i));
        patchx=[xp(1),xp,xp(end)];
        patchy=[0,Ff(xp,amp(i),mu(i),sigma(i)),0];
        %Used for Gaussian shaped patches that were shaded using powerpoint
        plot(x,y,'-','Color',cc(i,:),'LineWidth',1.5),hold on
        plot(patchx,patchy,'-','Color',cc(i,:),'LineWidth',1.5),hold on
    end
    xlabel('Bouton weight');ylabel('Probability density');
    xlim([0 5]);ylim([0 1.2])
    axis square,box on;
    drawnow;
end
end

function [] = calc_measure(~,P,detected,sel)
Pinit=repmat(P(1,:),size(P,1)-1,1);
Padded=(1-Pinit).*P(2:end,:);
Peliminated=Pinit.*(1-P(2:end,:));
t=4*(1:1:size(Pinit,1));

Padded_temp=nan((size(P,1)-1));
Peliminated_temp=nan((size(P,1)-1));
nbtns=nan((size(P,1)-1));
%Averaging over different starting points
for t0=1:(size(P,1)-1)
    Ptemp=P(t0:end,:);
    Pinit_temp=repmat(Ptemp(1,:),size(Ptemp,1)-1,1);
    Padded_temp(1:size(Pinit_temp,1),t0)=sum(((1-Pinit_temp).*Ptemp(2:end,:))>0.95,2);
    Peliminated_temp(1:size(Pinit_temp,1),t0)=sum((Pinit_temp.*(1-Ptemp(2:end,:)))>0.95,2);
    nbtns(:,t0)=sum(Pinit_temp(1,:));
end
display(mean(sum(P)))

Padded_avg=nansum(Padded_temp./nbtns,2)./sum(~isnan(Padded_temp),2);
Peliminated_avg=nansum(Peliminated_temp./nbtns,2)./sum(~isnan(Padded_temp),2);


%Generate bootstrap plots
nbsiter=1000;
bs_added=nan(size(P,1)-1,nbsiter);
bs_eliminated=nan(size(P,1)-1,nbsiter);
for bsiter=1:nbsiter
    sP=P;
    
    for b=1:size(sP,2)
        ind=randperm(size(P,1));
        sP(:,b)=P(ind,b);
    end
    sPinit=repmat(sP(1,:),size(sP,1)-1,1);
    snboutons=sum(sPinit,2);
    sPadded=(1-sPinit).*sP(2:end,:);
    sPeliminated=sPinit.*(1-sP(2:end,:));
    bs_added(:,bsiter)=sum(sPadded>0.95,2)./snboutons;
    bs_eliminated(:,bsiter)=sum(sPeliminated>0.95,2)./snboutons;
end
bsaddedval=mean(bs_added(:));
bseliminatedval=mean(bs_eliminated(:));

if sel==1
    cc=[0.8 0 0];
    figure(10),movegui(10,'northwest')
    nboutons=sum(Pinit,2);
    yy=sum(Padded>0.95,2)./nboutons;
    err=(sum(Padded>0.95,2)).^0.5./nboutons;
    err=mean(err)./(numel(err)).^0.5;
    plot(0,mean(yy),'Marker','.','MarkerSize',15,'Color',cc),hold on
    errorbar(0,mean(yy),err,'Marker','none','Color',cc),hold on
    plot([-2 50],[bsaddedval,bsaddedval],'-','Color',[1 0 0])
    drawnow;
    
    figure(11),movegui(11,'north')
    yy=sum(Peliminated>0.95,2)./nboutons;
    err=(sum(Peliminated>0.95,2)).^0.5./nboutons;
    err=mean(err)./(numel(err)).^0.5;
    plot(0,mean(yy),'Marker','.','MarkerSize',15,'Color',cc),hold on
    errorbar(0,mean(yy),err,'Marker','none','Color',cc),hold on
    plot([-2 50],[bseliminatedval,bseliminatedval],'-','Color',[1 0 0])
    
elseif sel>1
    %cc=[0.2 0.2 0.2];
    cc=lines(7);
    cc(6,:)=[0 0 0];
    cc=cc(sel-1,:);
    figure(10)
    
    %Only boutons that are present in a given session are counted from the
    %first session:
    temp=P(2:end,:);
    Pinit(isnan(temp))=nan;
    nboutons=nansum(Pinit,2);
    
    %Padded(isnan(Padded))=-inf;
    %Peliminated(isnan(Peliminated))=-inf;
    
    err=(sum(Padded>0.95,2)).^0.5./nboutons;
    if sel==7
        plot(t,sum(Padded>0.95,2)./nboutons,'.','MarkerSize',15,'Color',cc),hold on
        %plot(t,Padded_avg,'Marker','o','MarkerSize',15,'LineStyle','none','Color',cc),hold on
        errorbar(t,sum(Padded>0.95,2)./nboutons,err,'Marker','none','Color',cc,'LineStyle','none');hold on
        %plot([-2 50],[bsaddedval,bsaddedval],'-','Color',[0.5 0.5 0.5])
    else
        errorbar(t,sum(Padded>0.95,2)./nboutons,err,'Marker','none','Color',cc);hold on
    end
    xlabel('Time (days)');ylabel('Fraction of significant bouton additions')
    ylim([-0.005 0.105]);xlim([-2 26])
    set(gca,'XTick',[0:4:max(t)],'YTick',[0:0.02:0.2])
    set(gca,'FontName','Calibri','FontSize',13);
    box on, axis square;
    drawnow
    
    figure(11)
    err=(sum(Peliminated>0.95,2)).^0.5./nboutons;
    
    if sel==7
        plot(t,sum(Peliminated>0.95,2)./nboutons,'.','MarkerSize',15,'Color',cc),hold on
        errorbar(t,sum(Peliminated>0.95,2)./nboutons,err,'Marker','none','Color',cc,'LineStyle','none');hold on
    else
        errorbar(t,sum(Peliminated>0.95,2)./nboutons,err,'Marker','none','Color',cc);hold on
    end
    %plot(t,Peliminated_avg,'Marker','o','MarkerSize',15,'LineStyle','none','Color',cc),hold on
    %plot([-2 50],[bseliminatedval,bseliminatedval],'-','Color',[0.5 0.5 0.5])
    xlabel('Time (days)');ylabel('Fraction of significant bouton eliminations')
    %ylim([0 0.185]);xlim([-2 max(t)+2])
    ylim([-0.005 0.105]);xlim([-2 26])
    set(gca,'XTick',[0:4:max(t)],'YTick',[0:0.02:0.2])
    set(gca,'FontName','Calibri','FontSize',13);
    box on, axis square;
    
end
end


function [] = significantdelta_count(P,I,detected,alpha,level,sel)
%var_size=@(x) (1/2)*alpha*x;
I(I<0)=eps;

if sel==1
    nbsiter=1000;%1000 used for paper
    %P=P([1,2,3],:);
    %I=I([1,2,3],:);
else
    nbsiter=1;
end

fracmat=[];
errmat=[];
for i=1:nbsiter
    sP=P;
    sI=I;
    
    %Shuffle from the second iteration onwards
    if i>1
        for b=1:size(sP,2)
            ind=randperm(size(P,1));
            sP(:,b)=P(ind,b);
            sI(:,b)=I(ind,b);
        end
    end
    
    %If control data, then all sessions are compared to the first session
    %simultaneously
    if sel==1
        Ptemp=[];
        Itemp=[];
        for t=2:size(sP,1)
            Ptemp=[Ptemp,sP([1,t],:)];
            Itemp=[Itemp,sI([1,t],:)];
        end
        sP=Ptemp;
        sI=Itemp;
    end
    
    frac=nan(size(sI,1)-1,1);
    err=nan(size(sI,1)-1,1);
    n=1;
    for t=2:size(sI,1)
        ind=1:size(sP,2);
        %P_significant_change=(sI(t,ind)-sI(t0,ind)).^2./(var_size(sI(t,ind))+var_size(sI(t0,ind)))>level;
        %P_test=0.5*(1+erf(abs(I(t,ind)-I(t0,ind))./(((I(t,ind)+I(t0,ind)).^0.5)*alpha.^0.5)));
        %display([P_test(:)>0.975,P_significant_change(:)]); %One-sided
        %and two sided comparisons cause the difference in level.
        
        P_pot=0.5*(1+erf((sI(t,ind)-sI(1,ind))./(((sI(t,ind)+sI(1,ind)).^0.5)*alpha.^0.5))).*sP(1,:).*sP(t,:);
        P_dep=0.5*(1+erf((sI(1,ind)-sI(t,ind))./(((sI(t,ind)+sI(1,ind)).^0.5)*alpha.^0.5))).*sP(1,:).*sP(t,:);
        
        frac(n)=sum(P_pot>0.95)+sum(P_dep>0.95);
        err(n)=frac(n).^0.5;
        
        %frac(n)=frac(n)./sum(sP(1,:));
        %err(n)=err(n)./sum(sP(1,:));
        
        frac(n)=frac(n)./sum(sP(1,~isnan(sP(t,:))));
        err(n)=err(n)./sum(sP(1,~isnan(sP(t,:))));
        
        n=n+1;
    end
    fracmat=[fracmat,frac];
    errmat=[errmat,err];
end

frac=fracmat(:,1);
err=errmat(:,1);
fracmat=fracmat(:,2:end);
errmat=errmat(:,2:end);
t=4*(1:numel(frac));
if sel==1
    %Plot averaged measure for conditions:
    %pval = normcdf(level^0.5);
    %pval=(1-pval)*2;
    figure(12),movegui(12,'northeast')
    plot(0,nanmean(frac),'.','MarkerSize',15,'LineStyle','none','Color',[0.8 0 0]),hold on
    errorbar(0,nanmean(frac),nanmean(err),'Marker','none','Color',[0.8 0 0]),hold on
    plot([-2 50],[mean(fracmat),mean(fracmat)],'-','Color',[1 0 0]),hold on
elseif sel>1
    %Plot full curve for main dataset
    cc=lines(7);
    cc(6,:)=[0 0 0];
    cc=cc(sel-1,:);
    figure(12);
    if sel==7
        plot(t,frac,'.','MarkerSize',15,'Color',cc),hold on
        errorbar(t,frac,err,'Marker','none','LineStyle','none','Color',cc),hold on
    else
        errorbar(t,frac,err,'Marker','none','Color',cc),hold on
    end
end

%xlim([-2 max(t)+2]),ylim([0 0.40]);axis square;box on;
ylim([0 0.35]);xlim([-2 26])

xlabel('Time (Days)');ylabel('Fraction changed significantly')
set(gca,'XTick',0:4:max(t),'YTick',0:0.1:0.6)
set(gca,'FontName','Calibri','FontSize',13);
drawnow;
end


function [Dx,Dat]=combine_datasets(Dat,Dx,alignby)

if strcmp(alignby,'t_start')
    alignedtime=Dat.L.im_t-Dat.L.im_t(1);
else
    alignedtime=Dat.L.im_t-Dat.L.(alignby);
end
poi=find(alignedtime>=0,1,'first');
display([alignby,' ',num2str(poi)])

fnames={'P','amp','Ifg'};
if isempty(Dx)
    Dx=struct('P',[],'amp',[],'Ifg',[]);
    for f=1:numel(fnames)
        Dx.(fnames{f})=Dat.G.LoGxy.(fnames{f})(poi:end,:);
    end
else
    for f=1:numel(fnames)
        %Appending to the combined set
        tsiz=max([size(Dx.(fnames{f}),1),size(Dat.G.LoGxy.(fnames{f})(poi:end,:),1)]);
        nbtn=size(Dx.(fnames{f}),2)+size(Dat.G.LoGxy.(fnames{f})(poi:end,:),2);
        temp=nan(tsiz,nbtn);
        temp(1:size(Dx.(fnames{f}),1),1:size(Dx.(fnames{f}),2))=Dx.(fnames{f});
        temp(1:size(Dat.G.LoGxy.(fnames{f})(poi:end,:),1),1+size(Dx.(fnames{f}),2):end)=Dat.G.LoGxy.(fnames{f})(poi:end,:);
        Dx.(fnames{f})=temp;
    end
end

%Cutting the original data
for f=1:numel(fnames)
    %Cutting the original data
    Dat.G.LoGxy.(fnames{f})=Dat.G.LoGxy.(fnames{f})(poi:end,:);
end
end

