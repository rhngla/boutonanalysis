function [] = Figure1()
%This plots reproduces Figure 1 of Gala_2017 to illustrate challenges in 
%bouton detection.

parentdir=pwd;
parentdir=parentdir(1:(strfind(parentdir,'Plasticity')+numel('Plasticity')-1));

%IMorig=Original(550:650,120:220,90:140);
Im=load('E:\DatasetIM\DL083B001G','Original');
Im=double(Im.Original);
pth=isunixispc([parentdir,'\dat\Profiles_v3\DL083B001\']);


fname={'A090','A089','A088','A087','A086','A085'}';
xx=[110,230];yy=[544,664];zz=[90,150];
cc=[0 1000];

IMxz=zeros(1024,312);
IMzy=zeros(312,1024);
IMxy=zeros(1024,1024);
for f=1:numel(fname)
    A=load([pth,fname{f},'.mat']);
    A=genproj(A,A.AM.optim,A.r.optim,Im,xx,yy,zz);
    IMxz=max(IMxz,A.proj.G.xz.ax);
    IMzy=max(IMzy,A.proj.G.zy.ax);
    IMxy=max(IMxy,A.proj.G.xy.ax);
    %figure,imshow(A.proj.G.xy.ax),caxis([cc(1) cc(2)]),title(A.id)
end

IMpartz=max(Im(:,:,zz(1):zz(2)),[],3);
IMfull=max(Im,[],3);

IMzy=IMzy(zz(1):zz(2),xx(1):xx(2));
figure,imshow(IMzy),caxis([cc(1) cc(2)]);%,xlim([xx(1) xx(2)]),ylim([zz(1) zz(2)]),
set(gca,'position',[0 0 1 1],'units','normalized')
drawnow;

IMxy=IMxy(yy(1):yy(2),xx(1):xx(2));
figure,imshow(IMxy),caxis([cc(1) cc(2)]);%,xlim([xx(1) xx(2)]),ylim([yy(1),yy(2)])
set(gca,'position',[0 0 1 1],'units','normalized')
drawnow;

figure,imshow(IMpartz),caxis([cc(1) cc(2)]),xlim([0 1024]),ylim([0 1024]),hold on
plot([xx(1),xx(1)],[yy(1),yy(2)],'-w','LineWidth',1)
plot([xx(2),xx(2)],[yy(1),yy(2)],'-w','LineWidth',1)
plot([xx(1),xx(2)],[yy(1),yy(1)],'-w','LineWidth',1)
plot([xx(1),xx(2)],[yy(2),yy(2)],'-w','LineWidth',1)
set(gca,'position',[0 0 1 1],'units','normalized')
drawnow;

figure,imshow(IMfull),caxis([cc(1) cc(2)]),xlim([0 1024]),ylim([0 1024]),hold on
plot([xx(1),xx(1)],[yy(1),yy(2)],'-w','LineWidth',1)
plot([xx(2),xx(2)],[yy(1),yy(2)],'-w','LineWidth',1)
plot([xx(1),xx(2)],[yy(1),yy(1)],'-w','LineWidth',1)
plot([xx(1),xx(2)],[yy(2),yy(2)],'-w','LineWidth',1)
set(gca,'position',[0 0 1 1],'units','normalized')
drawnow;

end

function A=genproj(A,AM,r,Im,xx,yy,zz)
sizeIm=size(Im);%Size of all channels is the same.

keepind=r(:,1)>yy(1) & r(:,1)<yy(2) &...
    r(:,2)>xx(1) & r(:,2)<xx(2) &...
    r(:,3)>zz(1) & r(:,3)<zz(2);
r=r(keepind,:);
AM=AM(:,keepind);
AM=AM(keepind,:);

pad=20;
minr=min(r,[],1);
maxr=max(r,[],1);
minx=round(max(minr(1)-pad,1));maxx=round(min(maxr(1)+pad,sizeIm(1)));
miny=round(max(minr(2)-pad,1));maxy=round(min(maxr(2)+pad,sizeIm(2)));
minz=round(max(minr(3)-pad,1));maxz=round(min(maxr(3)+pad,sizeIm(3)));
[~,SVr,~]=AdjustPPM(AM,r,zeros(size(r,1),1),1);

%Perform fast marching on restricted volume:
%ppm is adjusted for the profile in run_optim.m
params.profile.ppm=4;                          %default: profile.ppm=4;
params.profile.umpervox=[0.26,0.26,0.80];      %default: profile.ppm=4;
%For projections
params.proj.fm_dist=5;                         %default: proj.fm_dist=7

[KT]=FastMarchingTube([maxx-minx+1,maxy-miny+1,maxz-minz+1],[SVr(:,1)-minx+1,SVr(:,2)-miny+1,SVr(:,3)-minz+1],params.proj.fm_dist,[1 1 1]);
Filter=false(sizeIm);
Filter(minx:maxx,miny:maxy,minz:maxz)=KT;
Filter=double(Filter);
Im_ax=Filter.*Im;
A.proj.G.xy.ax=max(permute(Im_ax,[1,2,3]),[],3);
A.proj.G.zy.ax=max(permute(Im_ax,[3,2,1]),[],3);
A.proj.G.xz.ax=max(permute(Im_ax,[1,3,2]),[],3);
end