%This function takes the profile data structure loaded directly from the
%file and displays the profile+fit and image projections.
function inspectprofile(Dat)

channel='G';
filter='LoGxy';

isshowbg=true;
isshowfgpeaks=true;
isshowignore=true;
isshowfit=true;

isshowproj=true;
projlist={'xy','zx'};

pathlist;

Ff = @(x,A,mu,sigma) (ones(size(x))*A).*exp(-(x*ones(size(A))-ones(size(x))*mu).^2./(ones(size(x))*sigma.^2)./2);

figure('name',Dat.id),
plot(Dat.d.optim,Dat.I.(channel).(filter).norm,'-'),hold on
if isshowfgpeaks
    for i=1:numel(Dat.fit.(channel).(filter).fg.ind)
        temp=Dat.fit.(channel).(filter).fg;
        xx=(-5:0.1:+5)+temp.mu(i);
        plot(xx,Ff(xx,temp.amp(i),temp.mu(i),temp.sig(i)),'-','Color',[0.4 0.4 0.4]),hold on;
        text(temp.mu(i),temp.amp(i)+0.5,num2str(i),'Color',[0 0.8 0.4],'FontSize',13,'HorizontalAlignment','center');
        text(temp.mu(i),-0.2,num2str(round(temp.amp(i),2)),'Color',[0.3 0.3 0.3],'FontSize',13,'HorizontalAlignment','center');
    end
end

if isshowbg
    bg=zeros(size(Dat.d.optim));
    for i=1:numel(Dat.fit.(channel).(filter).bg.ind)
        temp=Dat.fit.(channel).(filter).bg;
        bg=bg+Ff(Dat.d.optim,temp.amp(i),temp.mu(i),temp.sig(i));
    end
    plot(Dat.d.optim,bg,'-','Color',[1 0 0])
end

if isshowfit
    tot_fit=zeros(size(Dat.d.optim));
    for i=1:numel(Dat.fit.(channel).(filter).bg.ind)
        temp=Dat.fit.(channel).(filter).bg;
        tot_fit=tot_fit+Ff(Dat.d.optim,temp.amp(i),temp.mu(i),temp.sig(i));
    end
    for i=1:numel(Dat.fit.(channel).(filter).fg.ind)
        temp=Dat.fit.(channel).(filter).fg;
        tot_fit=tot_fit+Ff(Dat.d.optim,temp.amp(i),temp.mu(i),temp.sig(i));
    end
    plot(Dat.d.optim,tot_fit,'-','Color',[1 0 0])
    %plot(Dat.d.optim,Dat.I.G.LoG.caliber,'--','Color',[1 0 0])
    %plot(Dat.d.optim,Dat.I.(channel).(filter).norm-tot_fit,'-','Color',[0 0 1])
end

if isshowignore
    ind=Dat.annotate.ignore;
    %To plot patch
    diffind=diff(ind);
    startind=find(diffind>0);
    if ind(1)==1
        startind=[1;startind(:)];
    end
    endind=find(diffind<0);
    if ind(end)==1
        endind=[endind(:);length(ind)];
    end
    if ~isempty(startind)
        patchx=zeros(4,numel(startind));
        patchy=zeros(4,numel(startind));
        patchx(1,:)=Dat.d.optim(startind);
        patchx(2,:)=Dat.d.optim(endind);
        patchx(3,:)=patchx(2,:);patchx(4,:)=patchx(1,:);
        patchy(1,:)=0;
        patchy(2,:)=patchy(1,:);
        patchy(3,:)=10;
        patchy(4,:)=patchy(3,:);
        patch(patchx,patchy,[0,0,0],'FaceAlpha',0.25,'EdgeColor','none');
        drawnow
    end
end
xlim([min(Dat.d.optim),max(Dat.d.optim)]);ylim([0 10])
ylabel('Intensity')
xlabel('d in \mu{m}')
grid on,box on
set(gca,'FontName','Calibri','FontSize',13);
drawnow



if isshowproj
    for p=1:numel(projlist)
        if exist(isunixispc([proj_pth,Dat.id,'-',projlist{p},'.mat']),'file')
            temp=load(isunixispc([proj_pth,Dat.id,'-',projlist{p},'.mat']));
            if strcmp(projlist{p},'xy')
                sep=2;ex=1;ey=1;%(positive value=down/right in image)
                offset=round(sep.*[-ex,ey]./((ex.^2+ey.^2).^0.5));
                perm=[1,2,3];
            elseif strcmp(projlist{p},'yz')
                perm=[3,2,1];
                offset=[5 0];
            elseif strcmp(projlist{p},'zx')
                perm=[1,3,2];
                offset=[0 5];
            end
            
            figure('name',[Dat.id,'-',projlist{p}])
            imshow(temp.IM(:,:,1)),hold on;
            
            temp=Dat.fit.(channel).(filter).fg;
            plot(Dat.r.optim(temp.ind,perm(2)),Dat.r.optim(temp.ind,perm(1)),'s','Color',[0.8 0 0],'LineWidth',1,'MarkerSize',12)
            plot(Dat.r.optim(Dat.annotate.ignore,perm(2)),Dat.r.optim(Dat.annotate.ignore,perm(1)),'.','Color',[0.8 0 0]),hold on
            text(Dat.r.optim(temp.ind,perm(2))-offset(2),Dat.r.optim(temp.ind,perm(1))-offset(1),num2str((1:numel(temp.ind))'),'Color',[0 0.8 0.4],'FontSize',13,'HorizontalAlignment','center');
            %text(Dat.r.optim(temp.ind,perm(2))-offset(2),Dat.r.optim(temp.ind,perm(1))-offset(1),num2str(temp.id(:)),'Color',[0 0.8 0.4],'FontSize',13,'HorizontalAlignment','center');
            caxis([0 7*mean(Dat.I.(channel).(filter).raw(~Dat.annotate.ignore))])
            drawnow;
        else
            display([[proj_pth,Dat.id,'-',projlist{p},'.mat'],' not found'])
        end
    end
end



