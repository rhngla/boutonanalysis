function []=analysis_conditions(D)
%This function prepares figures for comparison over conditions. Histogram
%for conflicts, and precision-recall vs size curves are calculated.
%The first condition is considered to be ground truth.

% ----------------------------------Conditions:--------------------------------
% | file | power | pmt |  lz | blur  | agarose notes | Relabeledname | Status
% |    1 |    50 | 720 | Inf | false |               | DL001A002     | traced(1)
% |    2 |    75 | 720 | Inf | false |               | DL001B002     | traced(2)
% |    4 |    37 | 720 | Inf | false |               | DL001D002     | traced(3)
% |    5 |    37 | 760 | Inf | false |               | DL001E002     | traced(4)
% |    6 |    50 | 720 | Inf | true  | thin layer    | DL001F002     | traced(5)
% |    8 |    75 | 720 | Inf | false |               | DL001H002     | traced(6)
% |    9 |    50 | 720 | Inf | false |               | DL001I002     | traced(7)

filter='LoGxy';
X=~isnan(D.G.(filter).amp);
I=D.G.(filter).amp+D.G.(filter).Ibg;
d=D.G.(filter).d;
normtype='gauss2_norm';%Options: gauss1_norm,gauss2_norm,fit_norm,profile_norm,none
axlbl=unique(D.G.(filter).ax_id,'stable');
ax_id=D.G.(filter).ax_id;
Nf=ones(size(D.G.(filter).amp,1),numel(axlbl));
ax_length=ones(size(D.G.(filter).amp,1),numel(axlbl));

condind=1:7;
for ax=1:numel(axlbl)
    ax_id(ax_id==axlbl(ax))=ax;
    for ti=1:size(D.G.(filter).norm,1)
        if ~strcmp(normtype,'none')
            Nf(ti,ax)=D.G.(filter).norm{ti,ax}.(normtype);
        end
        ax_length(ti,ax)=D.G.(filter).norm{ti,ax}.axlen_legitimate;
    end
end
%display('Normalization: ')
%display(Nf);


display('Normalization avgeraged over time: ')
Nf=mean(Nf,1);
Nf=bsxfun(@times,ones(size(D.G.(filter).amp,1),1),Nf);
display(Nf)
%}

for ax=1:numel(axlbl)
    ax_id(ax_id==axlbl(ax))=ax;
    for ti=1:size(D.G.(filter).norm,1)
        I(ti,ax_id==ax)=I(ti,ax_id==ax)./Nf(ti,ax);
    end
end


%Bouton sizes are defined by mean over automatically detected boutons on
%all optimized traces.
btn_I=nanmean(I(condind,:),1);
%Bouton intensity can alternatively be defined by first normalizing each
%session based on boutons present in all sessions followed by chooseing
%mean over the number of sessions that a given bouton is present in.

axon = [1,2,3,4,7,8,9,10,11,12,13,14,17,18,19,20];
crit=(sum(X==0,1)~=0 & btn_I>2.5);
display('-------Errors in detection----------')
display([axon(ax_id(crit));d(crit);I(:,crit)]);
display('------------------------------------')

inall=sum(isnan(I),1)==0;
%GetConflict(X(condind,:),btn_I,ax_id,'Conditions');
GetPrecisionRecall(X,btn_I,ax_id,d,'Conditions');
GetBias(I(:,inall),btn_I(inall),ax_id(inall),d(inall));
%GetNoisevsSize(I(:,inall),btn_I(inall),ax_id(inall),d(inall),'Conditions');
GetNoisevsSize2(I(:,inall),btn_I(inall),ax_id(inall),d(inall),'Conditions');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [] = GetConflict(X,btnsiz,ax,thisis)
%X consists of ones and zeros
%binedges=0.5:1:16;

Y=sum(X,1);
N=sum(1-X,1);

btnsiz(Y==0)=[];
ax(Y==0)=[];
N(Y==0)=[];
Y(Y==0)=[];

conflicts=min(Y,N);

bincen=(binedges(1:(end-1))+binedges(2:(end)))./2;
figure,movegui(gcf,'northeast')
cc=parula(5);cc(end,:)=[0.4,0.4,0.4];
h3=histcounts(btnsiz(conflicts==0),binedges);
h2=histcounts(btnsiz(conflicts==1),binedges);
h1=histcounts(btnsiz(conflicts==2),binedges);
h0=histcounts(btnsiz(conflicts==3),binedges);
bar(bincen(:),[h0(:),h1(:),h2(:),h3(:)],1,'stacked','EdgeColor',[0 0 0]);
colormap(cc)
xlabel('Bouton intensity');ylabel('Counts')
hbar=findall(gca,'Type','Bar');
legend(hbar,{'0/7 Conflicts','1/7 Conflicts','2/7 Conflicts','3/7 Conflicts'},'Fontsize',15)
axis square,box on,set(gca,'FontName','Calibri','FontSize',13);
title(thisis)
ylim([0 350])
drawnow;

display(ax(btnsiz>2 & conflicts>0))

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function []=GetPrecisionRecall(X,btnsiz,ax,d,thisis)
%X consists of ones and zeros

%binedges=(0.5:1:16)+0.5;
binedges=1:1:16;
bincen=(binedges(1:(end-1))+binedges(2:(end)))./2;
xbin=bincen;

TP=nan(size(X,1),size(X,1),(numel(binedges)-1));
FP=TP;
FN=TP;

for i=1:1;%i is gold standard & j is test
    for j=1:size(X,1)
        for b=1:(numel(binedges)-1)
            ind=binedges(b)<btnsiz & btnsiz<binedges(b+1);
            TP(i,j,b)=sum(X(i,ind) & X(j,ind));
            FP(i,j,b)=sum(~X(i,ind) & X(j,ind));
            FN(i,j,b)=sum(X(i,ind) & ~X(j,ind));
            
            %temp=btnsiz(ind);
            %display(['Max bouton size: ' num2str(max(temp(~(X(i,ind) & X(j,ind))))), ' in bin ',num2str(bincen(b))])
        end
    end
end


sizthr=0.1:0.02:20;
cumfrac=nan(size(sizthr));
for s=1:numel(sizthr)
    ind=btnsiz>sizthr(s);
    
    %All-all agreement
    %cumfrac(s)=sum(sum(X(:,ind),1)==size(X,1))./sum(ind);
    
    %Compared to gold standard (Condition A)
    Y=bsxfun(@eq,X(1,:),X(2,end,:));
    cumfrac(s)=sum(sum(Y(:,ind),1)==size(Y,1))./sum(ind);
end
figure(966),clf(966)
plot(sizthr,cumfrac,'.-')
ylabel('Fraction of errors larger than bouton weight on x axis')
xlabel('Bouton weight')
ylim([0 1])
box on,axis square,


ncomp=size(X,1)-1;
AA_P=nan(ncomp,(numel(binedges)-1));
AA_R=nan(ncomp,(numel(binedges)-1));

for b=1:(numel(binedges)-1)
    P=TP(:,:,b)./(TP(:,:,b)+FP(:,:,b));
    R=TP(:,:,b)./(TP(:,:,b)+FN(:,:,b));
    diagind=find(eye(size(P,1)));
    P(diagind)=nan;
    R(diagind)=nan;
    
    AA_P(:,b)=P(1,2:end);
    AA_R(:,b)=R(1,2:end);
end
display(squeeze(TP(1,1,:)));

cc=lines(size(X,1)+2);
figure(71),clf(71),movegui(71,'northwest')
for i=1:size(AA_P,1)
    plot(xbin,AA_P(i,:),'.-','Color',cc(i,:),'MarkerSize',15),hold on
end
%errorbar(xbin,nanmean(AA_P,1),nanstd(AA_P,[],1)./(size(AA_P,1)).^0.5,'-','Color',cc(1,:),'LineWidth',1),hold on
ylabel('Precision');xlabel('Bouton intensity')
axis square,box on,set(gca,'FontName','Calibri','FontSize',15);
set(gca,'XTick',0:4:16,'YTick',0.2:0.2:1);
ylim([0.15 1.05]);
xlim([0 16]);
drawnow

figure(72),clf(72),movegui(72,'north')
for i=1:size(AA_R,1)
    plot(xbin,AA_R(i,:),'.-','Color',cc(i,:),'MarkerSize',15),hold on
end
%errorbar(xbin,nanmean(AA_R,1),nanstd(AA_R,[],1)./(size(AA_R,1)).^0.5,'-','Color',cc(1,:),'LineWidth',1),hold on
ylabel('Recall');xlabel('Bouton intensity');
axis square,box on,set(gca,'FontName','Calibri','FontSize',15);
set(gca,'XTick',0:4:16,'YTick',0.2:0.2:1);
ylim([0.15 1.05]);
xlim([0 16]);
drawnow


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [] = GetBias(I,btnsiz,ax,d)

fprintf('Condition wise slopes \n');
cc=lines(7);
c1=1;%Gold standard
ftype=fittype('a*x');

xrange=[0 16];
xmax=xrange(2);ymax=xrange(2);
for c2=2:size(I,1)
    col_id=c2-1;
    
    xx=I(c1,:);
    yy=I(c2,:);
    [ff,gg]=fit(xx(:),yy(:),ftype,'StartPoint',1);
    
    %v=pca([xx(:),yy(:)]);
    %a=v(2,1)/v(1,1);
    %ff=@(x) a.*x;
    
    display(['==========Condition', num2str(c2)])
    %display(gg)
    figure(901),movegui(901,'northeast')
    
    scatter(xx(:),yy(:),10,'MarkerFaceColor',cc(col_id,:),'MarkerEdgeColor','none'),hold on
    plot(xrange,ff(xrange),'-','Color',cc(col_id,:)),hold on,
    drawnow;
    
    C=corrcoef(xx(:),yy(:));
    CI=confint(ff);
    %fprintf('Condition %d #boutons %d, CorrCoef ',c2,size(I,2),C(1,2));
    fprintf('Slope = %.3f, (%.3f,%.3f), R2 %.3f\n',ff.a,CI(1),CI(2),gg.rsquare);
    %fprintf('Condition %d, Slope = %.3f \n',c2,a); 
    %P-value for bias
    [h,p]=ttest((xx(:))-yy(:));
    fprintf('h = %.3f, p = %.3f t-test result \n',h,p);
    
    drawnow
end

xlim([0,xmax]),ylim([0,ymax]),axis square,box on
xlabel(['Condition ',num2str(c1)])
ylabel('Condition j')
set(gca,'FontName','Calibri','FontSize',13);
set(gca,'XTick',0:4:16,'YTick',0:4:16);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [] = GetNoisevsSize(I,btnsiz,ax,d,thisis)
%This function performs a bootstrap to estimate errorbars

% display('Bias is removed!')
% meanI=mean(I(:));
% I=bsxfun(@minus,I,mean(I,2));
% I=I+meanI;

binedges=[1:2:16];
bincen=(binedges(1:end-1)+binedges(2:end))./2;

Idiff=[];
Imean=[];
for i=1:7
    for j=1:7
        if j~=i
            Idiff=[Idiff,I(i,:)-I(j,:)];
            Imean=[Imean,mean(I([i,j],:),1)];
        end
    end
end


rem=isnan(Imean);
Idiff(rem)=[];
Imean(rem)=[];
Idiff=Idiff';
Imean=Imean';

sigvssiz=nan(numel(bincen),5);
ccc=parula(numel(bincen)+2);
for b=1:numel(bincen)
    ind=Imean<binedges(b+1) & Imean>binedges(b);
    
    II=Idiff(ind);
    display([mean(II),numel(ind),b])
    figure(75),movegui(75,'south')
    subplot(3,6,b)
    histogram(II(:),-6:0.4:6,'FaceColor',ccc(b,:))
    axis square,box on;
    xlim([-6 6]);
    xlabel(sprintf('Binedges %.2f - %.2f',binedges(b),binedges(b+1)))
    [~,p] = kstest((II(:)-mean(II))./std(II));
    title(sprintf('Sigma %.2f \nn = %d \np = %.2f',std(II(:)),sum(ind),p))
    
    rind=randi(numel(II),1000,numel(II));
    
    N=numel(II);
    mu2=mean(II(:).^2);
    mu4=mean(II(:).^4);
    varofvar=(N-1).*((N-1)*mu4-(N-3)*mu2.^2)./N.^3;
    dv=varofvar.^0.5;
    s=std(II(:));
    ds=0.5*dv./s;
    %sigvssiz(b,:)=[bincen(b),s,ds];
    sigvssiz(b,:)=[mean(Imean(ind)),s,ds,dv,std(var(II(rind),[],2))];
end

maxsiz=inf;
ftype=fittype('a*x');
fitted=sigvssiz(:,1)<maxsiz;

figure(22),clf(22),movegui(22,'southwest'),

subplot(1,2,1)
%plot(sigvssiz(:,1),sigvssiz(:,2),'x-'),hold on
errorbar(sigvssiz(:,1),sigvssiz(:,2),sigvssiz(:,3),'.','MarkerSize',13),hold on
plot(sigvssiz(fitted,1),sigvssiz(fitted,2),'or'),hold on
[f1,g1]=fit(sigvssiz(fitted,1),sigvssiz(fitted,2),ftype,'StartPoint',[1]);
plot([0 16],f1([0 16]),'-','Color',[0.5 0.5 0.5])
display(f1)
display(g1)
xlim([0 16]);ylim([0 3]),box on,axis square
xlabel('Mean intensity')
ylabel('SD in difference of intensity (\sigma)')
%title('f1')
%}

subplot(1,2,2)
%plot(sigvssiz(:,1),sigvssiz(:,2).^2,'x-'),hold on
%dv=2.*sigvssiz(:,3).*sigvssiz(:,2);
errorbar(sigvssiz(:,1),sigvssiz(:,2).^2,sigvssiz(:,4),'.','MarkerSize',13),hold on
plot(sigvssiz(fitted,1),sigvssiz(fitted,2).^2,'or'),hold on
[f2,g2,o2]=fit(sigvssiz(fitted,1),sigvssiz(fitted,2).^2,ftype,'StartPoint',[1],'Weights',1./(sigvssiz(:,4).^2));
display(f2)
display(g2)
display(o2)
plot([0 16],f2([0 16]),'-','Color',[0.5 0.5 0.5])
xlim([0 16]);ylim([0 7]),box on,axis square
xlabel('Mean intensity')
ylabel('Variance in difference of intensity')
set(gca,'FontName','Calibri','FontSize',13);
%title('f2')

for b=1:max(find(fitted))
    ind=Imean<binedges(b+1) & Imean>binedges(b);
    
    II=Idiff(ind);
    
    [cdf_dI,bins_dI]=histcounts(II(:),-6:0.4:6,'Normalization','cdf');
    %[cdf_dInorm1,bins_dInorm]=histcounts(II(:)./f1(bincen(b)),-3:0.2:3,'Normalization','cdf');
    [cdf_dInorm2,bins_dInorm]=histcounts(II(:)./(f2(bincen(b))).^0.5,-6:0.4:6,'Normalization','cdf');
    xbinsnorm=(bins_dInorm(1:end-1)+bins_dInorm(2:end))./2;
    
    figure(76),movegui(76,'southeast')
    subplot(1,2,1)
    plot(bins_dI(2:end),cdf_dI,'-','Color',ccc(b,:)),hold on
    xlim([-6 6]),ylim([0 1]);
    axis square,box on;
    xlabel('Difference in Intensity'),ylabel('Spread CDF')
    set(gca,'FontName','Calibri','FontSize',13);
    
    %{
    subplot(1,3,2)
    if b==1
        mu = 0;
        sigma = 1;
        pd=makedist('Normal',mu,sigma);
        plot(xbinsnorm,cdf(pd,xbinsnorm),'.-','Color',[0.8 0 0],'LineWidth',2),hold on
    end
    plot(xbinsnorm,cdf_dInorm1,'.-','Color',ccc(b,:)),hold on
    xlim([-3 3]),ylim([0 1]);
    axis square,box on;
    xlabel('Bins'),ylabel('CDF')
    title('Normalized using f1')
    drawnow
    %}
    
    subplot(1,2,2)
    if b==1
        mu = 0;
        sigma = 1;
        pd=makedist('Normal',mu,sigma);
        plot(xbinsnorm,cdf(pd,xbinsnorm),'-','Color',[0.8 0 0],'LineWidth',2),hold on
    end
    plot(bins_dInorm(2:end),cdf_dInorm2,'-','Color',ccc(b,:)),hold on
    xlim([-6 6]),ylim([0 1]);
    axis square,box on;
    xlabel('Difference in Intensity'),ylabel('CDF')
    %title('Normalized using f2')
    set(gca,'FontName','Calibri','FontSize',13);
    drawnow
end

%{
figure(23),clf(23),movegui(23,'south'),
plot(Imean,Idiff,'.'),hold on
xlim([0 20]);ylim([-15 15]),box on,axis square
xlabel('Mean intensity (over pair of conditions)')
ylabel('Pairwise difference in Intensity')
%}


function [] = GetNoisevsSize2(I,btnsiz,ax,d,thisis)

% display('Bias is removed!')
% meanI=mean(I(:));
% I=bsxfun(@minus,I,mean(I,2));
% I=I+meanI;

binedges=1:2:17;
bincen=(binedges(1:end-1)+binedges(2:end))./2;

n_iter=1000;
Ibsmeans=nan(numel(bincen),n_iter);
Ibsdiff=nan(numel(bincen),n_iter);
Ibs=I;

ccc=parula(numel(bincen)+2);
for ii=1:n_iter
    %Calculate pairwise difference
    Idiff=[];
    Imean=[];
    for i=1:7
        for j=1:7
            if j~=i
                Idiff=[Idiff,Ibs(i,:)-Ibs(j,:)];
                Imean=[Imean,mean(Ibs([i,j],:),1)];
            end
        end
    end
    
    rem=isnan(Imean);
    Idiff(rem)=[];
    Imean(rem)=[];
    Idiff=Idiff';
    Imean=Imean';
    
    if ii==1
        sigvssiz=nan(numel(bincen),5);
        Idiff_orig=Idiff;
        Imean_orig=Imean;
    end
    
    for b=1:numel(bincen)
        ind=Imean<binedges(b+1) & Imean>binedges(b);
        Idiff_inbin=Idiff(ind);
        Imean_inbin=Imean(ind);
        
        if ii==1
            rind=randi(numel(Idiff_inbin),1000,numel(Idiff_inbin));
            N=numel(Idiff_inbin);
            mu2=mean(Idiff_inbin(:).^2);
            mu4=mean(Idiff_inbin(:).^4);
            varofvar=(N-1).*((N-1)*mu4-(N-3)*mu2.^2)./N.^3;
            dv=varofvar.^0.5;
            s=std(Idiff_inbin(:));
            ds=0.5*dv./s;

            %sigvssiz(b,:)=[mean(Imean_inbin),s,ds,dv,std(var(Idiff_inbin(rind),[],2))];
            sigvssiz(b,:)=[bincen(b),s,ds,dv,std(var(Idiff_inbin(rind),[],2))];
            %dv and std(var(Idiff_inbin,[],2)) are expected to be the same.
        end
        
        Ibsmeans(b,ii)=mean(Imean_inbin);
        Ibsdiff(b,ii)=var(Idiff_inbin);
        
        if ii==1
        display(['Number in bin ',num2str(numel(Idiff_inbin))])
        end
        
    end
    %fprintf('%0.2f %% Complete ...\n',ii*100./n_iter);
    %Generate bootstrap only from second iteration onwards
    rind=randi(size(I,1),size(I,1),size(I,2));
    rind=bsxfun(@plus,rind,((1:size(I,2))-1).*size(I,1));
    Ibs=I(rind);
end

maxsiz=inf;
ftype=fittype('a*x');
fitted=sigvssiz(:,1)<maxsiz;

figure(22),clf(22),movegui(22,'southwest'),
subplot(1,3,1)
%plot(sigvssiz(:,1),sigvssiz(:,2),'x-'),hold on
errorbar(sigvssiz(:,1),sigvssiz(:,2),sigvssiz(:,3),'.','MarkerSize',13),hold on
plot(sigvssiz(fitted,1),sigvssiz(fitted,2),'or'),hold on
[f1,g1]=fit(sigvssiz(fitted,1),sigvssiz(fitted,2),ftype,'StartPoint',[1]);
plot([0 16],f1([0 16]),'-','Color',[0.5 0.5 0.5])

%display(f1)
%display(g1)
xlim([0 16]);ylim([0 3]),box on,axis square
xlabel('Mean intensity')
ylabel('SD in difference of intensity (\sigma)')

subplot(1,3,2)
%plot(sigvssiz(:,1),sigvssiz(:,2).^2,'x-'),hold on
%dv=2.*sigvssiz(:,3).*sigvssiz(:,2);
errorbar(sigvssiz(:,1),sigvssiz(:,2).^2,sigvssiz(:,4),'.','MarkerSize',13),hold on
plot(sigvssiz(fitted,1),sigvssiz(fitted,2).^2,'or'),hold on
[f2,g2,o2]=fit(sigvssiz(fitted,1),sigvssiz(fitted,2).^2,ftype,'StartPoint',[1],'Weights',1./(sigvssiz(:,4).^2));
%display(f2)
%display(g2)
%display(o2)
plot([0 16],f2([0 16]),'-','Color',[0.5 0.5 0.5])
xlim([0 16]);ylim([0 8]),box on,axis square
xlabel('Mean intensity')
ylabel('Variance in difference of intensity')
set(gca,'FontName','Calibri','FontSize',13);

figure,
errorbar(sigvssiz(:,1),sigvssiz(:,2).^2,std(Ibsdiff,[],2),'.','MarkerSize',13),hold on
[f3,g3,o3]=fit(sigvssiz(fitted,1),sigvssiz(fitted,2).^2,ftype,'StartPoint',[1],'Weights',1./(std(Ibsdiff,[],2).^2));
display(std(Ibsdiff,[],2));
display(f3)
display(g3)
pval=1-chi2cdf(g3.sse,g3.dfe);
fprintf('P-value %0.3f \n',pval)
%display(o3)
plot([0 17],f3([0 17]),'-','Color',[0.5 0.5 0.5])
xlim([0 17]);ylim([0 8]),box on,axis square
xlabel('Mean intensity')
ylabel('Variance in difference of intensity')
set(gca,'FontName','Calibri','FontSize',13);
title('Bootstrap error bars')

for b=1:max(find(fitted))
    ind=Imean_orig<binedges(b+1) & Imean_orig>binedges(b);
    
    II=Idiff_orig(ind);
    
    [cdf_dI,bins_dI]=histcounts(II(:),-6:0.4:6,'Normalization','cdf');
    %[cdf_dInorm1,bins_dInorm]=histcounts(II(:)./f1(bincen(b)),-3:0.2:3,'Normalization','cdf');
    [cdf_dInorm2,bins_dInorm]=histcounts(II(:)./(f2(bincen(b))).^0.5,-6:0.4:6,'Normalization','cdf');
    [cdf_dInorm3,bins_dInorm]=histcounts(II(:)./(f3(bincen(b))).^0.5,-6:0.4:6,'Normalization','cdf');
    xbinsnorm=(bins_dInorm(1:end-1)+bins_dInorm(2:end))./2;
    
    display('KStest results')
    [h,p]=kstest(II(:)./(f3(bincen(b))).^0.5);
    display([h,p,b])
    
    figure(76),movegui(76,'southeast')
    subplot(1,3,1)
    plot(bins_dI(2:end),cdf_dI,'-','Color',ccc(b,:)),hold on
    xlim([-6 6]),ylim([0 1]);
    axis square,box on;
    xlabel('Difference in Intensity'),ylabel('Spread CDF')
    set(gca,'FontName','Calibri','FontSize',13);
    
    subplot(1,3,2)
    if b==1
        mu = 0;
        sigma = 1;
        pd=makedist('Normal',mu,sigma);
        plot(xbinsnorm,cdf(pd,xbinsnorm),'-','Color',[0.8 0 0],'LineWidth',2),hold on
    end
    plot(bins_dInorm(2:end),cdf_dInorm2,'-','Color',ccc(b,:)),hold on
    xlim([-6 6]),ylim([0 1]);
    axis square,box on;
    xlabel('Difference in Intensity'),ylabel('CDF')
    set(gca,'FontName','Calibri','FontSize',13);
    drawnow
    
    subplot(1,3,3)
    if b==1
        mu = 0;
        sigma = 1;
        pd=makedist('Normal',mu,sigma);
        plot(xbinsnorm,cdf(pd,xbinsnorm),'-','Color',[0.8 0 0],'LineWidth',2),hold on
    end
    plot(bins_dInorm(2:end),cdf_dInorm3,'-','Color',ccc(b,:)),hold on
    xlim([-6 6]),ylim([0 1]);
    axis square,box on;
    xlabel('Difference in Intensity'),ylabel('CDF')
    set(gca,'FontName','Calibri','FontSize',13);
    drawnow
end

%{
figure(23),clf(23),movegui(23,'south'),
plot(Imean,Idiff,'.'),hold on
xlim([0 20]);ylim([-15 15]),box on,axis square
xlabel('Mean intensity (over pair of conditions)')
ylabel('Pairwise difference in Intensity')
%}
