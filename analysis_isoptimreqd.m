function []=analysis_isoptimreqd(D)
%Code to generate results for inter-user variability in manual vs.
%optimized traces.

filter='LoGxy';
P=~isnan(D.G.(filter).amp);
I=D.G.(filter).amp+D.G.(filter).Ibg;
d=D.G.(filter).d;
normtype='gauss2_norm';%Options: gauss1_norm,gauss2_norm,fit_norm,profile_norm,none
axlbl=unique(D.G.(filter).ax_id,'stable');
ax_id=D.G.(filter).ax_id;
Nf=ones(size(D.G.(filter).amp,1),numel(axlbl));
ax_length=ones(size(D.G.(filter).amp,1),numel(axlbl));

optind=6:10;
manind=1:5;
for ax=1:numel(axlbl)
    ax_id(ax_id==axlbl(ax))=ax;
    for ti=1:size(D.G.(filter).norm,1)
        if ~strcmp(normtype,'none')
            Nf(ti,ax)=D.G.(filter).norm{ti,ax}.(normtype);
        end
        ax_length(ti,ax)=D.G.(filter).norm{ti,ax}.axlen_legitimate;
        I(ti,ax_id==ax)=I(ti,ax_id==ax)./Nf(ti,ax);
    end
end

%Bouton sizes are defined by mean over automatically detected boutons on
%all optimized traces
btn_I=nanmean(I(optind,:),1);
axonid=[1,2,3,4,7,8,9,10,11,12,13,14,17,18,19,20];
%axonid = [1,2,3,4,7,8,9,10,11,12,13,17,18,20];
display([axonid;Nf(optind,:)])

%Boutons in manual traces not detected automatically in optimized traces
%are assigned size based on average over manual traces in which they were
%detected.
nanind=isnan(btn_I);
temp=nanmean(I,1);
btn_I(nanind)=temp(nanind);
%Nf=mean(Nf,1);
%Nf=bsxfun(@times,ones(size(D.G.(filter).amp,1),1),Nf);
%display(Nf)
GetConflict(P(manind,:),btn_I,ax_id,d,'Manual');
GetConflict(P(optind,:),btn_I,ax_id,d,'Optimized');

inboth=sum(isnan(I),1)==0;
sum(inboth)
GetRMS(I(manind,inboth),btn_I(inboth),ax_id(inboth),d(inboth),'Manual');drawnow;
GetRMS(I(optind,inboth),btn_I(inboth),ax_id(inboth),d(inboth),'Optimized');

copt=[0.2 0.6 0.8];
cman=[0.8 0.3 0.3];
figure(10),
s=fittype('a*x');
scatter(I(1,inboth),I(2,inboth),10,'MarkerFaceColor',cman,'MarkerEdgeColor','none');hold on
[fman,gman]=fit(I(1,inboth)',I(2,inboth)',s,'StartPoint',[1]);
scatter(I(6,inboth),I(7,inboth),10,'MarkerFaceColor',copt,'MarkerEdgeColor','none');hold on
[fopt,gopt]=fit(I(6,inboth)',I(7,inboth)',s,'StartPoint',[1]);
plot([0 16],fopt([0 16]),'-','Color',copt)
plot([0 16],fman([0 15]),'-','Color',cman)
xlabel('User 1')
ylabel('User 2')
axis square;box on;
xlim([0 16]),ylim([0 16])
set(gca,'FontName','Calibri','FontSize',13);
set( gca,'XTick',0:4:16,'YTick',0:4:16);
display('-----------------Before optim----------------')
display(fman)
display(gman)
display('-----------------After optim----------------')
display(fopt)
display(gopt)
1;
 

function [] = GetConflict(X,btnsiz,ax,d,thisis)
%X consists of ones and zeros
binedges=(0.5:1:16);

Y=sum(X,1);
N=sum(1-X,1);

btnsiz(Y==0)=[];
ax(Y==0)=[];
N(Y==0)=[];
X(:,(Y==0))=[];
d(Y==0)=[];
Y(Y==0)=[];

conflicts=min(Y,N);

bincen=(binedges(1:(end-1))+binedges(2:(end)))./2;
figure
cc=lines(3);%cc=cc(end:-1:1,:);cc(end,:)=[0.5,0.5,0.5];
cc=[cc(2:3,:);[0.5,0.5,0.5]];
h3=histcounts(btnsiz(conflicts==0),binedges);
h2=histcounts(btnsiz(conflicts==1),binedges);
h1=histcounts(btnsiz(conflicts==2),binedges);
bar(bincen(:),[h1(:),h2(:),h3(:)],1,'stacked','EdgeColor',[0 0 0]);
colormap(cc)
xlabel('Bouton intensity');ylabel('Counts')
hbar=findall(gca,'Type','Bar');
legend(hbar,{'0/5 Conflicts','1/5 Conflicts','2/5 Conflicts'},'Fontsize',15)
axis square,box on,set(gca,'FontName','Calibri','FontSize',13);
set( gca,'YTick',0:75:225,'XTick',0:4:16);
title(thisis)
ylim([0 225])
xlim([0 16])
drawnow;
display([thisis, ' Conflicts break up']);
fprintf('Total(%d) = No conflicts (%d) + 1 conflict(%d) + 2 conflicts(%d) \n',...
    numel(conflicts),sum(conflicts==0),sum(conflicts==1),sum(conflicts==2))
if strcmp('Optimized',thisis)
    movegui(gcf,'north')
    display('-----------------For conflicts----------------')
    axonid = [1,2,3,4,7,8,9,10,11,12,13,14,17,18,19,20];
    crit=(btnsiz>1.5 & conflicts>0);
    display('Axon id (A00)')
    display(axonid(ax(crit)))
    display('Presence')
    display(X(:,crit));
    display('Distance on axon')
    display(d(crit));
    display('----------------------------------------')
else
    movegui(gcf,'northwest')
end

function [] = GetRMS(I,btnsiz,ax,d,thisis)
%X consists of ones and zeros
if strcmp('Optimized',thisis)
    cc=[0.2 0.6 0.8];
else
    cc=[0.8 0.3 0.3];
end

X=isnan(I);
inall=sum(X)==0;
I=I(:,inall);
btnsiz=btnsiz(inall);
d=d(inall);
rmsd=zeros(1,size(I,2));
terms=0;
for i=1:size(I,1)
    for j=1:size(I,1)
        if i~=j
            rmsd=rmsd+(I(i,:)-I(j,:)).^2;
            terms=terms+1;
        end
    end
end
rmsd=(rmsd./terms).^0.5;
s=fittype('a*x+b');
[f_rmsd,~]=fit(btnsiz(:),rmsd(:),s,'StartPoint',[1,0.1]);


xx=[0 20];
figure(54),movegui(54,'northeast'),hold on
scatter(btnsiz,rmsd(:),10,'MarkerFaceColor',cc,'MarkerEdgeColor','none');
plot(xx,f_rmsd(xx),'-','Color',cc,'LineWidth',1);
box on, axis square;set(gca,'FontName','Calibri','FontSize',13);
xlabel('Bouton intensity'),ylabel('Root mean square difference in intensity')
xlim([0 16]),ylim([0 4.5])
set( gca,'XTick',0:4:16,'YTick',0:1:5);
if strcmp('Optimized',thisis)
    display('---------------Optimized RMSD fit---------------')
    display(f_rmsd)
    display('---------------RMS problems---------------')
    %display(ax(rmsd>0.2))
    axonid = [1,2,3,4,7,8,9,10,11,12,13,14,17,18,19,20];
    crit=rmsd>0.3 & btnsiz<7;
    display([axonid(ax(:,crit));d(crit);rmsd(crit);I(:,crit)])
    
    
else
    movegui(gcf,'northwest')
end
