function [] = gui_editpeaks_selind(hObject, eventdata, ~)
[~,f]=gcbo;
dat=guidata(f);

t=hObject.UserData.t;
if ~isempty(regexp(hObject.Tag,'Node','match'))
    display('Node Selected')
    [~,ind]=min((...
        (hObject.XData-eventdata.IntersectionPoint(1)).^2 +...
        (hObject.YData-eventdata.IntersectionPoint(2)).^2).^0.5);
    ind=ind(1);
    ind=dat.Time{t}.fg_ind(ind); 
elseif ~isempty(regexp(hObject.Tag,'Trace','match'))
    [~,ind]=min((...
        (hObject.XData-eventdata.IntersectionPoint(1)).^2 +...
        (hObject.YData-eventdata.IntersectionPoint(2)).^2).^0.5);
    ind=ind(1);
end

dat.currentpoint=[];
delete(findobj(f,'Tag','Active'));
plot(dat.Time{t}.r(ind,2),dat.Time{t}.r(ind,1),'o','MarkerSize',15,'MarkerEdgeColor',[0 0.8 0.3],'MarkerFaceColor','none','LineWidth',1.5,'Tag','Active');
dat.currentpoint=[t,ind];
guidata(f,dat);

fp=findobj('Tag','ProfilePlot');
h_profilenode=findobj(fp,'Tag','ProfileNode');
if isempty(h_profilenode)
    figure(fp)
    h_profilenode=plot(dat.Time{dat.currentpoint(1)}.dman(dat.currentpoint(2),1),...
        dat.Time{dat.currentpoint(1)}.I.norm(dat.currentpoint(2))+dat.Time{dat.currentpoint(1)}.offset,...
        'o','MarkerSize',15,'MarkerEdgeColor',[0 0.8 0.3],'MarkerFaceColor','none','LineWidth',1.5,'Tag','ProfileNode');drawnow;
    figure(f)
end
h_profilenode.Parent.XLim=[dat.Time{dat.currentpoint(1)}.dman(dat.currentpoint(2),1)-30,dat.Time{dat.currentpoint(1)}.dman(dat.currentpoint(2),1)+30];
h_profilenode.Parent.YLim=[dat.Time{dat.currentpoint(1)}.offset-15,dat.Time{dat.currentpoint(1)}.offset+30];
h_profilenode.XData=dat.Time{dat.currentpoint(1)}.dman(dat.currentpoint(2),1);
h_profilenode.YData=dat.Time{dat.currentpoint(1)}.I.norm(dat.currentpoint(2))+dat.Time{dat.currentpoint(1)}.offset;