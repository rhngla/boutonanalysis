function []=EM_registration(shrinkage,LMd_peaks,EM_btn_ind)

yscale(1)=0.15;
yscale(2)=0.15;
yscale(3)=0.15;
yscale(4)=0.15;

xscale=1;
%shrinkage=[0.7100,0.8600,0.8900];

xshift(1)=0;
xshift(2)=0;
xshift(3)=0;
xshift(4)=0;

cc=[[0.1 0.7 0.2];[0.7 0.1 0.2];[0 0.4 0.8]];
for ax=1:4
    parentdir=pwd;
    parentdir=parentdir(1:(strfind(parentdir,'Plasticity')+numel('Plasticity')-1));
    emdat=isunixispc([parentdir,'\dat\EMData\Blender2Mat\A00',num2str(ax),'.mat']);
    lmdat=isunixispc([parentdir,'\dat\EMData\Profiles\DL100I005\A00',num2str(ax),'.mat']);
    
    EM=load(emdat);
    LM=load(lmdat);
    
    EM.Afilt=hampel(EM.A,5);
    EM.r=bsxfun(@times,EM.r,shrinkage);
    EM.d=[0;cumsum(sum((EM.r((2:end),:)-EM.r((1:end-1),:)).^2,2).^0.5)];
    
    %r=bsxfun(@times,LM.r.optim,[0.26,0.26,.8]);
    %d=cumsum(sum((r((2:end),:)-r((1:end-1),:)).^2,2).^0.5);
    d=LM.d.optim;
    
    if ax==1
        xx=[27,47.2];
        LM_ind=find(LM.d.optim>xx(1) & LM.d.optim<xx(2));
        
        LM.d.optim=[0;d(:)];
        IG=LM.I.G.LoG.norm(LM_ind).*yscale(ax);
        IR=LM.I.R.LoG.norm(LM_ind).*yscale(ax);
        
        dd=LM.d.optim(LM_ind);
        dd=abs(dd-dd(end));
        dd=dd.*xscale+xshift(ax);
        
    elseif ax==2
        xx=[27,43];
        LM_ind=find(LM.d.optim>xx(1) & LM.d.optim<xx(2));
        
        LM.d.optim=[0;d(:)];
        IG=LM.I.G.LoG.norm(LM_ind).*yscale(ax);
        IR=LM.I.R.LoG.norm(LM_ind).*yscale(ax);
        
        dd=LM.d.optim(LM_ind);
        dd=abs(dd-dd(1));
        dd=dd.*xscale+xshift(ax);
        
    elseif ax==3
        xx=[22,36.5];
        LM_ind=find(LM.d.optim>xx(1) & LM.d.optim<xx(2));
        
        LM.d.optim=[0;d(:)];
        IG=LM.I.G.LoG.norm(LM_ind).*yscale(ax);
        IR=LM.I.R.LoG.norm(LM_ind).*yscale(ax);
        
        dd=LM.d.optim(LM_ind);
        dd=abs(dd-dd(end));
        dd=dd.*xscale+xshift(ax);
        
    elseif ax==4
        xx=[0,15];
        LM_ind=find(LM.d.optim>xx(1) & LM.d.optim<xx(2));
        
        LM.d.optim=[0;d(:)];
        IG=LM.I.G.LoG.norm(LM_ind).*yscale(ax);
        IR=LM.I.R.LoG.norm(LM_ind).*yscale(ax);
        
        dd=LM.d.optim(LM_ind);
        dd=abs(dd-dd(end));
        dd=dd.*xscale+xshift(ax);
    end
    
    EMshift=mean(-EM.d(EM_btn_ind{ax})+LMd_peaks{ax}');
    EM.d=EM.d+EMshift;
    
    figure(ax),clf(ax),
    plot(dd,IG,'-','Color',cc(1,:)),hold on
    plot(dd,IR,'-','Color',cc(2,:)),hold on
    
    plot(EM.d,EM.Afilt,'-','Color',cc(3,:))
    
    for i=1:numel(LMd_peaks{ax})
        [~,ind]=min(abs(dd-LMd_peaks{ax}(i)));
        plot(dd(ind),IG(ind),'x','MarkerSize',13,'Color',cc(1,:))
    end
    
    for i=1:numel(EM_btn_ind{ax})
        plot(EM.d(EM_btn_ind{ax}(i)),EM.Afilt(EM_btn_ind{ax}(i)),'x','MarkerSize',13,'Color',cc(3,:))
    end
    
    xlim([EM.d(1) EM.d(end)]);
    ylim([0 1.5]);
end