%This function adds nodes to an active list. Both ActiveSet and
%PredictedMatches have the same set of associated UserData.
%Also plots nodes belonging to both these sets.
function [] = gui_regpeaks_selnodes (h_selnode, eventdata, ~)

[~,f] = gcbo;

sel_t=h_selnode.UserData.t;
sel_nodeind=h_selnode.UserData.nodeind;
sel_fg_ind=h_selnode.UserData.fg_ind;
sel_fg_id=h_selnode.UserData.fg_id;

%Remove node from active/predicted lists if already present.
h_exist=findobj(f,'-regexp','Tag','(Activ*)|(Predict*)');
rem=[];
if ~isempty(h_exist)
    dat_exist=cell2mat({h_exist.UserData});
    exist_fg_id=[dat_exist.fg_id];
    rem=find(exist_fg_id==sel_fg_id);
    delete(h_exist(rem));
end

if isempty(rem)
    plot3(h_selnode.XData,h_selnode.YData,0,...
        'Tag','ActiveSet','UserData',struct('t',sel_t,'nodeind',sel_nodeind,'fg_ind',sel_fg_ind,'fg_id',sel_fg_id),...
        'Marker','o','MarkerSize',20,'Color',[0 0.8 0.3],'LineWidth',1.5);hold on
    
    h_active=findobj(f,'Tag','ActiveSet');
    if numel(h_active)==1
        %Linked nodes will have the min(distance)=0 and so dont require special
        %treatment
        dat=guidata(f);
        sel_fg_d=dat.Time{sel_t}.d_man(sel_fg_ind);
        %Search in all but current time points.
        for ti=1:numel(dat.Time)
            if ti~=sel_t
                peak_d=dat.Time{ti}.d_man(dat.Time{ti}.fg_ind);
                [val,nodeind]=min(abs(peak_d-sel_fg_d));
                fg_ind=dat.Time{ti}.fg_ind(nodeind);
                fg_id=dat.Time{ti}.fg_id(nodeind);
                if val<2.5
                    plot3(dat.Time{ti}.r(fg_ind,2),dat.Time{ti}.r(fg_ind,1),zeros(size(dat.Time{ti}.r(fg_ind,1))),...
                        'Tag','PredictedMatches','UserData',struct('t',ti,'nodeind',nodeind,'fg_ind',fg_ind,'fg_id',fg_id),...
                        'Marker','o','MarkerSize',20,'Color',[0.4 0.4 0.4],'LineWidth',1.5);hold on
                end
            end
        end
    else
        %Replace PredictedMatches in same time with current selection.
        h_predict=findobj(f,'Tag','PredictedMatches');
        dat_active=cell2mat({h_active.UserData});
        t_active=[dat_active.t];
        nodeind_active=[dat_active.nodeind];
        
        if~isempty(h_predict)
            h_predict=findobj(f,'Tag','PredictedMatches');
            dat_predict=cell2mat({h_predict.UserData});
            t_predict=[dat_predict.t];
            [~,in_predict,~] = intersect(t_predict,t_active);
            delete(h_predict(in_predict));
        end
        
        %Replace previous ActiveSet node in same time with current selection.
        delete(h_active(t_active==sel_t & nodeind_active~=sel_nodeind))
    end
end