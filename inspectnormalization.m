%Profiles to inspect
animal={'DL001'};
timepoint={'A','B','D','E','F','H','I'};
section={'002'};
axon={[1,2,3,4,7,8,9,10,11,12,13,14,15,17,18,19,20]};
profile_pth='/Users/Fruity/Dropbox/Lab/Plasticity/dat/Controls/Conditions/Profiles_v2/';
%profile_pth='C:\Users\Rohan\Dropbox\Lab\Plasticity\dat\Controls\Conditions\Profiles_v2\';

for an=1:numel(animal)
    for se=1:numel(section)
        for ax=1:numel(axon{se})
            for ti=1:numel(timepoint)
                stackid=[animal{an},timepoint{ti},section{se}];
                axonstr=[sprintf('A%03d',axon{se}(ax))];
                Dat=load(isunixispc([profile_pth,stackid,'/',axonstr,'.mat']));
                
                %Define normalization:
                temp=Dat.I.G.Gauss2.raw;
                temp(Dat.annotate.ignore)=nan;
                norm=nanmedian(temp);
                
                figure(ax)
                subplot(numel(timepoint),1,ti)
                plot(Dat.d.optim,Dat.I.G.LoGxy.raw./norm,'-'),hold on
                %plot(Dat.d.optim([1,end]),norm*(ones(2,1)),'-k');
                box on, grid on;
                ylim([0 0.005])
                xlabel(['Axon:', axonstr,', Distance (\mu{m})']);
                ylabel('Intensity');
                drawnow;
            end
        end
    end
end
