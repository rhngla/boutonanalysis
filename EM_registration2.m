%This script calculates corresponding z-axes in LM and EM
%Data for matched co-ordinates is located in ROI_v4 .ppt file in the comments.
%Variables EM and LM are loaded from there.
%1 point from Axon 3 LM was updated to match trace re-optimization

Ff = @(x,A,mu,sigma) (ones(size(x))*A).*exp(-(x*ones(size(A))-ones(size(x))*mu).^2./(ones(size(x))*sigma.^2)./2);

parentdir=pwd;
parentdir=parentdir(1:(strfind(parentdir,'Plasticity')+numel('Plasticity')-1));

load(isunixispc([parentdir,'\dat\EMData\DatasetIM\DL100I005G.mat']),'IM')

L{1}=load(isunixispc([parentdir,'\dat\EMData\Profiles\DL100I005\A001.mat']),'r','I','d','fit');
L{2}=load(isunixispc([parentdir,'\dat\EMData\Profiles\DL100I005\A002.mat']),'r','I','d','fit');
L{3}=load(isunixispc([parentdir,'\dat\EMData\Profiles\DL100I005\A003.mat']),'r','I','d','fit');
L{4}=load(isunixispc([parentdir,'\dat\EMData\Profiles\DL100I005\A004.mat']),'r','I','d','fit');

E{1}=load(isunixispc([parentdir,'\dat\EMData\CrossSectionEst\Axon1.mat']));
E{2}=load(isunixispc([parentdir,'\dat\EMData\CrossSectionEst\Axon2.mat']));
E{3}=load(isunixispc([parentdir,'\dat\EMData\CrossSectionEst\Axon3.mat']));
E{4}=load(isunixispc([parentdir,'\dat\EMData\CrossSectionEst\Axon4.mat']));

%Fiduciary points marked by eye
LM_orig=...
[ 588.3200  403.2000  257.7400
  598.4900  402.4900  260.0000
  609.9800  411.2700  259.9800
  615.6200  412.0400  259.8700
  628.6900  409.5800  258.8600
  639.0600  408.5400  258.3400
  646.5100  409.0600  258.6600
  623.3859  404.2713  260.2900
  637.4999  383.5393  260.8362
  647.6931  396.8014  262.4242
  644.0706  387.9168  267.1464
  642.9696  413.6817  263.1770
  643.7598  403.4845  263.4609];

EM=...
[  -10.7100   -1.0340    6.0330
   -8.1370    0.3850    2.8410
   -4.1100   -1.4320    3.4090
   -2.2520   -1.0950    4.0120
    0.6430    0.7950    4.9040
    3.6210    1.7730    6.3450
    5.5330    2.1060    6.9990
   -1.1600    1.7890    3.0490
    0.8110    8.8370    2.9220
    5.0760    5.9080    2.1930
    3.2260    8.2180   -2.7800
    5.5970    1.2060    1.4290
    4.6360    3.6790    1.1950];

%Converting co-ordinates to micron units
pxtoum=[0.26 0.26 0.8];
LM_microns=bsxfun(@times,LM_orig,pxtoum);

cc=lines(4);
%%%%%%%%%%%%%%%%%%% Show electron microscopy points %%%%%%%%%%%%%%%%%%%
figure(1),clf(1);movegui(1,'northwest')
for i=1:4  
    plot3(E{i}.SV(:,1),E{i}.SV(:,2),E{i}.SV(:,3),'.','MarkerSize',1,'Color',cc(i,:)),hold on
    plot3(E{i}.r(:,1),E{i}.r(:,2),E{i}.r(:,3),'.','MarkerSize',2,'Color',[0 0 0]),hold on
end
plot3(EM(:,1),EM(:,2),EM(:,3),'ok','MarkerSize',20)

axis equal,box on;grid on;
drawnow;

%%%%%%%%%%%%%%%%%%% Show light microscopy points %%%%%%%%%%%%%%%%%%%
figure(2),clf(2);movegui(2,'north')
for i=1:4  
    plot3(L{i}.r.optim(:,1),L{i}.r.optim(:,2),L{i}.r.optim(:,3),'.','MarkerSize',1,'Color',cc(i,:)),hold on
end
plot3(LM_orig(:,1),LM_orig(:,2),LM_orig(:,3),'xk','MarkerSize',5)
axis equal,box on;grid on;
drawnow;

%%%%%%%%%%%%%%%%%%% Alignment %%%%%%%%%%%%%%%%%%%
%(LM)=L*(EM)+b
%Think of L = Scale*Rotation
[Tel,bel]=optimal_linear_transform(EM',LM_microns');
close(10);

%The relation is inverted to find the LM z-axis in EM
Zlm2=[0 0 1];
Zem_2=(Zlm2-bel')/(Tel');

Zlm1=[0 0 0];
Zem_1=(Zlm1-bel')/(Tel');

Zem=Zem_2-Zem_1;
Zem=Zem./norm(Zem);%This is used for calculating cross section parallel to z-axis
figure(1)
plot3([0;15*Zem(1)],[0;15*Zem(2)],[0;15*Zem(3)],'x-r'),drawnow;

%%%%%%%%%%%%%%%%%%% Alignment %%%%%%%%%%%%%%%%%%%
%(EM)=T*(LM)+b
[Tle,ble]=optimal_linear_transform(LM_microns',EM');
close(10);
%The relation is inverted to find the LM z-axis in EM
Zlm=[0 0 1];
Zem2=Zlm*(Tle');%+ble';
Zem2=Zem2./norm(Zem2);
figure(1)
plot3([0;15*Zem2(1)],[0;15*Zem2(2)],[0;15*Zem2(3)],'x-b'),
axis equal;view(Zem2);
drawnow;

%%%%%%%%%%%%%%%%%%% EM+LM+Image projection %%%%%%%%%%%%%%%%%%%
figure(3),clf(3),movegui(3,'northeast')
imshow(max(IM(:,:,254:273),[],3)),hold on
%imshow(max(IM(:,:,:),[],3)),hold on
caxis([0 1500])
for i=1:4
    P=bsxfun(@plus,E{i}.SV*Tel',bel');
    P=bsxfun(@times,P,1./pxtoum);
    pts=randperm(size(P,1));
    pts=pts(1:round(numel(pts)./2));
    plot3(P(pts,2),P(pts,1),P(pts,3),'.','MarkerSize',1,'Color',cc(i,:)),hold on
    plot3(L{i}.r.optim(:,2),L{i}.r.optim(:,1),L{i}.r.optim(:,3),'-','MarkerSize',1,'Color',cc(i,:)),hold on
end
plot3(LM_orig(:,2),LM_orig(:,1),LM_orig(:,3),'or','MarkerSize',5,'LineWidth',2)
Pm=bsxfun(@plus,EM*Tel',bel');
Pm=bsxfun(@times,Pm,1./pxtoum);
plot3(Pm(:,2),Pm(:,1),Pm(:,3),'ob','MarkerSize',5,'LineWidth',2)
axis equal;
box on;

%graphically determined using 'view' axis property
Zemp=[-0.0523,-0.0723,0.9960];
figure(1)
plot3([0;15*Zemp(1)],[0;15*Zemp(2)],[0;15*Zemp(3)],'--k'),drawnow;

%Plot profiles and cross sections
%extranormG=[0.1927,0.5888,0.2679,0.3520];%Old normalization from exclusion of foreground peaks
extranormG=[0.3147,0.6548,0.4736,0.5937];%From mean of fitted background to Gauss2 profile
extranormR=[0.1005,0.2463,0.1314,0.2174];
%scaleG=0.0490;%0.0490; %To match CSz

for i=1:4
    %Calculate fitted background in Gaussian profile(s)
    Gauss2bg=zeros(size(L{i}.d.optim));
    for j=1:numel(L{i}.fit.G.Gauss2.bg.ind)
        Gauss2bg=Gauss2bg+Ff(L{i}.d.optim,L{i}.fit.G.Gauss2.bg.amp(j),L{i}.fit.G.Gauss2.bg.mu(j),L{i}.fit.G.Gauss2.bg.sig(j));
    end
    extranormG(i)=mean(Gauss2bg);
    
    %Calculate fitted background in Gaussian profile(s)
    Gauss2bg=zeros(size(L{i}.d.optim));
    for j=1:numel(L{i}.fit.R.Gauss2.bg.ind)
        Gauss2bg=Gauss2bg+Ff(L{i}.d.optim,L{i}.fit.R.Gauss2.bg.amp(j),L{i}.fit.R.Gauss2.bg.mu(j),L{i}.fit.R.Gauss2.bg.sig(j));
    end
    extranormR(i)=mean(Gauss2bg);
end


scaleG=1;
scaleR=1;
for i=1:4
    EMr_microns=bsxfun(@plus,E{i}.r*Tel',bel');
    EMr_pixels=bsxfun(@times,EMr_microns,1./pxtoum);
    [~,startt]=min(abs(L{i}.r.optim(:,1)-EMr_pixels(1,1))+abs(L{i}.r.optim(:,2)-EMr_pixels(1,2)'));
    [~,endd]=min(abs(L{i}.r.optim(:,1)-EMr_pixels(end,1))+abs(L{i}.r.optim(:,2)-EMr_pixels(end,2)'));
    keep=sort(startt:sign(endd-startt):endd);
    EMd=[0;cumsum(sum((EMr_microns(2:end,:)-EMr_microns(1:end-1,:)).^2,2).^0.5)];
    figure(40+i),clf(40+i)
    LMd=abs(L{i}.d.optim(keep)-L{i}.d.optim(startt));
    plot(LMd,scaleG.*L{i}.I.G.LoGxy.norm(keep)./extranormG(i),'-','Color',[0 0.8 0.4]);hold on
    plot(LMd,scaleR.*L{i}.I.R.LoG.norm(keep)./extranormR(i),'-','Color',[0.8 0 0]);hold on
    xlabel('d (\mu{m})')
    ylabel('Intensity')
    ylim([-1 21]),xlim([-1 21])
    h=gca;
    h.YTick=[0:4:16];
    h.XTick=[0:5:20];
    grid off
    
    plot([-1 21],[0 0],'-','Color',[0.5 0.5 0.5])
    plot([-1 21],[16 16],'-','Color',[0.5 0.5 0.5])
    
    plot([0 0],[0 16],'-','Color',[0.5 0.5 0.5])
    plot([15 15],[0 16],'-','Color',[0.5 0.5 0.5])
    %{
    plot([-1 21],[0 0],'-','Color',[0.5 0.5 0.5])
    plot([-1 21],[0.25 0.25],'-','Color',[0.5 0.5 0.5])
    plot([-1 21],[0.5 0.5],'-','Color',[0.5 0.5 0.5])
    plot([-1 21],[0.75 0.75],'-','Color',[0.5 0.5 0.5])
    plot([-1 21],[1 1],'-','Color',[0.5 0.5 0.5])
    
    plot([0 0],[-0.05 1.05],'-','Color',[0.5 0.5 0.5])
    plot([5 5],[-0.05 1.05],'-','Color',[0.5 0.5 0.5])
    plot([10 10],[-0.05 1.05],'-','Color',[0.5 0.5 0.5])
    plot([15 15],[-0.05 1.05],'-','Color',[0.5 0.5 0.5])
    plot([20 20],[-0.05 1.05],'-','Color',[0.5 0.5 0.5])
    %}
    %saveas(gcf,['F:\Desktop\Axon',num2str(i),'.emf'])
    
    drawnow;
    figure(50+i),clf(50+i)
    plot(EMd,median(E{i}.CSz,2),'-','Color',cc(1,:)),hold on
    plot(EMd,median(E{i}.CS,2),'-','Color',cc(2,:))
    xlabel('d (\mu{m})')
    ylabel('Cross-section area (\mu{m}^{2})')
    %legend({'z-CS','CS (w.r.t centerline)'},'Location','northwest')
    ylim([-0.05 1.05]),xlim([-1 21])
    h=gca;
    h.YTick=[0 0.5 1];
    h.XTick=[0:5:20];
    
    plot([-1 21],[0 0],'-','Color',[0.5 0.5 0.5])
    plot([-1 21],[0.25 0.25],'-','Color',[0.5 0.5 0.5])
    plot([-1 21],[0.5 0.5],'-','Color',[0.5 0.5 0.5])
    plot([-1 21],[0.75 0.75],'-','Color',[0.5 0.5 0.5])
    plot([-1 21],[1 1],'-','Color',[0.5 0.5 0.5])
    
    plot([0 0],[-0.05 1.05],'-','Color',[0.5 0.5 0.5])
    plot([5 5],[-0.05 1.05],'-','Color',[0.5 0.5 0.5])
    plot([10 10],[-0.05 1.05],'-','Color',[0.5 0.5 0.5])
    plot([15 15],[-0.05 1.05],'-','Color',[0.5 0.5 0.5])
    plot([20 20],[-0.05 1.05],'-','Color',[0.5 0.5 0.5])
    
    grid off
    drawnow;
    %saveas(gcf,['F:\Desktop\EM',num2str(i),'.emf'])
end

%  fuli=1;
%  lmi=1;
%  dd=...
%  abs(bsxfun(@minus,L{fuli}.r.optim(:,2),LM(lmi,2)'))+...
%  abs(bsxfun(@minus,L{fuli}.r.optim(:,1),LM(lmi,1)'))+...
%  abs(bsxfun(@minus,L{fuli}.r.optim(:,3),LM(lmi,3)'));
%  [~,ind]=min(dd,[],1);
%  LM(lmi,:)=L{fuli}.r.optim(ind,:);