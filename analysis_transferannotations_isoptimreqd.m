%This code is used to transfer ignoreind annotation from an earlier dataset
%to a newer one.

animal={'DL001'};
timepoint=cellstr([char(double('K'):double('T'))]');
section={'002'};
axon={[1,2,3,4,7,8,9,10,11,12,13,14,15,17,18,19,20]};
projectn='xy';
filter={'LoG3'};
channel='G';
pathlist;

matchpath='C:\Users\Rohan\Dropbox\Lab\Plasticity\dat\Controls_v2\IsOptimizationReqd\Profiles\';
for an=1:numel(animal)
    for se=1:numel(section)
        for ax=1:numel(axon{se})
            axonstr=sprintf('A%03d',axon{se}(ax));
            for ti=1:numel(timepoint)
                stackid=[animal{an},timepoint{ti},section{se}];
                New=load(isunixispc([profile_pth,stackid,'/',axonstr,'.mat']));
                New.annotate.ignore=false(size(New.r.optim(:,1)));
             
                Old=load(isunixispc([matchpath,'DL001P002','\',axonstr,'.mat']));
                
                startt=find(diff(Old.annotate.ignore)==1)+1;
                endd=find(diff(Old.annotate.ignore)==-1);
                if Old.annotate.ignore(1)==true
                    startt=[1;startt(:)];
                end
                if Old.annotate.ignore(end)==true
                    endd=[endd(:);numel(Old.annotate.ignore)];
                end
                Old.r.optim(startt,:);
                Old.r.optim(endd,:);
                
                dx=bsxfun(@minus,New.r.optim(:,1),Old.r.optim(startt,1)');
                dy=bsxfun(@minus,New.r.optim(:,2),Old.r.optim(startt,2)');
                dz=bsxfun(@minus,New.r.optim(:,3),Old.r.optim(startt,3)');
                dd=(dx.^2+dy.^2+dz.^2).^0.5;
                [valstart,indstart]=min(dd,[],1);
                
                dx=bsxfun(@minus,New.r.optim(:,1),Old.r.optim(endd,1)');
                dy=bsxfun(@minus,New.r.optim(:,2),Old.r.optim(endd,2)');
                dz=bsxfun(@minus,New.r.optim(:,3),Old.r.optim(endd,3)');
                dd=(dx.^2+dy.^2+dz.^2).^0.5;
                [valend,indend]=min(dd,[],1);
                
                %Becuase the traces nodes are retained after cutting; 
                rem=(valend>2 | valstart>2);
                
                indstart(rem)=[];
                indend(rem)=[];
                
                for i=1:numel(indstart)
                    New.annotate.ignore(indstart(i):indend(i))=true;
                end
                
                save(isunixispc([profile_pth,stackid,'/',axonstr,'.mat']),'-struct','New');
                display([profile_pth,stackid,'/',axonstr,'.mat'])
                clear New Old
            end
        end
    end
end