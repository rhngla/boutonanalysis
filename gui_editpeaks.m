%This code loads data for an axon, generates Canvas and overlays peaks and
%traces. Peaks can be added and removed, and changes will be saved.
%Note:The corresponding profile plot only shows location of selected point,
%and is not updated to reflect addition or removal of peaks.
pathlist;

% animal={'DL083'};
% timepoint=cellstr(char(double('B'):double('N'))');
% section={'002'};
% axon={[2]};
% tracer={[]};
% projectn='xy';
% filter='LoG';
% channel='G';
animal={'DL001'};
%timepoint=cellstr([char(double('K'):(double('T')))']);
timepoint={'A','B','D','E','F','H','I'};
section={'002'};
axon={[1]};
tracer={[]}; %Default: tracer = {[]}; for 'A001.swc';
projectn='xy';
filter='LoGxy';
channel='G';

isreinitialize=false;
alignbymatches=true;
ex=10;ey=10;%(positive value=down/right in image)

if strcmp(projectn,'xy')
    dat.perm=[1,2,3];
elseif strcmp(projectn,'yz')
    dat.perm=[3,2,1];
elseif strcmp(projectn,'zx')
    dat.perm=[1,3,2];
end

Canvas=zeros(3048,3048);
dx=zeros(numel(timepoint),1);dy=dx;
rr=cell(numel(timepoint),1);
allshift=[];r=[];

an=1;se=1;ax=1;tr=1;
Time=cell(numel(timepoint),1);
for ti=1:numel(timepoint)
    axonstr=[sprintf('A%03d',axon{se}(ax)),tracer{tr}];
    stackid=[animal{an},timepoint{ti},section{se}];
    clear IM;
    
    if exist(isunixispc([proj_pth,stackid,'-',axonstr,'-',projectn,'.mat']),'file')
        load(isunixispc([proj_pth,stackid,'-',axonstr,'-',projectn,'.mat']),'IM')
        sizeIM=size(IM(:,:,1));
        temp=load(isunixispc([profile_pth,stackid,'\',axonstr,'.mat']));
        
        %Load r,d,I data
        Time{ti}.r=temp.r.optim;
        Time{ti}.r=Time{ti}.r(:,dat.perm);
        Time{ti}.d=temp.d.optim;
        Time{ti}.dman=temp.fit.(channel).(filter).d.man;
        Time{ti}.I.norm=temp.I.(channel).(filter).norm;
        Time{ti}.I.raw=temp.I.(channel).(filter).raw;
        Time{ti}.ignore=temp.annotate.ignore;
        
        %Load fg peak data
        Time{ti}.fg_ind=temp.fit.(channel).(filter).fg.ind;
        Time{ti}.fg_id=temp.fit.(channel).(filter).fg.id;
        Time{ti}.fg_manid=temp.fit.(channel).(filter).fg.manid;
        Time{ti}.fg_autoid=temp.fit.(channel).(filter).fg.autoid;
        Time{ti}.fg_flag=temp.fit.(channel).(filter).fg.flag;
        
        %Path where data will be saved within gui
        Time{ti}.id=temp.id;
        Time{ti}.save_pth=isunixispc([profile_pth,stackid,'\',axonstr,'.mat']);
        clear temp;
        
        %Initialize fg_id for all times with unique labels
        if isreinitialize
            display('Reinitializing all peak labels and matches')
            Time{ti}.fg_id=1000*ti+(1:numel(Time{ti}.fg_id))'; %# peaks not expected to be >1000 in one time
            Time{ti}.fg_manid=nan(numel(Time{ti}.fg_id),1);
            Time{ti}.fg_autoid=nan(numel(Time{ti}.fg_id),1);
            Time{ti}.fg_flag=nan(numel(Time{ti}.fg_id),1);
            [~,igind,~]=intersect(Time{ti}.fg_ind,find(Time{ti}.ignore));
            Time{ti}.fg_flag(igind)=1; %# peak flag is nan by default
        end
        
        %Preparing canvas
        if alignbymatches
            %Align images based on registered boutons
            if ti>1
                prevr=Time{ti-1}.r(Time{ti-1}.fg_ind,:);
                nowr=Time{ti}.r(Time{ti}.fg_ind,:);
                prevlbl=Time{ti-1}.fg_manid;
                nowlbl=Time{ti}.fg_manid;
                [~,prevind,nowind]=intersect(prevlbl,nowlbl);
                
                dx(ti)=mean(nowr(nowind,1)-prevr(prevind,1));
                dy(ti)=mean(nowr(nowind,2)-prevr(prevind,2));
            end
            display('Alignment of Canvas using matches')
            dxt=nansum(dx); dyt=nansum(dy);
            dxt=-round(dxt)+size(Canvas,1)/4+1;
            dyt=-round(dyt)+size(Canvas,2)/4+1;
            
            %ex=-20;ey=10;%(positive value=down/right in image)
            dxt=dxt+ex*ti;
            dyt=dyt+ey*ti;
            
            %Image normalization
            imnorm=5*mean(Time{ti}.I.raw(~Time{ti}.ignore));
            Canvas(dxt:dxt+size(IM,1)-1,dyt:dyt+size(IM,2)-1,1)=max(Canvas(dxt:dxt+size(IM,1)-1,dyt:dyt+size(IM,2)-1,1),IM(:,:,1)./imnorm);
            
            if ti==numel(timepoint)
                %Time{ti}.r not modified directly because value at ti-1
                %is used within parent loop
                rr{ti}=[Time{ti}.r(:,1)+dxt-1,Time{ti}.r(:,2)+dyt-1,Time{ti}.r(:,3)];
                for tti=1:numel(timepoint)
                    Time{tti}.r=rr{tti};
                end
            else
                %Saving temporary r
                rr{ti}=[Time{ti}.r(:,1)+dxt-1,Time{ti}.r(:,2)+dyt-1,Time{ti}.r(:,3)];
            end
        else
            %In the following, x y refer to Canvas x y.
            sep=30*(ti-1);
            %ex=max(Time{ti}.r(:,2))-min(Time{ti}.r(:,2));
            %ey=max(Time{ti}.r(:,1))-min(Time{ti}.r(:,1));
            
            %Add shift angle  manually
            %ex=1;ey=0.5;%(positive value=down/right in image)
            dxt=round(sep.*(-ex)./((ex.^2+ey.^2).^0.5))+1024;
            dyt=round(sep.*ey./((ex.^2+ey.^2).^0.5))+1024;
            
            %Image normalization
            imnorm=5*mean(Time{ti}.I.raw(~Time{ti}.ignore));
            Canvas(dxt:dxt+size(IM,1)-1,dyt:dyt+size(IM,2)-1,1)=max(Canvas(dxt:dxt+size(IM,1)-1,dyt:dyt+size(IM,2)-1,1),IM(:,:,1)./imnorm);
            
            Time{ti}.r(:,1)=Time{ti}.r(:,1)-1+dxt;
            Time{ti}.r(:,2)=Time{ti}.r(:,2)-1+dyt;
        end
    else
        display([isunixispc([proj_pth,stackid,'-',axonstr,'-',projectn]),' not found!']);
    end
end

h_traces = gobjects(numel(Time),1);
h_nodes = gobjects(numel(Time),1);
f=figure(1);clf(f);
imshow(Canvas),caxis([0 max(Canvas(:))./2]),hold on;drawnow;
set(f,'KeyPressFcn',@gui_editpeaks_actions)
cc=lines(7);
dat.Time=Time;
dat.filter=filter;
dat.channel=channel;
for ti=1:numel(dat.Time)
    h_traces(ti)=plot(dat.Time{ti}.r(:,2),dat.Time{ti}.r(:,1),...
        '.','MarkerSize',2,'Color',cc(mod(ti-1,7)+1,:),...
        'Tag','Trace','UserData',struct('t',ti),'ButtonDownFcn',@gui_editpeaks_selind);
        
    h_nodes(ti)=plot3(dat.Time{ti}.r(dat.Time{ti}.fg_ind,2),dat.Time{ti}.r(dat.Time{ti}.fg_ind,1),2*ones(numel(dat.Time{ti}.fg_ind),1),...
        '^','MarkerSize',15,'Color',cc(mod(ti-1,7)+1,:),'LineWidth',1.5,...
        'Tag','Node','UserData',struct('t',ti),'ButtonDownFcn',@gui_editpeaks_selind);
end

%Figure with profiles
fp=figure(2);clf(2);
fp.Tag='ProfilePlot';
offset=zeros(numel(dat.Time)+1,1);
for ti=1:numel(dat.Time)
    A=load(dat.Time{ti}.save_pth,'I','d','fit');
    offset(ti+1)=gui_editpeaks_plotprofile(fp,A.I.(channel).(filter).norm,A.d.optim,...
        A.fit.(channel).(filter).d.man,A.fit.(channel).(filter).fg,A.fit.(channel).(filter).bg,offset(ti));
    dat.Time{ti}.offset=offset(ti);
end
guidata(f,dat);
%clearvars -except f
