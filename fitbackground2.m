% This function fits Gaussian profiles, using the peak locations found from
% fitting the LoGxy profile.Parameters where relevant will match fitpeaks.m
% Merging is allowed. This is the version described in paper.
function [fg_new,bg_new]=fitbackground2(I_axon,d_axon,fg_axon,bg_axon)
min_d=1;
alpha=0.5;
shape='G';
isplot=false;

% Parameters
Nsteps=10000;
min_change=10^-6;
betta0.A=0.1;
betta0.mu=0.1;
betta0.sigma=0.1;

min_bouton_size=1;
max_bouton_size=3;

min_background_size=20;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
LLL=d_axon(end)-d_axon(1);
N_bouton=numel(fg_axon.ind);
N_background=numel(bg_axon.ind);

naninds=fg_axon.ind(isnan(fg_axon.amp));
%fg_axon.amp(isnan(fg_axon.amp))=I_axon(fg_axon.ind(isnan(fg_axon.amp)));
%fg_axon.sig(isnan(fg_axon.sig))=ones(size(fg_axon.sig(isnan(fg_axon.sig))));
fg_axon.mu(isnan(fg_axon.mu))=d_axon(fg_axon.ind(isnan(fg_axon.mu)));

% boutons
A_bouton=fg_axon.amp; min_A_bouton=0; max_A_bouton=max(I_axon)*1.2;
mu_bouton=fg_axon.mu; min_mu_bouton=0;max_mu_bouton=LLL;
%min_mu_bouton=fg_axon.mu'-0.5;%max_mu_bouton=fg_axon.mu'+0.5; 
sigma_bouton=fg_axon.sig; min_sigma_bouton=min_bouton_size/4; max_sigma_bouton=max_bouton_size;
%max_sigma_bouton=max_bouton_size/4;
noise_level=0.3;

% background
%A_background=0.3*ones(1,N_background);
A_background=bg_axon.amp;
min_A_background=0; max_A_background=max(I_axon);
mu_background=bg_axon.mu; min_mu_background=0; max_mu_background=LLL;
sigma_background=bg_axon.sig*2; min_sigma_background=min_background_size/4; %max_sigma_background=inf;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
A=[A_bouton(:)',A_background(:)'];
mu=[mu_bouton(:)',mu_background(:)'];
sigma=[sigma_bouton(:)',sigma_background(:)'];
N=N_bouton+N_background;
Label=[ones(1,N_bouton),zeros(1,N_background)];

if strcmp(shape,'G')
    Ff = @(x,A,mu,sigma) (ones(size(x))*A).*exp(-(x*ones(size(A))-ones(size(x))*mu).^2./(ones(size(x))*sigma.^2)./2);
elseif strcmp(shape,'L')
    Ff = @(x,A,mu,sigma) (ones(size(x))*A)./((x*ones(size(A))-ones(size(x))*mu).^2./(ones(size(x))*sigma.^2)+1);
else
    disp('Incorrect shape')
    return
end

if isplot
    figure(10)
    plot(d_axon,I_axon,'b-'),hold on
    plot(d_axon(naninds),I_axon(naninds),'xr','MarkerSize',15)
    hold on
end

count=1;
betta.A=betta0.A;
betta.sigma=betta0.sigma;
betta.mu=betta0.mu;
delE=inf;

if strcmp(shape,'G')
    F=(ones(size(d_axon))*A).*exp(-(d_axon*ones(size(A))-ones(size(d_axon))*mu).^2./(ones(size(d_axon))*sigma.^2)./2);
elseif strcmp(shape,'L')
    F=(ones(size(d_axon))*A)./((d_axon*ones(size(A))-ones(size(d_axon))*mu).^2./(ones(size(d_axon))*sigma.^2)+1);
end
I_fit=sum(F,2);
E=sum((I_fit-I_axon).^2);

if isplot
    h=plot(d_axon,I_fit,'r-');
    drawnow
end

while (count<=Nsteps && abs(delE)/E>min_change)
    d_axon_mu=d_axon*ones(size(A))-ones(size(d_axon))*mu;
    sigma_rep2=ones(size(d_axon))*(sigma.^2);
    if strcmp(shape,'G')
        FA=exp(-d_axon_mu.^2./sigma_rep2./2);
        F=(ones(size(d_axon))*A).*FA;
        Fmu=(d_axon_mu./sigma_rep2).*F;
        Fsigma=(d_axon_mu.^2./(ones(size(d_axon))*(sigma.^3))).*F;
    elseif strcmp(shape,'L')
        FA=1./(d_axon_mu.^2./sigma_rep2+1);
        F=(ones(size(d_axon))*A).*FA;
        Fmu=2.*(d_axon_mu./sigma_rep2).*F.*FA;
        Fsigma=2.*F.*(1-FA)./(ones(size(d_axon))*sigma);
    end
    
    del_A=(I_fit-I_axon)'*FA;
    del_mu=(I_fit-I_axon)'*Fmu;
    del_sigma=(I_fit-I_axon)'*Fsigma;
    
    A_temp=A-betta.A.*del_A;
    A_temp(A_temp<min_A_bouton & Label==1)=min_A_bouton;
    A_temp(A_temp<min_A_background & Label==0)=min_A_background;
    A_temp(A_temp>max_A_bouton & Label==1)=max_A_bouton;
    A_temp(A_temp>max_A_background & Label==0)=max_A_background;
    mu_temp=mu-betta.mu.*del_mu;
    mu_temp(mu_temp<min_mu_bouton & Label==1)=min_mu_bouton;
    mu_temp(mu_temp<min_mu_background & Label==0)=min_mu_background;
    mu_temp(mu_temp>max_mu_bouton & Label==1)=max_mu_bouton;
    mu_temp(mu_temp>max_mu_background & Label==0)=max_mu_background;
    sigma_temp=sigma-betta.sigma.*del_sigma;
    sigma_temp(sigma_temp<min_sigma_bouton & Label==1)=min_sigma_bouton;
    sigma_temp(sigma_temp<min_sigma_background & Label==0)=min_sigma_background;
    sigma_temp(sigma_temp>max_sigma_bouton & Label==1)=max_sigma_bouton;
    %sigma_temp(sigma_temp>max_sigma_background & Label==0)=max_sigma_background;
    
    if strcmp(shape,'G')
        F=(ones(size(d_axon))*A_temp).*exp(-(d_axon*ones(size(A_temp))-ones(size(d_axon))*mu_temp).^2./(ones(size(d_axon))*sigma_temp.^2)./2);
    elseif strcmp(shape,'L')
        F=(ones(size(d_axon))*A_temp)./((d_axon*ones(size(A_temp))-ones(size(d_axon))*mu_temp).^2./(ones(size(d_axon))*sigma_temp.^2)+1);
    end
    I_fit_temp=sum(F,2);
    E_temp=sum((I_fit_temp-I_axon).^2);
    delE=E_temp-E;
    if delE<0
        A=A_temp;
        mu=mu_temp;
        sigma=sigma_temp;
        I_fit=I_fit_temp;
        E=E_temp;
        count=count+1;
        
        % merge peaks based on distance
        if count>100 && mod(count,15)==0
            merge_ind=find(mu(2:end)-mu(1:end-1)<min_d & Label(2:end)==1 & Label(1:end-1)==1 & A(2:end)>0 & A(1:end-1)>0);
            if ~isempty(merge_ind)
                [~,ind]=min(mu(merge_ind+1)-mu(merge_ind));
                merge_ind=merge_ind(ind);
                mu(merge_ind)=(mu(merge_ind)*A(merge_ind)+mu(merge_ind+1)*A(merge_ind+1))/(A(merge_ind)+A(merge_ind+1));
                mu(merge_ind+1)=[];
                sigma(merge_ind)=(sigma(merge_ind)*A(merge_ind)+sigma(merge_ind+1)*A(merge_ind+1))/(A(merge_ind)+A(merge_ind+1));
                sigma(merge_ind+1)=[];
                A(merge_ind)=A(merge_ind)+A(merge_ind+1);
                A(merge_ind+1)=[];
                Label(merge_ind+1)=[];
                
                if strcmp(shape,'G')
                    F=(ones(size(d_axon))*A).*exp(-(d_axon*ones(size(A))-ones(size(d_axon))*mu).^2./(ones(size(d_axon))*sigma.^2)./2);
                elseif strcmp(shape,'L')
                    F=(ones(size(d_axon))*A)./((d_axon*ones(size(A))-ones(size(d_axon))*mu).^2./(ones(size(d_axon))*sigma.^2)+1);
                end
                I_fit=sum(F,2);
                E=sum((I_fit-I_axon).^2);
                N=N-1;
            end
        end
        
        % merge peaks based on overlap
        if count>100 && mod(count,15)==5
            if strcmp(shape,'G')
                aa=sigma(2:end).^(-2)-sigma(1:end-1).^(-2);
                bb2=-(mu(2:end)./sigma(2:end).^2-mu(1:end-1)./sigma(1:end-1).^2);
                cc=mu(2:end).^2./sigma(2:end).^2-mu(1:end-1).^2./sigma(1:end-1).^2-2.*log(A(2:end)./A(1:end-1));
            elseif strcmp(shape,'L')
                aa=1./A(2:end)./sigma(2:end).^2-1./A(1:end-1)./sigma(1:end-1).^2;
                bb2=-(mu(2:end)./A(2:end)./sigma(2:end).^2-mu(1:end-1)./A(1:end-1)./sigma(1:end-1).^2);
                cc=mu(2:end).^2./A(2:end)./sigma(2:end).^2-mu(1:end-1).^2./A(1:end-1)./sigma(1:end-1).^2+1./A(2:end)-1./A(1:end-1);
            end
            D=bb2.^2-aa.*cc;
            merge_ind=find(D<0 & Label(2:end)==1 & Label(1:end-1)==1 & A(1:end-1)>0);
            if ~isempty(merge_ind)
                [~,ind]=min(mu(merge_ind+1)-mu(merge_ind));
                merge_ind=merge_ind(ind);
            else
                x1=(-bb2+D.^0.5)./aa;
                x1(aa==0)=-cc(aa==0)./bb2(aa==0)./2;
                x2=(-bb2-D.^0.5)./aa;
                x2(aa==0)=x1(aa==0);
                ind1=(x1>mu(1:end-1) & x1<mu(2:end));
                ind2=(x2>mu(1:end-1) & x2<mu(2:end));
                x0=nan(size(x1));
                x0(ind1)=x1(ind1);
                x0(ind2)=x2(ind2);
                
                if strcmp(shape,'G')
                    hh=A(1:end-1).*exp(-(x0-mu(1:end-1)).^2./2./sigma(1:end-1).^2);
                elseif strcmp(shape,'L')
                    hh=A(1:end-1)./((x0-mu(1:end-1)).^2./sigma(1:end-1).^2+1);
                end
                merge_ind=find(( hh>alpha.*min([A(1:end-1);A(2:end)])) & Label(2:end)==1 & Label(1:end-1)==1 & A(2:end)>0 & A(1:end-1)>0);
                if ~isempty(merge_ind)
                    [~,ind]=max(hh(merge_ind)./min([A((merge_ind));A((merge_ind)+1)]));
                    merge_ind=merge_ind(ind);
                end
            end
            
            %{
            if strcmp(shape,'G')
                hh=exp(-(mu(2:end)-mu(1:end-1)).^2./2./(sigma(2:end).^2+sigma(1:end-1).^2));
            elseif strcmp(shape,'L')
                hh=exp(-(mu(2:end)-mu(1:end-1)).^2./2./(sigma(2:end).^2+sigma(1:end-1).^2));
            end
            merge_ind=find(hh>alpha & Label(2:end)==1 & Label(1:end-1)==1 & A(2:end)>0 & A(1:end-1)>0);
            [~,ind]=max(hh(merge_ind));
            merge_ind=merge_ind(ind);
            %}
            
            if ~isempty(merge_ind)
                mu(merge_ind)=(mu(merge_ind)*A(merge_ind)+mu(merge_ind+1)*A(merge_ind+1))/(A(merge_ind)+A(merge_ind+1));
                mu(merge_ind+1)=[];
                sigma(merge_ind)=(sigma(merge_ind)*A(merge_ind)+sigma(merge_ind+1)*A(merge_ind+1))/(A(merge_ind)+A(merge_ind+1));
                sigma(merge_ind+1)=[];
                A(merge_ind)=A(merge_ind)+A(merge_ind+1);
                A(merge_ind+1)=[];
                Label(merge_ind+1)=[];
                
                if strcmp(shape,'G')
                    F=(ones(size(d_axon))*A).*exp(-(d_axon*ones(size(A))-ones(size(d_axon))*mu).^2./(ones(size(d_axon))*sigma.^2)./2);
                elseif strcmp(shape,'L')
                    F=(ones(size(d_axon))*A)./((d_axon*ones(size(A))-ones(size(d_axon))*mu).^2./(ones(size(d_axon))*sigma.^2)+1);
                end
                I_fit=sum(F,2);
                E=sum((I_fit-I_axon).^2);
                N=N-1;
            end
        end
        
        % remove small boutons based on amplitude
        if count>100 && mod(count,15)==10
            %min_A=n_stds*std(I_axon-I_fit);
            %min_A=n_medians*median(abs(I_axon-I_fit));
            %remove_ind=find(A<min_A & Label==1);
            
            remove_ind=find(A<noise_level & Label==1);
            if ~isempty(remove_ind)
                [~,ind]=min(A(remove_ind));
                remove_ind=remove_ind(ind);
                A(remove_ind)=[];
                mu(remove_ind)=[];
                sigma(remove_ind)=[];
                Label(remove_ind)=[];
                
                if strcmp(shape,'G')
                    F=(ones(size(d_axon))*A).*exp(-(d_axon*ones(size(A))-ones(size(d_axon))*mu).^2./(ones(size(d_axon))*sigma.^2)./2);
                elseif strcmp(shape,'L')
                    F=(ones(size(d_axon))*A)./((d_axon*ones(size(A))-ones(size(d_axon))*mu).^2./(ones(size(d_axon))*sigma.^2)+1);
                end
                I_fit=sum(F,2);
                E=sum((I_fit-I_axon).^2);
                N=N-1;
            end
        end
       
        if mod(count,100)==0
            betta=betta0;
        end
    else
        betta.A=betta.A/1.2;
        betta.sigma=betta.sigma./1.2;
        betta.mu=betta.mu./1.2;
        count=count+1;
        if mod(count,10)==0
            disp([count,N,E,abs(delE)/E])
        end
    end
end

if isplot
    delete(h)
    plot(d_axon,I_fit,'r-');
end
if exist('g','var')
    delete(g);
end
Bouton_ind=find(Label==1);

if isplot
    col=lines(length(Bouton_ind));
    for i=1:length(Bouton_ind)
        plot(d_axon,Ff(d_axon,A(Bouton_ind(i)),mu(Bouton_ind(i)),sigma(Bouton_ind(i))),'-','Color',col(i,:))
    end
end

Background_ind=find(Label==0);
if isplot
    plot(d_axon,sum(Ff(d_axon,A(Background_ind),mu(Background_ind),sigma(Background_ind)),2),'k-')
end

A=A(:);mu=mu(:);sigma=sigma(:);Label=Label(:);
inds=nan(size(mu));
for k=1:length(mu)
    [~,inds(k)]=min(abs(d_axon-mu(k)));
end

fg_new.ind=inds(Label==1);
fg_new.mu=mu(Label==1);
fg_new.sig=sigma(Label==1);
fg_new.amp=A(Label==1);

%Sort the fields by distance to match format in fg_axon.
[~,si]=sort(fg_new.ind);
fg_new.ind=fg_new.ind(si);
fg_new.mu=fg_new.mu(si);
fg_new.sig=fg_new.sig(si);
fg_new.amp=fg_new.amp(si);

fg_new.id=fg_axon.id;
fg_new.manid=fg_axon.manid;
fg_new.autoid=fg_axon.manid;
fg_new.flag=fg_axon.flag;
%Convert to column vectors, and finding indices - Rohan

bg_new.ind=inds(Label==0);
bg_new.mu=mu(Label==0);
bg_new.sig=sigma(Label==0);
bg_new.amp=A(Label==0);