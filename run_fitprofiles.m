%See method technical summary for fields that are created in the profile.
pathlist;
paramset;

%Options
channel={'G'};
filter={'Gauss2'};

%Profiles to create

%Conditions
%{
animal={'DL001'};
%timepoint=cellstr([char(double('P'):double('T'))]');
timepoint={'A','B','D','E','F','H','I'};
%timepoint={'I'};
section={'002'};
axon={[1,2,3,4,7,8,9,10,11,12,13,14,17,18,19,20]};
%axon={[1]};
tracer={[]}; %Default: tracer = {[]}; for 'A001.swc';
setim=[];
%setim='DL001I002';%For manual vs optimized trace comparisons
%}

%EM
%{
animal={'DL100'};
timepoint={'I'};%cellstr([char(double('I'))]);
section={'005'};
axon={[1,2,3,4]};
tracer={[]}; %Default: tracer = {[]}; for 'A001.swc';
setim=[];
%}

%Full set
animal={'DL083'};
timepoint={'B'};%cellstr([char(double('I'))]);
section={'001'};
axon={[99]};
tracer={[]}; %Default: tracer = {[]}; for 'A001.swc';
setim=[];
%}

%errorlogs
if ~exist(isunixispc([err_pth,'run_fitprofiles.txt']), 'file')
    display('No error log present, creating log')
    fid=fopen([err_pth,'run_fitprofiles.txt'],'wt+');fclose(fid);
end

for an=1:numel(animal)
    for se=1:numel(section)
        for ti=1:numel(timepoint)
            stackid=[animal{an},timepoint{ti},section{se}];
            imid=setim;
            if isempty(imid)
                imid=stackid;
            end
            found_ch=false(numel(channel));
            for ch=1:numel(channel)
                if exist(isunixispc([im_pth,imid,channel{ch},'.mat']),'file')
                    temp=load(isunixispc([im_pth,imid,channel{ch},'.mat']),'Original');
                    IM.(channel{ch})=temp.Original;clear temp;
                    found_ch(ch)=true;
                else
                    display([stackid,'-',channel{ch},' image not found']);
                    msg=([stackid,'-',channel{ch},' - image not found']);
                    [~,sysname]=system('hostname');
                    dt=datestr(datetime('now'),'dd-mm-yy');
                    fid=fopen([err_pth,'run_fitprofiles.txt'],'a');
                    fprintf(fid,['\n',msg,' ',sysname(1:3),' ',dt]);
                    fclose(fid);
                end
            end
            
            for ax=1:numel(axon{se})
                for tr=1:numel(tracer)
                    axonstr=[sprintf('A%03d',axon{se}(ax)),tracer{tr}];
                    
                    if exist(isunixispc([profile_pth,stackid,'/',axonstr,'.mat']),'file')
                        Dat=load(isunixispc([profile_pth,stackid,'/',axonstr,'.mat']));
                        Dat.d.optim = vx2um(Dat.r.optim);
                        
                        for ch=1:numel(channel)
                            if found_ch(ch)
                                for fi=1:numel(filter)
                                    [temp.I,temp.R]=profilefilters(Dat.r.optim,IM.(channel{ch}),filter{fi});
                                    Dat.I.(channel{ch}).(filter{fi}).raw=temp.I;
                                    Dat.I.(channel{ch}).(filter{fi}).caliber=temp.R;
                                    Dat.I.(channel{ch}).(filter{fi}).norm=temp.I./mean(temp.I(~Dat.annotate.ignore));
                                    
                                    if strcmp(filter{fi},'Gauss2')
                                        [Dat.fit.(channel{ch}).(filter{fi}).fg,...
                                            Dat.fit.(channel{ch}).(filter{fi}).bg]=fitbackground2(...
                                            Dat.I.(channel{ch}).(filter{fi}).norm,...
                                            Dat.d.optim,...
                                            Dat.fit.(channel{ch}).('LoGxy').fg,...
                                            Dat.fit.(channel{ch}).('LoGxy').bg);
                                    else
                                        [temp.amp,temp.mu,temp.sig,temp.Label,temp.ind]=fitpeaks(Dat.I.(channel{ch}).(filter{fi}).norm,Dat.d.optim);
                                        
                                        %Foreground
                                        Dat.fit.(channel{ch}).(filter{fi}).fg.ind=temp.ind(temp.Label>0);
                                        Dat.fit.(channel{ch}).(filter{fi}).fg.mu=temp.mu(temp.Label>0);
                                        Dat.fit.(channel{ch}).(filter{fi}).fg.sig=temp.sig(temp.Label>0);
                                        Dat.fit.(channel{ch}).(filter{fi}).fg.amp=temp.amp(temp.Label>0);
                                        
                                        %Background
                                        Dat.fit.(channel{ch}).(filter{fi}).bg.ind=temp.ind(temp.Label<0);
                                        Dat.fit.(channel{ch}).(filter{fi}).bg.mu=temp.mu(temp.Label<0);
                                        Dat.fit.(channel{ch}).(filter{fi}).bg.sig=temp.sig(temp.Label<0);
                                        Dat.fit.(channel{ch}).(filter{fi}).bg.amp=temp.amp(temp.Label<0);
                                        clear temp;
                                        
                                        %Registration
                                        Dat.fit.(channel{ch}).(filter{fi}).fg.id=nan(size(Dat.fit.(channel{ch}).(filter{fi}).fg.ind));
                                        Dat.fit.(channel{ch}).(filter{fi}).fg.manid=nan(size(Dat.fit.(channel{ch}).(filter{fi}).fg.ind));
                                        Dat.fit.(channel{ch}).(filter{fi}).fg.autoid=nan(size(Dat.fit.(channel{ch}).(filter{fi}).fg.ind));
                                        Dat.fit.(channel{ch}).(filter{fi}).fg.flag=nan(size(Dat.fit.(channel{ch}).(filter{fi}).fg.ind));
                                        
                                        Dat.fit.(channel{ch}).(filter{fi}).d.man=Dat.d.optim;
                                        Dat.fit.(channel{ch}).(filter{fi}).d.auto=Dat.d.optim;
                                        Dat.fit.(channel{ch}).(filter{fi}).deform.man=nan(size(Dat.d.optim));
                                        Dat.fit.(channel{ch}).(filter{fi}).deform.auto=nan(size(Dat.d.optim));
                                    end
                                end
                            end
                        end
                        %save & clear here
                        Dat = orderfields(Dat,{'AM','r','d','I','fit','annotate','id'});
                        save(isunixispc([profile_pth,stackid,'/',axonstr,'.mat']),'-struct','Dat')
                        clear Dat;
                    else
                        display([isunixispc([profile_pth,stackid,'/',axonstr,'.mat']),' Profile not found!'])
                        msg=([stackid,'-',axonstr,' - Profile not found']);
                        [~,sysname]=system('hostname');
                        dt=datestr(datetime('now'),'dd-mm-yy');
                        fid=fopen([err_pth,'run_fitprofiles.txt'],'a');
                        fprintf(fid,['\n',msg,' ',sysname(1:3),' ',dt]);
                        fclose(fid);
                    end
                end
            end
            clear IM;
        end
    end
end