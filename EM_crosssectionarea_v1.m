%This script reads data from .obj files and returns cross section area of
%the axon as a function of position for a single axon. A newer, more stable
%version of this script was implemented.

parentdir=pwd;
parentdir=parentdir(1:(strfind(parentdir,'Plasticity')+numel('Plasticity')-1));
for axid=1
    %EM data calculations
    centerline_fname=isunixispc([parentdir,'\dat\EMData\Blender2Mat\Axon',num2str(axid),'Centerline.obj']);
    surf_fname=isunixispc([parentdir,'\dat\EMData\Blender2Mat\Axon',num2str(axid),'.obj']);
    
    %Read .obj file to get centerline nodes in correct order
    nodeind=1;
    fid = fopen(centerline_fname);
    tline = fgetl(fid);
    r=cell(1);
    while ischar(tline)
        if ~isempty(regexp(tline,'^v ','start'))
            t=regexp(tline,' ','split');
            r{nodeind}=[str2double(t{2}),str2double(t{3}),str2double(t{4})];
            nodeind=nodeind+1;
        end
        tline = fgetl(fid);
    end
    r=cell2mat(r');
    fclose(fid);
    
    %Read adjacency matrix for centerline nodes
    AM=zeros(size(r,1));
    fid = fopen(centerline_fname);
    tline = fgetl(fid);
    while ischar(tline)
        if ~isempty(regexp(tline,'^l ','start'))
            t=regexp(tline,' ','split');
            i=str2double(t{2});
            j=str2double(t{3});
            AM(i,j)=1;
            AM(j,i)=1;
        end
        tline = fgetl(fid);
    end
    fclose(fid);
    
    %Read surface vertices
    svind=1;
    svn_ind=1;
    fid = fopen(surf_fname);
    tline = fgetl(fid);
    SV=cell(1);
    SVn=cell(1);
    while ischar(tline)
        if ~isempty(regexp(tline,'^v ','start'))
            t=regexp(tline,' ','split');
            SV{svind}=[str2double(t{2}),str2double(t{3}),str2double(t{4})];
            svind=svind+1;
        end
        tline = fgetl(fid);
    end
    fclose(fid);
    SV=cell2mat(SV');
    
    %Checks
    figure(11),clf(11);movegui(11,'northeast')
    plotAM(AM,r,[]),hold on
    plot3(SV(:,2),SV(:,1),SV(:,3),'.','MarkerSize',1,'Color',[0.7 0.7 0])
    axis equal;
    box on;
    
    [AM, r, ~] = AdjustPPM(AM,r,zeros(size(r,1),1),10);%Default ppm is 50
    terms=find(sum(AM,1)==1);
    %Ordering AM and r
    %Step along trace assuming no branching
    ind=nan(size(AM,1),1);
    ind(1)=terms(1);
    [~,ind(2)]=find(AM(ind(1),:));
    i=3;
    while ind(i-1)~=terms(2)
        [~,next]=find(AM(ind(i-1),:));
        ind(i)=next(next~=ind(i-2));
        i=i+1;
    end
    AM=AM(ind,:);
    AM=AM(:,ind);
    r=r(ind,:);
    d=cumsum(sum((r((2:end),:)-r((1:end-1),:)).^2,2).^0.5);
    d=[0;d(:)];
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    thr1=1.8;  %Box dimensions
    thr2=0.02; %distance along tangent direction: Was 0.03
    thr3=1.8;  %distance inplane
    n_iter=100;%Iterations over starting points to create appropriate loop
    n_kmean_iter=100;%Number of kmeans iterations
    loopnodes_dthr=0.3;%Max allowed dist. between successive nodes of the loop
    n_steps=5; %inter node index difference for calculation of tangent vector
    LMz=false;  %True if calculating cross section parallel to LM z direction
    Zemp =[0.0495,0.0503,-0.9975];%Calculated using linear transform [Tel,bel]; approximate was [0.0523,0.0723,-0.9960]
    startind=1+n_steps;%Default 1+n_steps;
    endind=size(r,1)-n_steps;%Default: size(r,1)-n_steps;
    isplot=true;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    r_CS=nan(size(r,1),n_kmean_iter);
    A=nan(size(r,1),n_kmean_iter);
    view(Zemp);
    
    nodeind=startind;
    while nodeind<=endind
        display(nodeind)
        tang_nodes=[nodeind-n_steps,nodeind+n_steps];
        
        t1=r(nodeind,:)-r(tang_nodes(1),:);
        t2=r(tang_nodes(2),:)-r(nodeind,:);
        tgt=(t1+t2)./norm(t1+t2);
        
        if LMz
            tgt=tgt-(tgt*Zemp')*Zemp;%Remove component along LM z-axis
        end
        
        inplane1=[tgt(2),-tgt(1),0];% => constructed to ensure dot product with tgt is 0
        inplane1=inplane1./norm(inplane1);
        inplane2= cross(inplane1,tgt);
        inplane2=inplane2./norm(inplane2);
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %Limit calculation to vertices within box around particular centerline
        %vertex
        box_ind =...
            SV(:,1)>(r(nodeind,1)-thr1) & SV(:,1)<(r(nodeind,1)+thr1) &...
            SV(:,2)>(r(nodeind,2)-thr1) & SV(:,2)<(r(nodeind,2)+thr1) &...
            SV(:,3)>(r(nodeind,3)-thr1) & SV(:,3)<(r(nodeind,3)+thr1);
        
        %Within box criteria
        box_ind=find(box_ind);
        SV_keep=SV(box_ind,:);
        SV_rel=bsxfun(@minus,SV(box_ind,:),r(nodeind,:));
        
        %Along tangent distance criteria
        keep=(abs(SV_rel*tgt')<thr2);
        
        box_ind=box_ind(keep);
        SV_rel=SV_rel(keep,:);
        SV_keep=SV_keep(keep,:);
        
        %Perpendicular to tangent distance criteria: 1
        alongtgt=bsxfun(@times,tgt,(SV_rel*tgt'));
        SV_inplane_dist=(sum((SV_rel-alongtgt).^2,2)).^0.5;
        keep=(SV_inplane_dist<thr3);
        
        box_ind=box_ind(keep);
        SV_rel=SV_rel(keep,:);
        SV_keep=SV_keep(keep,:);
        
        %Perpendicular to tangent distance criteria: 2
        %{
            alongtgt=bsxfun(@times,tgt,(SV_rel*tgt'));
            SV_inplane_dist=(sum((SV_rel-alongtgt).^2,2)).^0.5;
            keep=SV_inplane_dist<(mean(SV_inplane_dist)+2*std(SV_inplane_dist));

            box_ind=box_ind(keep);
            SV_rel=SV_rel(keep,:);
            SV_keep=SV_keep(keep,:);
        %}
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        x0=SV_rel*inplane1';
        y0=SV_rel*inplane2';
        if numel(x0)>150
            k=75;
        elseif numel(x0)<20
            k=numel(x0);
        elseif numel(x0)>20 && numel(x0)<150
            k=ceil(numel(x0)/2);
        end
        
        figure(11)
        h=plot3(SV(box_ind,2),SV(box_ind,1),SV(box_ind,3),'xr','MarkerSize',2);
        ylim([r(nodeind,1)-2, r(nodeind,1)+2])
        xlim([r(nodeind,2)-2, r(nodeind,2)+2])
        drawnow;
        
        %To graphically remove points from consideration
        figure(2),clf(2),movegui(2,'southeast')
        xlim(3*[-0.6 0.6]);ylim(3*[-0.6 0.6]);axis square;box on;hold on
        g1=plot(x0,y0,'.','Color',[0.5 0.5 0.5]);
        drawnow;
        %{
        %Create X using paintbrush
        [val,remindx0]=min(abs(bsxfun(@minus,x0(:),X(:,1)')),[],1);
        remindx0=remindx0(val<0.01);
        x0(remindx0)=[];
        y0(remindx0)=[];

        figure(2),clf(2),movegui(2,'southeast')
        xlim(3*[-0.6 0.6]);ylim(3*[-0.6 0.6]);axis square;box on;hold on
        g1=plot(x0,y0,'.','Color',[0.3 0.3 0.3]);
        %}
        1;
        for kmean_iter=1:n_kmean_iter
            
            [~,C] = kmeans([x0,y0],k);
            x=C(:,1);y=C(:,2);
            Dinit=(bsxfun(@minus,x,x').^2+(bsxfun(@minus,y,y').^2));
            Dinit=Dinit+diag(nan*ones(size(Dinit,1),1)); %To avoid self connections
            
            tempAarray=zeros(n_iter,1);
            listoflist=cell(n_iter,1);
            for ii=1:n_iter
                %All to all distances
                D=Dinit;
                d_orig=(x.^2+y.^2);
                list=cell(1);
                l=1;
                while sum(nansum(D,1)>0)>2
                    temp=find(~isnan(d_orig));
                    list{l}(1)=temp(randi(numel(temp)));
                    i=1;
                    [~,list{l}(i+1)]=nanmin(D(list{l}(i),:));
                    D(list{l}(i+1),list{l}(i))=nan;
                    i=i+1;
                    while list{l}(i)~=list{l}(1)
                        [~,list{l}(i+1)]=nanmin(D(list{l}(i),:));
                        D(:,list{l}(i))=nan;
                        i=i+1;
                    end
                    d_orig(list{l})=nan;
                    D(:,list{l}(1))=nan;
                    l=l+1;
                end
                
                cm=nan(numel(list),1);
                for l=1:numel(list)
                    cm(l)=mean(x(list{l}))^2+mean(y(list{l}))^2;
                end
                [~,l]=min(cm);
                
                tempA=0;
                for i=1:numel(list{l})-1
                    tempA=tempA+0.5.*(x(list{l}(i))*y(list{l}(i+1))-x(list{l}(i+1))*y(list{l}(i)));
                end
                tempAarray(ii)=abs(tempA);
                listoflist{ii}=list{l};
            end
            minsofar=inf;
            redo=true;
            while redo
                [A(nodeind,kmean_iter),iimax]=nanmax(tempAarray);
                dtemp=((x(listoflist{iimax}(1:end-1))-x(listoflist{iimax}(2:end))).^2+...
                    (y(listoflist{iimax}(1:end-1))-y(listoflist{iimax}(2:end))).^2).^0.5;
                if max(dtemp)>loopnodes_dthr
                    if max(dtemp)<minsofar
                        bestmax=max(dtemp);
                        bestind=iimax;
                        minsofar=bestmax;
                    end
                    tempAarray(iimax)=nan;
                    redo=true;
                else
                    redo=false;
                end
                
                if nnz(~isnan(tempAarray))==0
                    redo=false;
                    iimax=bestind;
                end
                
            end
            r_CS(nodeind,kmean_iter)=mean((x(listoflist{iimax}).^2+y(listoflist{iimax}).^2).^0.5);
            
            if isplot
                figure(2),movegui(2,'northwest')
                xlim(3*[-0.6 0.6]);ylim(3*[-0.6 0.6]);axis square;box on;hold on
                g1=plot(x0,y0,'.','Color',[0.5 0.5 0.5]);
                g2=plot(x,y,'xb');
                g3=plot(x(listoflist{iimax}),y(listoflist{iimax}),'-r');
                drawnow;
                
                if exist('g1','var')
                    delete(g1);
                end
                if exist('g2','var')
                    delete(g2);
                end
                if exist('g3','var')
                    delete(g3);
                end
            end
        end
        if exist('h','var')
            delete(h);
        end
        nodeind=nodeind+1;
    end
    
    % figure(5),movegui(5,'southwest')
    % plot(d,r_CS),hold on
    % xlabel('d (A.U.)');
    % ylabel('<r>');
    % box on;
    % set(gca,'FontName','Calibri','FontSize',13);
    
    % figure(6),movegui(6,'south')
    % plot(d,pi.*r_CS.^2),hold on
    % xlabel('d (A.U.)');
    % ylabel('Cross section area');
    % box on;
    % set(gca,'FontName','Calibri','FontSize',13);
    
    figure(7),movegui(7,'southeast')
    plot(d,median(A,2)),hold on
    xlabel('d (A.U.)');
    ylabel('Cross section area');
    box on;
    set(gca,'FontName','Calibri','FontSize',13);
    
    fname=isunixispc([parentdir,'\dat\EMData\CrossSectionEst\A00',num2str(axid),'_z',num2str(LMz),'_',num2str(startind),'-',num2str(endind),'.mat']);
%    save(fname,'AM','r','SV','A','r_CS','d','thr1','thr2','thr3','n_iter','n_kmean_iter','loopnodes_dthr','n_steps','LMz','Zemp','startind','endind');
end