%Notes:
%v4 contains data that was sent in the update
%Data for Axon 3 in v5 comes from newly optimized trace (A007)
%v6 contains data that is shown in the paper draft.
%v7 includes mitochondria volumes

parentdir=pwd;
parentdir=parentdir(1:(strfind(parentdir,'Plasticity')+numel('Plasticity')-1));
[xldat,temp,txt]=xlsread(isunixispc([parentdir,'\dat\EMData\Size data.xlsx']),'v7');

txt={txt{2:20,2}}';
cc=lines(4);
cc=cc(xldat(:,1),:);
no2ndnorm=false;

for t=1:numel(txt)
    if isa(txt{t},'double')
        txt{t}=num2str(txt{t});
    end
end
Ff = @(x,A,mu,sigma) (ones(size(x))*A).*exp(-(x*ones(size(A))-ones(size(x))*mu).^2./(ones(size(x))*sigma.^2)./2);
filter={'LoGxy'};
bgfilt={'Gauss2'};
for f=1:numel(filter)
    %Extra normalization
    extranorm.G=ones(4,1);
    extranorm.R=ones(4,1);
    axlbl=([1,2,3,4]);
    channel={'G','R'};
    
    for ax=1:4
        for ch=1:numel(channel)
            A=load(isunixispc([parentdir,'\dat\EMData\Profiles\DL100I005\A00',num2str(axlbl(ax)),'.mat']));

            bg=zeros(size(A.d.optim));
            for i=1:numel(A.fit.(channel{ch}).(bgfilt{1}).bg.ind)
                temp=A.fit.(channel{ch}).(bgfilt{1}).bg;
                bg=bg+Ff(A.d.optim,temp.amp(i),temp.mu(i),temp.sig(i));
            end
            
            if no2ndnorm
                extranorm.(channel{ch})(ax)=1;
            else
                extranorm.(channel{ch})(ax)=mean(bg);
            end
            clear A;
        end
    end
    
    fprintf([filter{f},'Green axonwise normalization factors'])
    display(extranorm.G)
    fprintf([filter{f},'Red axonwise normalization factors'])
    display(extranorm.R)
    
    extranorm.G=extranorm.G(xldat(:,1));
    extranorm.R=extranorm.R(xldat(:,1));
    
    xldat(8,2:end)=nan;%Terminal bouton excluded
    hf=13;
        
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %Green Channel
    figure(hf+1),movegui(hf+1,'northwest')
    xcolno=[11,12];
    %xcolno=[18];%Use no2ndnorm=true; Correlations will be slightly different
    %because of rounding errors.
    ycolno=(5);
    xx=sum(xldat(:,xcolno),2)./extranorm.G;
    yy=xldat(:,ycolno);
    scatter(xx,yy,[],cc,'filled'),axis square,box on,hold on
    text(xx+0.1,yy,txt,'Color',[0.4,0.4,0.4])
    xlabel(['Green intensity ',filter{f}])
    ylabel('Bouton-Volume (\mu{m}^{3})')
    xlim([0 16])
    %ylim([0 ceil(max(yy))])
    ylim([0 1.2]);
    C=corrcoef(xx,yy,'rows','pairwise');
    notnan=~isnan(xx) & ~isnan(yy);
    [ff,goodness]=fit(xx(notnan),yy(notnan),fittype('a*x+b'),'StartPoint',[1,0]);
    plot([0,16],ff([0,16]),'-r')
    text(1,0.9,['R^{2} = ',num2str(goodness.rsquare)])
    set(gca,'XTick',[0:4:16]);
    drawnow
    
    figure(hf+2),movegui(hf+2,'northeast')
    %xcolno=[11,12];
    ycolno=(8);
    xx=sum(xldat(:,xcolno),2)./extranorm.G;
    yy=xldat(:,ycolno);
    scatter(xx,yy,[],cc,'filled'),axis square,box on,hold on
    text(xx+0.1,yy,txt,'Color',[0.4,0.4,0.4])
    xlabel(['Green intensity ',filter{f}])
    ylabel('Cross-Section (\mu{m}^{2})')
    xlim([0 16])
    %ylim([0 ceil(max(yy))])
    ylim([0 1.2])
    C=corrcoef(xx,yy,'rows','pairwise');
    notnan=~isnan(xx) & ~isnan(yy);
    [ff,goodness]=fit(xx(notnan),yy(notnan),fittype('a*x+b'),'StartPoint',[1,0]);
    plot([0,16],ff([0,16]),'-r')
    text(1,0.9,['R^{2} = ',num2str(goodness.rsquare)])
    drawnow
    
    figure(hf+3),movegui(hf+3,'southwest')
    %xcolno=[11,12];
    ycolno=(9);
    yy=xldat(:,ycolno);
    xx=sum(xldat(:,xcolno),2)./extranorm.G;
    scatter(xx,yy,[],cc,'filled'),axis square,box on,hold on
    text(xx+0.1,yy,txt,'Color',[0.4,0.4,0.4])
    xlabel(['Green intensity ',filter{f}])
    ylabel('z-Cross-Section (\mu{m}^{2})')
    xlim([0 ceil(max(xx))])
    ylim([0 ceil(max(yy))])
    C=corrcoef(xx,yy,'rows','pairwise');
    notnan=~isnan(xx) & ~isnan(yy);
    [ff,goodness]=fit(xx(notnan),yy(notnan),fittype('a*x+b'),'StartPoint',[1,0]);
    plot([0,16],ff([0,16]),'-r')
    set( gca,'XTick',0:4:16);
    text(1,0.9,['R^{2} = ',num2str(goodness.rsquare)])
    title(num2str(C(1,2)))
    drawnow
    
    figure(hf+4),movegui(hf+4,'southeast')
    %xcolno=[11,12];
    ycolno=(6);
    xx=sum(xldat(:,xcolno),2)./extranorm.G;
    yy=xldat(:,ycolno);
    scatter(xx,yy,[],cc,'filled'),axis square,box on,hold on
    text(xx+0.1,yy,txt,'Color',[0.4,0.4,0.4])
    xlabel(['Green intensity ',filter{f}])
    ylabel('PSD surface area (\mu{m}^{2})')
    xlim([0 16])
    ylim([0 ceil(max(yy))])
    notnan=~isnan(xx) & ~isnan(yy);
    [ff,goodness]=fit(xx(notnan),yy(notnan),fittype('a*x+b'),'StartPoint',[1,0]);
    plot([0,16],ff([0,16]),'-r')
    set( gca,'XTick',0:4:16);
    text(1,0.9,['R^{2} = ',num2str(goodness.rsquare)])
    drawnow
    
    figure(hf+5),movegui(hf+1,'northwest')
    ycolno=(5);
    xx=sum(xldat(:,xcolno),2)./extranorm.G;
    yy=xldat(:,ycolno)-xldat(:,15);
    scatter(xx,yy,[],cc,'filled'),axis square,box on,hold on
    text(xx+0.1,yy,txt,'Color',[0.4,0.4,0.4])
    xlabel(['Green intensity ',filter{f}])
    ylabel('Bouton-Vol. - Mitochondria vol.(\mu{m}^{3})')
    xlim([0 16])
    ylim([0 1])
    C=corrcoef(xx,yy,'rows','pairwise');
    notnan=~isnan(xx) & ~isnan(yy);
    [ff,goodness]=fit(xx(notnan),yy(notnan),fittype('a*x+b'),'StartPoint',[1,0]);
    plot([0,16],ff([0,16]),'-r')
    text(1,0.9,['R^{2} = ',num2str(goodness.rsquare)])
    set(gca,'XTick',[0:4:16]);
    drawnow
    
    extranorm.R(xldat(:,2)==9)=[];
    cc(xldat(:,2)==9,:)=[];
    txt(xldat(:,2)==9)=[];
    xldat(xldat(:,2)==9,:)=[];
    
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %{
    %Red channel
    hf=20;
    figure(hf+1),movegui(hf+1,'northwest')
    xcolno=[13,14];
    ycolno=(5);
    xx=sum(xldat(:,xcolno),2)./extranorm.R;
    yy=xldat(:,ycolno);
    scatter(xx,yy,[],cc,'filled'),axis square,box on,hold on
    text(xx+0.1,yy,txt,'Color',[0.4,0.4,0.4])
    xlabel(['Red intensity ',filter{f}])
    ylabel('Bouton-Volume (\mu{m}^{3})')
    xlim([0 16])
    ylim([0 ceil(max(yy))])
    C=corrcoef(xx,yy,'rows','pairwise');
    notnan=~isnan(xx) & ~isnan(yy);
    [ff,goodness]=fit(xx(notnan),yy(notnan),fittype('a*x+b'),'StartPoint',[1,0]);
    plot([0,16],ff([0,16]),'-r')
    text(1,0.9,['R^{2} = ',num2str(goodness.rsquare)])
    drawnow
    
    figure(hf+2),movegui(hf+2,'northeast')
    %xcolno=[11,12];
    ycolno=(8);
    xx=sum(xldat(:,xcolno),2)./extranorm.R;
    yy=xldat(:,ycolno);
    scatter(xx,yy,[],cc,'filled'),axis square,box on,hold on
    text(xx+0.1,yy,txt,'Color',[0.4,0.4,0.4])
    xlabel(['Red intensity ',filter{f}])
    ylabel('Cross-Section (\mu{m}^{2})')
    xlim([0 16])
    ylim([0 ceil(max(yy))])
    C=corrcoef(xx,yy,'rows','pairwise');
    notnan=~isnan(xx) & ~isnan(yy);
    [ff,goodness]=fit(xx(notnan),yy(notnan),fittype('a*x+b'),'StartPoint',[1,0]);
    plot([0,16],ff([0,16]),'-r')
    text(1,0.9,['R^{2} = ',num2str(goodness.rsquare)])
    drawnow
    
    figure(hf+3),movegui(hf+3,'southwest')
    %xcolno=[11,12];
    ycolno=(9);
    yy=xldat(:,ycolno);
    xx=sum(xldat(:,xcolno),2)./extranorm.R;
    scatter(xx,yy,[],cc,'filled'),axis square,box on,hold on
    text(xx+0.1,yy,txt,'Color',[0.4,0.4,0.4])
    xlabel(['Red intensity ',filter{f}])
    ylabel('z-Cross-Section (\mu{m}^{2})')
    xlim([0 ceil(max(xx))])
    ylim([0 ceil(max(yy))])
    C=corrcoef(xx,yy,'rows','pairwise');
    notnan=~isnan(xx) & ~isnan(yy);
    [ff,goodness]=fit(xx(notnan),yy(notnan),fittype('a*x+b'),'StartPoint',[1,0]);
    plot([0,16],ff([0,16]),'-r')
    set( gca,'XTick',0:4:16);
    text(1,0.9,['R^{2} = ',num2str(goodness.rsquare)])
    title(num2str(C(1,2)))
    drawnow
    
    figure(hf+4),movegui(hf+4,'southeast')
    %xcolno=[11,12];
    ycolno=(6);
    xx=sum(xldat(:,xcolno),2)./extranorm.R;
    yy=xldat(:,ycolno);
    scatter(xx,yy,[],cc,'filled'),axis square,box on,hold on
    text(xx+0.1,yy,txt,'Color',[0.4,0.4,0.4])
    xlabel(['Red intensity ',filter{f}])
    ylabel('PSD surface area (\mu{m}^{2})')
    xlim([0 16])
    ylim([0 ceil(max(yy))])
    notnan=~isnan(xx) & ~isnan(yy);
    [ff,goodness]=fit(xx(notnan),yy(notnan),fittype('a*x+b'),'StartPoint',[1,0]);
    plot([0,16],ff([0,16]),'-r')
    set( gca,'XTick',0:4:16);
    text(1,0.9,['R^{2} = ',num2str(goodness.rsquare)])
    drawnow
    %}
end
