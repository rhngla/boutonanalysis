%This code is used to transfer ignoreind annotation from an earlier dataset
%to a newer one.

animal={'DL001'};
timepoint={'A','B','D','E','F','H','I'};
section={'002'};
axon={[1,2,3,4,7,8,9,10,11,12,13,14,15,17,18,19,20]};
projectn='xy';
filter={'LoG3'};
channel='G';
pathlist;

matchpath='C:\Users\Rohan\Dropbox\Lab\Plasticity\dat\Controls\Conditions\Profiles_v2\';
for an=1:numel(animal)
    for se=1:numel(section)
        for ax=1:numel(axon{se})
            axonstr=sprintf('A%03d',axon{se}(ax));
            for ti=1:numel(timepoint)
                stackid=[animal{an},timepoint{ti},section{se}];
                New=load(isunixispc([profile_pth,stackid,'/',axonstr,'.mat']));
                New.annotate.ignore=false(size(New.r.optim(:,1)));
             
                Old=load(isunixispc([matchpath,stackid,'\',axonstr,'.mat']));
                
                startt=find(diff(Old.annotate.ignore)==1)+1;
                endd=find(diff(Old.annotate.ignore)==-1);
                if Old.annotate.ignore(1)==true
                    startt=[1;startt(:)];
                end
                if Old.annotate.ignore(end)==true
                    endd=[endd(:);numel(Old.annotate.ignore)];
                end
                Old.r.optim(startt,:);
                Old.r.optim(endd,:);
                
                full_list=[];
                for i=1:numel(startt)
                    dx=bsxfun(@minus,New.r.optim(:,1),Old.r.optim(startt(i):endd(i),1)');
                    dy=bsxfun(@minus,New.r.optim(:,2),Old.r.optim(startt(i):endd(i),2)');
                    dz=bsxfun(@minus,New.r.optim(:,3),Old.r.optim(startt(i):endd(i),3)');
                    dd=(dx.^2+dy.^2+dz.^2).^0.5;
                    [vallist,indlist]=min(dd,[],1);
                    indlist(vallist>0.5)=[];
                    full_list=[full_list;indlist(:)];
                end
                New.annotate.ignore(full_list)=true;
                
                save(isunixispc([profile_pth,stackid,'/',axonstr,'.mat']),'-struct','New');
                display([profile_pth,stackid,'/',axonstr,'.mat'])
                clear New Old
            end
        end
    end
end