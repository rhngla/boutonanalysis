%Notes:
%v4 contains data that was sent in the update
%Data for Axon 3 in v5 comes from newly optimized trace (A007)

parentdir=pwd;
parentdir=parentdir(1:(strfind(parentdir,'Plasticity')+numel('Plasticity')-1));
[xldat,temp,txt]=xlsread(isunixispc([parentdir,'\dat\EMData\Size data.xlsx']),'v5');

txt={txt{2:18,9}}';
cc=lines(4);
cc=cc(xldat(:,1),:);
no2ndnorm=false;
amp_thr=0;
fg_amp_thr=0.5;
for t=1:numel(txt)
    if isa(txt{t},'double')
        txt{t}=num2str(txt{t});
    end
end
Ff = @(x,A,mu,sigma) (ones(size(x))*A).*exp(-(x*ones(size(A))-ones(size(x))*mu).^2./(ones(size(x))*sigma.^2)./2);
filter={'LoG','LoG15','LoG20','LoG25','LoG30','LoG35','LoG40','median333','LoGxy'};

for f=1:numel(filter)
    %Extra normalization
    extranorm.G=ones(4,1);
    axlbl=([1,2,3,4]);
    channel={'G'};
    
    for ax=1:4
        
        for ch=1:numel(channel)
            A=load(isunixispc([parentdir,'\dat\EMData\Profiles\DL100I005\A00',num2str(axlbl(ax)),'.mat']));
            
            nsig=3;%was 2.5
            btn_end=A.fit.(channel{ch}).(filter{f}).fg.mu+nsig*A.fit.(channel{ch}).(filter{f}).fg.sig;
            btn_start=A.fit.(channel{ch}).(filter{f}).fg.mu-nsig*A.fit.(channel{ch}).(filter{f}).fg.sig;
            
            %Amplitude threshold
            btn_end(A.fit.(channel{ch}).(filter{f}).fg.amp<amp_thr)=[];
            btn_start(A.fit.(channel{ch}).(filter{f}).fg.amp<amp_thr)=[];
            
            [~,indstart]=min(abs(bsxfun(@minus,A.d.optim,btn_start')),[],1);
            [~,indend]=min(abs(bsxfun(@minus,A.d.optim,btn_end')),[],1);
            inds=[indstart(:),indend(:)];
            backbone=true(size(A.d.optim));
            for i=1:size(inds,1)
                backbone(inds(i,1):inds(i,2))=false;
            end
            
            bg=zeros(size(A.d.optim));
            for i=1:numel(A.fit.(channel{ch}).(filter{f}).bg.ind)
                temp=A.fit.(channel{ch}).(filter{f}).bg;
                bg=bg+Ff(A.d.optim,temp.amp(i),temp.mu(i),temp.sig(i));
            end
            
            if no2ndnorm
                extranorm.(channel{ch})(ax)=1;
            else
                extranorm.(channel{ch})(ax)=mean(A.I.(channel{ch}).(filter{f}).norm(backbone));
                extranorm.(channel{ch})(ax)=mean(bg);
            end
            
            %Normalization Based on median333 histogram
            II=A.I.(channel{ch}).('Gauss1').norm;
            fg=zeros(size(A.d.optim));
            for i=1:numel(A.fit.(channel{ch}).('Gauss1').fg.ind)
                temp=A.fit.(channel{ch}).('Gauss1').fg;
                if temp.amp(i)>fg_amp_thr
                    fg=fg+Ff(A.d.optim,temp.amp(i),temp.mu(i),temp.sig(i));
                end
            end
            II=II-fg;
            sortI=sort(II);
            %extranorm.(channel{ch})(ax)=sortI(round(0.15*numel(sortI)));
            %extranorm.(channel{ch})(ax)=mean(II);
            %temp=A.I.(channel{ch}).('Gauss2').norm;
            %temp(temp>1)=nan;
            %extranorm.(channel{ch})(ax)=nanmedian(temp)./(4000*mean(A.I.(channel{ch}).('LoGxy').raw));
            bg=zeros(size(A.d.optim));
            for i=1:numel(A.fit.(channel{ch}).('Gauss2').bg.ind)
                temp=A.fit.(channel{ch}).('Gauss2').bg;
                bg=bg+Ff(A.d.optim,temp.amp(i),temp.mu(i),temp.sig(i));
            end
            extranorm.(channel{ch})(ax)=mean(bg);
            %Display
            if ch==1 && strcmp(filter{f},'Gauss1')
                figure(ax)
                plot(A.d.optim,A.I.(channel{ch}).(filter{f}).norm,'-'),hold on
                plot(A.d.optim,II,'-'),hold on
                %plot(A.d.optim(backbone),A.I.(channel{ch}).(filter{f}).norm(backbone),'x'),hold on
                plot(A.d.optim,bg,'-b');
                Ff = @(x,A,mu,sigma) (ones(size(x))*A).*exp(-(x*ones(size(A))-ones(size(x))*mu).^2./(ones(size(x))*sigma.^2)./2);
                for i=1:numel(A.fit.(channel{ch}).(filter{f}).fg.ind)
                    temp=A.fit.(channel{ch}).(filter{f}).fg;
                    xx=(-5:0.1:+5)+temp.mu(i);
                    %plot(xx,Ff(xx,temp.amp(i),temp.mu(i),temp.sig(i)),'-','Color',[0.4 0.4 0.4]),hold on;
                end
                
                bestvals=[0.17,0.5440,0.230,0.27]';
                plot([0 70], bestvals(ax)*ones(2,1),'-r')
                plot([0 70], mean(II)*ones(2,1),'-k')
                grid on
                drawnow;
                title(filter{f})
                
                figure(40),
                subplot(2,2,ax)
                histogram(II,[0:0.025:3]);hold on
                plot(bestvals(ax)*ones(2,1),[0 120],'-r')
                plot(mean(II)*ones(2,1),[0 120],'-k')
                sortI=sort(II);
                percval=sortI(round(0.15*numel(sortI)));
                plot(percval*ones(2,1),[0 120],'-g')
                xlabel(sprintf('Intensity, Axon %d \n %.3f %%ile',ax,sum(II<bestvals(ax))*100./numel(II)))
                ylabel('Counts')
                axis square,box on,
                drawnow;                
            end
            %}
            clear A;
        end
    end
    fprintf([filter{f},' axonwise normalization factors'])
    display(extranorm.G)
    %extranorm.G=[0.17,0.5440,0.230,0.27]';
    extranorm.G=extranorm.G(xldat(:,1));
    
    xldat(8,2:end)=nan;%Terminal bouton excluded
    %xldat(:,[12,13,14]); fitted values for 3 filters
    
    figure(13),
    subplot(2,5,f),hold on;
    xcolno=(23+f);
    ycolno=(6);
    xx=xldat(:,xcolno)./extranorm.G;
    yy=xldat(:,ycolno);
    scatter(xx,yy,[],cc,'filled'),axis square,box on
    text(xx+0.1,yy,txt,'Color',[0.4,0.4,0.4])
    xlabel(['Green intensity ',filter{f}])
    ylabel('Bouton-Volume (\mu{m}^{3})')
    xlim([0 ceil(max(xx))])
    ylim([0 ceil(max(yy))])
    C=corrcoef(xx,yy,'rows','pairwise');
    notnan=~isnan(xx) & ~isnan(yy);
    [ff,goodness]=fit(xx(notnan),yy(notnan),fittype('a*x+b'),'StartPoint',[1,0]);
    plot([0,max(xx)],ff([0,max(xx)]),'-r')
    text(1,0.9,['R^{2} = ',num2str(goodness.rsquare)])
    title(num2str(C(1,2)))
    drawnow
    
    figure(14),
    subplot(2,5,f),hold on;
    xcolno=(23+f);
    ycolno=(18);
    xx=xldat(:,xcolno)./extranorm.G;
    yy=xldat(:,ycolno);
    scatter(xx,yy,[],cc,'filled'),axis square,box on
    text(xx+0.1,yy,txt,'Color',[0.4,0.4,0.4])
    xlabel(['Green intensity ',filter{f}])
    ylabel('Cross-Section (\mu{m}^{2})')
    xlim([0 ceil(max(xx))])
    ylim([0 ceil(max(yy))])
    C=corrcoef(xx,yy,'rows','pairwise');
    notnan=~isnan(xx) & ~isnan(yy);
    [ff,goodness]=fit(xx(notnan),yy(notnan),fittype('a*x+b'),'StartPoint',[1,0]);
    plot([0,max(xx)],ff([0,max(xx)]),'-r')
    text(1,0.9,['R^{2} = ',num2str(goodness.rsquare)])
    title(num2str(C(1,2)))
    drawnow
    
    figure(15),
    subplot(2,5,f),hold on;
    xcolno=(23+f);
    ycolno=19;
    xx=xldat(:,xcolno)./extranorm.G;
    yy=xldat(:,ycolno);
    scatter(xx,yy,[],cc,'filled'),axis square,box on
    text(xx+0.1,yy,txt,'Color',[0.4,0.4,0.4])
    xlabel(['Green intensity ',filter{f}])
    ylabel('z-Cross-Section (\mu{m}^{2})')
    xlim([0 ceil(max(xx))])
    ylim([0 1.2])
    C=corrcoef(xx,yy,'rows','pairwise');
    notnan=~isnan(xx) & ~isnan(yy);
    [ff,goodness]=fit(xx(notnan),yy(notnan),fittype('a*x+b'),'StartPoint',[1,0]);
    plot([0,max(xx)],ff([0,max(xx)]),'-r')
    text(1,0.9,['R^{2} = ',num2str(goodness.rsquare)])
    title(num2str(C(1,2)))
    drawnow
end
