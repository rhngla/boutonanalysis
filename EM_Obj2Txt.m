%Blender files were exported into .obj files
%AM, r for centerlines and positions vectors of all axon and mitochondria 
%surfaces are stored in .mat files. The .mat files are used for
%registration and for calculating area of cross section.

%{
%These bash commands were used to extract object line numbers:
grep -n "o " DL100_v2.obj

%Next only mitochondria were separated out into different files:
sed -n 3,84911p DL100_v2.obj > Ax4-SV
sed -n 84912,114585p DL100_v2.obj > Ax4-Mito-B16-SV
sed -n 114586,307111p DL100_v2.obj > Ax3-SV
sed -n 307112,327251p DL100_v2.obj > Ax3-Mito-B13-SV
sed -n 327252,338235p DL100_v2.obj > Ax3-Mito-B10-SV
sed -n 338236,422165p DL100_v2.obj > Ax2-SV
sed -n 422166,446277p DL100_v2.obj > Ax2-Mito-B9-SV
sed -n 446278,462587p DL100_v2.obj > Ax2-Mito-B7-SV
sed -n 462588,578684p DL100_v2.obj > Ax1-SV
sed -n 578685,598662p DL100_v2.obj > Ax1-Mito-B5-SV
sed -n 598663,611686p DL100_v2.obj > Ax1-Mito-B4-SV
sed -n 611687,633966p DL100_v2.obj > Ax1-Mito-B2-SV
sed -n 633967,658306p DL100_v2.obj > Ax1-Mito-B1-SV
sed -n 658307,675614p DL100_v2.obj > Ax1-Mito-B6-SV
sed -n 675622,730422p DL100_v2.obj > Ax4-SV-orig
sed -n 730423,730623p DL100_v2.obj > Ax4-Centerline
sed -n 730624,812174p DL100_v2.obj > Ax1-SV-orig
sed -n 812175,812574p DL100_v2.obj > Ax1-Centerline
sed -n 812575,842962p DL100_v2.obj > Ax3-SV-orig
sed -n 842963,843163p DL100_v2.obj > Ax3-Centerline
sed -n 843164,895525p DL100_v2.obj > Ax2-SV-orig
sed -n 895526,895925p DL100_v2.obj > Ax2-Centerline
%}

%Compile data into single axon .mat files
%EM data calculations
parentdir=pwd;
parentdir=parentdir(1:(strfind(parentdir,'Plasticity')+numel('Plasticity')-1));
for axid=1:4
    dirname=isunixispc([parentdir,'\dat\EMData\Blender2Mat\Obj2Txt\']);
    
    %Get centerline from .txt file
    centerline_fname=[dirname,'Ax',num2str(axid),'-Centerline'];
    nodeind=1;
    fid = fopen(centerline_fname);
    tline = fgetl(fid);
    r=cell(1);
    while ischar(tline)
        if ~isempty(regexp(tline,'^v ','start'))
            t=regexp(tline,' ','split');
            r{nodeind}=[str2double(t{2}),str2double(t{3}),str2double(t{4})];
            nodeind=nodeind+1;
        end
        tline = fgetl(fid);
    end
    r=cell2mat(r');
    fclose(fid);
    
    %Read adjacency matrix for centerline nodes
    AM=zeros(size(r,1));
    fid = fopen(centerline_fname);
    tline = fgetl(fid);
    i=[];j=[];
    while ischar(tline)
        if ~isempty(regexp(tline,'^l ','start'))
            t=regexp(tline,' ','split');
            i=[i;str2double(t{2})];
            j=[j;str2double(t{3})];
        end
        tline = fgetl(fid);
    end
    fclose(fid);
    
    %Object file goes as line number rather than assigning vertex indices
    %separately for each object.
    if isempty(i)
       i=(1:(size(r,1)-1))';
       j=(2:size(r,1))';
    end
    refind=min([i;j])-1;
    i=i-refind;
    j=j-refind;
    for ind=1:numel(i)
        AM(i(ind),j(ind))=1;
        AM(j(ind),i(ind))=1;
    end
    
    %Read mitochondria surface vertices from individual mitochondria files
    fmitoname=dir([dirname,'Ax',num2str(axid),'-Mito*']);
    fmitoname={fmitoname(:).name};
    SVm=[];
    for m=1:numel(fmitoname)
        svind=1;
        svn_ind=1;
        fid = fopen([dirname,fmitoname{m}]);
        tline = fgetl(fid);
        SVmito=cell(1);
        SVn=cell(1);
        while ischar(tline)
            if ~isempty(regexp(tline,'^v ','start'))
                t=regexp(tline,' ','split');
                SVmito{svind}=[str2double(t{2}),str2double(t{3}),str2double(t{4})];
                svind=svind+1;
            end
            tline = fgetl(fid);
        end
        fclose(fid);
        SVmito=cell2mat(SVmito');
        SVm=[SVm;SVmito];
    end
    SVmito=SVm;
    clear SVm;
    %}
    
    %Read original axon surface vertices
    svind=1;
    fid = fopen([dirname,'Ax',num2str(axid),'-SV-orig']);
    tline = fgetl(fid);
    SV=cell(1);
    while ischar(tline)
        if ~isempty(regexp(tline,'^v ','start'))
            t=regexp(tline,' ','split');
            SV{svind}=[str2double(t{2}),str2double(t{3}),str2double(t{4})];
            svind=svind+1;
        end
        tline = fgetl(fid);
    end
    fclose(fid);
    SVorig=cell2mat(SV');
    %}
    
    %Read new axon surface vertices
    svind=1;
    fid = fopen([dirname,'Ax',num2str(axid),'-SV']);
    tline = fgetl(fid);
    SV=cell(1);
    while ischar(tline)
        if ~isempty(regexp(tline,'^v ','start'))
            t=regexp(tline,' ','split');
            SV{svind}=[str2double(t{2}),str2double(t{3}),str2double(t{4})];
            svind=svind+1;
        end
        tline = fgetl(fid);
    end
    fclose(fid);
    SVnew=cell2mat(SV');
    clear SV;
    %}
    
    [AM, r, ~] = AdjustPPM(AM,r,zeros(size(r,1),1),10);
    terms=find(sum(AM,1)==1);
    %Ordering AM and r
    %Step along trace assuming no branching
    ind=nan(size(AM,1),1);
    ind(1)=terms(1);
    [~,ind(2)]=find(AM(ind(1),:));
    i=3;
    while ind(i-1)~=terms(2)
        [~,next]=find(AM(ind(i-1),:));
        ind(i)=next(next~=ind(i-2));
        i=i+1;
    end
    AM=AM(ind,:);
    AM=AM(:,ind);
    r=r(ind,:);
    d=cumsum(sum((r((2:end),:)-r((1:end-1),:)).^2,2).^0.5);
    d=[0;d(:)];
    
    save([dirname,'Axon',num2str(axid),'.mat'],'SVnew','SVorig','SVmito','AM','r','d')
    %}
    %figure(1),plotAM(AM,r,[]),hold on
    clearvars -except axid;
    
    figure,
    plotAM(AM,r,[]),hold on
    h1=plot3(SVorig(:,2),SVorig(:,1),SVorig(:,3),'.','MarkerSize',1,'Color',[0.7 0.7 0]);
    h2=plot3(SVnew(:,2),SVnew(:,1),SVnew(:,3),'.','MarkerSize',1,'Color',[0.7 0.7 0.7]);
    h3=plot3(SVmito(:,2),SVmito(:,1),SVmito(:,3),'.','MarkerSize',1,'Color',[0 0.7 0.7]);
    axis equal;
    box on;
end