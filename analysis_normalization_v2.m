function []=analysis_normalization_v2(D01)
%This is made to understand effect of filter choice with data from
%conditions. Second normalization can be set with a flag.

ftype=fittype('a*x');
filter={'LoG','LoGxy'};
normtype='gauss2_norm';%'none','profile_norm','fit_norm','gauss1_norm','gauss2_norm'

if ~strcmp(normtype,'none')
    xrange=[0,45];
    xmax=25;
    ymax=25;
    hf=85;
else
    xrange=[0,10];
    xmax=10;
    ymax=10;
    hf=81;
end
for  f=1:numel(filter)
    axlbl=unique(D01.G.(filter{f}).ax_id,'stable');
    ax_id=D01.G.(filter{f}).ax_id;
    Nf=ones(size(D01.G.(filter{f}).amp,1),numel(axlbl));
    ax_length=ones(size(D01.G.(filter{f}).amp,1),numel(axlbl));
    
    for ax=1:numel(axlbl)
        ax_id(ax_id==axlbl(ax))=ax;
        for ti=1:size(D01.G.(filter{f}).norm,1)
            if ~strcmp(normtype,'none')
                Nf(ti,ax)=D01.G.(filter{f}).norm{ti,ax}.(normtype);
            end
            ax_length(ti,ax)=D01.G.(filter{f}).norm{ti,ax}.axlen_legitimate;
        end
    end
    
    display(Nf)
    %display(std(Nf,[],1))
    %display(std(Nf,[],2))
    
    Nf=mean(Nf,1);
    Nf=bsxfun(@times,ones(size(D01.G.(filter{f}).amp,1),1),Nf);
    display(Nf)
    
    inall=sum(isnan(D01.G.(filter{f}).amp),1)==0;
    I=D01.G.(filter{f}).amp(:,inall)+D01.G.(filter{f}).Ibg(:,inall);
    ax_id=ax_id(inall);
    %Normalizing data
    for ax=1:numel(unique(ax_id))
        for ti=1:size(I,1)
            I(ti,ax_id==ax)=I(ti,ax_id==ax)./Nf(ti,ax);
        end
    end
    
    %I1=I;
    %putvar('I1')
    
    %{
    fprintf(['Axon wise slopes for ',filter{f},'\n']);
    %----------------Axon-wise slopes------------------

    c1=1;
    for c2=2:7
        cc=parula(17);
        for ax=1%:numel(axlbl)
            xx=I(c1,ax_id==ax);
            yy=I(c2,ax_id==ax);
            [ff,~]=fit(xx(:),yy(:),ftype,'StartPoint',1);
            CI=confint(ff);
            if (CI(1)-1)*(CI(2)-1)>0
                fprintf('Condition %d Axon %d # boutons on axon = %d density = %.3f\n ',c2,ax,sum(ax_id==ax),sum(ax_id==ax)./ax_length(c2,ax));
                fprintf('Slope = %.3f, (%.3f,%.3f)\n',ff.a,CI(1),CI(2));
            end
            figure(75),movegui(75,'northeast')
            subplot(1,3,f)
            scatter(xx(:),yy(:),15,cc(ax,:),'filled'),hold on
            plot(xrange,ff(xrange),'-','Color',cc(ax,:)),hold on,
            xlim([0,xmax]),ylim([0,ymax]),axis square,box on
            xlabel(['Condition ',num2str(c1)])
            ylabel(['Condition ',num2str(c2)])
            title(filter{f})
        end
        fprintf('\n')
    end
    %}
    
    %----------------Conditions-wise slopes------------------
    fprintf(['Condition wise slopes for ',filter{f},'\n']);
    cc=lines(20);
    c1=1;
    for c2=2:size(I,1)
        %c1=c2-1;
        col_id=c2-1;
        
        xx=I(c1,:);
        yy=I(c2,:);
        [ff,gg]=fit(xx(:),yy(:),ftype,'StartPoint',1);
        figure(hf),movegui(hf,'northwest')
        subplot(1,2,f)
        scatter(xx(:),yy(:),15,cc(col_id,:),'filled'),hold on
        if c2==2
            %text(xx(:),yy(:)+1,num2str(bt_id(:)),'Color',[0.7 0.7 0.7],'HorizontalAlignment','center')
        end
        plot(xrange,ff(xrange),'-','Color',cc(c2-1,:)),hold on,
        xlim([0,xmax]),ylim([0,ymax]),axis square,box on
        xlabel(['Condition ',num2str(c1)])
        ylabel('Condition j')
        title(filter{f})
        set(gca,'FontName','Calibri','FontSize',15);
        C=corrcoef(xx(:),yy(:));
        CI=confint(ff);
        
        fprintf('Condition %d #boutons %d ',c2,size(I,2));
        fprintf('Slope = %.3f, (%.3f,%.3f), R2 %.3f\n',ff.a,CI(1),CI(2),gg.rsquare);
        
        drawnow
    end
    
    %{
    %For comparison with quantity reported by Song_2015
    c1=1;c2=2;
    xx=I(c1,:);
    yy=I(c2,:);
    ind=xx>2 & yy>2;
    xx=xx(ind);
    yy=yy(ind);
    I1=max(xx,yy);
    I2=min(xx,yy);
   
    figure(900)
    subplot(1,2,f)
    plot(I1./I2,'x'),axis square,box on,hold on
    plot([0 300],[mean(I1./I2),mean(I1./I2)],'-k')
    title([filter{f},'mean I ratio',num2str(mean(I1./I2))])
    drawnow;
    fprintf('\n');
    %}
end
end




