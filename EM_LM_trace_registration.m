%This code generates results reported for correspondence between EM and LM
%traces. A linear transform is performed to match the fiducial points (boutons), and
%the transformation is used to register EM centerlines and LM traces.
%Data for fiducial points is also copied in CLEM_v5.ppt file within the comments.

Ff = @(x,A,mu,sigma) (ones(size(x))*A).*exp(-(x*ones(size(A))-ones(size(x))*mu).^2./(ones(size(x))*sigma.^2)./2);

parentdir=pwd;
parentdir=parentdir(1:(strfind(parentdir,'Plasticity')+numel('Plasticity')-1));

load([parentdir,'\dat\EMData\DatasetIM\DL100I005G.mat'],'IM')

L{1}=load(isunixispc([parentdir,'\dat\EMData\Profiles\DL100I005\A001.mat']),'r','I','d','fit');
L{2}=load(isunixispc([parentdir,'\dat\EMData\Profiles\DL100I005\A002.mat']),'r','I','d','fit');
L{3}=load(isunixispc([parentdir,'\dat\EMData\Profiles\DL100I005\A003.mat']),'r','I','d','fit');
L{4}=load(isunixispc([parentdir,'\dat\EMData\Profiles\DL100I005\A004.mat']),'r','I','d','fit');

E{1}=load(isunixispc([parentdir,'\dat\EMData\Blender2Mat\Obj2Txt\Axon1.mat']));
E{2}=load(isunixispc([parentdir,'\dat\EMData\Blender2Mat\Obj2Txt\Axon2.mat']));
E{3}=load(isunixispc([parentdir,'\dat\EMData\Blender2Mat\Obj2Txt\Axon3.mat']));
E{4}=load(isunixispc([parentdir,'\dat\EMData\Blender2Mat\Obj2Txt\Axon4.mat']));
%}

%Fiduciary points marked by eye
LM_orig=...
[ 588.3200  403.2000  257.7400
  598.4900  402.4900  260.0000
  609.9800  411.2700  259.9800
  615.6200  412.0400  259.8700
  628.6900  409.5800  258.8600
  639.0600  408.5400  258.3400
  646.5100  409.0600  258.6600
  623.3859  404.2713  260.2900
  637.4999  383.5393  260.8362
  647.6931  396.8014  262.4242
  644.0706  387.9168  267.1464
  642.9696  413.6817  263.1770
  643.7598  403.4845  263.4609];

EM=...
   [0.6875   12.2205   -2.9920
    0.4835    9.4239    0.3186
    3.7628    6.4520   -0.1864
    4.1950    4.5933   -0.7228
    3.6211    1.1488   -1.4678
    3.9054   -2.0258   -2.7876
    4.3623   -3.9343   -3.3735
    2.0150    2.4708    0.3633
   -3.6324   -2.1676    0.7724
    0.7820   -4.8568    1.5349
   -1.9937   -3.8974    6.5214
    5.3059   -3.4068    2.1646
    2.6598   -3.5159    2.4482];

%Flipping the z component of EM related measurements
EM=bsxfun(@times,EM,[1 1 -1]);
for i=1:4
    E{i}.SVorig=bsxfun(@times,E{i}.SVorig,[1 1 -1]);
    E{i}.r=bsxfun(@times,E{i}.r,[1 1 -1]);
    EM=bsxfun(@times,EM,[1 1 -1]);
end

%Converting LM co-ordinates to micron units (EM units are assumed to be in microns)
pxtoum=[0.26 0.26 0.8];
LM_microns=bsxfun(@times,LM_orig,pxtoum);
for i=1:4
    L{i}.r.microns=bsxfun(@times,L{i}.r.optim,pxtoum);
end

cc=lines(4);
%Show electron microscopy points-------------------------------------------
figure(1),clf(1);movegui(1,'northwest')
for i=1:4
    ind=1:25:size(E{i}.SVorig,1);
    plot3(E{i}.SVorig(ind,1),E{i}.SVorig(ind,2),E{i}.SVorig(ind,3),'.','MarkerSize',1,'Color',cc(i,:)),hold on
    plot3(E{i}.r(:,1),E{i}.r(:,2),E{i}.r(:,3),'.','MarkerSize',1,'Color',cc(i,:)),hold on
end
plot3(EM(:,1),EM(:,2),EM(:,3),'ok','MarkerSize',20,'LineWidth',2)
axis equal,box on;grid on;
view([0 0 1]);
drawnow;

%Show light microscopy points----------------------------------------------
figure(2),clf(2);movegui(2,'north')
imshow(max(IM(:,:,250:280),[],3),[0 2500]);hold on
for i=1:4  
    plot3(L{i}.r.optim(:,2),L{i}.r.optim(:,1),L{i}.r.optim(:,3),'.','MarkerSize',1,'Color',cc(i,:)),hold on
end
plot3(LM_orig(:,2),LM_orig(:,1),LM_orig(:,3),'or','MarkerSize',20,'LineWidth',2)
axis equal,box on;grid on;
xlim([370.5 430.5]);ylim([585.5 653.5]);
drawnow;

%Calclate transformations--------------------------------------------------
[Tel,bel]=optimal_linear_transform(EM',LM_microns');
[Tle,ble]=optimal_linear_transform(LM_microns',EM');
for i=1:4
    L{i}.r.transformed=bsxfun(@plus,Tle*L{i}.r.microns',ble)';
    E{i}.r_transformed=bsxfun(@plus,Tel*E{i}.r',bel)';
end

% Show EMand LM aligned traces in EM frame of reference
figure(3),clf(3),movegui(3,'northeast')
for i=1:4
    plot3(E{i}.r(:,1),E{i}.r(:,2),E{i}.r(:,3),'-','MarkerSize',1,'Color',cc(i,:)),hold on
    plot3(L{i}.r.transformed(:,1),L{i}.r.transformed(:,2),L{i}.r.transformed(:,3),'-r','MarkerSize',1,'Color',cc(i,:)),hold on
end
title('LM traces transformed to EM co-ordinates')
axis equal;grid on;box on;
view([0 0 1])
drawnow;

%Distance calculation based on nearest point in LM
dd=[];
d=cell(4,1);
for i=1:4
    %{
    d{i}=(bsxfun(@minus,L{i}.r.transformed(:,1),E{i}.r(:,1)')).^2+...
        (bsxfun(@minus,L{i}.r.transformed(:,2),E{i}.r(:,2)')).^2+...
        (bsxfun(@minus,L{i}.r.transformed(:,3),E{i}.r(:,3)')).^2;
    %}
    
    d{i}=(bsxfun(@minus,L{i}.r.microns(:,1), E{i}.r_transformed(:,1)')).^2+...
        (bsxfun(@minus,L{i}.r.microns(:,2),E{i}.r_transformed(:,2)')).^2+...
        (bsxfun(@minus,L{i}.r.microns(:,3),E{i}.r_transformed(:,3)')).^2;
    
    d{i}=d{i}.^0.5;
    dd=[dd,min(d{i},[],1)];
end
dd=dd(:);
display('Min and max distances of transformed LM traces from EM traces:')
display([min(dd(:)),max(dd(:))]);
figure(4);clf(4);movegui(4,'south')
subplot(1,2,1);
histogram(dd,0:0.075:1.5);
xlim([0 1.5])
axis square;box on;grid on;
display('Mean and Std of distances in EM coordinates:')
display([mean(dd(:)),std(dd(:))]);
drawnow

%Distance calculation based on established correspondence
dd2=[];
figure(5);clf(5)
for i=1:4
    L{i}.AM=diag(ones(size(L{i}.r.transformed,1)-1,1),1);
    E{i}.AM=diag(ones(size(E{i}.r,1)-1,1),1);
    [ee,temp]=TraceDistance(E{i}.AM, E{i}.r, L{i}.AM, L{i}.r.transformed,false);
    dd2=[dd2;temp];
    figure(5)
    subplot(1,2,1)
    histogram(temp,0:0.075:3,'Normalization','PDF','DisplayStyle','stairs');hold on
    xlabel('Inter-trace distance, \mu{m}');ylabel('PDF')
    xlim([0 1.5])
    axis square;box on;grid on;
    drawnow;
    subplot(1,2,2)
    histogram(temp,0:0.075:3,'DisplayStyle','stairs');hold on
    xlim([0 1.5])
    axis square;box on;grid on;
    xlabel('Inter-trace distance, \mu{m}');ylabel('Counts')
    drawnow;
end
dd2=dd2(:);
disp('Min and max distances of transformed LM traces from EM traces - based on best match:')
display([min(dd2(:)),max(dd2(:))]);
figure(4);
subplot(1,2,2);
histogram(dd2,0:0.075:3);
xlim([0 1.5])
axis square;box on;grid on;
disp('Mean and Std of distances in EM coordinates - based on best match:')
display([mean(dd2(:)),std(dd2(:))]);
drawnow
