%This script reports the volume for bouton extents marked by hand based on
%cross-section area profiles. Data was used to do a quick check of 
%variability in volume estimate related to precision in determining bouton boundaries 

clear

parentdir=pwd;
parentdir=parentdir(1:(strfind(parentdir,'Plasticity')+numel('Plasticity')-1));

load(isunixispc([parentdir,'\dat\EMData\CrossSectionEst\Axon1.mat']));
csarea=median(CS,2);
d=[0;cumsum(sum((r((2:end),:)-r((1:end-1),:)).^2,2).^0.5)];
figure(1),plot(d,csarea,'-')

 btnstart=[0.59, 2.28, 6.05, 10.81, 12.29, 17.55]; %For axon 1
 btnend=[1.88, 4.36, 8.61, 11.6, 14.57, 20.24]; %For axon 1

% btnstart=[5.97,14.1]; %For axon 2
% btnend=[8.45,16.4]; %For axon 2

% btnstart=[0.98,5.25,8.61,10.09]; %For axon 3
% btnend=[2.87,6.73,9.40,12.47]; %For axon 3

%btnstart=[0.5,3.78,6.76,13.22]; %For axon 4
%btnend=[1.99,5.47,8.54,15,11]; %For axon 4

[~,ind1]=min(abs(bsxfun(@minus,d,btnstart)),[],1);
[~,ind2]=min(abs(bsxfun(@minus,d,btnend)),[],1);
boutonarea=nan(numel(btnstart),1);
for i=1:numel(btnstart)
    boutonarea(i)=trapz(d(ind1(i):ind2(i)),csarea(ind1(i):ind2(i)));
end
boutonarea