%This code determines shrinkage correction by brute force search. The cost for
%each value of shrinkage is calculated on a grid. EM{ax}.btn_ind and
%LMd_peaks{ax} are determined by hand.

EM_btn_ind=cell(4,1);
EM_r=cell(4,1);
EMd_boutons=cell(4,1);
btn_ind=cell(4,1);

parentdir=pwd;
parentdir=parentdir(1:(strfind(parentdir,'Plasticity')+numel('Plasticity')-1));
for ax=1:4
    emdat=isunixispc([parentdir,'\dat\EMData\Blender2Mat\A00',num2str(ax),'.mat']);
    lmdat=isunixispc([parentdir,'\dat\EMData\Profiles\DL100I005\A00',num2str(ax),'.mat']);
    EM_r{ax}=load(emdat,'r');
end

%{
EM_btn_ind{1}=[67,172,367,671,976]; 
EM_btn_ind{2}=[347,797];
EM_btn_ind{3}=[93,299,609];
EM_btn_ind{4}=[63,242,396,715];

LMd_peaks{1}=[0.7901,2.909,5.768,10.7,14.92];
LMd_peaks{2}=[5.763,12.84];
LMd_peaks{3}=[1.435,4.991,10.6];
LMd_peaks{4}=[0.867,3.864,6.605,12.05];
%}
EM_btn_ind{1}=[67,172,367,671,976]; 
EM_btn_ind{2}=[347];
EM_btn_ind{3}=[93,299,609];
EM_btn_ind{4}=[63,242,396,715];

LMd_peaks{1}=[0.9901,3.109,5.968,10.9,15.12];
LMd_peaks{2}=[5.263];
LMd_peaks{3}=[1.135,4.691,10.3];
LMd_peaks{4}=[0.867,3.864,6.605,12.05];

xs=0.65:0.01:0.95;
ys=0.65:0.01:0.95;
zs=0.65:0.01:0.95;
C=zeros(numel(xs),numel(ys),numel(zs));
clk=tic;
for i=1:numel(xs)
    for j=1:numel(ys)
        for k=1:numel(zs)
            shrinkage=[xs(i),ys(j),zs(k)];
            for ax=1:4
                r=bsxfun(@times,EM_r{ax}.r,shrinkage);
                EMd=[0;cumsum(sum((r((2:end),:)-r((1:end-1),:)).^2,2).^0.5)];
                EMd_boutons{ax}=EMd(EM_btn_ind{ax});
                C(i,j,k)=C(i,j,k)+sum((LMd_peaks{ax}(:)-EMd_boutons{ax}(:)-mean(LMd_peaks{ax}(:)-EMd_boutons{ax}(:))).^2);
            end
        end
    end
    display([num2str(i*100/numel(xs)), '% Time elapsed: ', num2str(toc(clk))]);
end

[val,ind]=min(C(:));[ii,jj,kk]=ind2sub(size(C),ind);
display('Global minima X,Y,Z,Value')
ss1=[xs(ii),ys(jj),zs(kk),val];
display(ss1)
[xx,yy,zz] = ndgrid(xs,ys,zs);
%scatter3(xx(:),yy(:),zz(:),2,C(:),'filled')

dCx=diff(C,1,1);dCy=diff(C,1,2);dCz=diff(C,1,3);
indx=find(dCx(1:end-1,:,:)<0 & dCx(2:end,:,:)>0);
indy=find(dCy(:,1:end-1,:)<0 & dCy(:,2:end,:)>0);
indz=find(dCz(:,:,1:end-1)<0 & dCz(:,:,2:end)>0);

[ax,bx,cx]=ind2sub(size(dCx(1:end-1,:,:)),indx);
ax=ax+1;
[ay,by,cy]=ind2sub(size(dCy(:,1:end-1,:)),indy);
by=by+1;
[az,bz,cz]=ind2sub(size(dCz(:,:,1:end-1)),indz);
cz=cz+1;

indx=sub2ind(size(C),ax,bx,cx);
indy=sub2ind(size(C),ay,by,cy);
indz=sub2ind(size(C),az,bz,cz);

M=zeros(size(C));
M(indx)=M(indx)+1;
M(indy)=M(indy)+1;
M(indz)=M(indz)+1;

indmin=find(M==3);
display('Local minima X,Y,Z,Value')
ss2=[xx(indmin),yy(indmin),zz(indmin),C(indmin)];
[~,indsort]=sort(ss2(:,4));
ss2=ss2(indsort,:);
display(ss2)
% figure,imagesc(squeeze(min(C,[],1)),[0.5 1]),axis equal,axis tight,xlabel('Z'),ylabel('Y')
% figure,imagesc(squeeze(min(C,[],2)),[0.5 1]),axis equal,axis tight,xlabel('Z'),ylabel('X')
% figure,imagesc(squeeze(min(C,[],3)),[0.5 1]),axis equal,axis tight,xlabel('Y'),ylabel('X')

EM_registration(ss1(1:3),LMd_peaks,EM_btn_ind)
