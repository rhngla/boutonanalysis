function [offset] = gui_editpeaks_plotprofile(fp,I,dorig,dplot,fg,bg,offset)
%Plots profiles to mark active point for reference.
if ~isempty(fp)
   fp=2; 
end
h=findall(fp,'Tag','Profile');
cc=lines(7);
plot(dplot,I+offset,'-','Color',cc(mod(numel(h),7)+1,:),'LineWidth',1,'Tag','Profile'),hold on
Ff = @(x,A,mu,sigma) (ones(size(x))*A).*exp(-(x*ones(size(A))-ones(size(x))*mu).^2./(ones(size(x))*sigma.^2)./2);

for i=1:numel(fg.ind)
    xx=(-5:0.1:+5)+dplot(fg.ind(i));
    plot(xx,Ff(xx,fg.amp(i),dplot(fg.ind(i)),fg.sig(i))+offset,'-','Color',[0.4 0.4 0.4]),hold on;    
end

Ibg=zeros(size(dplot));
for i=1:numel(bg.ind)
    Ibg=Ibg+Ff(dorig,bg.amp(i),dorig(bg.ind(i)),bg.sig(i));
end

plot(dplot,Ibg+offset,'-','Color',[1 0.5 0.5],'LineWidth',0.5)
plot([dplot(1),dplot(end)],[offset,offset],'-','Color',[0.5 0.5 0.5],'LineWidth',0.5)
plot([dplot(1),dplot(end)],[offset+0.5,offset+0.5],'-','Color',[0.5 0.5 0.5],'LineWidth',0.5)
plot([dplot(1),dplot(end)],[offset+1,offset+1],'-','Color',[0.5 0.5 0.5],'LineWidth',0.5)

%offset=offset+max(I)+0.5;
offset=offset+10;
end

