function [] = gui_regpeaks_actions(hObject, eventdata, ~)
%eventdata.Source
[~,f]=gcbo;
keypressval=eventdata.Key;
if ~isempty(regexp(keypressval,'[0-5]','start'));
    flag_val=str2double(keypressval);
    keypressval='f';
end
switch keypressval;
    case 'a' %Add suggested nodes
        display('Adding predicted matches to active set');
        h_active=findobj(f,'Tag','ActiveSet');
        h_predict=findobj(f,'Tag','PredictedMatches');
        if ~isempty(h_active)
            set(h_predict,'Tag','ActiveSet');
        end
        if ~isempty(h_predict)
            set(h_predict,'Marker',h_active(1).Marker,'MarkerSize',h_active(1).MarkerSize,'Color',h_active(1).Color,'LineWidth',h_active(1).LineWidth);
        end
        
    case 'r' %Reset active set
        display('Resetting active set')
        h=findobj(f,'-regexp','Tag','(Active*)|(Predict*)');
        delete(h)
        
    case 'c' %Connect nodes
        %Error for single node
        %Man_id propagation
        %Warning for skipped time
        display('Adding links between nodes in active set')
        h_active=findobj(f,'Tag','ActiveSet');
        
        if ~isempty(h_active)
            dat_active=cell2mat({h_active.UserData});
            t_active=[dat_active.t];
            fg_id_active=[dat_active.fg_id];
            
            %re-order active nodes with earliest time first
            [t_active,sorti]=sort(t_active,'ascend');
            fg_id_active=fg_id_active(sorti);
            
            %Modify AM to add link between active nodes.
            %Ordering is assumed.
            dat=guidata(f);
            AMind=ismember(dat.fg_id,fg_id_active);
            AMind=find(AMind);
            addind=sub2ind(size(dat.AM),AMind(1:end-1),AMind(2:end));
            dat.AM(addind)=min(fg_id_active);
            dat=gui_regpeaks_plotedges(dat,f);
            for ti=1:numel(dat.Time)
                dat.Time{ti}.fg_manid=dat.fg_manid(dat.t==ti);
            end
            [dat.Time]=gui_regpeaks_align(dat.Time);
            gui_regpeaks_updatenodes(dat)
            guidata(f,dat);
            
            %Purging active set & predicted matches
            h=findobj(f,'-regexp','Tag','(Active*)|(Predict*)');
            delete(h)
        else
            display('No nodes selected!')
        end
        
    case 'f' %Flag nodes
        display('Flagging nodes in active set')
        h_active=findobj(f,'Tag','ActiveSet');
        if ~isempty(h_active)
            dat_active=cell2mat({h_active.UserData});
            fg_id_active=[dat_active.fg_id];
            dat=guidata(f);
            if ~exist('flag_val','var')
                flag_val=input(sprintf('Flag active nodes: \nFlag 0. Reset flag to nan \nFlag 2. Cross-over/ location is ignored elsewhere \nFlag 3. Terminal terminal bouton \nFlag 4. Spurious peak \nFlag 5. No match found'));
            end
            if ~isempty(flag_val)
                flag_val(flag_val==0)=nan;
                dat.fg_flag(ismember(dat.fg_id,fg_id_active))=flag_val;
                gui_regpeaks_updatenodes(dat);
                guidata(f,dat);
            end
            
            h=findobj(f,'-regexp','Tag','(Active*)|(Predict*)');
            delete(h)
        end
        
    case 's' %Save data (2step)
        display('Writing data')
        dat=guidata(f);
        for ti=1:numel(dat.Time)
            dat.Time{ti}.fg_manid=dat.fg_manid(dat.t==ti);
        end
        [dat.Time]=gui_regpeaks_align(dat.Time);
        guidata(f,dat);
        
        ii=input('Press y to save, any other key to continue','s');
        if strcmp(ii,'y')
            for ti=1:length(dat.Time)
                display(dat.Time{ti}.save_pth);
                temp=load(dat.Time{ti}.save_pth);
                temp.fit.(dat.channel).(dat.filter).fg.id=dat.fg_id(dat.t==ti);
                temp.fit.(dat.channel).(dat.filter).fg.manid=dat.fg_manid(dat.t==ti);
                temp.fit.(dat.channel).(dat.filter).fg.flag=dat.fg_flag(dat.t==ti);
                temp.fit.(dat.channel).(dat.filter).d.man=dat.Time{ti}.d_man;
                temp.fit.(dat.channel).(dat.filter).deform.man=dat.Time{ti}.deform_man;
                save(dat.Time{ti}.save_pth,'-struct','temp');
            end
        end
        
    case 'v'
        h_elements=findobj(f,'-regexp','Tag','(Node*)|(Edge*)');
        
        if ~isempty(h_elements)
            stateval=h_elements(1).Visible;
            if strcmp(stateval,'on')
                stateval='off';
            else
                stateval='on';
            end
            for i=1:numel(h_elements)
                h_elements(i).Visible=stateval;
            end
        end
        
    case 'hyphen' %Control contrast
        f.Children.CLim(2)=f.Children.CLim(2)+0.5;
        
    case 'equal' %Control contrast
        f.Children.CLim(2)=f.Children.CLim(2)-0.5;
        
    otherwise
        display('Not a valid action key')
        
end