%This code is used to transfer ignoreind annotation all-to-all time points.
%aligned d is used to achieve this. This was done on the conditions
%dataset.

%Some regions were found to be noisy in some timepoints but not others.
%Marking out different regions introduces slight, additional variability in
%the normalization procedure; it is not expected to change the results by
%much, but doing this to not penalize algorithm unnecessarily.

animal={'DL001'};
timepoint={'A','B','D','E','F','H','I'};
section={'002'};
axon={[1,2,3,4,7,8,9,10,11,12,13,14,15,17,18,19,20]};
projectn='xy';
filter={'LoGxy'};
channel='G';
pathlist;

for an=1:numel(animal)
    for se=1:numel(section)
        for ax=1:numel(axon{se})
            axonstr=sprintf('A%03d',axon{se}(ax));
            full_list=[];
            %This loop finds all the annotated regions (based on aligned d) 
            %in all the sessions
            for ti=1:numel(timepoint)
                stackid=[animal{an},timepoint{ti},section{se}];
                Old=load(isunixispc([profile_pth,stackid,'\',axonstr,'.mat']));
                startt=find(diff(Old.annotate.ignore)==1)+1;
                endd=find(diff(Old.annotate.ignore)==-1);
                if Old.annotate.ignore(1)==true
                    startt=[1;startt(:)];
                end
                if Old.annotate.ignore(end)==true
                    endd=[endd(:);numel(Old.annotate.ignore)];
                end
                full_list=[full_list;Old.fit.G.(filter{1}).d.man(startt),Old.fit.G.(filter{1}).d.man(endd)];
                figure(1),movegui(1,'northwest');
                plot(Old.fit.G.(filter{1}).d.man,Old.annotate.ignore+2*(ti-1),'-'),hold on
                axis square
            end
            
            
            %This loop writes the annotations into all timepoints
            for ti=1:numel(timepoint)
                stackid=[animal{an},timepoint{ti},section{se}];
                Old=load(isunixispc([profile_pth,stackid,'\',axonstr,'.mat']));
                dstartt=abs(bsxfun(@minus,Old.fit.G.(filter{1}).d.man(:),full_list(:,1)'));
                dendd=abs(bsxfun(@minus,Old.fit.G.(filter{1}).d.man(:),full_list(:,2)'));
                [~,indstartt]=min(dstartt,[],1);
                [~,indendd]=min(dendd,[],1);
                rem=(indstartt==indendd);
                indstartt(rem)=[];
                indendd(rem)=[];
                for i=1:numel(indstartt)
                    Old.annotate.ignore(indstartt(i):indendd(i))=true;
                end
                figure(2),movegui(2,'northeast');
                plot(Old.fit.G.(filter{1}).d.man,Old.annotate.ignore+2*(ti-1),'-'),hold on
                axis square
                
                save(isunixispc([profile_pth,stackid,'/',axonstr,'.mat']),'-struct','Old');
                display([profile_pth,stackid,'/',axonstr,'.mat'])
                clear Old
            end
        clf(1),clf(2);drawnow;
        end
    end
end