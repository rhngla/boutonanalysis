function []=analysis_kasthuridat()
%This code visualizes some distributions from Kasthuri_2015 
%http://linkinghub.elsevier.com/retrieve/pii/S0092867415008247
%Some fields wree converted to 'Number' format.


XL=xlsread('C:\Users\Rohan\Dropbox\Lab\Plasticity\dat\Kasthuri_2015_dat.xls');
XL=XL(:,[11,13,14,15,16,20,22]); %Ax.type(inh/exc),boutontype(epb/term),Vessiclecount,Mito.no.,PSDsize, 

nbins=25;
%Vessicle counts distribution
crit=(XL(:,1)==0 & XL(:,2)==0 & XL(:,3)>0);%Excitatory,enpassant,counts available
[vesscounts,vesscount_binedges]=histcounts(XL(crit,3),nbins);
figure(1);clf(1);
plot((vesscount_binedges(1:end-1)+vesscount_binedges(2:end))/2,vesscounts,'-x');
axis square,box on;
xlabel('Vessicle counts');
ylabel('Counts');

%PSD distribution
crit=(XL(:,1)==0 & XL(:,2)==0 & XL(:,3)>0);%Excitatory,enpassant,counts available
[PSDcounts,PSDcount_binedges]=histcounts(XL(crit,6),nbins);
figure(3);clf(3);
plot((PSDcount_binedges(1:end-1)+PSDcount_binedges(2:end))/2,PSDcounts,'-x');
axis square,box on;
xlabel('PSD area counts (pixels)');
ylabel('Counts');

%PSD distribution
crit=(XL(:,1)==0 & XL(:,2)==0 & XL(:,3)>0 & XL(:,7)>0);%Excitatory,enpassant,counts available
[Spinevolcounts,Spinevolcount_binedges]=histcounts(XL(crit,7),nbins);
figure(4);clf(4);
plot((Spinevolcount_binedges(1:end-1)+Spinevolcount_binedges(2:end))/2,Spinevolcounts,'-x');
axis square,box on;
xlabel('Spine volume counts (pixels)');
ylabel('Counts');

%PSD size vs vessivle number scatter
crit=(XL(:,1)==0 & XL(:,2)==0 & XL(:,3)>0);
figure(2),clf(2);
ftype=fittype('a*x+b');
[ff,~]=fit(XL(crit,3),XL(crit,6),ftype,'StartPoint',[1,0]);
scatter(XL(crit,3),XL(crit,6),'filled');hold on
plot(XL(crit,3),ff(XL(crit,3)),'-');
axis square,box on;
xlabel('Vessicle count');
ylabel('PSD area (pixels)');


display('PSD area(y) vs For vessicle count(x)')
display(ff);
cc=corrcoef(XL(crit,3),XL(crit,6));
display(['Corrcoef ',num2str(cc(1,2))]);