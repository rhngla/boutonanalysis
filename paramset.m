%For optimization
opt.Rtypical=3;         %default: opt.Rtypical=3;
opt.Optimize_bps=0;     %default: opt.Optimize_bps=0;
opt.Optimize_tps=0;     %default: opt.Optimize_tps=1
opt.pointsperum=0.5;    %default: opt.pointsperum=0.5;
opt.MaxIterations=2000; %default: opt.MaxIterations=2000;
opt.alpha_r=0.001;      %default: opt.alpha_r=0.001
opt.alpha_R=0.1;        %default: opt.alpha_R=0.1;
opt.betta_r=10;         %default: opt.betta_r=10
opt.betta_R=0;          %default: opt.betta_R=0;
opt.adjustPPM=1;        %default: opt.adjustPPM=1; This is a true/false variable
opt.output=1;           %default: opt.output=1;
opt.varargin=[];        %default: opt.varargin=[];

%ppm is adjusted for the profile in run_optim
profile.ppm=4;          %default: profile.ppm=4;

%For projections
proj.fm_dist=5;         %default: proj.fm_dist=7