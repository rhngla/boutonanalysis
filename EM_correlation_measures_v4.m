%Notes:
%Excel sheet v8 is used to plot these
%Extra data in sheet v7 was removed

parentdir=pwd;
parentdir=parentdir(1:(strfind(parentdir,'Plasticity')+numel('Plasticity')-1));
[xldat,temp,txt]=xlsread(isunixispc([parentdir,'\dat\EMData\Size data.xlsx']),'v8');

txt={txt{2:20,2}}';
cc=lines(4);
cc=cc(xldat(:,1),:);
no2ndnorm=false;

for t=1:numel(txt)
    if isa(txt{t},'double')
        txt{t}=num2str(txt{t});
    end
end

%xldat(xldat(:,2)==8,2:end)=nan;%Excluding Terminal bouton 
hf=13;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Green Channel
%Correlations are slightly different from before because of rounding errors.
figure(hf+1),movegui(hf+1,'northwest')
xcolno=8;
ycolno=3;
xx=xldat(:,xcolno);
yy=xldat(:,ycolno);
scatter(xx,yy,[],cc,'filled'),axis square,box on,hold on
text(xx+0.15,yy,txt,'Color',[0.4,0.4,0.4])
xlabel('Bouton weight')
ylabel('Bouton volume (\mu{m}^{3})')
xlim([0 16]);ylim([0 1]);
C=corrcoef(xx,yy,'rows','pairwise');
notnan=~isnan(xx) & ~isnan(yy);
[ff,goodness]=fit(xx(notnan),yy(notnan),fittype('a*x+b'),'StartPoint',[1,0]);
plot([0,16],ff([0,16]),'-k')
text(1,0.9,['R^{2} = ',num2str(goodness.rsquare)])
set(gca,'XTick',[0:4:16],'YTick',[0:0.2:1.0]);
drawnow

figure(hf+2),movegui(hf+2,'north')
xx=xldat(:,8);
yy=xldat(:,3)-xldat(:,7);
scatter(xx,yy,[],cc,'filled'),axis square,box on,hold on
text(xx+0.15,yy,txt,'Color',[0.4,0.4,0.4])
xlabel('Bouton weight')
ylabel('Bouton volume excluding mitochondria (\mu{m}^{3})')
xlim([0 16]);ylim([0 1])
C=corrcoef(xx,yy,'rows','pairwise');
notnan=~isnan(xx) & ~isnan(yy);
[ff,goodness]=fit(xx(notnan),yy(notnan),fittype('a*x+b'),'StartPoint',[1,0]);
plot([0,16],ff([0,16]),'-k')
text(1,0.9,['R^{2} = ',num2str(goodness.rsquare)])
set(gca,'XTick',[0:4:16],'YTick',[0:0.2:1.0]);
drawnow

figure(hf+3),movegui(hf+3,'northeast')
xx=xldat(:,8);
yy=xldat(:,4);
scatter(xx,yy,[],cc,'filled'),axis square,box on,hold on
text(xx+0.15,yy,txt,'Color',[0.4,0.4,0.4])
xlabel('Bouton weight')
ylabel('Total PSD surface area (\mu{m}^{2})')
xlim([0 16])
ylim([0 3])
notnan=~isnan(xx) & ~isnan(yy);
[ff,goodness]=fit(xx(notnan),yy(notnan),fittype('a*x+b'),'StartPoint',[1,0]);
plot([0,16],ff([0,16]),'-k')
set(gca,'XTick',[0:4:16],'YTick',[0:0.5:3.0]);
text(1,2,['R^{2} = ',num2str(goodness.rsquare)])
drawnow

figure(hf+4),movegui(hf+4,'southwest')
xx=xldat(:,3);
yy=xldat(:,4);
scatter(xx,yy,[],cc,'filled'),axis square,box on,hold on
text(xx+0.015,yy,txt,'Color',[0.4,0.4,0.4])
xlabel('Bouton volume (\mu{m}^3)')
ylabel('Total PSD surface area (\mu{m}^{2})')
xlim([0 1]);ylim([0 3]);
notnan=~isnan(xx) & ~isnan(yy);
[ff,goodness]=fit(xx(notnan),yy(notnan),fittype('a*x+b'),'StartPoint',[1,0]);
plot([0,16],ff([0,16]),'-k')
set(gca,'XTick',0:0.2:1,'YTick',0:0.5:3.0);
text(.1,2,['R^{2} = ',num2str(goodness.rsquare)])
drawnow

figure(hf+5),movegui(hf+5,'south')
xx=xldat(:,3)-xldat(:,7);
yy=xldat(:,4);
scatter(xx,yy,[],cc,'filled'),axis square,box on,hold on
text(xx+0.015,yy,txt,'Color',[0.4,0.4,0.4])
xlabel('Bouton volume excluding mitochondria (\mu{m}^3)')
ylabel('Total PSD surface area (\mu{m}^{2})')
xlim([0 1]);ylim([0 3]);
notnan=~isnan(xx) & ~isnan(yy);
[ff,goodness]=fit(xx(notnan),yy(notnan),fittype('a*x+b'),'StartPoint',[1,0]);
plot([0,16],ff([0,16]),'-k')
set(gca,'XTick',0:0.2:1,'YTick',0:0.5:3.0);
text(.1,2,['R^{2} = ',num2str(goodness.rsquare)])
drawnow

