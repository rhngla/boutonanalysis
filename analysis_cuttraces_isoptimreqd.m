%This code is used to generate swc traces lengths that are common to all
%optimized traces for manual vs. optimized trace comparisons

animal={'DL001'};
timepoint=cellstr([char(double('K'):double('T'))]');
section={'002'};
axon={[1,2,3,4,7,8,9,10,11,12,13,14,15,17,18,19,20]};
projectn='xy';
filter={'LoGxy'};
channel='G';
pathlist;

thr=0.5;
cc=lines(12);
for an=1:numel(animal)
    for se=1:numel(section)
        for ax=1:numel(axon{se})
            axonstr=sprintf('A%03d',axon{se}(ax));
            
            
            for ti=1:5%numel(timepoint)
                stackid=[animal{an},timepoint{ti},section{se}];
                Current=load(isunixispc([profile_pth,stackid,'/',axonstr,'.mat']));
                
                indstart=-inf;
                indend=inf;
                for tother=6:numel(timepoint)
                    stackidother=[animal{an},timepoint{tother},section{se}];
                    Other=load(isunixispc([profile_pth,stackidother,'/',axonstr,'.mat']));
                    
                    %Finding indices of overlap
                    dx=bsxfun(@minus,Current.r.optim(:,1),Other.r.optim(:,1)');
                    dy=bsxfun(@minus,Current.r.optim(:,2),Other.r.optim(:,2)');
                    dz=bsxfun(@minus,Current.r.optim(:,3),Other.r.optim(:,3)');
                    
                    %Ignore distance in z:
                    dz=zeros(size(dz));
                    
                    dd=(dx.^2+dy.^2+dz.^2).^0.5;
                    [val,~]=min(dd,[],2);
                    indovrlap_curr=1:size(dd,1);
                    indovrlap_curr(val>thr)=[];
                    
                    %indovrlap_other=1:size(dd,2);
                    %[val,~]=min(dd,[],2);
              
                    indstart=max(indstart,min(indovrlap_curr));
                    indend=min(indend,max(indovrlap_curr));
                    
                          
%                     AM=Current.AM.optim(indstart:indend,:);
%                     AM=AM(:,indstart:indend);
%                     r=Current.r.optim(indstart:indend,:);
%                     R=zeros(size(r(:,1)));
%                     figure(1)
%                     %plot(r(:,1),r(:,2),'.-','Color',cc(ti,:)),hold on
%                     plot(r(1,1),r(1,2),'o','Color',cc(tother,:)),hold on
%                     plot(r(end,1),r(end,2),'x','Color',cc(tother,:),'MarkerSize',20),hold on
%                     drawnow;
%                     axis equal;
                end
                
                indstart=1;indend=size(Current.AM.optim,1);
                
                AM=Current.AM.optim(indstart:indend,:);
                AM=AM(:,indstart:indend);
                r=Current.r.optim(indstart:indend,:);
                R=zeros(size(r(:,1)));
                %figure(ax)
                %plot(r(:,1),r(:,2),'.-','Color',cc(ti,:)),hold on
                %plot(r(1,1),r(1,2),'o','Color',cc(ti,:)),hold on
                %plot(r(end,1),r(end,2),'x','Color',cc(ti,:),'MarkerSize',20),hold on
                %drawnow;
                %axis equal;
               
                swcmat=AM2swc(AM,r,R,1,1,1);
                fname=isunixispc([optim_pth,stackid,'/',axonstr,'.swc']);
                dlmwrite(fname,swcmat,'delimiter',' ');
                clear AM r New
                %}
            end
        end
    end
end