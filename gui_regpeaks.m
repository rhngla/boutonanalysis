%This code loads data for an axon, generates Canvas, (opt) initializes
%unique labels for peaks,displays editable manual+auto matches, provides
%suto match suggestions on demand.
pathlist;

animal={'DL001'};
%timepoint=cellstr([char(double('K'):(double('T')))']);
timepoint={'A','B','D','E','F','H','I'};
section={'002'};
axon={[9]};
tracer={[]}; %Default: tracer = {[]}; for 'A001.swc';
projectn='xy';
filter='LoGxy';
channel='G';

isreinitialize=false;
alignbymatches=true;
ex=-10;ey=15;%(positive value=down/right in image)

if strcmp(projectn,'xy')
    dat.perm=[1,2,3];
elseif strcmp(projectn,'yz')
    dat.perm=[3,2,1];
elseif strcmp(projectn,'zx')
    dat.perm=[1,3,2];
end

Canvas=zeros(3048,3048);
dx=zeros(numel(timepoint),1);dy=dx;
rr=cell(numel(timepoint),1);
allshift=[];r=[];

an=1;se=1;ax=1;tr=1;
Time=cell(numel(timepoint),1);
for ti=1:numel(timepoint)
    axonstr=[sprintf('A%03d',axon{se}(ax)),tracer{tr}];
    stackid=[animal{an},timepoint{ti},section{se}];
    clear IM;
    
    if exist(isunixispc([proj_pth,stackid,'-',axonstr,'-',projectn,'.mat']),'file')
        load(isunixispc([proj_pth,stackid,'-',axonstr,'-',projectn,'.mat']),'IM')
        sizeIM=size(IM(:,:,1));
        temp=load(isunixispc([profile_pth,stackid,'\',axonstr,'.mat']));
        
        %Load r,d,I data
        Time{ti}.r=temp.r.optim;
        Time{ti}.r=Time{ti}.r(:,dat.perm);
        Time{ti}.d=temp.d.optim;
        Time{ti}.I.norm=temp.I.(channel).(filter).norm;
        Time{ti}.I.raw=temp.I.(channel).(filter).raw;
        Time{ti}.ignore=temp.annotate.ignore;
        
        %Load fg peak data
        Time{ti}.fg_ind=temp.fit.(channel).(filter).fg.ind;
        Time{ti}.fg_id=temp.fit.(channel).(filter).fg.id;
        Time{ti}.fg_manid=temp.fit.(channel).(filter).fg.manid;
        Time{ti}.fg_autoid=temp.fit.(channel).(filter).fg.autoid;
        Time{ti}.fg_flag=temp.fit.(channel).(filter).fg.flag;
        Time{ti}.d_man=temp.fit.(channel).(filter).d.man;
        Time{ti}.deform_man=zeros(size(temp.fit.(channel).(filter).d.man));
        
        %Path where data will be saved within gui
        Time{ti}.id=temp.id;
        Time{ti}.save_pth=isunixispc([profile_pth,stackid,'\',axonstr,'.mat']);
        clear temp;
        
        %Initialize fg_id for all times with unique labels
        if isreinitialize
            display('Reinitializing all peak labels and matches')
            Time{ti}.fg_id=1000*ti+(1:numel(Time{ti}.fg_id))'; %# peaks not expected to be >1000 in one time
            Time{ti}.fg_manid=nan(numel(Time{ti}.fg_id),1);
            Time{ti}.fg_autoid=nan(numel(Time{ti}.fg_id),1);
            Time{ti}.fg_flag=nan(numel(Time{ti}.fg_id),1);
            [~,igind,~]=intersect(Time{ti}.fg_ind,find(Time{ti}.ignore));
            Time{ti}.fg_flag(igind)=1; %# peak flag is nan by default
        end
        
        %Preparing canvas
        if alignbymatches
            %Align images based on registered boutons
            if ti>1
                prevr=Time{ti-1}.r(Time{ti-1}.fg_ind,:);
                nowr=Time{ti}.r(Time{ti}.fg_ind,:);
                prevlbl=Time{ti-1}.fg_manid;
                nowlbl=Time{ti}.fg_manid;
                [~,prevind,nowind]=intersect(prevlbl,nowlbl);
                
                dx(ti)=mean(nowr(nowind,1)-prevr(prevind,1));
                dy(ti)=mean(nowr(nowind,2)-prevr(prevind,2));
            end
            display('Alignment of Canvas using matches')
            dxt=nansum(dx); dyt=nansum(dy);
            dxt=-round(dxt)+size(Canvas,1)/4+1;
            dyt=-round(dyt)+size(Canvas,2)/4+1;
            
            %ex=-20;ey=10;%(positive value=down/right in image)
            dxt=dxt+ex*ti;
            dyt=dyt+ey*ti;
            
            %Image normalization
            imnorm=5*mean(Time{ti}.I.raw(~Time{ti}.ignore));
            Canvas(dxt:dxt+size(IM,1)-1,dyt:dyt+size(IM,2)-1,1)=max(Canvas(dxt:dxt+size(IM,1)-1,dyt:dyt+size(IM,2)-1,1),IM(:,:,1)./imnorm);
            
            if ti==numel(timepoint)
                %Time{ti}.r not modified directly because value at ti-1
                %is used within parent loop
                rr{ti}=[Time{ti}.r(:,1)+dxt-1,Time{ti}.r(:,2)+dyt-1,Time{ti}.r(:,3)];
                for tti=1:numel(timepoint)
                    Time{tti}.r=rr{tti};
                end
            else
                %Saving temporary r
                rr{ti}=[Time{ti}.r(:,1)+dxt-1,Time{ti}.r(:,2)+dyt-1,Time{ti}.r(:,3)];
            end
        else
            %In the following, x y refer to Canvas x y.
            sep=50*(ti-1);
            %ex=max(Time{ti}.r(:,2))-min(Time{ti}.r(:,2));
            %ey=max(Time{ti}.r(:,1))-min(Time{ti}.r(:,1));
            
            %Add shift angle  manually
            %ex=1;ey=0.5;%(positive value=down/right in image)
            dxt=round(sep.*(ex)./((ex.^2+ey.^2).^0.5))+1024;
            dyt=round(sep.*ey./((ex.^2+ey.^2).^0.5))+1024;
            
            %Image normalization
            imnorm=5*mean(Time{ti}.I.raw(~Time{ti}.ignore));
            Canvas(dxt:dxt+size(IM,1)-1,dyt:dyt+size(IM,2)-1,1)=max(Canvas(dxt:dxt+size(IM,1)-1,dyt:dyt+size(IM,2)-1,1),IM(:,:,1)./imnorm);
            
            Time{ti}.r(:,1)=Time{ti}.r(:,1)-1+dxt;
            Time{ti}.r(:,2)=Time{ti}.r(:,2)-1+dyt;
        end
    else
        display([isunixispc([proj_pth,stackid,'-',axonstr,'-',projectn]),' not found!']);
    end
end

%Create graph representation
temp=cell2mat(Time);
temp={temp(:).fg_id};
temp=cell2mat(temp(:));

dat.AM=zeros(size(temp,1));
dat.t=nan(size(temp));
dat.nodeind=nan(size(temp));
dat.fg_id=nan(size(temp));
dat.fg_manid=nan(size(temp));
dat.fg_ind=nan(size(temp));
dat.fg_flag=nan(size(temp));
ss=1;
for ti=1:numel(Time)
    ee=ss+numel(Time{ti}.fg_id)-1;
    dat.t(ss:ee)=ti;
    dat.nodeind(ss:ee)=(1:numel(Time{ti}.fg_id));
    dat.fg_id(ss:ee)=Time{ti}.fg_id;
    dat.fg_manid(ss:ee)=Time{ti}.fg_manid;
    dat.fg_ind(ss:ee)=Time{ti}.fg_ind;
    dat.r(ss:ee,:)=Time{ti}.r(Time{ti}.fg_ind,:);
    dat.fg_flag(ss:ee)=Time{ti}.fg_flag;
    ss=ee+1;
end

%Initialize AM from loaded data
mid=unique(dat.fg_manid(~isnan(dat.fg_manid)));
for m=1:numel(mid)
    ind=find(dat.fg_manid==mid(m));
    for i=1:(numel(ind)-1)
        dat.AM(ind(i),ind(i+1))=mid(m);
        dat.AM(ind(i+1),ind(i))=mid(m);
    end
end

[dat.AM,dat.fg_manid]=gui_regpeaks_lblAM(dat.AM,dat.fg_id);
dat.channel=channel;
dat.filter=filter;
dat.Time=Time;

f=figure(2);clf(f);
imshow(Canvas),caxis([0 max(Canvas(:))./2]),hold on;drawnow;

h_nodes = gobjects(numel(dat.t),1);
for i=1:numel(dat.t)
    h_nodes(i)=line(dat.r(i,2),dat.r(i,1),2,...
        'Tag','Nodes','ButtonDownFcn',@gui_regpeaks_selnodes,'PickableParts','all',...
        'UserData',struct('t',dat.t(i),'nodeind',dat.nodeind(i),'fg_ind',dat.fg_ind(i),'fg_id',dat.fg_id(i)),...
        'Marker','o','MarkerSize',15,'MarkerEdgeColor',[0.5 0 0],'MarkerFaceColor','none','LineStyle','none','LineWidth',0.5);
end

dat.h_nodes=h_nodes;
set(f,'KeyPressFcn',@gui_regpeaks_actions);
gui_regpeaks_updatenodes(dat);
[dat]=gui_regpeaks_plotedges(dat,f);

guidata(f,dat);
%clearvars -except f
