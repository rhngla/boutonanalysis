%This function adds nodes to an active list when an edge is selected
function [n1,n2] = gui_regpeaks_deledge (h_edge, eventdata, ~)
%display(h_edge.UserData);
hl=(eventdata.Source);
f=hl.Parent.Parent;
temp=regexp(h_edge.Tag,'-','split');
n1=str2double(temp{2});
n2=str2double(temp{3});

delete(h_edge);
dat=guidata(f);
dat.AM(n1,n2)=0;
dat.AM(n2,n1)=0;
dat=gui_regpeaks_plotedges(dat,f);
for ti=1:numel(dat.Time)
    dat.Time{ti}.fg_manid=dat.fg_manid(dat.t==ti);
end
[dat.Time]=gui_regpeaks_align(dat.Time);
gui_regpeaks_updatenodes(dat);
guidata(f,dat);
end