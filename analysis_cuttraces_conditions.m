%This code is used to generate swc traces lengths that are common to all
%traces in the conditions data. The overlap portions are found from matched
%data.

animal={'DL001'};
timepoint={'A','B','D','E','F','H','I'};
section={'002'};
axon={[1,2,3,4,7,8,9,10,11,12,13,14,15,17,18,19,20]};
projectn='xy';
filter={'LoGxy'};
channel='G';
pathlist;

thr=0.5;
cc=lines(12);
for an=1:numel(animal)
    for se=1:numel(section)
        for ax=1:numel(axon{se})
            axonstr=sprintf('A%03d',axon{se}(ax));
            
            dmin=-inf;
            dmax=inf;
            for ti=1:numel(timepoint)
                stackid=[animal{an},timepoint{ti},section{se}];
                Current=load(isunixispc([profile_pth,stackid,'/',axonstr,'.mat']));
                
                dmin=max(dmin,Current.fit.G.LoGxy.d.man(1));
                dmax=min(dmax,Current.fit.G.LoGxy.d.man(end)); 
            end
            
            for ti=1:numel(timepoint)
                stackid=[animal{an},timepoint{ti},section{se}];
                Current=load(isunixispc([profile_pth,stackid,'/',axonstr,'.mat']));

                indovrlap_curr=dmin<Current.fit.G.LoGxy.d.man & Current.fit.G.LoGxy.d.man < dmax;
                indstart=find(indovrlap_curr,1,'first');
                indend=find(indovrlap_curr,1,'last');
                
                AM=Current.AM.optim(indstart:indend,:);
                AM=AM(:,indstart:indend);
                r=Current.r.optim(indstart:indend,:);
                R=zeros(size(r(:,1)));
                figure(1)
                plot(r(:,1),r(:,2),'.-','Color',cc(ti,:)),hold on
                plot(r(1,1),r(1,2),'o','Color',cc(ti,:)),hold on
                plot(r(end,1),r(end,2),'x','Color',cc(ti,:),'MarkerSize',20),hold on
                drawnow;
                axis equal;
                
                swcmat=AM2swc(AM,r,R,1,1,1);
                fname=isunixispc([optim_pth,stackid,'/',axonstr,'.swc']);
                dlmwrite(fname,swcmat,'delimiter',' ');
                clear AM r Current
            end
        end
    end
end
