%This code copies selected files from one folder to another.

parentdir=pwd;
parentdir=parentdir(1:(strfind(parentdir,'Plasticity')+numel('Plasticity')-1));
sourcedir=isunixispc([parentdir,'\Plasticity\dat\ManualTraces\']);
destdir=isunixispc([parentdir,'\Plasticity\dat\ManualTraces_v2\']);

animal={'DL083'};
timepoint = cellstr((char(double('B'):double('N')))');
section = {'001'};
axon = {[17]};
tracer={[]}; %Default: tracer = {[]}; for 'A001.swc';

for an=1:numel(animal)
    for se=1:numel(section)
        for ti=1:numel(timepoint)
            stackid=[animal{an},timepoint{ti},section{se}];
            for ax=1:numel(axon{se})
                AM=[]; r=[]; R=[];
                for tr=1:numel(tracer)
                    axonstr=[sprintf('A%03d',axon{se}(ax)),tracer{tr}];
                    
                    sourcefname=isunixispc([sourcedir,stackid,'/',axonstr,'.swc']);
                    destfname=isunixispc([destdir,stackid,'/',axonstr,'.swc']);
                    if exist(sourcefname,'file')
                        if ~exist(isunixispc([destdir,stackid]),'dir')
                            mkdir(isunixispc(destdir),stackid)
                            display(['Creating new directory',isunixispc([destdir,stackid])])
                        end
                        copyfile(sourcefname,destfname)
                        display(['Saved ',isunixispc([destdir,stackid,'/',axonstr])]);
                    else
                        disp([sourcefname,' not found!'])
                    end
                end
            end
        end
    end
end